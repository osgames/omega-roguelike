/* omega copyright (c) 1987,1988,1989 by Laurence Raphael Brothers */
/* mtalk.c */
/* monster talk functions */

#include "game_time.h"
#include "glob.h"
#include "lev.h"
#include "liboutil/list.h"
#include "util.h"
#include "findloc.h"

static void apply_make_woodhenge_friendly (void * vmon)
{
  monster * mon;
  mon = vmon;
  m_status_reset(mon, HOSTILE);
}

/* The druid's altar is in the northern forest */
void m_talk_druid (monster_t * m, player_t * p)
{
  if (m_statusp(m, HOSTILE))
    {
      mprint("The ArchDruid looks at you and cries: 'Unclean! Unclean!'");
      disrupt(p->x, p->y, 100);

      mprint("This seems to have satiated his desire for vengeance.");
      mprint("'Have you learned your lesson?' The ArchDruid asks. [yn] ");

      if (ynq() != 'n')
        {
          list_apply(m->dlevel->monster_list, apply_make_woodhenge_friendly);
          /* m_vanish(m); */ /* how can he vanish and then talk? */
          mprint("'I certainly hope so!' says the ArchDruid.");
        }
      else
        {
          mprint("'Idiot.' mutters the ArchDruid.");
          p_damage(p, 500, UNSTOPPABLE, "the ArchDruid's Vengeance");
        }

      return;
    }

  print1("The Archdruid raises a hand in greeting.");

  if (gamestatusp(SPOKE_TO_DRUID) == FALSE)
    {
      setgamestatus(SPOKE_TO_DRUID);
      morewait();

      print1("The Archdruid congratulates you on reaching his sanctum.");
      print2("You feel competent.");
      gain_experience(300);

      if (p->patron == DRUID)
	{
	  int i;

	  print1("The Archdruid conveys to you the wisdom of nature....");
	  print2("You feel like a sage.");
	  morewait();

	  for (i = 0; i < NUMRANKS; ++i)
	    if (p->guildxp[i] > 0)
	      p->guildxp[i] += 300;
	}
    }

  mprint("Do you request a ritual of neutralization? [yn] ");

  if ('y' != ynq()) return;

  switch (moon_phase())
    {
    case PHASE_NEW: case PHASE_FULL:
      mprint("\"Unfortunately, I cannot perform a ritual of balance on");
      if (6 == (time_phase() / 2))
	mprint("this lawful day.\"");
      else
	mprint("this chaotic day.\"");
      break;

    case PHASE_HALF:
      mprint("You take part in this day's holy celebration of balance...");

      if (p->patron == DRUID)
	{
	  p->alignment = 0;
	  p->mana = calcmana();
	  gain_experience(200);
	  /* if a druid wants to spend 2 days celebrating for 1600 xp, why not? */
	}
      else
	{
	  /* the higher level the character is, the more set in his/her ways */
	  p->alignment -= (p->alignment * max(10, 20 - p->level) / 20);
	}

      set_waste_time(TRUE);
      monster_tickets_off();
      ticket_process_for_hours(6);
      monster_tickets_on();
      set_waste_time(FALSE);

    default:
      mprint("The ArchDruid conducts a sacred rite of balance...");

      if (p->patron == DRUID)
	{
	  p->alignment = 0;
	  p->mana = calcmana();
	}
      else
	{
	  /* the higher level the character is, the more set in his/her ways */
	  p->alignment -= p->alignment * max(1, 20 - p->level) / 20;
	}

      set_waste_time(TRUE);
      monster_tickets_off();
      ticket_process_for_minutes(60);
      monster_tickets_on();
      set_waste_time(FALSE);
      break;
    }

  dataprint();
}

void m_talk_silent (monster_t * m, player_t * p)
{
  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s", m->monstring);
  else
    strcpy(Str2, m->monstring);

  switch (random_range(8))
    {
    case 0: strcat(Str2, " does not reply. ");                 break;
    case 1: strcat(Str2, " shrugs silently. ");                break;
    case 2: strcat(Str2, " holds a finger to his mouth. ");    break;
    case 3: strcat(Str2, " glares at you but says nothing. "); break;
    case 4: strcat(Str2, " is not going to answer you. ");     break;
    case 5: strcat(Str2, " has taken a vow of silence. ");     break;
    case 6: strcat(Str2, " attempts sign language. ");         break;
    case 7: strcat(Str2, " fails to respond.");                break;
    }

  mprint(Str2);
}

void m_talk_stupid (monster_t * m, player_t * p)
{
  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s", m->monstring);
  else
    strcpy(Str2, m->monstring);

  switch (random_range(4))
    {
    case 0: strcat(Str2, " looks at you with mute incomprehension."); break;
    case 1: strcat(Str2, " growls menacingly and ignores you.");      break;
    case 2: strcat(Str2, " does not seem to have heard you.");        break;
    case 3: strcat(Str2, " tries to pretend it didn't hear you.");    break;
    }

  mprint(Str2);
}

void m_talk_greedy (monster_t * m, player_t * p)
{
  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s", m->monstring);
  else
    strcpy(Str2, m->monstring);

  switch (random_range(4))
    {
    case 0: strcat(Str2, " says: 'Give me a treasure....' ");    break;
    case 1: strcat(Str2, " says: 'Stand and deliver, knave!' "); break;
    case 2: strcat(Str2, " says: 'Your money or your life!' ");  break;
    case 3: strcat(Str2, " says: 'Yield or Die!' ");             break;
    }

  mprint(Str2);
}

void m_talk_hungry (monster_t * m, player_t * p)
{
  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s", m->monstring);
  else
    strcpy(Str2, m->monstring);

  switch (random_range(4))
    {
    case 0: strcat(Str2, " says: 'I hunger, foolish adventurer!' "); break;
    case 1: strcat(Str2, " drools menacingly at you. ");             break;
    case 2: strcat(Str2, " says: \"You're invited to be lunch!\" "); break;
    case 3: strcat(Str2, " says: 'Feeed Meee!' ");                   break;
    }

  mprint(Str2);
}

void m_talk_guard (monster_t * m, player_t * p)
{
  if (m_statusp(m,HOSTILE) == TRUE)
    {
      print1("'Surrender in the name of the Law!'");
      print2("Do it? [yn] ");

      if ('y' != ynq2())
        {
          clearmsg();
          print1("'All right, you criminal scum, you asked for it!'");
	  return;
        }

      ++(p->alignment);

      if (p->dlevel == E_CITY)
	{
	  print1("Go directly to jail. Do not pass go, do not collect 200Au.");
	  print2("You are taken to the city gaol.");
	  morewait();

	  send_to_jail(p);
	  drawvision(p->x, p->y);
	  return;
	}

      clearmsg();
      if (p->possessions[O_WEAPON_HAND] == NULL)
	{
	  print1("Mollified, the guard sends you away.");
	  return;
	}

      print1("Mollified, the guard disarms you and sends you away.");
      dispose_lost_objects(1, p->possessions[O_WEAPON_HAND]);
    }
  else /* not hostile */
    {
      if (m->dlevel->environment == E_CITY && make_loc_known(L_TOURIST))
        {
          print1("'You should visit the tourist information booth and get their guidebook.");
          print2("The booth is located near the entrance to the city.'");
          morewait();
        } 
      else if (p->rank[ORDER] > 0) 
	{
	  print1("'Greetings comrade! May you always tread the paths of Law.'");
	}
      else
	{
	  if (random_range(2))
	    print1("'Move it right along, stranger!'");
	  else
	    print1("'Nothing to see here.'");
	}
    }
}

void m_talk_mp (monster_t * m, player_t * p)
{
  mprint("The mendicant priest asks you to spare some treasure for the needy.");
}

void m_talk_titter (monster_t * m, player_t * p)
{
  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s titters obscenely at you.", m->monstring);
  else
    sprintf(Str2, "%s titters obscenely at you.", m->monstring);

  mprint(Str2);
}

void m_talk_ninja (monster_t * m, player_t * p)
{
  mprint("The black-garbed figure says apologetically:");
  mprint("'Situree simasita, wakarimasen.'");
}

void m_talk_thief (monster_t * m, player_t * p)
{
  if (p->rank[THIEVES] == 0)
    {
      m_talk_man(m,p);
      return
    }

  if (m->level == 2) 
    m->monstring = "sneak thief";
  else
    m->monstring = "master thief";

  print1("The cloaked figure makes a gesture which you recognize...");
  print2("...the thieves' guild recognition signal!");
  print3("'Sorry mate, thought you were a mark...'");
  morewait();

  m_vanish(m);
}

void m_talk_assassin (monster_t * m, player_t * p)
{
  m->monstring = "master assassin";

  print1("The ominous figure does not reply, but hands you an embossed card:");
  print2("'Guild of Assassins Ops are forbidden to converse with targets.'");
}
    
void m_talk_im (monster_t * m, player_t * p)
{
  object * obj;
  long price;

  if (strcmp(mon->monstring, "itinerant merchant") != 0)
    mon->monstring = "itinerant merchant";

  if (list_size(mon->possession_list) == 0)
    {
      if (mon->possession_list)
        {
          list_delete(mon->possession_list);
          mon->possession_list = 0;
        }

      mprint("The merchant says: 'Alas! I have nothing to sell!'");
      return;
    }

  obj = m_remove_random_item(mon);
  price = max(10, 4 * true_item_value(obj));

  clearmsg();
  mprint("'I have a fine");
  mprint(true_itemid(obj));
  mprint("for only");
  mlongprint(price);
  mprint("Au.");
  mprint("Want it?' [yn] ");

  if ('y' != ynq())
    {
      mprint("'Sorry I couldn't help you.'");
      m_pickup(mon, obj);
    }
  else 
    {
      if (p->cash < price)
        {
	  mprint("'You don't have enough money!");

	  if (p->alignment > 10)
	    {
	      if (p->cash >= true_item_value(obj))
		{
		  mprint("'Well, I'll let you have it for what you've got.'");
		  p->cash = 0;
		  gain_item(obj);
		}
	      else
		mprintf("'Maybe some other time?'");
	    }
          else
            mprint("'Beat it, you deadbeat!'");
        }
      else
        {
          mprint("'Here you are. Have a good day.'");
	  know_object_specialness(obj);
          p->cash -= price;
          gain_item(obj);
        }
    }

  if (0 == list_size(mon->possession_list))
    {
      list_delete(mon->possession_list);
      mon->possession_list = 0;
      mprint("'Well then, I must be off. Good day.'");
      m_vanish(mon);
    }
}

void m_talk_man (monster_t * m, player_t * p)
{
  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s", m->monstring);
  else
    strcpy(Str2, m->monstring);

  switch (random_range(8))
    {
    case 0: strcat(Str2, " asks you for the way home.");    break;
    case 1: strcat(Str2, " wishes you a pleasant day.");    break;
    case 2: strcat(Str2, " sneers at you contemptuously."); break;
    case 3: strcat(Str2, " smiles and nods.");              break;
    case 4: strcat(Str2, " tells you a joke.");             break;
    case 5: strcat(Str2, " says: 'I must be on my way!'");  break;
    case 6: strcat(Str2, " asks if you've seen Wanda.");    break;
    case 7: strcat(Str2, " grins and waves.");              break;
    }

  mprint(Str2);
}

void m_talk_evil (monster_t * m, player_t * p)
{
  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s", m->monstring);
  else
    strcpy(Str2, m->monstring);

  switch (random_range(20))
    {
    case 0:  strcat(Str2, " says: 'THERE CAN BE ONLY ONE!'");                  break;
    case 1:  strcat(Str2, " says: 'Prepare to die, Buckwheat!'");              break;
    case 2:  strcat(Str2, " says: 'Time to die!'");                            break;
    case 3:  strcat(Str2, " says: 'There will be no mercy.'");                 break;
    case 4:  strcat(Str2, " insults your mother-in-law.");                     break;
    case 5:  strcat(Str2, " says: 'Kurav tu ando mul!'");                      break;
    case 6:  strcat(Str2, " says: '!va al infierno!'");                        break;
    case 7:  strcat(Str2, " says: 'verpiss dich!");                            break;
    case 8:  strcat(Str2, " says: 'dame desu nee.'");                          break;
    case 9:  strcat(Str2, " spits on your rug and calls your cat a bastard."); break;
    case 10: strcat(Str2, " snickers malevolently and draws a weapon.");       break;
    case 11: strcat(Str2, " sends 'rm -r *' to your shell!");                  break;
    case 12: strcat(Str2, " tweaks your nose and cackles evilly.");            break;
    case 13: strcat(Str2, " thumbs you in the eyes.");                         break;
    case 14: strcat(Str2, " accuses you of downloading mp3s.");                break;
    case 15: strcat(Str2, " sends you a cease-and-desist letter.");            break;
    case 16: strcat(Str2, " demands that you buy a license.");                 break;
    case 17: strcat(Str2, " spams your email account.");                       break;
    case 18: strcat(Str2, " disrespects you.");                                break;
    case 19: strcat(Str2, " says: 'My dad can beat up your dad!'.");           break;
  }

  mprint(Str2);
}

void m_talk_robot (monster_t * m, player_t * p)
{
  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s", m->monstring);
  else
    strcpy(Str2, m->monstring);

  switch (random_range(8))
    {
    case 0: strcat(Str2, " says: 'exterminate! ExTermiNate!! EXTERMINATE!!!'");      break;
    case 1: strcat(Str2, " says: 'Kill ... Crush ... Destroy'");                     break;
    case 2: strcat(Str2, " says: 'Danger -- Danger'");                               break;
    case 3: strcat(Str2, " says: 'Yo Mama -- core dumped.'");                        break;
    case 4: strcat(Str2, " says: 'Are you Sarah Connor?'");                          break;
    case 5: strcat(Str2, " says: 'You must be cyber-converted'");                    break;
    case 6: strcat(Str2, " says: 'Resistance is futile! You will be assimilated!'"); break;
    case 7: strcat(Str2, " says: 'A fatal exception has occurred'");                 break;
    }

  mprint(Str2);
}

void m_talk_slithy (monster_t * m, player_t * p)
{
  mprint("It can't talk -- it's too slithy!");
}

void m_talk_mimsy (monster_t * m, player_t * p)
{
  mprint("It can't talk -- it's too mimsy!");
}

void m_talk_burble (monster_t * m, player_t * p)
{
  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s burbles hatefully at you.", m->monstring);
  else
    sprintf(Str2, "%s burbles hatefully at you.", m->monstring);

  mprint(Str2);
}

void m_talk_beg (monster_t * m, player_t * p)
{
  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s asks you for alms.", m->monstring);
  else
    sprintf(Str2, "%s asks you for alms.", m->monstring);

  mprint(Str2);
}

void m_talk_hint (monster_t * m, player_t * p)
{
  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s", m->monstring);
  else
    strcpy(Str2, m->monstring);

  if (m_statusp(m,HOSTILE) == TRUE)
    {
      strcat(Str2," only sneers at you. ");
      mprint(Str2);
      return;
    }

  strcat(Str2, " whispers in your ear: ");
  mprint(Str2);

  hint();
  m->talkf = M_TALK_SILENT;
}

void m_talk_gf (monster_t * m, player_t * p)
{
  mprint("The good fairy glints: Would you like a wish?");
  if (ynq() != 'y') goto fairy_leaves;

  mprint("The good fairy glows: Are you sure?");
  if (ynq() != 'y') goto fairy_leaves;

  mprint("The good fairy radiates: Really really sure?");
  if (ynq() != 'y') goto fairy_leaves;

  mprint("The good fairy beams: I mean, like, sure as sure can be?");
  if (ynq() != 'y') goto fairy_leaves;

  mprint("The good fairy dazzles: You don't want a wish, right?");

  if (ynq() == 'y')
    mprint("The good fairy laughs: I thought not.");
  else
    wish(0);

 fairy_leaves:

  mprint("In a flash of sweet-smelling light, the fairy vanishes....");
  m_vanish(m);

  p->hp = max(p->hp, p->maxhp);
  p->mana = max(p->mana, calcmana());

  mprint("You feel mellow.");
}

void m_talk_ef (monster_t * m, player_t * p)
{
  mprint("The evil fairy roils: Eat my pixie dust!");
  mprint("She waves her black-glowing wand, which screams thinly....");

  m->movef    = M_MOVE_SMART;
  m->meleef   = M_MELEE_POISON;
  m->specialf = M_SP_THIEF;

  m_status_set(m, HOSTILE);

  acquire(p,-1);
  bless(p,-1);
  sleep_player(p,m->level/2);

  summon(-1,-1);
  summon(-1,-1);
  summon(-1,-1);
  summon(-1,-1);
}

void m_talk_seductor (monster_t * m, player_t * p)
{
  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s", m->monstring);
  else
    strcpy(Str2, m->monstring);

  if (p->preference == 'n')
    {
      strcat(Str2, " notices your disinterest and leaves with a pout.");
      mprint(Str2);
    }
  else
    {
      strcat(Str2," beckons seductively...");
      mprint(Str2);
      mprint("Flee? [yn] ");

      if (ynq() == 'y')
	{
	  mprint("You feel stupid.");
	}
      else
	{
	  sprintf("The %s shows you a good time...", m->monstring);
	  mprint(Str2);
	  gain_experience(500);
	  ++(p->con);
	}
    }

  m_vanish(m);
}

void m_talk_demonlover (monster_t * m, player_t * p)
{
  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s", m->monstring);
  else
    strcpy(Str2, m->monstring);

  if (p->preference == 'n')
    {
      strcat(Str2, " notices your disinterest and changes with a snarl...");
      mprint(Str2);
      morewait();
    }
  else
    {
      strcat(Str2," beckons seductively...");
      mprint(Str2);
      mprint("Flee? [yn] ");

      if (ynq() == 'y')
	{
	  mprint("You feel fortunate....");
	}
      else
	{
	  sprintf("The %s shows you a good time...", m->monstring);
	  mprint(Str2);
	  morewait();

	  mprint("You feel your life energies draining...");
	  level_drain(p, random_range(3) + 1, "a demon's kiss");
	  morewait();
	}
    }

  m->talkf    = M_TALK_EVIL;
  m->meleef   = M_MELEE_SPIRIT;
  m->specialf = M_SP_DEMON;

  m_status_reset(m,NEEDY);
  m_status_set(m,HOSTILE);

  if ((m->monchar & 0xff) == 's')
    {
      m->monchar = 'I'|CLR(RED);
      m->monstring = "incubus";
    }
  else
    {
      m->monchar = 'S'|CLR(RED);
      m->monstring = "succubus";
    }

  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s laughs insanely.", m->monstring);
  else
    sprintf(Str2, "%s laughs insanely.", m->monstring);

  mprint(Str2);
  mprint("You now notice the fangs, claws, batwings...");
}

void m_talk_horse (monster_t * m, player_t * p)
{
  if (m_statusp(m,HOSTILE) == TRUE)
    {
      mprint("The horse neighs angrily at you.");
      return;
    }

  if (pflagp(p,MOUNTED))
    {
      mprint("The horse and your steed don't seem to get along.");
      return;
    }

  if (m_statusp(m,HUNGRY) == TRUE)
    {
      mprint("The horse noses curiously at your pack.");
      return;
    }

  else if (env_is_dungeon(p->dlevel->environment))
    {
      mprint("The horse shies; maybe he doesn't like the dungeon air....");
      return;
    }

  mprint("The horse lets you pat his nose. Want to ride him? [yn] ");
  if (ynq() == 'y')
    {
      m_remove(m);
      setpflag(MOUNTED);
      calc_melee(p);
      mprint("You are now equitating!");
    }
}

void m_talk_hyena (monster_t * m, player_t * p)
{
  mprint("The hyena only laughs at you...");
}

void m_talk_parrot (monster_t * m, player_t * p)
{
  mprint("Polly wanna cracker?");
}

void m_talk_servant (monster_t * m, player_t * p)
{
  int x, y;
  int target;

  x = p->x;
  y = p->y;

  if (m->id == SERV_LAW)
    {
      target = SERV_CHAOS;
      mprint("The Servant of Law pauses in thought for a moment.");
      mprint("You are asked: 'Are there any Servants of Chaos hereabouts?' [yn] ");
    }
  else
    {
      target = SERV_LAW;
      mprint("The Servant of Chaos grins mischievously at you.");
      mprint("You are asked: 'Are there any Servants of Law hereabouts?' [yn] ");
    }

  if (ynq() == 'y')
    {
      print1("Show me.");
      show_screen();
      drawmonsters(TRUE);
      setspot(&x,&y);

      if (get_monster(p->dlevel, x, y))
	{
	  monster_t * mon;
	  mon = get_monster(p->dlevel, x, y);

	  if (mon->id == target)
	    {
	      mprint("The Servant launches itself towards its enemy.");
	      mprint("In a blaze of combat, the Servants annihilate each other!");
	      m_death(mon);
	      move_monster(m->dlevel, m, x, y);
	      m_death(m);
	    }
	  else
	    mprint("'Right. Tell me about it. Idiot!'");
	}
      else
	mprint("'Right. Tell me about it. Idiot!'");
    }
  else
    mprint("The servant shrugs and turns away.");
}

void m_talk_animal (monster_t * m, player_t * p)
{
  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s", m->monstring);
  else
    strcpy(Str2, m->monstring);

  mprint(Str2);
  mprint("shows you a scholarly paper by Dolittle, D. Vet.");
  mprint("which demonstrates that animals don't have speech centers");
  mprint("complex enough to communicate in higher languages.");
  mprint("It giggles softly to itself and takes back the paper.");
}


void m_talk_scream (monster_t * m, player_t * p)
{
  mprint("A thinly echoing scream reaches your ears....");
  morewait();
  mprint("You feel doomed....");
  morewait();
  mprint("A bird appears and flies three times widdershins around your head.");
  summon(-1,QUAIL);
  m->talkf = M_TALK_EVIL;
}


void m_talk_archmage (monster_t * m, player_t * p)
{
  if (m_statusp(m,HOSTILE) == TRUE)
    {
      mprint("The Archmage ignores your attempt at conversation");
      mprint("and concentrates on his spellcasting....");
    }
  else if (p->dlevel->environment == E_COURT)
    {
      mprint("The Archmage congratulates you on getting this far.");
      mprint("He invites you to attempt the Throne of High Magic");
      mprint("but warns you that it is important to wield the Sceptre");
      mprint("before sitting on the throne.");

      if (p->dlevel->site[m->x][m->y].p_locf == L_THRONE)
	{
	  mprint("The Archmage smiles and makes an arcane gesture....");
	  m_vanish(m);
	}
    }
  else
    {
      mprint("The Archmage tells you to find him again in his");
      mprint("Magical Court at the base of his castle in the mountains");
      mprint("of the far North-East; if you do he will give you some");
      mprint("important information.");
    }
}

void m_talk_maharaja (monster_t * m, player_t * p)
{
  if (m_statusp(m,HOSTILE) == FALSE)
    {
      switch (random_range(4))
	{
	case 0: mprint("The Maharaja says: 'So Mote it be!'");        break;
	case 1: mprint("The Maharaja says: 'Let it be written!'");    break;
	case 2: mprint("The Maharaja says: 'Let it be done!'");       break;
	case 3: mprint("The Maharaja offers you goat-eyeball soup."); break;
	}
    }
  else if (p->dlevel->environment == E_PALACE)
    {
      mprint("The Maharaja insists that you leave. Or die.");
    }
  else
    {
      mprint("'Where did I put those dungeons?'");
    }
}

void m_talk_merchant (monster_t * m, player_t * p)
{
  if (m_statusp(m,HOSTILE))
    {
      mprint("The merchant ignores you and screams:");
      mprint("'Help! Help! I'm being oppressed!'");
      return;
    }

  if (p->dlevel->environment == E_VILLAGE)
    {
      mprint("The merchant asks you if you want to buy a horse for 250GP.");
      mprint("Pay the merchant? [yn] ");

      if (ynq() == 'y')
	{
	  if (p->cash < 250) 
	    mprint("The merchant says: 'Come back when you've got the cash!'");
	  else
	    {
	      p->cash -= 250;
	      mprint("The merchant takes your money and tells you to select");
	      mprint("any horse you want in the stables.");
	      mprint("He says: 'You'll want to get to know him before trying to");
	      mprint("ride him. By the way, food for the horse is not included.'");
	      mprint("The merchant runs off toward the bank, cackling gleefully.");
	      m_vanish(m);
	    }
	}
      else
	mprint("The merchant tells you to stop wasting his time.");
    }
  else
    {
      mprint("The merchant tells you to visit his stables at his village");
      mprint("for a great deal on a horse.");
    }
}

void m_talk_prime (monster_t * m, player_t * p)
{
  if (m_statusp(m,HOSTILE))
    {
      m_talk_evil(m);
      return;
    }   

  if (p->dlevel->environment == E_CIRCLE)
    {
      print1("The Prime nods brusquely at you, removes a gem from his");
      print2("sleeve, places it on the floor, and vanishes wordlessly.");
      morewait();
      m_dropstuff(m);
      m_vanish(m);
    }
  else
    {
      print1("The Prime makes an intricate gesture, which leaves behind");
      print2("glowing blue sparks... He winks mischievously at you....");

      if (p->rank[CIRCLE] > 0)
	{
	  morewait();
	  print1("The blue sparks strike you! You feel enhanced!");
	  print2("You feel more experienced....");
	  p->pow += p->rank[CIRCLE];
	  p->mana += calcmana();
	  gain_experience(1000);
	  m_vanish(m);
	}
    }
}

/* end */
