/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/* Functions to load a level, or part of one, or otherwise use loaders. */

#include "glob.h"
#include "lev.h"
#include "util.h"
#include "game_time.h"
#include "load.h"
#include "lut.h"
#include <limits.h>
#include "findloc.h"

/* ** Internal declarations ** */
static void make_dungeon_level ( env_params_t * p_env_params, int populate);
static void make_tacmap(int type, int populate);
static void switch_to_real_level (env_params_t* p_env_params, int entry_style);
static void reify_subroom(subroom_data_t * p_subroom,
			  env_params_t  * p_env_params,
			  int flags);


/*******************************/
/* Functions to get information about loaders */

def_lookup_table_funcs(env_loader_t, env_loader, env_idx);

int environment_family(env_params_t* p_env_params)
{
  return p_env_params->p_env_loader->family;
}

/* **** The level-makers **** */

/* ** Make a dungeon. ** */

/* Get the relevant sub-loader */
dun_subenv_data_t * get_dun_subenv(dun_loader_t * p_dun_loader,
				   int logical_subtype)
{
  /* Table is 1-based, for historic reasons.  */
  int subtype = logical_subtype - env_subtype_min;
  dun_subenv_data_table_t table = p_dun_loader->subenvs;
  int num_normal_levs = table.num_els - 1;
  assert(table.array);
  /* Final level is only allowed at the bottom of a dungeon. */
  if(logical_subtype >= p_dun_loader->max_levels)
    {
      return &table.array[table.num_els - 1];
    }
  /* First element is the default. */
  if((subtype < 0) || (subtype >= num_normal_levs))
    {
      return &table.array[0];
    }
  else
    {
      return &table.array[subtype];
    }
}

int get_levplan(random_levplan_t table)
{
  int choice = random_pick(&table.t);
  return table.level_type[choice];
}

void make_dungeon_level ( env_params_t * p_env_params, int populate)
{
  int env_idx = p_env_params->env_idx;
  int tolevel = p_env_params->subtype;
  dun_loader_t * p_dun_loader =
    p_env_params->p_env_loader->data.dun;
  dun_subenv_data_t * p_sub =
    get_dun_subenv(p_dun_loader, tolevel);

  /* Conform loader as neccessary. */
  SET_TO_MIN(p_dun_loader->width,  MAXWIDTH);
  SET_TO_MIN(p_dun_loader->length, MAXLENGTH);

  /* Assign some information to Level. */
  Level->generated    = TRUE;
  Level->environment  = p_env_params->env_idx;
  Level->depth        = tolevel;
  Level->level_width  = p_dun_loader->width;
  Level->level_length = p_dun_loader->length;
  Lev_Length = Level->level_length;
  Lev_Width  = Level->level_width;

  /* Assign some global information. */
  SET_TO_MAX(deepest[env_idx], tolevel);
  MaxDungeonLevels = p_dun_loader->max_levels;

  /* Set wall_showchar.  Cheat:  Assume it's wall. */
  wall_showchar = WALL;

  {
    int levplan = get_levplan(p_sub->levplans);
    switch(levplan)
      {
      default:
	error("Unknown type of level");
	break;

      case dls_room_level:
	room_level();
	break;

      case dls_cavern_level:
	cavern_level();
	break;

      case dls_sewer_level:
	sewer_level();
	break;

      case dls_maze_level:
	maze_level();
	break;
      }
  }

  install_traps();
  install_specials();
  make_stairs_new();
  make_stairs_new();
  if(populate)
    {
      populate_level(env_idx);
      stock_level();
    }
}


/* ** Make a tacmap. ** */

void make_tacmap(int type, int populate)
{
  int x, y;

  /* Set wall_showchar.  Punt:  Assume it's wall. */
  wall_showchar = WALL;
  make_country_screen(type);
  if(populate)
    { make_country_monsters(type); }

  {
    int found = find_central_space(&x, &y);
    if(!found)
      {
	Level->site[x][y].locchar = FLOOR;
	Level->site[x][y].p_locf  = L_NO_OP;	
      }
    change_special_loc(sl_py_entrance, x, y);
  }

  /* Since tacmap can't be regenerated (no proper randomseed), it's
     entirely marked "changed". Eventually we could just store a
     proper randomseed with levels.  */
  /* This also does "nite's gloom", which should be a post-process. */
  for(y=0;y<Lev_Length;y++)
    {
      for(x=0;x<Lev_Width;x++)
	{
	  if (nighttime())
	    { Level->site[x][y].lstatus = CHANGED; }
	  else
	    { lset(x, y, CHANGED); }
	}
    }
}


/* ** Make country. ** */

def_lookup_table_funcs(cmap_read_data_t, cmap_lut, sym);

cmap_read_data_t* find_cmap_read_data( char sym, cmap_lut_table_t table )
{
  return cmap_lut_find(sym, table);
}

/* loads the countryside level from the data file */
void load_country(void)
{
  int length, width;
  int i,j;
  env_loader_t  * p_env_loader =
    env_loader_find( E_COUNTRYSIDE, env_loader_table );
  cmap_lut_table_t table = * p_env_loader->data.coun;
  map  * the_map = map_open(MAP_country);
  map_setLevel(the_map,0);
  length = map_getLength(the_map);
  width  = map_getWidth(the_map);

  for(j=0;j<length;j++) {
    for(i=0;i<width;i++) {
      struct terrain * p_terr = &Country[i][j];	
      char site = map_getSiteChar(the_map,i,j);
      cmap_read_data_t* read_data = find_cmap_read_data(site, table);

      p_terr->status = 0;
      p_terr->base_terrain_type    = read_data->base_terrain_type;
      p_terr->current_terrain_type = read_data->current_terrain_type;
      p_terr->aux                  = read_data->aux;

    }
  }
  map_close(the_map);
}

/* Auxilliary for game effect */
def_lookup_table_funcs_2(cmap_read_data_t, cmap_lut_table_t,
			 cmap_basetype_lut, base_terrain_type);

void flatten_country_loc(int x, int y)
{
  env_loader_t  * p_env_loader =
    env_loader_find( E_COUNTRYSIDE, env_loader_table );
  cmap_lut_table_t table = * p_env_loader->data.coun;
  int base_terrain_type =
    Country[x][y].base_terrain_type;
  cmap_read_data_t* read_data =
    cmap_basetype_lut_find(base_terrain_type, table);

  Country[x][y].current_terrain_type =
    Country[x][y].base_terrain_type =
    read_data->current_terrain_type;

  c_set(x, y, CHANGED);
}


/* ** Make a mapped level. ** */

/* Get the relevant sub-loader. */
subenv_data_t* get_map_subenv(maploader_t * p_map_loader,
			       int logical_subtype)
{
  /* Table is 1-based, for historic reasons.  */
  int subtype = logical_subtype - env_subtype_min;
  subenv_data_table_t table = p_map_loader->subenvs;
  if((subtype < 0) || (subtype >= table.num_els))
    {
      /* First element is the default. */
      return &table.array[0];
    }
  else
    {
      return &table.array[subtype];
    }
}


/*  Load a map for any reason. */
void load_map (int x0,
	       int y0,
	       env_params_t* p_env_params,
	       maploader_t * p_map_loader,
	       int new_level,
	       int flags)
{
  int x,y;
  int length, width;
  /*lmap_table_t table = p_map_loader->table;*/
  subenv_data_t * p_subenv =
    get_map_subenv( p_map_loader, p_env_params->subtype);
  readtable_t * p_sub_table =
    p_subenv->table;
  sitemaker_params_t site_parms =
  { p_env_params,
    flags,
    0,
    0,
    p_map_loader->shuffled_sites,
  };

  map * the_map = map_open(p_subenv->map_idx);
  int num_variants = map_getDepth(the_map);

  map_setLevel(the_map,random_range(num_variants));

  length = map_getLength(the_map);
  width  = map_getWidth(the_map);

  if(num_variants > 1)
    {
      site_parms.flags |= flag_position_varies;
    }

  if(new_level)
    {
      /* Initialize the level itself.  */
      Level->environment  = p_env_params->env_idx;
      Level->subtype      = p_env_params->subtype;
      Level->level_length = length;
      Level->level_width  = width;
      Level->generated    = TRUE;

      Lev_Length = Level->level_length;
      Lev_Width  = Level->level_width;

      /* Init shuffled sites. */
      if(p_map_loader->shuffled_sites)
	{ init_shuffler(p_map_loader->shuffled_sites);	}

      /* Set wall_showchar.  Kluuge:  Make one and see what it is. */
      reify_lmap_sym( '#', p_sub_table, &site_parms );
      wall_showchar = Level->site[site_parms.x][site_parms.y].locchar;

      /* Don't let whole levels have offsets. */
      x0 = 0;
      y0 = 0;
    }

  for (y = y0; y < y0 + length; ++y)
    {
      site_parms.y = y;
      for (x = x0; x < x0 + width; ++x)
        {
	  char site = map_getSiteChar(the_map,x-x0,y-y0);
	  site_parms.x = x;
	
	  /* Pre-process. */
	  do_site_action( &site_parms, p_subenv->prepare_action);

	  /*  Process map character.  */
	  reify_lmap_sym( site, p_sub_table, &site_parms );

	  /* Post-process. */
	  p_map_loader->post_process( &site_parms );
        }
    }
  map_close(the_map);

  /*  Do any subrooms. */
  {
    int i;
    for( i = 0; i < p_subenv->subrooms.num_els; i++)
      {
	reify_subroom(&p_subenv->subrooms.array[i], p_env_params, flags);
      }
  }
}

void make_map_level (env_params_t* p_env_params, int populate)
{

  maploader_t * p_map_loader = p_env_params->p_env_loader->data.map;
  flag_getter_f get_flags = p_env_params->p_env_loader->flag_getter;
  int flags = get_flags(p_env_params->subtype);

  if (!populate)
    {
      flags |= flag_empty;
    }

  load_map (0, 0, p_env_params, p_map_loader, TRUE, flags);
}

/*  Subroom.  */


void reify_subroom(subroom_data_t * p_subroom,
		   env_params_t  * p_env_params,
		   int flags)
{
  int dummy;
  int min_x, min_y, max_x, max_y;

  get_special_loc(p_subroom->min_x, &min_x, &dummy);
  get_special_loc(p_subroom->min_y, &dummy, &min_y);
  get_special_loc(p_subroom->max_x, &max_x, &dummy);
  get_special_loc(p_subroom->max_y, &dummy, &max_y);

  switch(p_subroom->type)
    {
    default:
    case subroom_none:
      /* Do nothing. */
      break;

    case subroom_name:
      /*  Set just the room name.  */
      {
	int x, y;
	for (y = min_y; y < max_y; ++y)
	  {
	    for (x = min_x; x < max_x; ++x)
	      {
		Level->site[x][y].roomnumber = p_subroom->index;
	      }
	  }
      }
      break;

    case subroom_map:
      /*  Reify a nested map.  */
      if((p_subroom->index >= 0) &&
	 (p_subroom->index < aux_loaders_table.num_els))
      {
	maploader_t * p_maploader_data =
	  aux_loaders_table.array[p_subroom->index];

	load_map (min_x, min_y, p_env_params, p_maploader_data, FALSE,
		  flags);
      }
      break;

#if 0 /* For future extension. */
    case subroom_actions:
      {
	int x, y;
	for (y = min_y; y < max_y; ++y)
	  {
	    for (x = min_x; x < max_x; ++x)
	      {
		sitemaker_params_t site_parms =
		{ p_env_params, flags, x, y, 0, };
		/* do_site_action( &site_parms, ??actlist?? ); */
	      }
	  }
      }
      break;
#endif
    }
}

/* *************************************** */
/* ***  Load a given level of any type *** */


/* Raw level-creation, no contingences.  */
void make_level (env_params_t* p_env_params, int populate)
{
  int set_randomizer = p_env_params->p_env_loader->set_randomizer;
  if(set_randomizer)
    {
      initrand(p_env_params->env_idx, p_env_params->subtype);
    }


  switch(environment_family(p_env_params))
    {
    default:
      error("Unknown family of environment");
      break;

    case enst_coun:
      error("Countryside is not a `Level'");
      break;

    case enst_map:
	make_map_level (p_env_params, populate);
	break;

    case enst_dun:
	make_dungeon_level (p_env_params, populate);
	break;

    case enst_tacmap:
	make_tacmap(p_env_params->subtype, populate);
	break;
    }

  if(set_randomizer)
    {
      initrand(E_RESTORE, 0);
    }
}


/* A variant of make_level, used by save.c restore_level. */
void make_level_by_indexes(char new_environment, int subtype, int populate)
{

  env_params_t env_params =
  { new_environment, subtype, 0, 0, };
  env_params.p_env_loader =
    env_loader_find( new_environment, env_loader_table );

  make_level( &env_params, populate);
}


/* All level-switching to a non-country Level happens here. */
void switch_to_real_level (env_params_t * p_env_params,
		   int entry_style)
{
  general_find_level(p_env_params);

  if (entry_style == lem_rewrite)
    {
      if(Level->generated)
	{ clear_level(Level); };
    }

  if(!Level->generated)
    {
      make_level (p_env_params, TRUE);
    }

  /* Paranoia:  Catch any possible misses. */
  Lev_Length = Level->level_length;
  Lev_Width  = Level->level_width;
}


/* Physically switch environments.  Don't do associated effects. */
void switch_to_level (env_params_t* p_env_params, int entry_style)
{
  p_env_params->p_env_loader =
    env_loader_find( p_env_params->env_idx, env_loader_table );

  /*  This can happen in wizard. */
  if(!p_env_params->p_env_loader) { return; }

  switch(environment_family(p_env_params))
    {
    default:
      mprint("That's not a real environment!");
      break;

    case enst_map:
    case enst_tacmap:
      /* Ordinarily, use a level. */
      switch_to_real_level ( p_env_params, entry_style);
      break;

    case enst_dun:
      /* Dungeon level depth is specially forced to be at least
	 env_subtype_min (1).  This is an arguable approach but
	 lets the 1-based assumptions thruout the game keep working.  */
      if(entry_style == lem_enter)
	{
	  SET_TO_MAX(p_env_params->subtype, env_subtype_min);
	}
      switch_to_real_level ( p_env_params, entry_style);
      break;

    case enst_coun:
      /* Country doesn't use Level and is always loaded.  */
      Lev_Length = 64;
      Lev_Width  = 64;
      break;
    }
}


int get_start_loc(env_params_t* p_env_params,
		  int entry_style,
		  int *px,
		  int *py)
{
  int env_family = environment_family(p_env_params);
  switch(entry_style)
    {
    default:
      return FALSE;

    case lem_enter:
      /*  The normal start position. */
      if(env_family == enst_coun)
	{
	  /* Cheat:  Country's start location is known to be CITY,0.  */
	  find_country_site(CITY, 0, px, py );
	}
      else
	{ get_special_loc(sl_py_entrance, px, py); }
      break;

    case lem_emerge:
      /* Where we previously were in the environment.  Dungeons,
	 because they have multiple levels, store "emerge" location in
	 each level's special loc[1].  Other environments store it on
	 the env-stack, lest they lose it if levels are lost. */
      if(env_family == enst_dun)
	{ get_special_loc(sl_py_emerge, px, py); }
      else
	{
	  *px = p_env_params->x;
	  *py = p_env_params->y;
	}
      break;
	
    case lem_rewrite:
    case lem_teleport:
      /* Get a random tolerable location. */
      if(env_family == enst_coun)
	{ find_random_country_loc(px,py); }
      else
	{ find_floorspace(px,py); }
      break;
    }
  return TRUE;
}

/* All level-switching, whether within dungeon or changing
   environments, goes thru here. */
void put_player_in_level (env_params_t* p_env_params, int entry_style)
{
  int x, y, have_start_loc;

  nullify_current_level_effects();
  switch_to_level (p_env_params, entry_style);

  have_start_loc = get_start_loc(p_env_params, entry_style, &x, &y);
  if(have_start_loc)
    {
      setPlayerXY(x, y);
      if(environment_family(p_env_params) != enst_coun)
	{ sign_print(x, y, TRUE); }
    }

  set_notice_flags( notice_new_level );
}


/*******************************/
/* Auxilliary functions, used for game effects, not loading ~per se~. */

/* Reloader. */
void reload_map(maploader_t * p_map_loader, int ulc_index, int flags)
{
  int x, y, found_ulc;
  found_ulc = get_special_loc(ulc_index, &x, &y);
  if(!found_ulc) { return; }
  load_map(x, y, current_env_params(), p_map_loader, FALSE, flags);
}


/* Use the "reify" mechanism for a one-shot change.  Assumes level is
   already loaded.  Only fires if it's a map or dungeon level.
*/
void reify_sym_at_aux(char sym, int x, int y)
{
  env_params_t * p_env_params = current_env_params();
  /* There seems no point to making responsive flags here. */
  int flags = 0;
  /* shuffled is NULL because no shuffled sites should be made as
     one-shot sites. */
  sitemaker_params_t site_parms =
  { p_env_params, flags, x, y, NULL, };

  switch(environment_family(p_env_params))
    {
    default:
      /* Do nothing. */
      return;

    case enst_dun:
      {
	dun_loader_t * p_dun_loader =
	  p_env_params->p_env_loader->data.dun;
	dun_subenv_data_t * p_sub =
	  get_dun_subenv(p_dun_loader, p_env_params->subtype);
	readtable_t * p_sub_table = p_sub->table;

	reify_lmap_sym( sym, p_sub_table, &site_parms );
      }
      break;

    case enst_map:
      {
	maploader_t * p_map_loader =
	  p_env_params->p_env_loader->data.map;
	subenv_data_t * p_subenv =
	  get_map_subenv( p_map_loader, p_env_params->subtype);
	readtable_t * p_sub_table =
	  p_subenv->table;

	reify_lmap_sym( sym, p_sub_table, &site_parms );
      }
      break;
    }

}


/* Only considers changes affecting the permanent location, not
   changes of monster or objects. */
static int locations_same_p(plc loc_0, plc loc_1)
{
  return
    (
     (loc_0->p_locf      == loc_1->p_locf     ) &&
     (loc_0->lstatus     == loc_1->lstatus    ) &&
     (loc_0->roomnumber  == loc_1->roomnumber ) &&
     (loc_0->locchar     == loc_1->locchar    ) &&
     (loc_0->aux         == loc_1->aux        )
     );
}

/* Change to the given sym, one-shot. */
int reify_sym_at(char sym, int x, int y)
{
  location old_loc = Level->site[x][y];
  reify_sym_at_aux(sym, x, y);

  if( !locations_same_p(&old_loc, &Level->site[x][y]) )
    {
      plotspot(x, y, TRUE);
      lset(x, y, CHANGED);
      set_notice_flags(notice_lev_altered);
      return TRUE;
    }

  return FALSE;
}


/*  ***************************************************  */
/*  Functions available to support loaders.  */

/**** Flag-setting functions ****/

int no_flags(int ignore) { return 0; }

int temple_flags(int deity)
{
  /* Whether player is high priest. */
  int player_is_hp =
    (  Player.patron &&
       (strcmp(Player.name, Priest[Player.patron]) == 0) &&
       Player.rank[PRIESTHOOD] == HIGHPRIEST);

  /* Main Temple is peaceful for player of same sect,druids always
     peaceful. */
  int temple_hostile = ((deity != Player.patron) && (deity != DRUID));

  int flags =
    ( temple_hostile ? flag_hostile : 0)
    |
    flag_set_safety
    |
    ( player_is_hp ? flag_3 : 0);

  return flags;
}

int dlair_flags(int ignore)
{ return (gamestatusp(KILLED_DRAGONLORD) ? flag_empty : 0); }


int speak_flags(int ignore)
{
  int hostile = Player.alignment <= 0;
  int flags =
    hostile ? flag_hostile : 0
    |
    flag_set_safety
    |
    (gamestatusp(KILLED_LAWBRINGER) ? flag_empty : 0)
    ;

  return flags;
}

int misle_flags(int ignore)
{ return (gamestatusp(KILLED_EATER) ? flag_empty : 0); }

int circle_flags(int ignore)
{
  int safe = (Player.rank[CIRCLE] > 0);
  return
    flag_set_safety
    |
    (safe ? 0 : flag_hostile);
}

int safe_flags(int ignore)
{ return flag_set_safety; }

/**** Post-processing functions. ****/
/* These almost all apply to monsters, but post_process_destroy_order
   also sets "CHANGED". */

void post_process_no_op(sitemaker_params_t * p_site_parms) {}


void post_process_dlair(sitemaker_params_t * p_site_parms)
{
  /*  Change dragons' specialf.  */
  int x = p_site_parms->x;
  int y = p_site_parms->y;
  monster * mon = Level->site[x][y].creature;
  if(mon && mon->meleef == M_MELEE_DRAGON)
    {
      mon->specialf = M_SP_LAIR;
    }
}

void post_process_set_m_sp_court(sitemaker_params_t * p_site_parms)
{
  /* Change monsters' specialf, except for uniques (Oz and the prime
      sorceror), who set their specialf themselves.  */
  int x = p_site_parms->x;
  int y = p_site_parms->y;
  monster * mon = Level->site[x][y].creature;
  if(mon && !mon->uniqueness)
    {
      mon->specialf = M_SP_COURT;
    }
}

void post_process_house(sitemaker_params_t * p_site_parms)
{
  int x = p_site_parms->x;
  int y = p_site_parms->y;
  monster * mon = Level->site[x][y].creature;

  if(mon)
    {
      if (nighttime())
	m_status_reset(mon, AWAKE);
      else
	m_status_set(mon, AWAKE);

      /* Cheat:  We know we only make NPCs, Hi NPCs, and ghosts.  */
      if ((mon->id == NPC) || (mon->id == HISCORE_NPC))
	mprint("You detect signs of life in this house.");
      else
	/* we got a ghost off the npc list */
	mprint("An eerie shiver runs down your spine as you enter....");
    }
}

/* make all city monsters asleep, and shorten their wakeup range to 2 */
/* to prevent players from being molested by vicious monsters on */
/* the streets */
void post_process_calm_monster(sitemaker_params_t * p_site_parms)
{
  int x = p_site_parms->x;
  int y = p_site_parms->y;
  monster * mon = Level->site[x][y].creature;
  if(mon)
    {
      m_status_reset(mon, AWAKE);
      mon->wakeup = 2;
    }
}

void post_process_destroy_order(sitemaker_params_t * p_site_parms)
{
  int x = p_site_parms->x;
  int y = p_site_parms->y;
  monster * mon = Level->site[x][y].creature;

  /*  Put a ghost on each spot. */
  if(mon)
    { m_remove(mon); }
  mon = make_site_monster(x, y, GHOST);
  mon->monstring = "ghost of a Paladin";
  m_status_set(mon, HOSTILE);

  /* We've also changed each spot. */
  lset(x, y, CHANGED);
}

#if 0 /* Tacmap has no place to use this, but it should. */
/* WDT: changed variant spelling to 'night'.  'gh' may be ugly, but
 * it's standard in Omega, and exceptions are uglier. */
void post_process_nightgloom(sitemaker_params_t * p_site_parms)
{
  if (nighttime())
    {
      int x = p_site_parms->x;
      int y = p_site_parms->y;
      Level->site[x][y].lstatus = 0;
    }
}
#endif
