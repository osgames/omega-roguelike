/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/***** Site functions (incliding country) that change environment *****/

#include "glob.h"
#include "load.h"
#include "command.h" /* For cmd_dismount_steed */


/* Dungeons enter level 0, and it's made 1-based later.  That lets us
   use sensible 0's here, and we can fix level-description and
   is-dungeon tests later.  */
void enter_site(Symbol site)
{
  int subtype = Country[Player.x][Player.y].aux;
  switch(site) {
  default:
    print3("There's nothing to enter here!"); 
    break;

  case CITY:
    change_environment_push(E_CITY, 0); 
    break;

  case VILLAGE: 
    change_environment_push(E_VILLAGE, subtype); 
    break;

  case CAVES:
    change_environment_push(E_CAVES, 0); 
    break;

  case CASTLE:
    change_environment_push(E_CASTLE, 0); 
    break;

  case VOLCANO:
    change_environment_push(E_VOLCANO, 0); 
    break;

  case TEMPLE:
    change_environment_push(E_TEMPLE, subtype); 
    break;

  case DRAGONLAIR:
    change_environment_push(E_DLAIR, 0); 
    break;

  case STARPEAK:
    change_environment_push(E_STARPEAK, 0); 
    break;

  case MAGIC_ISLE:
    change_environment_push(E_MAGIC_ISLE, 0); 
    break;

  case PALACE:
    change_environment_push(E_PALACE, 0); 
    break;
  }
}

/* All the environment exits.  */

/* Used by dlair, misle, speak, temple, tacmap.  */
void l_tactical_exit(void)
{
  if (optionp(CONFIRM)) {
    if (cinema_confirm("You're about to leave this place.") != 'y')
      return;
  }
  change_environment_emerge();
}

void l_arena_exit(void)
{
  if (optionp(CONFIRM) && !Arena_Victory)
    {
      if (cinema_confirm("Really flee the match?") != 'y')
       { return; }
    }  
  change_environment_emerge();
}


void l_house_exit(void)
{
  if (optionp(CONFIRM)) {
    clearmsg();
    if (cinema_confirm("You're about to step out of this abode.") != 'y')
      return;
  }
  change_environment_emerge();
}

void l_countryside(void)
{
  if (optionp(CONFIRM)) {
    if (cinema_confirm("You're about to return to the countryside.") != 'y')
      return;
  }
  change_environment_emerge();
}


/* Stairs are now always locf's. */

void l_stairs_up(void)
{
  if (pflagp(MOUNTED))
    { print2("You manage to get your horse upstairs.");}

  change_special_loc(sl_py_entrance, Player.x, Player.y);
  if (Level->depth <= env_subtype_min)
    { change_environment_emerge(); }
  else
    {
      print1("You ascend a level.");
      change_level_aux(Level->depth-1, lem_emerge);
    }
}

void l_stairs_down(void)
{
  if (pflagp(MOUNTED))
    { print2("You manage to get your horse downstairs."); }

  change_special_loc(sl_py_emerge, Player.x, Player.y);
  if (Current_Environment == Current_Dungeon)
    {
      print1("You descend a level.");
      change_level_aux(Level->depth+1, lem_enter);
    }
  else
    { print3("This stairway is deviant. You can't use it."); }
}

void l_sewer(void)
{
  print1("You pry open a manhole and descend into the sewers below.");
  if (pflagp(MOUNTED))
    {
      print2("You horse waits patiently outside the sewer entrance....");
      cmd_dismount_steed(no_dir, TRUE);
    }
  change_environment_push(E_SEWERS, 0);
}
  


/*  ***********************************  */


