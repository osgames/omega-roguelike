/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */
/*  ***********************************  */

#include "lut.h"


/*** Enumerations ***/

/*  Enumerations for the tests that actions do.  */
/*  This tests the entire bitarray of flags.  Only actions use
    maskedtester_satisfied.  Other tests just test bitwise.
    
    Both test and mask are stored in a single scalar.  Dirty but
    easier than building and working with a structure.
*/
#define define_maskedtester(test, mask) \
  ((test & 0xff) << 8 | (mask & 0xff))
#define maskedtester_satisfied(flags, maskntest) \
  (((flags & maskntest) << 8) == (maskntest & 0xff00))
enum lmap_read_test_flags
{
  flag_empty           = 0x01,
  flag_hostile         = 0x02,
  flag_set_safety      = 0x04,
  flag_position_varies = 0x08,
  flag_3               = 0x10,
  flag_4               = 0x20,
};
enum lmap_read_testers
{  
  always       	= define_maskedtester(0, 0),
  if_empty 	= define_maskedtester(flag_empty,     flag_empty),
  not_empty     = define_maskedtester(0,              flag_empty),
  if_hostile    = define_maskedtester(flag_hostile,   flag_hostile),
  not_hostile   = define_maskedtester(0,              flag_hostile),
  if_flag3     	= define_maskedtester(flag_3,         flag_3),
  not_flag3    	= define_maskedtester(0,              flag_3),
  if_flag4     	= define_maskedtester(flag_4,         flag_4),
  not_flag4    	= define_maskedtester(0,              flag_4),
};

/* All the possible actions. */
enum lmap_read_action
{
  lmap_make_monster,
  lmap_make_logical_monster, 
  lmap_set_locf,
  lmap_set_locchar,
  lmap_set_aux,
  lmap_set_aux_to_subtype,
  lmap_set_loc_flags,
  lmap_clear_loc_flags,
  lmap_set_roomnumber,
  lmap_make_shuffled_site,
  lmap_make_random_site,   
  lmap_make_logical_site,
  lmap_make_treasure,
  lmap_make_specific_treasure,
  lmap_set_special_loc,
  lmap_retain_max,
  lmap_retain_min,
  lmap_set_multi_pos,
  lmap_copy_prev_loc,

};

enum logical_monsters
{ 
  mon_random,
  mon_elite_dragon,
  mon_undead_guard,
  mon_house_npc,
  mon_mansion_npc,
  mon_justiciar,
  mon_high_priest,
  mon_prime_sorceror,
  mon_archmage,
  mon_arena_monster,
};

enum logical_sites
{
  site_food_bin,
  site_random_altar,
  site_alert_statue,
  site_all_stops,
};

enum subroom_types
{
  subroom_none,
  subroom_name,
  subroom_map,
  subroom_actions,
};

enum level_entry_styles
{
  lem_enter, lem_emerge, lem_other, lem_rewrite, lem_restore,
  lem_teleport, lem_descend, lem_ascend,
};

enum env_families
{
  enst_map, enst_dun, enst_coun, enst_tacmap,
};

enum cache_styles
{ cache_dont, cache_in_temp, cache_in_city, cache_in_dungeon,
  cache_not_a_level, };

enum dun_level_styles
{
  dls_room_level,
  dls_cavern_level,
  dls_sewer_level,
  dls_maze_level,
};


/******** Types ********/

/** Symbol-to-action translation tables **/

typedef struct lmap_reify_data_t
{
  int test;   /* See lmap_read_testers */
  int action; /* Enumerated in lmap_read_action */
  int param1;
} lmap_reify_data_t;


def_lookup_table_type(lmap_reify_data_t, lmap_reify_data);
def_lookup_table_type(lmap_reify_data_table_t, actlist);

typedef struct lmap_read_data_t
{
  int sym;
  lmap_reify_data_table_t  act;
} lmap_read_data_t;

def_lookup_table_type(lmap_read_data_t, lmap);

typedef struct readtable_t
{
  lmap_table_t         data;
  struct readtable_t * superclass;  /* Possibly NULL */
} readtable_t;

/** Sets of id's, eg for monsters. **/
typedef int pseudoid_t;
def_lookup_table_type(pseudoid_t, pseudoid);
typedef pseudoid_table_t pseudoid_set_t;
def_lookup_table_type(pseudoid_set_t, pseudoid_set);


/** Randomized actions **/

/* Random-pick types. */
typedef struct random_table_t
{
  pseudoid_table_t t;
  int sum;
} random_table_t;

typedef struct freq_table_t
{
  random_table_t t;
  /* Cache recognition data  */
  pseudoid_t id;
  int difficulty;
} freq_table_t;

def_lookup_table_type(freq_table_t, freq_table);


typedef struct random_action_t
{
  random_table_t t;
  lmap_reify_data_table_t * data_array;
} random_action_t;

def_lookup_table_type(random_action_t *, random_action);



/** Shuffled sites, as in village or city. **/
typedef struct shuffled_site_table_t
{
  lmap_reify_data_table_t * act_list;
  int                       num_act_lists;
  int                       shuffle_size;
  lmap_reify_data_table_t   default_action;
} shuffled_site_table_t;

/*** Map loader types. ***/

/* Subrooms:  Mini-map loaders and room-number assigners.  */
typedef struct subroom_data_t
{
  /*  Which special loc to get each co-ordinate from.  */
  int   min_x, min_y, max_x, max_y;
  /* Type, enumerated in subroom_types. */
  int                 type;
  /* Index, interpreted according to type. */
  int                 index; 
} subroom_data_t;

def_lookup_table_type(subroom_data_t, subroom);

typedef struct subenv_data_t
{
  char *                   name;
  int                      map_idx;
  subroom_table_t          subrooms;
  lmap_reify_data_table_t  prepare_action;
  readtable_t *            table;
} subenv_data_t;


def_lookup_table_type(subenv_data_t, subenv_data);

struct sitemaker_params_t;
typedef void (*post_process_f_t)(struct sitemaker_params_t * p_site_parms);

/* Map loader. */
typedef struct maploader_t
{
  /* Sub-environments, eg different villages or temples.  Must have 1
     or more elements.  Indexed from env_subtype_min, not 0. */
  subenv_data_table_t    subenvs;
  /* Post-processing function.  For setup too anomalous to be
     accomplished by actions.  */
  post_process_f_t       post_process;
  /* Shuffled site table.  Possibly NULL */
  shuffled_site_table_t * shuffled_sites; 
} maploader_t;
def_lookup_table_type(maploader_t *, maploader);


/*** Dungeon loader types. ***/

typedef struct random_dun_monster_table_t
{
  random_table_t t;
  pseudoid_t * data_array; /* Same number of entries. */
} random_dun_monster_table_t;


typedef struct random_levplan_t
{
  random_table_t t;
  int * level_type;
} random_levplan_t;


/* Dungeon sub-environment type. */
typedef struct dun_subenv_data_t
{
  char *             name;
  /* For future expansion. */
  readtable_t *      table;
  random_levplan_t   levplans;
  /* For future expansion. */
  random_dun_monster_table_t random_monsters;
} dun_subenv_data_t;
def_lookup_table_type(dun_subenv_data_t, dun_subenv_data);

/* Dungeon loader. */
typedef struct dun_loader_t
{
  int width;
  int length;
  int max_levels;
  dun_subenv_data_table_t    subenvs;
  /* For future expansion. */
  int num_monsters_base;
} dun_loader_t;

/*** Tacmap loader types ***

   In theory, `sym' indicates which one to use.  Theoretically,
   countryside's `aux' could do this instead.

   This could be extended to fully describe tacmaps:

   Add some way to distinguish nite from day (for road)
   Add list of probabilized random monsters
   Add list of random terrain-characters.
   Add lmap_table_t to interpret characters, but not in
   tacmap_subenv_data_t because the table is shared by all tacmaps.

   And most importantly, create that data in maptable.el and the
   interpreter (use tools/make-maptable.el) */
typedef struct tacmap_subenv_data_t
{
  int sym;  
} tacmap_subenv_data_t;

def_lookup_table_type(tacmap_subenv_data_t, tacmap_subenv);
typedef tacmap_subenv_table_t tacmap_loader_t;

/*** Country loader types.  ***/
typedef struct cmap_read_data_t
{
  int sym;
  int base_terrain_type;
  int current_terrain_type;
  int aux;
} cmap_read_data_t;

def_lookup_table_type(cmap_read_data_t, cmap_lut);
typedef cmap_lut_table_t cmap_loader_t;

/*** Types for all loaders. ***/
typedef union specific_env_data_p_t
{
  void               * dummy; /* To shut off misleading warnings. */
  maploader_t        * map;
  dun_loader_t       * dun;
  tacmap_loader_t    * tacmap;
  cmap_loader_t      * coun;
} specific_env_data_p_t;

typedef int (*flag_getter_f)(int subtype);

/* Top-level loader type, for all environments. */
typedef struct env_loader_t
{
  int env_idx;
  int family;
  specific_env_data_p_t data;
  int set_randomizer;
  int cache_type;
  int base_difficulty;
  flag_getter_f  flag_getter;
} env_loader_t;

def_lookup_table_type(env_loader_t, env_loader);

/***** Non-loader types *****/

/* Data applying to a specific environment on the stack. */
typedef struct env_params_t
{
  int env_idx;
  int subtype;
  /*  The "last known" location, used for emerging to.  */
  int x, y;
  /*  The normal loader and data for this environment.  */
  env_loader_t  * p_env_loader;
} env_params_t;

/* Data applying to a specific x,y site being made in a map. */
typedef struct sitemaker_params_t
{
  env_params_t  * p_env_params;
  /* See lmap_read_test_flags for the meanings of the bits.  */
  int             flags;
  /* The co-ordinates of the site. */
  int             x, y;
  /* Shuffled site table, eg for city or village.  Possibly NULL */
  shuffled_site_table_t * shuffled_sites;
} sitemaker_params_t;

/** Configuration **/

enum { env_subtype_min = 1, };


/*  ***********************************  */

/* Table of loaders for all environments. */
extern env_loader_table_t env_loader_table;

/* Table of auxilliary loaders for subrooms. */
extern maploader_table_t aux_loaders_table;

/*  Special named auxilliary loaders, for some game effects. */
extern maploader_t city_map_loader_resurrect_guards;
extern maploader_t jail_map_loader;
extern maploader_t city_map_loader_destroy_order;

/* Table of the random actions.  Positions match lmap_make_random_site
   indexes. */ 
extern random_action_table_t random_act_table;

/* load.c */
/* Plus declarations in extern.h */
void make_level_by_indexes(char new_environment, int subtype, int entry_style);
void put_player_in_level (env_params_t* p_env_params, int entry_style);
void reload_map(maploader_t * p_map_loader, int ulc_index, int flags);
char* get_env_name(void); /* Not implemented yet. */

/* load.c post-processing functions. */
extern void post_process_no_op          (sitemaker_params_t * p_site_parms);
extern void post_process_dlair          (sitemaker_params_t * p_site_parms);
extern void post_process_set_m_sp_court (sitemaker_params_t * p_site_parms);
extern void post_process_house          (sitemaker_params_t * p_site_parms);
extern void post_process_calm_monster   (sitemaker_params_t * p_site_parms);
extern void post_process_destroy_order  (sitemaker_params_t * p_site_parms);
extern void post_process_nitegloom      (sitemaker_params_t * p_site_parms);


/* load.c flag-making functions. */
int safe_flags   (int ignore);
int temple_flags (int deity);
int dlair_flags  (int ignore);
int speak_flags  (int ignore);
int misle_flags  (int ignore);
int no_flags     (int ignore);
int circle_flags (int ignore);

/* sitereify.c */
void reify_lmap_sym( char site,
		     readtable_t * p_table,
		     sitemaker_params_t * p_site_parms);
void init_shuffler(shuffled_site_table_t * table);
void do_site_action(sitemaker_params_t * p_site_parms,
		    lmap_reify_data_table_t act);
int random_pick(random_table_t * p_table);

/* levchange.c */
/* Plus declarations in extern.h */
env_params_t * current_env_params(void);

/* levmem.c */
plv general_find_level(env_params_t* p_env_params);
plv get_fresh_level(void);
void clear_level (plv level);
     
/* levgame.c  */
void player_enter_current_environment(void);
void player_emerge_to_current_environment(void);
void player_leave_current_environment(void);
void nullify_current_level_effects(void);

/* findloc.c */
/* Plus declarations in extern.h */
void fully_free_knowable_locs(list_t ** p_list);
void fully_free_special_locs(list_t ** p_list);
void change_special_loc(int which, int x, int y);
void special_loc_retain_max(int which, int x, int y);
void special_loc_retain_min(int which, int x, int y);

/* lev.c*/
int resolve_monster_id(int monsterid);
