/* Keymap data generated automatically by make-keymap, by Tehom */

#include "defs.h"
#include "extern.h"
#include "keys.h"

char * how_to_escape = "C-g to abort";

cmd_lookup_t cmd_lookup_array_90880[] =
{
   { '4', 5, cmd_run, },
   { '2', 6, cmd_run, },
   { '8', 7, cmd_run, },
   { '6', 4, cmd_run, },
   { '1', 2, cmd_run, },
   { '3', 0, cmd_run, },
   { '7', 3, cmd_run, },
   { '9', 1, cmd_run, },
   { '5', 9, cmd_run, },
   { 'u', 1, cmd_single_move, },
   { 'y', 3, cmd_single_move, },
   { 'n', 0, cmd_single_move, },
   { 'b', 2, cmd_single_move, },
   { 'l', 4, cmd_single_move, },
   { 'k', 7, cmd_single_move, },
   { 'j', 6, cmd_single_move, },
   { 'h', 5, cmd_single_move, },
   { '?', -1, cmd_help, },
   { '/', -1, cmd_charid, },
   { '#', -1, cmd_editstats, },
   { '>', -1, cmd_enter_site, },
   { 'X', -1, cmd_check_memory, },
   { 'V', -1, cmd_version, },
   { 'S', -1, cmd_save, },
   { 'R', -1, cmd_rename_player, },
   { 'Q', -1, cmd_quit, },
   { 'P', -1, cmd_show_license, },
   { 'O', -1, cmd_setoptions, },
   { 'I', -1, cmd_inventory_control, },
   { 'H', -1, cmd_hunt, },
   { 'E', -1, cmd_dismount_steed, },
   { 'x', -1, cmd_examine, },
   { 's', -1, cmd_countrysearch, },
   { 'i', -1, cmd_do_inventory_control, },
   { 'e', -1, cmd_eat, },
   { 'd', -1, cmd_drop, },
   { ' ', -1, cmd_no_op, },
   { 24, -1, cmd_wish, },
   { 23, -1, cmd_wizard_draw, },
   { 18, -1, cmd_redraw, },
   { 16, -1, cmd_bufferprint, },
   { 13, -1, cmd_no_op, },
   { 12, -1, cmd_xredraw, },
   { 11, -1, cmd_frobgamestatus, },
   { 9, -1, cmd_display_pack, },
   { 8, -1, cmd_wizard, },
   { 7, -1, cmd_escape, },
   { 27, 2, cmd_keymap, },
};
cmd_lookup_table_t country_command_table =
{ cmd_lookup_array_90880, sizeof(cmd_lookup_array_90880)/sizeof(cmd_lookup_t), };

cmd_lookup_t cmd_lookup_array_90881[] =
{
   { '[', 3, cmd_keymap, },
   { 'O', 4, cmd_keymap, },
};
cmd_lookup_table_t cmd_lookup_table_2 =
{ cmd_lookup_array_90881, sizeof(cmd_lookup_array_90881)/sizeof(cmd_lookup_t), };

cmd_lookup_t cmd_lookup_array_90882[] =
{
   { 'P', -1, cmd_help, },
};
cmd_lookup_table_t cmd_lookup_table_4 =
{ cmd_lookup_array_90882, sizeof(cmd_lookup_array_90882)/sizeof(cmd_lookup_t), };

cmd_lookup_t cmd_lookup_array_90883[] =
{
   { '6', 5, cmd_keymap, },
   { 'C', 4, cmd_single_move, },
   { '5', 6, cmd_keymap, },
   { 'B', 6, cmd_single_move, },
   { 'E', -1, cmd_run, },
   { 'A', 7, cmd_single_move, },
   { 'F', 2, cmd_single_move, },
   { 'D', 5, cmd_single_move, },
   { 'H', 3, cmd_single_move, },
};
cmd_lookup_table_t cmd_lookup_table_3 =
{ cmd_lookup_array_90883, sizeof(cmd_lookup_array_90883)/sizeof(cmd_lookup_t), };

cmd_lookup_t cmd_lookup_array_90884[] =
{
   { '~', 1, cmd_single_move, },
};
cmd_lookup_table_t cmd_lookup_table_6 =
{ cmd_lookup_array_90884, sizeof(cmd_lookup_array_90884)/sizeof(cmd_lookup_t), };

cmd_lookup_t cmd_lookup_array_90885[] =
{
   { '~', 0, cmd_single_move, },
};
cmd_lookup_table_t cmd_lookup_table_5 =
{ cmd_lookup_array_90885, sizeof(cmd_lookup_array_90885)/sizeof(cmd_lookup_t), };

cmd_lookup_t cmd_lookup_array_90886[] =
{
   { '4', 5, cmd_run, },
   { '2', 6, cmd_run, },
   { '8', 7, cmd_run, },
   { '6', 4, cmd_run, },
   { '1', 2, cmd_run, },
   { '3', 0, cmd_run, },
   { '7', 3, cmd_run, },
   { '9', 1, cmd_run, },
   { 'U', 1, cmd_run, },
   { 'Y', 3, cmd_run, },
   { 'N', 0, cmd_run, },
   { 'B', 2, cmd_run, },
   { 'L', 4, cmd_run, },
   { 'K', 7, cmd_run, },
   { 'J', 6, cmd_run, },
   { 'H', 5, cmd_run, },
   { '5', 9, cmd_run, },
   { 'u', 1, cmd_single_move, },
   { 'y', 3, cmd_single_move, },
   { 'n', 0, cmd_single_move, },
   { 'b', 2, cmd_single_move, },
   { 'l', 4, cmd_single_move, },
   { 'k', 7, cmd_single_move, },
   { 'j', 6, cmd_single_move, },
   { 'h', 5, cmd_single_move, },
   { ':', -1, cmd_name_spot, },
   { '$', -1, cmd_repeat_cmd, },
   { '\"', -1, cmd_moveto_spot, },
   { '?', -1, cmd_help, },
   { '/', -1, cmd_charid, },
   { '#', -1, cmd_editstats, },
   { '@', -1, cmd_stay, },
   { '<', -1, cmd_upstairs, },
   { '>', -1, cmd_downstairs, },
   { ',', -1, cmd_nap, },
   { '.', -1, cmd_rest, },
   { 'Z', -1, cmd_bash_item, },
   { 'X', -1, cmd_check_memory, },
   { 'V', -1, cmd_version, },
   { 'T', -1, cmd_tunnel, },
   { 'S', -1, cmd_save, },
   { 'R', -1, cmd_rename_player, },
   { 'Q', -1, cmd_quit, },
   { 'P', -1, cmd_show_license, },
   { 'O', -1, cmd_setoptions, },
   { 'M', -1, cmd_city_move, },
   { 'I', -1, cmd_inventory_control, },
   { 'G', -1, cmd_give, },
   { 'F', -1, cmd_tacoptions, },
   { 'E', -1, cmd_dismount_steed, },
   { 'D', -1, cmd_disarm, },
   { 'C', -1, cmd_callitem, },
   { 'A', -1, cmd_activate, },
   { 'z', -1, cmd_bash_location, },
   { 'x', -1, cmd_examine, },
   { 'v', -1, cmd_vault, },
   { 't', -1, cmd_talk, },
   { 's', -1, cmd_search, },
   { 'r', -1, cmd_peruse, },
   { 'q', -1, cmd_quaff, },
   { 'p', -1, cmd_pickpocket, },
   { 'o', -1, cmd_opendoor, },
   { 'm', -1, cmd_magic, },
   { 'i', -1, cmd_do_inventory_control, },
   { 'g', -1, cmd_pickup, },
   { 'f', -1, cmd_fire, },
   { 'e', -1, cmd_eat, },
   { 'd', -1, cmd_drop, },
   { 'c', -1, cmd_closedoor, },
   { 'a', -1, cmd_zapwand, },
   { ' ', -1, cmd_no_op, },
   { 24, -1, cmd_wish, },
   { 23, -1, cmd_wizard_draw, },
   { 18, -1, cmd_redraw, },
   { 16, -1, cmd_bufferprint, },
   { 13, -1, cmd_no_op, },
   { 12, -1, cmd_xredraw, },
   { 11, -1, cmd_frobgamestatus, },
   { 9, -1, cmd_display_pack, },
   { 8, -1, cmd_wizard, },
   { 7, -1, cmd_escape, },
   { 6, -1, cmd_abortshadowform, },
   { 4, -1, cmd_player_dump, },
   { 2, -1, cmd_show_key, },
   { 1, -1, cmd_print_allocation_info, },
   { 27, 2, cmd_keymap, },
};
cmd_lookup_table_t dungeon_command_table =
{ cmd_lookup_array_90886, sizeof(cmd_lookup_array_90886)/sizeof(cmd_lookup_t), };

cmd_lookup_table_t * cmd_lookup_tables[] =
{&dungeon_command_table,
&country_command_table,
&cmd_lookup_table_2,
&cmd_lookup_table_3,
&cmd_lookup_table_4,
&cmd_lookup_table_5,
&cmd_lookup_table_6,
};

const int num_cmd_lookup_tables =
    sizeof(cmd_lookup_tables)/sizeof(cmd_lookup_table_t *);

