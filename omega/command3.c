/* omega copyright (C) by Laurence Raphael Brothers, 1987,1988,1989 */
/* command3.c */

/* This file contains some more top level command functions
   called from command1.c */

#include <pwd.h>
#include <unistd.h>

#include "game_time.h"
#include "glob.h"
#include "lev.h"
#include "liboutil/list.h"
#include "memory.h"
#include "util.h"
#include "findloc.h"
#include "command.h"


/* look at some spot */
static void apply_print_floor_object (void * vobj)
{
  object * obj;
  obj = vobj;
  menuprint("\n");
  menuprint(itemid(obj));
}


post_command_state_t cmd_examine(int dir, int first_time)
{
  int x, y;

  clearmsg();

  /* WDT HACK: I'm not sure I buy that one shouldn't be able to examine
   * when one is blind.  However, the 'right' way to do it is certainly
   * too difficult (I would expect to be able to examine only the items
   * I actually recall).  So, for now I'll use David Given's compromise.
   * 12/30/98
   */

  if (Player.status[BLINDED] > 0)
    {
      mprint("You're blind - you can't examine things.");
      return base_state;
    }

  mprint("Examine --");
  set_spot_from_player(&x, &y);

  if (!inbounds(x,y))
    { return base_state; }

  clearmsg();
  if (Current_Environment == E_COUNTRYSIDE)
    {
      if (!c_statusp(x, y, SEEN))
	{
	  print3("How should I know what that is?");
	  return base_state;
	}
      else
	{
	  mprint("That terrain is:");
	  mprint(countryid(Country[x][y].current_terrain_type));
	  RETURN_post_command_state( 0,  1 );
	}
      /*NOTREACHED*/
    }

    if (!view_los_p(Player.x, Player.y, x, y))
      {
	print3("I refuse to examine something I can't see.");
	return base_state;
      }

    {
      if (get_monster(Level, x, y))
	mprint(mstatus_string(get_monster(Level,x, y)));
      else if (Player.x == x && Player.y == y)
	describe_player();

      if (loc_statusp(x,y,SECRET))
	/*  This should be respective of Level's "secret" terrain */
	{ print2("An age-worn stone wall."); }
      else
	switch (Level->site[x][y].locchar)
	  {
	  case SPACE: print2("An infinite void."); break;
	  case PORTCULLIS: print2("A heavy steel portcullis"); break;
	  case ABYSS: print2("An entrance to the infinite abyss"); break;
	  case FLOOR:
	    if (Current_Dungeon == Current_Environment)
	      print2("A dirty stone floor.");
	    else
	      print2("The ground.");
	    break;

	  case WALL:
	    if (Level->site[x][y].aux == 0)
	      print2("A totally impervious wall.");
	    else if (Level->site[x][y].aux < 10)
	      print2("A pitted concrete wall.");
	    else if (Level->site[x][y].aux < 30)
	      print2("An age-worn sandstone wall.");
	    else if (Level->site[x][y].aux < 50)
	      print2("A smooth basalt wall.");
	    else if (Level->site[x][y].aux < 70)
	      print2("A solid granite wall.");
	    else if (Level->site[x][y].aux < 90)
	      print2("A wall of steel.");
	    else if (Level->site[x][y].aux < 210)
	      {
		if (Current_Environment == E_CITY)
		  print2("A thick wall of Rampart bluestone");
		else
		  print2("A magically reinforced wall.");
	      }
	    else
	      print2("An almost totally impervious wall.");
	    break;

	  case RUBBLE: print2("A dangerous-looking pile of rubble."); break;
	  case SAFE: print2("A steel safe inset into the floor."); break;
	  case CLOSED_DOOR: print2("A solid oaken door, now closed."); break;
	  case OPEN_DOOR: print2("A solid oaken door, now open."); break;
	  case STATUE: print2("A strange-looking statue."); break;
	  case STAIRS_UP: print2("A stairway leading up."); break;
	  case STAIRS_DOWN: print2("A stairway leading down...."); break;
	  case TRAP: print2(trapid(Level->site[x][y].p_locf)); break;

	  case HEDGE:
	    if (Level->site[x][y].p_locf == L_EARTH_STATION)
	      print2("A weird fibrillation of oozing tendrils.");
	    else
	      print2("A brambly, thorny hedge.");
	    break;

	  case LAVA: print2("A bubbling pool of lava."); break;
	  case LIFT: print2("A strange glowing disk."); break;
	  case ALTAR: print2("An (un?)holy altar."); break;
	  case CHAIR: print2("A chair."); break;
	  case WHIRLWIND: print2("A strange cyclonic electrical storm."); break;

	  case WATER:
	    if (Level->site[x][y].p_locf == L_WATER)
	      print2("A deep pool of water.");
	    else if (Level->site[x][y].p_locf == L_CHAOS)
	      print2("A pool of primal chaos.");
	    else if (Level->site[x][y].p_locf == L_WATER_STATION)
	      print2("A bubbling pool of acid.");
	    else
	      print2("An eerie pool of water.");
	    break;

	  case FIRE: print2("A curtain of fire."); break;
	  default: print2("Wow, I haven't the faintest idea!"); break;
	  }

      if (Level->site[x][y].object_list && !loc_statusp(x,y,SECRET))
	{
	  list_t * olist;
	  olist = Level->site[x][y].object_list;

	  if (1 == list_size(olist))
	    print3(itemid((object *)list_get_head(olist)));
	  else
	    {
	      set_notice_flags(notice_drew_menu);
	      menuclear();
	      menuprint("Things on floor:\n");
	      list_apply(olist, apply_print_floor_object);
	      showmenu();
	    }
	}

      morewait();
      sign_print(x,y,TRUE);
      RETURN_post_command_state( 0,  1 );
    }
}

post_command_state_t cmd_help(int dir, int first_time)
{
  char c;
  char filestr[80];
#if !defined(MSDOS) && !defined(AMIGA)
  FILE *in, *out;
  int n;
#endif

  clearmsg();
  print1("Please enter the letter indicating what topic you want help on.");
  menuclear();
  menuprint("a: Overview\n");
  menuprint("b: Characters\n");
  menuprint("c: Inventories\n");
  menuprint("d: Movement\n");
  menuprint("e: Combat\n");
  menuprint("f: Bugs\n");
  menuprint("g: Magic\n");
  menuprint("h: The Countryside\n");
  menuprint("i: The Screen Display\n");
  menuprint("j: Saving and Restoring\n");
  menuprint("k: Options Settings\n");
  menuprint("l: Dungeon/City/Other Command List\n");
  menuprint("m: Countryside Command List\n");
#if !defined(MSDOS) && !defined(AMIGA)
  menuprint("n: Everything\n");
#endif
  menuprint("ESCAPE: Forget the whole thing.");
  showmenu();
  do
    c = (char) mcigetc();
#if defined(MSDOS) || defined(AMIGA)
  while ((c < 'a' || c > 'm') && c != ESCAPE);
  if (c != ESCAPE) {
    sprintf(filestr, "%shelp%d.txt", Omegalib, c+1-'a');
    displayfile(filestr);
  }
#else
  while ((c < 'a' || c > 'n') && c != ESCAPE);
  if (c == 'n') {
    if (cinema_confirm("I'm about to copy all help files into 'omega.doc'.")=='y') {
      change_to_user_perms();
      out = checkfopen("omega.doc", "w");
      print2("Copying");
      for (n = 1; n <= 13; n++)
      {
	nprint2(".");
	sprintf(Str1, "%shelp%d.txt", Omegalib, n);
	in = checkfopen(Str1, "r");
	while (fgets(Str1, STRING_LEN, in))
	  fputs(Str1, out);
	fclose(in);
      }
      fclose(out);
      change_to_game_perms();
      nprint2(" Done.");
    }
  }
  else if (c != ESCAPE) {
    sprintf(filestr, "%shelp%d.txt", Omegalib, c+1-'a');
    print1("Display help file, or Copy help file to file in wd. [dc] ");
    do
      c = (char) mcigetc();
    while ((c != 'd') && (c != 'c')&& (c!=ESCAPE));
    if (c == 'd')
      displayfile(filestr);
    else if (c == 'c') copyfile(filestr);
  }
#endif
  set_notice_flags(notice_drew_menu);
  return base_state;

}

extern const char * LAST_OMEGA_BUILD_TIME;

post_command_state_t cmd_version(int dir, int first_time)
{
  print3(VERSIONSTRING);
  nprint3(":");
  nprint3(" build date: ");
  nprint3((char *)LAST_OMEGA_BUILD_TIME);
  return base_state;
}
void drop_1_missile_at(int x, int y, pob missile)
{
  setpflag(SUPPRESS_PRINTING);
  p_drop_at(x,y,missile);
  resetpflag(SUPPRESS_PRINTING);
  conform_lost_objects(1,missile);
}

post_command_state_t cmd_fire(int dir, int first_time)
{
  int index,x1,y1,x2,y2;
  struct monster *m;

  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  index = getitem_prompt("Fire/Throw --", NULL_ITEM);
  if (index == ABORT)
    { return base_state; }

  if (index == CASHVALUE)
    {
      print3("Can't fire money at something!");
      return base_state;
    }

  {
    pob missile = Player.possessions[index];
    if (cursed(missile) && missile->used)
      {
	print3("You can't seem to get rid of it!");
	return base_state;
      }

    {
      /* load a crossbow */
      pob bow = Player.possessions[O_WEAPON_HAND];
      if ((bow != NULL) &&
	  (bow->id == OB_CROSSBOW) &&
	  (bow->aux != LOADED) &&
	  (missile->id == OB_BOLT)) {
	/* WDT: how much time? */
	mprint("You crank back the crossbow and load a bolt.");
	bow->aux = LOADED;
	RETURN_post_command_state( 0, Player.speed * 5 / 5 );
      }
    }


    if (missile->used) {
      missile->used = FALSE;
      item_use(missile);
    }

    set_spot_from_player(&x2,&y2);

    if ((x2 == Player.x) && (y2 == Player.y))
      {
	mprint("You practice juggling for a moment or two.");
	RETURN_post_command_state( 0, Player.speed * 5 / 5 );
      }

    {
      x1 = Player.x;
      y1 = Player.y;
      do_object_los(missile->objchar,&x1,&y1,x2,y2);
      m = get_monster(Level, x1, y1);
      if (m)
        {
	  if (missile->dmg == 0) {
	    if (m->treasure > 0) { /* the monster can have treasure/objects */
	      mprint("Your gift is caught!");
	      givemonster(m,split_item(1,missile));
	      dispose_lost_objects(1,missile);
	    }
	    else {
	      mprint("Your thrown offering is ignored.");
	      drop_1_missile_at( x1, y1, missile);
	    }
	  }
	  else if (missile->aux == I_SCYTHE) {
	    mprint("It isn't very aerodynamic... you miss.");
	    drop_1_missile_at( x1, y1, missile);
	  }
	  else if (hitp(Player.hit,m->ac)) {
	    /* ok already, hit the damn thing */
	    weapon_use(2*statmod(Player.str),missile,m);
	    /* Did it break? */
	    if ((missile->id == OB_ARROW || missile->id == OB_BOLT) &&
		!random_range(4))
	      { dispose_lost_objects(1,missile); }
	    else
	      { drop_1_missile_at( x1, y1, missile); }
	  }
	  else
	    {
	      mprint("You miss it.");
	      drop_1_missile_at( x1, y1, missile);
	    }
	  RETURN_post_command_state( 0, Player.speed * 5 / 5 );
	}
      else
	{
	  /*  Fired at a place, not a monster. */
	  drop_1_missile_at( x1, y1, missile);
	  plotspot(x1,y1,TRUE);
	  RETURN_post_command_state( 0, Player.speed * 5 / 5 );
	}
    }
  }
}


/* WDT: This function is used in interrupt handling, so it has the potential
 * to mess up the display.  Perhaps I should have a Curses window just for it,
 * so that I could clean up nicely if the user answers 'no' to its question.
 */
post_command_state_t cmd_quit(int dir, int first_time)
{
  clearmsg();
  change_to_game_perms();
  mprint("Quit: Are you sure? [yn] ");
  if (ynq() != 'y')
    { return base_state; }

  {
    if (Player.rank[ADEPT] == 0) { display_quit(); }
    else { display_bigwin(); }
    lev_term();
    endgraf();
    printf("Bye!\n");
    exit(0);
    /*NOTREACHED*/
  }
}



/* rest in 10 second segments so if woken up by monster won't
die automatically.... */
post_command_state_t cmd_nap(int dir, int first_time)
{
  static int naptime;

  if (first_time)
  {
    clearmsg();
    naptime = (int) parsenum("Rest for how long? (in minutes) ");
    if (naptime > 600)
      {
	print3("You can only sleep up to 10 hours (600 minutes)");
	naptime = 3600;
      }
    else
      { naptime *= 6; }

    if (naptime > 1) {
      clearmsg();
      mprint("Resting.... ");
      RETURN_post_command_state( naptime, 10 );
    }
    else
      { return base_state; }
  }

  if (naptime-- < 1)
    {
      clearmsg();
      mprint("Yawn. You wake up.");
      set_notice_flags(notice_vision);
      RETURN_post_command_state( 0, 10 );
    }

  /* Keep napping. */
  RETURN_post_command_state( 1, 10 );
}


post_command_state_t cmd_charid(int dir, int first_time)
{
  char id;
  int countryside=FALSE;
  char cstr[80];

  clearmsg();
  mprint("Character to identify: ");
  id = mgetc();
  if (Current_Environment == E_COUNTRYSIDE) {
    countryside = TRUE;
    strcpy(cstr,countryid(id));
    if (strcmp(cstr,"I have no idea.")==0)
      countryside = FALSE;
    else mprint(cstr);
  }
  if (! countryside) {
    if ((id >= 'a' && id <= 'z') || (id >= 'A' && id <= 'Z') || id == '@')
      mprint("A monster or NPC -- examine (x) to find out exactly.");
    else switch(id) {
    case (SPACE&0xff):
      mprint(" : An airless void (if seen) or unknown region (if unseen)");
      break;
    case (WALL&0xff):
      mprint(" : An (impenetrable?) wall");
      break;
    case (OPEN_DOOR&0xff):
      mprint(" : An open door");
      break;
    case (CLOSED_DOOR&0xff):
      mprint(" : A closed (possibly locked) door");
      break;
    case (LAVA&0xff):
      mprint(" : A pool of lava");
      break;
    case (HEDGE&0xff):
      mprint(" : A dense hedge");
      break;
    case (WATER&0xff):
      mprint(" : A deep body of water");
      break;
    case (FIRE&0xff):
      mprint(" : A curtain of fire");
      break;
    case (TRAP&0xff):
      mprint(" : An uncovered trap");
      break;
    case (STAIRS_UP&0xff):
      mprint(" : A stairway leading up");
      break;
    case (STAIRS_DOWN&0xff):
      mprint(" : A stairway leading down");
      break;
    case (FLOOR&0xff):
      mprint(" : The dungeon floor");
      break;
    case (PORTCULLIS&0xff):
      mprint(" : A heavy steel portcullis");
      break;
    case (ABYSS&0xff):
      mprint(" : An entrance to the infinite abyss");
      break;
    case (PLAYER&0xff):
      mprint(" : You, the player");
      break;
    case (CORPSE&0xff):
      mprint(" : The remains of some creature");
      break;
    case (THING&0xff):
      mprint(" : Some random miscellaneous object");
      break;
    case (SAFE&0xff):
      mprint(" : A steel safe inset into the floor");
      break;
    case (RUBBLE&0xff):
      mprint(" : A dangerous-looking pile of rubble");
      break;
    case (STATUE&0xff):
      mprint(" : A statue");
      break;
    case (ALTAR&0xff):
      mprint(" : A (un?)holy altar");
      break;
    case (CASH&0xff):
      mprint(" : Bills, specie, gems: cash");
      break;
    case (PILE&0xff):
      mprint(" : A pile of objects");
      break;
    case (FOOD&0xff):
      mprint(" : Something edible");
      break;
    case (WEAPON&0xff):
      mprint(" : Some kind of weapon");
      break;
    case (MISSILEWEAPON&0xff):
      mprint(" : Some kind of missile weapon");
      break;
    case (SCROLL&0xff):
      mprint(" : Something readable");
      break;
    case (POTION&0xff):
      mprint(" : Something drinkable");
      break;
    case (ARMOR&0xff):
      mprint(" : A suit of armor");
      break;
    case (SHIELD&0xff):
      mprint(" : A shield");
      break;
    case (CLOAK&0xff):
      mprint(" : A cloak");
      break;
    case (BOOTS&0xff):
      mprint(" : A pair of boots");
      break;
    case (STICK&0xff):
      mprint(" : A stick");
      break;
    case (RING&0xff):
      mprint(" : A ring");
      break;
    case (ARTIFACT&0xff):
      mprint(" : An artifact");
      break;
    case (CHAIR&0xff):
      mprint(" : A chair");
      break;
    case (WHIRLWIND&0xff):
      mprint(" : A whirlwind");
      break;
    default:
      mprint("That character is unused.");
      break;
    }
  }

  return base_state;
}

post_command_state_t cmd_wizard(int dir, int first_time)
{
  char *lname;
  struct passwd *dastuff;

  if (gamestatusp(CHEATED)) mprint("You're already in wizard mode!");
  else {
    clearmsg();
    if (cinema_confirm("You just asked to enter wizard mode.")=='y') {
      lname = getlogin();
      if (!lname || strlen(lname) == 0)
      {
	dastuff = getpwuid(getuid());
	lname = dastuff->pw_name;
      }
      if (strcmp(lname,WIZARD)==0 || strlen(WIZARD) == 0) {
	setgamestatus(CHEATED);
	mprint("Wizard mode set.");
      }
      else {
	mprint("There is a shrieking sound, as of reality being distorted.");
	strcpy(Str1,WIZARD);
	strcat(Str1,", the Wizard of Omega appears before you....");
	if (Str1[0] >= 'a' && Str1[0] <= 'z')
	    Str1[0] += 'A'-'a'; /* capitalise 1st letter */
	mprint(Str1);
	mprint("'Do not meddle in the affairs of Wizards --");
	if (random_range(2)) mprint("it makes them soggy and hard to light.'");
	else mprint("for they are subtle, and swift to anger!'");
      }
    }
  }

  return base_state;
}


/* Sets sequence of combat maneuvers. */
post_command_state_t cmd_tacoptions(int dir, int first_time)
{
  int actionsleft,done,place;
  char defatt, *attstr, *defstr; /* for the default setting */
  int draw_again = 1;

  done = FALSE;
  actionsleft = maneuvers();
  place = 0;
  do {
    if (draw_again) {
      menuclear();
      menuprint("Enter a combat maneuvers sequence.\n");
      menuprint("? for help, ! for default, backspace to start again,\n");
      menuprint(" RETURN to save sequence\n");
      showmenu();
      draw_again = 0;
    }
    clearmsg();
    mprint("Maneuvers Left:");
    mnumprint(actionsleft);
    switch(mgetc()) {
    case '?':
      combat_help();
      draw_again = 1;
      break;
    case 'a': case 'A':
      if (actionsleft < 1) print3("No more maneuvers!");
      else {
	if (Player.possessions[O_WEAPON_HAND] == NULL) {
	  Player.meleestr[place] = 'C';
	  menuprint("\nPunch:");
	}
	else if (Player.possessions[O_WEAPON_HAND]->type == THRUSTING) {
	  Player.meleestr[place] = 'T';
	  menuprint("\nThrust:");
	}
	else if (Player.possessions[O_WEAPON_HAND]->type == STRIKING) {
	  Player.meleestr[place] = 'C';
	  menuprint("\nStrike:");
	}
	else {
	  menuprint("\nCut:");
	  Player.meleestr[place] = 'C';
	}
	place++;
	Player.meleestr[place]=getlocation();
	place++;
	actionsleft--;
      }
      break;
    case 'b': case 'B':
      if (actionsleft<1) print3("No more maneuvers!");
      else {
	Player.meleestr[place] = 'B';
	if (Player.possessions[O_WEAPON_HAND] == NULL)
	  menuprint("\nDodge (from):");
	else if (Player.possessions[O_WEAPON_HAND]->type == THRUSTING)
	  menuprint("\nParry:");
	else menuprint("\nBlock:");
	place++;
	Player.meleestr[place]= getlocation();
	place++;
	actionsleft--;
      }
      break;
    case 'l': case 'L':
      if (actionsleft<2) print3("Not enough maneuvers to lunge!");
      else {
	if (Player.possessions[O_WEAPON_HAND] != NULL) {
	  if (Player.possessions[O_WEAPON_HAND]->type != MISSILE) {
	    menuprint("\nLunge:");
	    Player.meleestr[place] = 'L';
	    place++;
	    Player.meleestr[place]=getlocation();
	    place++;
	    actionsleft -= 2;
	  }
	  else {
	    print3("Can't lunge with a missile weapon!");
	    morewait();
	  }
	}
	else {
	  print3("Can't lunge without a weapon!");
	  morewait();
	}
      }
      break;
    case 'r': case 'R':
      if (actionsleft<2) print3("Not enough maneuvers to riposte!");
      else {
	if (Player.possessions[O_WEAPON_HAND] != NULL) {
	  if (Player.possessions[O_WEAPON_HAND]->type == THRUSTING) {
	    Player.meleestr[place++] = 'R';
	    menuprint("\nRiposte:");
	    Player.meleestr[place++]=getlocation();
	    actionsleft -= 2;
	  }
	  else {
	    print3("Can't riposte without a thrusting weapon!");
	    morewait();
	  }
	}
	else {
	  print3("Can't riposte without a thrusting weapon!");
	  morewait();
	}
      }
      break;
    case BACKSPACE:
    case DELETE:
      place = 0;
      actionsleft=maneuvers();
      draw_again = 1;
      break;
    case '!':
      if (Player.possessions[O_WEAPON_HAND] == NULL) {
	defatt = 'C';
	attstr = "Punch";
      }
      else if (Player.possessions[O_WEAPON_HAND]->type == THRUSTING) {
	defatt = 'T';
	attstr = "Thrust";
      }
      else if (Player.possessions[O_WEAPON_HAND]->type == STRIKING) {
	defatt = 'C';
	attstr = "Strike";
      }
      else {
	defatt = 'C';
	attstr = "Cut";
      }
      if (Player.possessions[O_WEAPON_HAND] == NULL)
	defstr = "Dodge";
      else if (Player.possessions[O_WEAPON_HAND]->type == THRUSTING)
	defstr = "Parry";
      else defstr = "Block";
      menuclear();
      menuprint("Enter a combat maneuvers sequence.\n");
      menuprint("? for help, ! for default, backspace to start again,\n");
      menuprint(" RETURN to save sequence\n\n");
      for(place=0;place<maneuvers();place++)
	if (place&1) { /* every 2nd time around */
	    Player.meleestr[place*2] = 'B';
	    Player.meleestr[(place*2)+1] = 'C';
	    menuprint(defstr);
	    menuprint(" Center.\n");
	}
	else {
	    Player.meleestr[place*2] = defatt;
	    Player.meleestr[(place*2)+1] = 'C';
	    menuprint(attstr);
	    menuprint(" Center.\n");
	}
      actionsleft = 0;
      showmenu();
      Player.meleestr[place*2]='\0';
      break;
    case RETURN:
    case LINEFEED:
    case ESCAPE:
      done = TRUE;
      break;
    }
/*    if (actionsleft < 1) morewait(); */ /* FIXED 12/30/98 */
  } while (! done);
  Player.meleestr[place] = 0;

  set_notice_flags(notice_drew_menu);
  return base_state;
}

/* Do the Artful Dodger trick */
post_command_state_t cmd_pickpocket(int dir, int first_time)
{
  int dx,dy,index=0;

  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  clearmsg();

  mprint("Pickpocketing --");

  index = getdir();

  if (index == ABORT)
    { return base_state; }

    {
      dx = Dirs[0][index];
      dy = Dirs[1][index];


      if (!inbounds(Player.x + dx, Player.y + dy)
          || !get_monster(Level, Player.x + dx, Player.y + dy))
        {
          print3("There's nothing there to steal from!!!");
	  return base_state;
        }

        {
	  struct monster *m;
          m = get_monster(Level, Player.x + dx, Player.y + dy);
          if (m->id == GUARD)
            {
              mprint("Trying to steal from a guardsman, eh?");
              mprint("Not a clever idea.");
              if (Player.cash > 0)
                {
                  mprint("As a punitive fine, the guard takes all your money.");
                  {
                    pob o = detach_money(Player.cash);
                    m_pickup(m,o);
                  }
		  set_notice_flags(notice_py_data);
                }
              else
                {
                  mprint("The guardsman places you under arrest.");
                  morewait();
                  send_to_jail();
                }
            }
          else if (0 == list_size(m->possession_list))
            {
              mprint("You couldn't find anything worth taking!");
              mprint("But you managed to annoy it...");
              m_status_set(m,HOSTILE);
            }
          else
            {
              /* DAG -- code and idea contributed by Ross Presser <ross_presser@imtek.com> */
              int i,j,k;
              i = Player.dex*5 + Player.rank[THIEVES]*20 + random_range(100);
              j = random_range(100) + m->level*20;
              k = random_range(j);

              if (i > j)
                {
                  object * obj;
                  obj = m_remove_random_item(m);

                  /* update player state */
                  Player.alignment--;
                  gain_experience(m->level * m->level);
                  mprint("You successfully complete your crime!");
                  mprint("You stole:");
                  mprint(itemid(obj));
                  gain_item(obj);
                }
              else if (i<k)
                {
                  mprint("No luck ... yet.");
                }
              else
                {
                  mprint("You were unsucessful at your crime.");
                  mprint("Even worse, you managed to annoy it...");
                  m_status_set(m,HOSTILE);
                }
            }
	  RETURN_post_command_state( 0,  Player.speed * 20 / 5 );
        }
    }
}


post_command_state_t cmd_rename_player(int dir, int first_time)
{
  clearmsg();
  mprint("Rename Character: ");
  strcpy(Str1,msgscanstring());
  if (strlen(Str1) == 0)
    mprint(Player.name);
  else {
    if (Str1[0] >= 'a' && Str1[0] <= 'z')
      Str1[0] += 'A' - 'a';
    strcpy(Player.name, Str1);
  }
  sprintf(Str1, "Henceforth, you shall be known as %s", Player.name);
  print2(Str1);

  return base_state;
}


post_command_state_t cmd_abortshadowform(int dir, int first_time)
{
  if (Player.status[SHADOWFORM] && (Player.status[SHADOWFORM]<1000)) {
    mprint("You abort your spell of Shadow Form.");
    Player.immunity[NORMAL_DAMAGE]--;
    Player.immunity[ACID]--;
    Player.immunity[THEFT]--;
    Player.immunity[INFECTION]--;
    mprint("You feel less shadowy now.");
    Player.status[SHADOWFORM] = 0;
  }

  return base_state;
}

post_command_state_t cmd_tunnel(int dir, int first_time)
{
  int dindex,ox,oy,aux;

  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  clearmsg();
  mprint("Tunnel -- ");
  dindex = getdir();
  if (dindex == ABORT)
    { return base_state; }

  {
    ox = Player.x + Dirs[0][dindex];
    oy = Player.y + Dirs[1][dindex];
    if (loc_statusp(ox,oy,SECRET))
      mprint("You have no success as yet.");
    else if (Level->site[ox][oy].locchar != WALL)
      {
	print3("You can't tunnel through that!");
	return base_state;
      }
    else {
      aux = Level->site[ox][oy].aux;
      if (random_range(20)==1){
	if (Player.possessions[O_WEAPON_HAND] == NULL) {
	  mprint("Ouch! broke a fingernail...");
	  p_damage(Player.str / 6,UNSTOPPABLE,"a broken fingernail");
	}
	else if ((Player.possessions[O_WEAPON_HAND]->type == THRUSTING) ||
		 ((Player.possessions[O_WEAPON_HAND]->type != STRIKING) &&
		  (Player.possessions[O_WEAPON_HAND]->fragility <
		   random_range(20)))) {
	  mprint("Clang! Uh oh...");
	  (void) damage_item(Player.possessions[O_WEAPON_HAND]);
	}
	else mprint("Your digging implement shows no sign of breaking.");
      }
      if (Player.possessions[O_WEAPON_HAND] == NULL) {
	if ((aux > 0) && ((Player.str/3)+random_range(100) > aux)) {
	  mprint("You carve a tunnel through the stone!");
	  tunnelcheck();
	  Level->site[ox][oy].locchar = RUBBLE;
	  Level->site[ox][oy].p_locf = L_RUBBLE;
	  lset(ox, oy, CHANGED);
	}
	else mprint("No joy.");
      }
      else if (Player.possessions[O_WEAPON_HAND]->type == THRUSTING) {
	if ((aux > 0) &&
	    (Player.possessions[O_WEAPON_HAND]->dmg*2+random_range(100) >
	     aux)) {
	  mprint("You carve a tunnel through the stone!");
	  tunnelcheck();
	  Level->site[ox][oy].locchar = RUBBLE;
	  Level->site[ox][oy].p_locf = L_RUBBLE;
	  lset(ox, oy, CHANGED);
	}
	else mprint("No luck.");
      }
      else if ((aux > 0) &&
	       (Player.possessions[O_WEAPON_HAND]->dmg+random_range(100)
		> aux)) {
	mprint("You carve a tunnel through the stone!");
	tunnelcheck();
	Level->site[ox][oy].locchar = RUBBLE;
	Level->site[ox][oy].p_locf = L_RUBBLE;
	lset(ox, oy, CHANGED);
      }
      else mprint("You have no success as yet.");
    }
    /*  This could trivially be made to repeat,  */
    RETURN_post_command_state( 0,   Player.speed * 30 / 5 );
  }
}

/*  Perhaps this should use terrain data, not symbol?  */
post_command_state_t cmd_hunt(int dir, int first_time)
{
  int date;
  int fertility = 0;
  long duration = 0;
  Symbol terrain_sym;

  if (Current_Environment != E_COUNTRYSIDE)
    { return base_state; }

  terrain_sym = Country[Player.x][Player.y].current_terrain_type;
  switch (terrain_sym)
    {
    case SWAMP:
      mprint("You hesitate to hunt for food in the marshy wasteland.");
      break;

  case VOLCANO: case CASTLE: case TEMPLE: case CAVES: case STARPEAK: case MAGIC_ISLE: case DRAGONLAIR:
    mprint("There is nothing alive here (or so it seems)");
    break;

  case VILLAGE: case CITY:
    mprint("You can find no food here; perhaps if you went inside....");
    break;

  case ROAD:
    mprint("You feel it would be a better idea to hunt off the road.");
    break;

  case CHAOS_SEA:
    mprint("Food in the Sea of Chaos? Go on!");
    break;

  case DESERT:
    mprint("You wander off into the trackless desert in search of food...");
    fertility = 10;
    break;

  case PALACE: case JUNGLE:
    mprint("You search the lush and verdant jungle for game....");
    fertility = 80;
    break;

  case PLAINS:
    mprint("You set off through the tall grass; the game is afoot.");
    fertility = 50;
    break;

  case TUNDRA:
    mprint("You blaze a trail through the frozen wasteland....");
    fertility = 30;
    break;

  case FOREST:
    mprint("You try to follow the many tracks through the forest loam....");
    fertility = 70;
    break;

  case MOUNTAINS: case PASS:
    mprint("You search the cliff walls looking for something to eat....");
    fertility = 30;
    break;

  case RIVER:
    mprint("The halcyon river is your hopeful food source...");
    fertility = 80;
    break;
  }

  /*  If it's clearly pointless, no time taken. */
  if(fertility <= 0)
    { return base_state; }

  duration = 100 * 60 * TICKS_PER_SECOND;


  date = 30 * time_month() + time_day();
  if ((date <= 75 || date >= 330)
      && !(DESERT == terrain_sym)
      && !(JUNGLE == terrain_sym))
    {
      mprint("The cold weather impedes your hunt....");
      fertility = fertility / 2;
    }

  if (fertility > random_range(100))
    {
      mprint("You have an encounter...");
      change_environment_push(E_TACTICAL_MAP, terrain_sym );
      /* Now that monsters only "start" when player does, hunting
	 can take normal time. */
      RETURN_post_command_state( 0, random_range(duration) );
    }
  else
    {
      mprint("Your hunt is fruitless.");
      RETURN_post_command_state( 0, duration );
    }
}

post_command_state_t cmd_dismount_steed(int dir, int first_time)
{
  if (!pflagp(MOUNTED))
    {
      print3("You're on foot already!");
      return base_state;
    }

  if (Current_Environment == E_COUNTRYSIDE)
    {
      if ('y' == cinema_confirm("If you leave your steed here he will wander away!"))
	{
	  resetpflag(MOUNTED);
	  set_notice_flags(notice_equip);
	  RETURN_post_command_state( 0,  Player.speed * 10 / 5 );
	}
      else
	{ return base_state; }
    }

  if(Level->site[Player.x][Player.y].creature)
    {
      mprint("You can't dismount here.");
      return base_state;
    }

  {
    monster * mon = make_site_monster(Player.x, Player.y, HORSE);
    mon->status = MOBILE + SWIMMING;
    resetpflag(MOUNTED);
    set_notice_flags( notice_x_mounted );
    RETURN_post_command_state( 0,  Player.speed * 10 / 5 );
  }
}

post_command_state_t cmd_frobgamestatus(int dir, int first_time)
{
  char response;
  long num;

  if (!gamestatusp(CHEATED))
    { return base_state; }

  mprint("Set or Reset or Forget it [s,r,ESCAPE]:");
  do
    { response = (char) mcigetc(); }
  while ((response != 'r') && (response != 's') && (response != ESCAPE));

  if (response != ESCAPE) {
    num = (int) parsenum("Enter log2 of flag:");
    if (num > -1) {
      num = pow2(num);
      if (num == CHEATED) {
	mprint("Can't alter Wizard flag.");
	return base_state;
      }
      if (response == 's') setgamestatus(num);
      else resetgamestatus(num);
      mprint("Done....");
    }
  }
  return base_state;
}
