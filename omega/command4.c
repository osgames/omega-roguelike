/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/*  *** Miscellaneous player commands including all pseudo-commands ***  */

#include "glob.h"
#include "memory.h"
#include "command.h"
#include "pick.h"



post_command_state_t cmd_no_op(int dir, int first_time)
{ return base_state; }


/* This "magic" value is used to indicate the player tried to abort a
   key-lookup command.  */
post_command_state_t cmd_escape(int dir, int first_time)
{ return base_state; }

/*  "Magic" value that indicates the command has more keys coming.  */
post_command_state_t cmd_keymap(int dir, int first_time)
{ return base_state; }


post_command_state_t cmd_print_allocation_info(int dir, int first_time)
{
#ifdef USE_OPCURSES
  print_allocation_info();
#endif
  return base_state;
}

post_command_state_t cmd_player_dump(int dir, int first_time)
{
  player_dump();
  return base_state;
}


post_command_state_t cmd_display_pack(int dir, int first_time)
{
  display_pack();
  morewait();
  set_notice_flags(notice_drew_menu);
  return base_state;
}


post_command_state_t cmd_xredraw(int dir, int first_time)
{
  xredraw();
  return base_state;
}

post_command_state_t cmd_bufferprint(int dir, int first_time)
{
  bufferprint();
  return base_state;
}

post_command_state_t cmd_redraw(int dir, int first_time)
{
  redraw();
  return base_state;
}

post_command_state_t cmd_wizard_draw(int dir, int first_time)
{
  if (gamestatusp(CHEATED))
    { drawscreen(); }
  return base_state;
}

/* Handle wizard-wishing (Now different than adept wishing). */
typedef struct string_to_effect_t
{
  char * name;
  void (*func)(void);
} string_to_effect_t;


/* The wizard-wish effect functions: */
static void wiz_destruction(void) { annihilate(TRUE); }
static void wiz_acquisition(void) { acquire(TRUE); }
static void wiz_summoning  (void) { summon(TRUE, RANDOM); }
static void wiz_stats      (void)
{
  Player.str = Player.maxstr = Player.con = Player.maxcon =
    Player.agi = Player.maxagi = Player.dex = Player.maxdex =
    Player.iq = Player.maxiq = Player.pow = Player.maxpow = 200;
  set_notice_flags(notice_py_data);
}
static void wiz_death      (void) { p_death("a deathwish"); }
static void wiz_power      (void) { Player.mana=calcmana()*10; }
static void wiz_skill      (void) { gain_experience(25000); }
static void wiz_wealth     (void)
{
  print2("You are showered with gold pieces");
  Player.cash += 1000000;
  dataprint();
}
static void wiz_balance    (void) { Player.alignment = 0;   }
static void wiz_chaos      (void) { Player.alignment -= 25; }
static void wiz_law        (void) { Player.alignment += 25; }
static void wiz_location   (void) { strategic_teleport(1); }
static void wiz_knowledge  (void)
{
  int i;
  for (i = 0; i < NUMSPELLS; ++i)
    {
      Spells[i].known = TRUE;
      Spells[i].powerdrain = 1;
    }
}
static void wiz_health     (void)
{
  print2("You feel vigorous");
  Player.hp = max( Player.hp, Player.maxhp);
  Player.status[DISEASED] = 0;
  Player.status[POISONED] = 0;
  Player.food = 43;
  /* Wish for Health when starving does some good. PGM */
}
/* Since wizard-wishes block non-wizard wishes, `wiz_wish' lets
   wizards test that command. */
static void wiz_wish       (void) { wish(1); }
static void wiz_drawscreen (void) { drawscreen(); }
static void wiz_editstats  (void) { editstats(); }


/* Array, presorted in alphabetical order. */
static string_to_effect_t wizard_wishes[] =
{
  { "Acquisition", wiz_acquisition, },
  { "Balance",     wiz_balance,     },
  { "Chaos",       wiz_chaos,       },
  { "Death",       wiz_death,       },
  { "Destruction", wiz_destruction, },
  { "Edit stats",  wiz_editstats,   },
  { "Health",      wiz_health,      },
  { "Knowledge",   wiz_knowledge,   },
  { "Law",         wiz_law,         },
  { "Location",    wiz_location,    },
  { "Map level",   wiz_drawscreen,  },
  { "Power",       wiz_power,       },
  { "Skill",       wiz_skill,       },
  { "Stats",       wiz_stats,       },
  { "Summoning",   wiz_summoning,   },
  { "Wealth",      wiz_wealth,      },
  { "Wish",        wiz_wish,        },
};

bool wwish_test(int index) { return TRUE; }

char * wwish_name(int index)
{ return wizard_wishes[index].name; }

enum
{
  last_wizard_wish =
  sizeof(wizard_wishes)/sizeof(string_to_effect_t) - 1,
};



/* Replaces the old cmd_wish. */
post_command_state_t cmd_wish(int dir, int first_time)
{
  if (gamestatusp(CHEATED))
    {
      int index =
	arraypick("Wizard command: ", "", wwish_test,
		  wwish_name, NULL, "wizard commands",
		  last_wizard_wish);
      if(index >= 0)
	{ wizard_wishes[index].func(); }
      return base_state;
    }
  else
    {
      if(Player.rank[ADEPT])
	{ wish(1); }
      RETURN_post_command_state( 0, 5 );
    }
}

post_command_state_t cmd_do_inventory_control(int dir, int first_time)
{
  RETURN_post_command_state( 0, do_inventory_control() );
}

post_command_state_t cmd_inventory_control(int dir, int first_time)
{
  int duration;
  if (!optionp(TOPINV))
    { duration = top_inventory_control(); }
  else
    {
      /* Do we need menuclear here?  Only country had it.  */
      display_possessions();
      duration = inventory_control();
    }
  RETURN_post_command_state( 0, duration );
}

post_command_state_t cmd_show_license(int dir, int first_time)
{
  show_license();
  return base_state;
}


post_command_state_t cmd_check_memory(int dir, int first_time)
{
  return base_state;
}

post_command_state_t cmd_editstats(int dir, int first_time)
{
  if (gamestatusp(CHEATED))
    { editstats(); }
  return base_state;
}

post_command_state_t cmd_countrysearch(int dir, int first_time)
{
  if (Current_Environment != E_COUNTRYSIDE)
    { return base_state; }
  {
    long int duration = countrysearch();
    RETURN_post_command_state( 0, duration );
  }
}

post_command_state_t cmd_enter_site(int dir, int first_time)
{
  if (Current_Environment != E_COUNTRYSIDE)
    { return base_state; }
  enter_site(Country[Player.x][Player.y].base_terrain_type);
  return base_state;
}

/*  Pseudo-command, can't be activated deliberately. */
post_command_state_t cmd_goberserk(int dir, int first_time)
{
  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  {
    int went_berserk = goberserk();
    if(went_berserk)
      {
	set_notice_flags(notice_vision);
	RETURN_post_command_state(0, Player.speed * 5 / 5);
      }
  else
    {
      return base_state;
    }
  }
}

/* New command to help generate key-tables.  To support extended
    keycodes, we take an int, not a char.  This is not yet supported
    in other code.
*/
post_command_state_t cmd_show_key(int dir, int first_time)
{
  int i;
  print1("Next 4 key values: " );
  for(i = 0; i < 4; i++)
  {
    int key = mgetkey();
    mnumprint( key );
    nprint1(", " );
  }
  return base_state;
}
