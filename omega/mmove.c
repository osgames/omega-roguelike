/* omega copyright (c) 1987,1988,1989 by Laurence Raphael Brothers */
/* mmove.c */
/* monster move functions */

#include "glob.h"
#include "lev.h"
#include "liboutil/list.h"
#include "util.h"
#include "findloc.h"
#include "limits.h"

/* can monsters move onto a spot */
static int can_move_monster (monster_t * m, int x, int y)
{
  location_t * site;

  /* site isn't in C array */
  if (inbounds(lev, x, y) == FALSE) return FALSE;

  site = &(m->dlevel->site[x][y]);

  /* site is occupied */
  if (site->player != NULL) return FALSE;
  if (site->monster != NULL) return FALSE;

  /* site is bad for all (even intangible) monsters */
  if (site->locchar == SPACE || site->locchar == VOID_CHAR) return FALSE;

  /* special case monsters */
  if (m_statusp(m, ONLYSWIM)) return (site->locchar == WATER);
  if (m_statusp(m, INTANGIBLE)) return TRUE;

  /* always ok for everybody else */
  if (site->locchar == LIFT) return TRUE;
  if (site->locchar == SAFE) return TRUE;
  if (site->locchar == ALTAR) return TRUE;
  if (site->locchar == CHAIR) return TRUE;
  if (site->locchar == FLOOR) return TRUE;
  if (site->locchar == OPEN_DOOR) return TRUE;
  if (site->locchar == STAIRS_UP) return TRUE;
  if (site->locchar == STAIRS_DOWN) return TRUE;

  /* always not ok for everybody else */
  if (site->locchar == WALL) return FALSE;
  if (site->locchar == STATUE) return FALSE; /* why can't a monster bash this? */
  if (site->locchar == PORTCULLIS) return FALSE; /* why can't a monster bash/lift this? */

  /* the world of CLOSED_DOOR possibilities*/
  if (site->locchar == CLOSED_DOOR)
    {
      if (ploc_status(site, SECRET) == TRUE)
	{
	  if (m->movef != M_MOVE_SMART) return FALSE;

	  /* Is this function ever called (and thus the messages below printed) when the */
	  /* monster isn't actually going to move on this spot, but perhaps just wants to */
	  /* see if it could? If so, then this printing should go somewhere else */

	  if (is_monster_visible(p,m))
	    {
	      mprint("You see a secret door swing open!");
	      ploc_reset(site, SECRET);
	      ploc_set(site, CHANGED);
	    }
	  else
	    {
	      mprint("You hear a door creak open, and then close again.");
	      /* smart monsters would close secret doors behind them if the */
	      /* player didn't see them using it */
	    }

	  return TRUE;
	}
      else
	{
	  if (m->movef == M_MOVE_SMART)
	    {
	      mprint("You hear a door creak open.");
	      site->locchar = OPEN_DOOR;
	      ploc_set(site, CHANGED);
	      return TRUE;
	    }

	  /* Why should a monster always try to break down a door? */
	  if (random_range(m->dmg) > random_range(100))
	    {
	      mprint("You hear a door shattering.");
	      site->locchar = RUBBLE;
	      ploc_set(site, CHANGED);
	      return TRUE;
	    }

	  return FALSE;
	}
    }

  /* from now on, it depends on the monster */
  if (site->locchar == FIRE) return m_immunityp(m, FLAME);
  if (site->locchar == HEDGE) return m_statusp(m, FLYING);
  if (site->locchar == WHIRLWIND) return m_immunityp(m, ELECTRICITY);

  if (site->locchar == LAVA)
    {
      if (m_statusp(m, FLYING)) return TRUE; /* it might get hot, though... */
      if (m_statusp(m, SWIMMING) && m_immunityp(m, FLAME)) return TRUE;
      return FALSE;
    }

  if (site->locchar == WATER)
    {
      if (m_statusp(m, FLYING)) return TRUE;
      if (m_statusp(m, SWIMMING)) return TRUE;
      if (m->movef == M_MOVE_CONFUSED) return TRUE; /* oops! */
    }

  if (site->locchar == TRAP || site->locchar == ABYSS || site->locchar == RUBBLE)
    {
      if (m_statusp(m, FLYING)) return TRUE;
      if (m->movef == M_MOVE_CONFUSED) return TRUE; /* oops! */
      return FALSE;
    }

  /* deny by default */
  return FALSE;
}

void m_random_move (monster_t * m)
{
  int idx;
  int count = 0;
  int ok_directions[8];

  for (idx = 0; idx < 8; ++idx)
    if (can_move_monster(m, m->x + direction[idx].x, m->y + direction[idx].y))
      ok_directions[count++] = idx;

  if (count == 0) return; /* nowhere to move */

  idx = ok_directions[random_range(count)];

  erase_monster(m);
  move_monster(m, m->x + direction[idx].x, m->y + direction[idx].y);
}

/* used by both m_normal_move and m_smart_move */
static void m_simple_move_aux (monster_t * m, player_t * p, int scaredy)
{
  int dx, dy;

  dx = sign(p->x - m->x);
  dy = sign(p->y - m->y);

  if (scaredy == TRUE)
    {
      dx = -dx;
      dy = -dy;
    }

  if (m_statusp(m, NEEDY) == FALSE) goto random_move;
  if (m_statusp(m, HOSTILE) == FALSE) goto random_move;

  if (p->status[INVISIBLE] > 0)
    if (m_statusp(m, SEE_INVIS) == FALSE)
      goto random_move;

  erase_monster(m);

  if (can_move_monster(m, m->x+dx, m->y+dy))
    {
      move_monster(m, m->x+dx, m->y+dy);
    }
  else if (dx == 0 && can_move_monster(m, m->x+1, m->y+dy))
    {
      move_monster(m, m->x+1, m->y+dy);
    }
  else if (dx == 0 && can_move_monster(m, m->x-1, m->y+dy))
    {
      move_monster(m, m->x-1, m->y+dy);
    }
  else if (dy == 0 && can_move_monster(m, m->x+dx, m->y+1))
    {
      move_monster(m, m->x+dx, m->y+1);
    }
  else if (dy == 0 && can_move_monster(m, m->x+dx, m->y-1))
    {
      move_monster(m, m->x+dx, m->y-1);
    }
  else if (can_move_monster(m, m->x+dx, m->y))
    {
      move_monster(m, m->x+dx, m->y);
    }
  else if (can_move_monster(m, m->x, m->y+dy))
    {
      move_monster(m, m->x, m->y+dy);
    }

 random_move:
  m_random_move(m);
  return;
}

void m_simple_move (monster_t * m, player_t * p)
{
  int scaredy = FALSE;

  if (m->hp < Monsters[m->id].hp / 4)
    {
      scaredy = TRUE;
      m->movef = M_MOVE_SCAREDY;

      if (COMMON == m->uniqueness)
	sprintf(Str2, "The %s", m->monstring);
      else
        strcpy(Str2, m->monstring);

      if (list_size(m->possession_list) > 0)
        {
          strcat(Str2, " drops its treasure and flees!");
          m_dropstuff(m);
        }
      else
        strcat(Str2, " flees!");

      mprint(Str2);
      m->speed = min(2, m->speed - 1);
    }

  m_simple_move_aux(m, p, scaredy);
}

/* like m_normal_move, but can open doors */
void m_smart_move (monster_t * m, player_t * p)
{
  m_simple_move(m,p);
}

/* not very smart, but not altogether stupid movement */
void m_normal_move (monster_t * m, player_t * p)
{
  m_simple_move(m,p);
}

void m_move_animal (monster_t * m, player_t * p)
{
  if (m_statusp(m, HOSTILE) == TRUE)
    m_simple_move_aux(m, p, FALSE);
  else
    m_simple_move_aux(m, p, TRUE);
}

/* same as simple move except run in opposite direction */
void m_scaredy_move (monster_t * m, player_t * p)
{
  m_simple_move_aux(m, p, TRUE);
}

/* for spirits (and earth creatures) who can ignore blockages because
   either they are noncorporeal or they can move through stone */
void m_spirit_move (monster_t * m, player_t * p)
{
  /* m_simple_move already allows INTANGIBLE monsters to ignore 'blockages' */
  m_simple_move(m,p);
}
  
/* fluttery dumb movement */
void m_flutter_move (monster_t * m, player_t * p)
{
  int idx;
  int mx, my, mrange;
  int tx, ty, trange;

  if (p->status[INVISIBLE] > 0)
    if (m_statusp(m, SEE_INVIS) == FALSE)
      {
	m_random_move(m,p);
	return;
      }

  mx = m->x;
  my = m->y;

  mrange = distance(m->x, m->y, p->x, p->y);

  for (idx = 0; idx < 8; ++idx)
    {
      tx = m->x + direction[idx].x;
      ty = m->y + direction[idx].y;

      trange = distance(tx, ty, p->x, p->y);

      if (m->hp < (Monsters[m->id].hp / 4))
	{
	  if (trange > mrange && can_move_monster(m, tx, ty))
	    {
	      mrange = trange;
	      mx = tx;
	      my = ty;
	    }
	}
      else if (trange <= mrange && can_move_monster(m, tx, ty))
	{
	  mrange = trange;
	  mx = tx;
	  my = ty;
	}
    }

  if (m->x != mx || m->y != my)
    {
      erase_monster(m);
      move_monster(m, mx, my);
    }
}

void m_follow_move (monster_t * m, player_t * p)
{
  if (m_statusp(m,HOSTILE) == FALSE)
    m_normal_move(m,p);
  else
    m_simple_move_aux(m, p, TRUE);
}

/* allows monsters to fall into pools, revealed traps, etc */
void m_confused_move (monster_t * m, player_t * p)
{
  m_random_move(m,p);
}

void m_move_leash (monster_t * m, player_t * p)
{
  int mx, my;
  int set_chain = FALSE;
  monster_t * other;

  if (m->aux1 == 0)
    {
      m->aux1 = m->x;
      m->aux2 = m->y;
      m_simple_move(m,p);
      return;
    }

  m_simple_move(m,p);

  if (distance(m->x, m->y, m->aux1, m->aux2) <= 5) return;

  other = get_monster(m->dlevel, m->aux1, m->aux2);

  if (other != NULL && other->movef == M_MOVE_SMART)
    {
      if (is_monster_visible(p, other))
	{
	  /* some other monster is where the chain starts */
	  if (COMMON == other->uniqueness)
	    sprintf(Str1, "The %s releases the dog's chain!", other->monstring);
	  else
	    sprintf(Str1, "%s releases the dog's chain!", other->monstring);

	  mprint(Str1);
	}

      m->movef = M_MOVE_NORMAL;
      return;
    }

  mx = m->aux1;
  my = m->aux2;
  m->aux1 = 0;
  m->aux2 = 0;

  /* start of chain is blocked - jerk back close to start of chain */
  if (other != NULL)
    {
      int idx;
      int best = 0;
      int bdist = INT_MAX;

      for (idx = 0; idx < 8; ++idx)
	if (can_monster_move(m, mx + direction[idx].x, my + direction[idx].y))
	  {
	    int dist;
	    dist = distance(m->x, m->y, mx + direction[idx].x, my + direction[idx].y);

	    if (dist < bdist)
	      {
		best = idx;
		bdist = dist;
	      }
	  }

      if (bdist == INT_MAX)
	{
	  /* can't move anywhere - break chain */
	  if (is_monster_visible(p,m))
	    {
	      if (COMMON == other->uniqueness)
		sprintf(Str1, "The %s breaks its chain!", other->monstring);
	      else
		sprintf(Str1, "%s breaks its chain!", other->monstring);

	      mprint(Str1);
	    }

	  m->movef = M_MOVE_NORMAL;
	  return;
	}

      mx += direction[idx].x;
      my += direction[idx].y;
    }

  if (is_monster_visible(p,m))
    {
      if (COMMON == other->uniqueness)
	sprintf(Str1, "The %s was jerked back by its chain!", m->monstring);
      else
	sprintf(Str1, "%s was jerked back by its chain!", m->monstring);

      mprint(Str1);
      erase_monster(m);
    }
  else
    mprint("You hear a strangled sort of yelp!");

  move_monster(m, mx, my);
}

/* monster removed from play */
void m_vanish (monster_t * m)
{
  if (COMMON == m->uniqueness)
    sprintf(Str2, "The %s vanishes in the twinkling of an eye!", m->monstring);
  else
    sprintf(Str2, "%s vanishes in the twinkling of an eye!", m->monstring);

  mprint(Str2);
  m_remove(m);
}

/* monster still in play */
void m_teleport (monster_t * m)
{
  int nx, ny;

  if (m_statusp(m, AWAKE) == FALSE) return;

  erase_monster(m);
  find_floorspace(m->dlevel, &nx, &ny);
  move_monster(m, nx, ny);
}

/* end */
