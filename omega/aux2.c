/* omega copyright (C) by Laurence Raphael Brothers, 1987,1988,1989 */
/* aux2.c */
/* some functions called by ocom.c, also see aux1.c and aux3.c*/ 
/* This is a real grab bag file. It contains functions used by
   aux1.c and omega.c, as well as elsewhere. It is mainly here so aux1.c
   and aux3.c are not huge */

#include "game_time.h"
#include "glob.h"
#include "lev.h"
#include "liboutil/list.h"
#include "ticket.h"
#include "util.h"
#include "command.h" /* For cmd_drop, cmd_bash */

/* Player stats like str, agi, etc give modifications to various abilities
   chances to do things, etc. Positive is good, negative bad. */
int statmod (int stat)
{
  return (stat - 10) / 2;
}

/* effects of hitting */
void p_hit (ppl p, pmt m, int dmg, int dtype)
{
  int dmult = 0;

  /* chance for critical hit..., 3/10 */
  switch (random_range(10))
    {  
    case 0:
      if (random_range(100) < (p->level + p->rank[MONKS]))
	{
	  strcpy(Str3, "You annihilate ");
	  dmult = 1000;
	}
      else
	{
	  strcpy(Str3, "You blast "); 
	  dmult = 5;
	}
      break;

    case 1:
    case 2: 
      strcpy(Str3, "You smash "); 
      dmult = 2;
      break;

    default: 
      dmult = 1;
      if (random_range(10))
	strcpy(Str3,"You hit ");
      else
	switch (random_range(12))
	  {
	  case 0:  strcpy(Str3, "You damage ");                 break;
	  case 1:  strcpy(Str3, "You inflict bodily harm on "); break;
	  case 2:  strcpy(Str3, "You injure ");                 break;
	  case 3:  strcpy(Str3, "You molest ");                 break;
	  case 4:  strcpy(Str3, "You tweak ");                  break;
	  case 5:  strcpy(Str3, "You smush ");                  break;
	  case 6:  strcpy(Str3, "You smurf ");                  break;
	  case 7:  strcpy(Str3, "You grind ");                  break;
	  case 8:  strcpy(Str3, "You hurt ");                   break;
	  case 9:  strcpy(Str3, "You bring pain to ");          break;
	  case 10: strcpy(Str3, "You recite nasty poetry at "); break;
	  case 11: strcpy(Str3, "You smack ");                  break;
	  }
      break;
    }

  dmult += (get_lunarity(p) * dmult / 2);

  if (m->uniqueness == COMMON)
    strcat(Str3, "the ");

  strcat(Str3, m->monstring);
  strcat(Str3, ".");

  if (Verbosity != TERSE)
    mprint(Str3);
  else
    mprint("You hit it.");

#if 0 /* I find this impossible to justify */
  if (p->rank[MONKS] > MONK_MASTER_SIGHS)
    {
      if (p->possessions[O_WEAPON_HAND] == NULL) /* barehanded */
	{
	  /* high level monks do unstoppable hand damage */
	  dtype = UNSTOPPABLE;
	}
    }
#endif

  m_damage(m, dmult * random_range(dmg), dtype);

  if (Verbosity != TERSE && random_range(10) == 3 && m->hp > 0)
    mprint("It laughs at the injury and fights on!");
}

/* try to drop a weapon (from fumbling) */
void drop_weapon (ppl p)
{
  if (p->possessions[O_WEAPON_HAND] == NULL)
    {
      mprint("You feel fortunate.");
      return;
    }

  sprintf(Str1, "You dropped your %s", p->possessions[O_WEAPON_HAND]->objstr);
  mprint(Str1);
  morewait();

  p_drop_at(p, p->x, p->y, p->possessions[O_WEAPON_HAND]);
  conform_lost_object(p->possessions[O_WEAPON_HAND]);
}

/* try to break a weapon (from fumbling) */
void break_weapon (ppl p)
{
  if (p->possessions[O_WEAPON_HAND] == NULL) return;

  sprintf(Str1, "Your %s vibrates in your hand....", itemid(p->possessions[O_WEAPON_HAND]));
  mprint(Str1);
  
  damage_item(p, p->possessions[O_WEAPON_HAND]);
  morewait();
}

/* oh nooooo, a fumble.... */
void p_fumble (ppl p, int dtype)
{
  mprint("Ooops! You fumbled....");

  switch (random_range(10))
    {
    case 0: case 1: case 2: case 3: case 4: case 5:
      drop_weapon(p);
      break;

    case 6: case 7: case 8:
      break_weapon(p);
      break;

    case 9:
      mprint("Oh No! You hit yourself!");
      p_damage(p, p->dmg, dtype, "stupidity");
      break;
    }
}	    

/* and effects of missing */
void player_miss (ppl p, pmt m, int dtype)
{
  if (random_range(30) == 1) /* fumble 1 in 30 */
    {
      p_fumble(p, dtype);
      return;
    }

  if (Verbosity == TERSE)
    {
      mprint("You missed it.");
      return;
    }

  if (random_range(10))
    strcpy(Str3, "You miss ");
  else
    switch (random_range(4))
      {
      case 0: strcpy(Str3, "You flail lamely at ");            break;
      case 1: strcpy(Str3, "You only amuse ");                 break;
      case 2: strcpy(Str3, "You fail to even come close to "); break;
      case 3: strcpy(Str3, "You totally avoid contact with "); break;
      }

  if (m->uniqueness == COMMON)
    strcat(Str3,"the ");

  strcat(Str3, m->monstring);
  strcat(Str3, ". ");
  mprint(Str3);
}

/* hooray */
void p_win (void)
{
  morewait();
  clearmsg();

  print1("You won!");
  morewait();

  display_win();
  endgraf();

  exit(0);
}

/* handle a h,j,k,l, etc., to change x and y by dx and dy */
/* for targeting in dungeon */
void movecursor (plv l, int * x, int * y, int dx, int dy)
{
  int lx, ly;

  lx = *x;
  ly = *y;

  if (inbounds(l, lx + dx, ly + dy))
    {
      lx += dx;
      ly += dy;
      screencheck(lx,ly);
    }

  omshowcursor(lx, ly);

  *x = lx;
  *y = ly;
}

/* deal with each possible stati -- values are per move */
/* this function is executed every move */
/* A value over 1000 indicates a permanent effect */
void minute_status_check (ppl p)
{
  int i;

  if (p->status[HASTED] > 0)
    {
      if (p->status[HASTED] < 1000)
	{
	  p->status[HASTED] -= 1;
	  if (p->status[HASTED] == 0)
	    {
	      mprint("The world speeds up.");
	      calc_melee( p);
	    }
	}
    }

  if (p->status[POISONED] > 0)
    {
      p->status[POISONED] -= 1;
      p_damage(p, 3, POISON, "poison");
      if (p->status[POISONED] == 0)
	{
	  showflags(p);
	  mprint("You feel better now.");
	}
    }

  if (p->immunity[UNSTOPPABLE] > 0)
    {
      for (i = 0; i < NUMIMMUNITIES; ++i)
	p->immunity[i] -= 1;

      if (p->immunity[UNSTOPPABLE] == 1)
	mprint("You feel vincible again.");
    }

  if (p->status[IMMOBILE] > 0)
    {
      p->status[IMMOBILE] -= 1;
      if (p->status[IMMOBILE] == 0) 
	mprint("You can move again.");
    }


  if (p->status[SLEPT] > 0)
    {
      p->status[SLEPT] -= 1;
      if (p->status[SLEPT] == 0)
	mprint("You woke up.");
  }

  if (p->status[REGENERATING] > 0)
    {
      if (p->hp < p->maxhp && p->mana > 0)
	{
	  ++(p->hp);
	  --(p->mana);
	  dataprint();
	}

      if (p->status[REGENERATING] < 1000)
	{
	  p->status[REGENERATING] -= 1;
	  if (p->status[REGENERATING] == 0)
	    mprint("You feel less homeostatic.");
	}
    }

  if (p->status[SLOWED] > 0)
    {
      if (p->status[SLOWED] < 1000)
	{
	  p->status[SLOWED] -= 1;
	  if (p->status[SLOWED] == 0)
	    {
	      mprint("You feel quicker now.");
	      calc_melee(p);
	    }
	}
    }

  if (p->status[RETURNING] > 0)
    {
      p->status[RETURNING] -= 1;

      if (p->status[RETURNING] == 10)
	mprint("Your return spell slowly hums towards activation...");
      else if (p->status[RETURNING] == 8)
	mprint("There is an electric tension in the air!");
      else if (p->status[RETURNING] == 5)
	mprint("A vortex of mana begins to form around you!");
      else if (p->status[RETURNING] == 1)
	mprint("Your surroundings start to warp and fade!");
      if (p->status[RETURNING] == 0)
	level_return();
    }
  
  if (p->status[AFRAID] > 0)
    {
      if (p->status[AFRAID] < 1000)
	{
	  p->status[AFRAID] -= 1;
	  if (p->status[AFRAID] == 0)
	    mprint("You feel bolder now.");
	}
    }
}

/* check once per hour for torch to burn out if used */
void torch_check (ppl p, int print)
{
  int i;

  for (i = O_READY_HAND; i <= O_WEAPON_HAND; ++i)
    {
      if (p->possessions[i] == NULL) continue;
      if (p->possessions[i]->id != OB_TORCH) continue;
      if (p->possessions[i]->aux <= 0) continue;

      --(p->possessions[i]->aux);
      if (p->possessions[i]->aux > 0) continue;

      if (print)
	mprint("Your torch goes out!!!");

      conform_unused_object(p->possessions[i]);

      if (p->possessions[i]->number > 1)
	{
	  --(p->possessions[i]->number);
	  p->possessions[i]->aux = 6;
	}
      else
	{
	  p->possessions[i]->usef = I_NO_OP;
	  p->possessions[i]->cursestr = "burnt-out torch";
	  p->possessions[i]->truename = "burnt-out torch";
	  p->possessions[i]->objstr = "burnt-out torch";
	}
    }
}

/* values are in multiples of ten minutes */
/* values over 1000 indicate a permanent effect */
void ten_minute_status_check (ppl p, int print)
{
  if (p->status[SHADOWFORM] > 0 && p->status[SHADOWFORM] < 1000)
    {
      --(p->status[SHADOWFORM]);
      if (p->status[SHADOWFORM] == 0)
	{
	  --(p->immunity[NORMAL_DAMAGE]);
	  --(p->immunity[ACID]);
	  --(p->immunity[THEFT]);
	  --(p->immunity[INFECTION]);

	  if (print)
	    mprint("You feel less shadowy now.");
	}
    }

  if (p->status[ILLUMINATION] > 0 && p->status[ILLUMINATION] < 1000)
    {
      --(p->status[ILLUMINATION]);
      if (p->status[ILLUMINATION] == 0)
	if (print)
	  mprint("Your light goes out!");
    }

  if (p->status[VULNERABLE] > 0 && p->status[VULNERABLE] < 1000)
    {
      --(p->status[VULNERABLE]);
      if (p->status[VULNERABLE] == 0)
	if (print)
	  mprint("You feel less endangered.");
    }

  if (p->status[DEFLECTION] > 0 && p->status[DEFLECTION] < 1000)
    {
      --(p->status[DEFLECTION]);
      if (p->status[DEFLECTION] == 0)
	if (print)
	  mprint("You feel less well defended.");
    }

  if (p->status[ACCURATE] > 0 && p->status[ACCURACY] < 1000)
    {
      --(p->status[ACCURATE]);
      if (p->status[ACCURATE] == 0)
	{
	  calc_melee(p);
	  if (print)
	    mprint("The bulls' eyes go away.");
	}
    }

  if (p->status[HERO] > 0 && p->status[HERO] < 1000)
    {
      --(p->status[HERO]);
      if (p->status[HERO] == 0)
	{
	  calc_melee(p);
	  if (print)
	    mprint("You feel less than super.");
	}
    }

  if (p->status[LEVITATING] > 0 && p->status[LEVITATING] < 1000)
    {
      --(p->status[LEVITATING]);
      if (p->status[LEVITATING] == 0)
	if (print)
	  mprint("You're no longer walking on air.");
    }

  if (p->status[DISEASED] > 0)
    {
      --(p->status[DISEASED]);
      if (p->status[DISEASED] == 0)
	{
	  showflags(p);
	  if (print)
	    mprint("You feel better now.");
	}
    }

  if (p->status[INVISIBLE] > 0 && p->status[INVISIBLE] < 1000)
    {
      --(p->status[INVISIBLE]);
      if (p->status[INVISIBLE] == 0)
	if (print)
	  mprint("You feel more opaque now.");
    }

  if (p->status[BLINDED] > 0 && p->status[BLINDED] < 1000)
    {
      --(p->status[BLINDED]);
      if (p->status[BLINDED] == 0) 
	if (print)
	  mprint("You can see again.");
    }

  if (p->status[TRUESIGHT] > 0 && p->status[TRUESIGHT] < 1000)
    {
      --(p->status[TRUESIGHT]);
      if (p->status[TRUESIGHT] == 0) 
	if (print)
	  mprint("You feel less keen now.");
    }

  if (p->status[BERSERK] > 0 && p->status[BERSERK] < 1000)
    {
      --(p->status[BERSERK]);
      if (p->status[BERSERK] == 0) 
	if (print)
	  mprint("You stop foaming at the mouth.");
    }

  if (p->status[ALERT] > 0 && p->status[ALERT] < 1000)
    {
      --(p->status[ALERT]);
      if (p->status[ALERT] == 0) 
	if (print)
	  mprint("You feel less alert now.");
    }

  if (p->status[BREATHING] > 0 && p->status[BREATHING] < 1000)
    {
      --(p->status[BREATHING]);
      if (p->status[BREATHING] == 0) 
	if (print)
	  mprint("You feel somewhat congested."); 
    }

  if (p->status[DISPLACED] > 0 && p->status[DISPLACED] < 1000)
    {
      --(p->status[DISPLACED]);
      if (p->status[DISPLACED] == 0) 
	if (print)
	  mprint("You feel a sense of position.");
    }

  if (print)
    dataprint();
}

/* Increase in level at appropriate experience gain */
void gain_level (player_t * p)
{
  int gained = FALSE;
  int hp_gain; /* FIXED! 12/30/98 */
  
  if (pflagp(p, SUPPRESS_PRINTING))
    return;

  while (expval(p->level + 1) <= p->xp)
    {
      if (gained == FALSE)
	morewait();

      gained = TRUE;
      ++(p->level);

      print1("You have attained a new experience level!");
      print2("You are now ");
      nprint2(getarticle(levelname(p->level)));
      nprint2(levelname(p->level));

      hp_gain = random_range(p->con) + 1;

      if (p->hp < p->maxhp )
	p->hp += (hp_gain * p->hp / p->maxhp);
      else if (p->hp < (p->maxhp + hp_gain))
	p->hp = p->maxhp + hp_gain;

      /* else leave current hp alone */
      p->maxhp += hp_gain;
      p->maxmana = calcmana(p);

      /* If the character was given a bonus, let him keep it.  Otherwise recharge him. */
      p->mana = max(p->mana, p->maxmana);
      morewait();
    }

  if (gained)
    clearmsg();

  calc_melee(p);
}

/* experience requirements */
long expval (int plevel)
{
  switch (plevel)
    {
    case 0:  return 0L;
    case 1:  return 20L;
    case 2:  return 50L;
    case 3:  return 200L;
    case 4:  return 500L;
    case 5:  return 1000L;
    case 6:  return 2000L;
    case 7:  return 3000L;
    case 8:  return 5000L;
    case 9:  return 7000L;
    case 10: return 10000L;

    default:
      if (plevel < 20)
        return (plevel - 9) * 10000L;
      else
        return ((plevel - 9) * 10000L) + ((plevel - 19) * (plevel - 19) * 500L);
    }
}

/* figures value based on item base-value, charge, plus, and blessing */
long true_item_value (object_t * item)
{
  long value;
  
  if (item->objchar == THING)
    return item->basevalue;

  value = item->basevalue;

  if (item->objchar == STICK)
    value += (value * item->charge / 20);

  if (item->plus > -1)
    value += (value * item->plus / 4);
  else
    value /= -item->plus;

  if (item->blessing > 0)
    value *= 2;

  return value;
}

/* If an item is unidentified, it isn't worth much to those who would buy it */
long item_value (object_t * item)
{
  if (item->known == 0)
    {
      if (item->objchar == THING)
	return 1;

      return true_item_value(item) / 10;
    }

  if (item->known == 1)
    {
      if (item->objchar == THING)
	return item->basevalue;
  
      return item->basevalue / 2;
    }

  return true_item_value(item);
}

/* kill off player if he isn't got the "breathing" status */
void p_drown (ppl p)
{
  int i;
  int attempts = 3;
  location_t * site;

  if (p->status[BREATHING] > 0)
    {
      mprint("Your breathing is unaffected!");
      return;
    }

  while (p->possessions[O_ARMOR] || p->itemweight > (p->maxweight / 2))
    {
      menuclear();

      switch (attempts--)
	{
	case 3: print3("You try to hold your breath..."); break;
	case 2: print3("You try to hold your breath... You choke..."); break;
	case 1: print3("You try to hold your breath... You choke... Your lungs fill..."); break;
	case 0: p_death(p, "drowning");
	}

      morewait();
      menuprint("a: Drop an item.\n");
      menuprint("b: Bash an item.\n");
      menuprint("c: Drop your whole pack.\n");
      showmenu();

      site = &(p->dlevel->site[p->x][p->y]);

      switch (menugetc())
	{
	case 'a':
	  {
	    /* paranoia cleanup */
	    if(site->p_locf == L_WATER)
	      fully_free_object_list(&site->object_list);

	    cmd_drop(p, no_dir, TRUE);

	    if(site->p_locf == L_WATER)
	      {
		fully_free_object_list(&site->object_list);
		mprint("It sinks without a trace.");
	      }
	  }
	  break;

	case 'b':
	  cmd_bash_item(p, no_dir, TRUE);
	  break;

	case 'c':
	  setpflag(p, SUPPRESS_PRINTING);
	  for(i = 0; i < MAXPACK; ++i)
	    {
	      if (p->pack[i] != NULL)
		{
		  if (site->p_locf != L_WATER)
		    {
		      p_drop_at(p, p->x, p->y, p->pack[i]);
		      free_obj(p->pack[i], TRUE);
		    }
		  else
		    {
		      free_obj(p->pack[i], TRUE);
		    }
		}

	      p->pack[i] = NULL;
	    }

	  if (site->p_locf == L_WATER)
	    mprint("It sinks without a trace.");

	  p->packptr = 0;
	  resetpflag(p, SUPPRESS_PRINTING);
	  calc_melee(p);
	  break;
	}

      /* Kluuge. notice_stuff would be more than we need, plus menu is up, which would be messy. */
      calc_weight(p);
    }

  show_screen();
  return;
}

/* the effect of some weapon on monster m, with dmgmod a bonus to damage */
void weapon_use (ppl p, int dmgmod, pob weapon, pmt m)
{
  if (weapon == NULL)
    {
      weapon_bare_hands(dmgmod, m);
      return;
    }

  switch (weapon->aux)
    {
    default:             weapon_bare_hands(dmgmod,m);          break;
    case I_BOLT:         weapon_bolt(dmgmod,weapon,m);         break;
    case I_ARROW:        weapon_arrow(dmgmod,weapon,m);        break;
    case I_NO_OP:        weapon_normal_hit(dmgmod,weapon,m);   break;
    case I_DEFEND:       weapon_defend(dmgmod,weapon,m);       break;
    case I_SCYTHE:       weapon_scythe(dmgmod,weapon,m);       break;
    case I_TANGLE:       weapon_tangle(dmgmod,weapon,m);       break;
    case I_VORPAL:       weapon_vorpal(dmgmod,weapon,m);       break;
    case I_VICTRIX:      weapon_victrix(dmgmod,weapon,m);      break;
    case I_ACIDWHIP:     weapon_acidwhip(dmgmod,weapon,m);     break;
    case I_FIRESTAR:     weapon_firestar(dmgmod,weapon,m);     break;
    case I_DESECRATE:    weapon_desecrate(dmgmod,weapon,m);    break;
    case I_DEMONBLADE:   weapon_demonblade(dmgmod,weapon,m);   break;
    case I_LIGHTSABRE:   weapon_lightsabre(dmgmod,weapon,m);   break;
    case I_MACE_DISRUPT: weapon_mace_disrupt(dmgmod,weapon,m); break;
  }
}

/* for printing actions in printactions above */
char * actionlocstr (char dir)
{
  switch (dir)
    {
    case 'L': strcpy(Str3, "low.");    break;
    case 'C': strcpy(Str3, "center."); break;
    case 'H': strcpy(Str3, "high.");   break;
    default:  strcpy(Str3, "wildly."); break;
    }

  return Str3;
}

/* checks to see if player hits with hitmod vs. monster m at location hitloc */
int player_hit( ppl p, int hitmod, char hitloc, pmt m )
{
  int hit;
  int i = 0;
  int blocks = FALSE;
  int goodblocks = 0;

  if (m->hp < 1)
    {
      mprint("Unfortunately, your opponent is already dead!");
      return FALSE;
    }

  if (hitloc == 'X')
    hitloc = random_loc();

  transcribe_monster_actions(m);

  for (i = 0; i < strlen(m->meleestr); i += 2)
    if (m->meleestr[i] == 'B' || m->meleestr[i] == 'R')
      {
	blocks = TRUE;
	if (hitloc == m->meleestr[i+1])
	  goodblocks++;
      }

  if (!blocks)
    goodblocks = -1;

  hit = hitp(p->hit + hitmod, m->ac + 10 * goodblocks);

  if (hit == 0 && goodblocks > 0)
    {
      if (m->uniqueness == COMMON)
	sprintf(Str1, "The %s blocks it!", m->monstring);
      else
	sprintf(Str1, "%s blocks it!", m->monstring);

      if (Verbosity == VERBOSE)
	mprint(Str1);
    }

  return hit;
}

/* execute player combat actions versus monster m */
void tacplayer(ppl p, pmt m)
{
  int i;

  for (i = 0; i < strlen(p->meleestr); i += 2)
    {
      if (m->hp > 0)
	{
	  switch(p->meleestr[i])
	    {
	    case 't': case 'T':
	      if (p->possessions[O_WEAPON_HAND] == NULL) 
		strcpy(Str1,"You punch ");
	      else
		strcpy(Str1,"You thrust ");

	      strcat(Str1, actionlocstr(p->meleestr[i+1]));

	      if (Verbosity == VERBOSE)
		mprint(Str1);

	      if (player_hit(p, 2 * statmod(p->dex), p->meleestr[i+1], m))
		weapon_use(p, 0, p->possessions[O_WEAPON_HAND], m);
	      else
		player_miss(p, m, NORMAL_DAMAGE);
	      break;

	    case 'c': case 'C':
	      if (p->possessions[O_WEAPON_HAND] == NULL) 
		strcpy(Str1,"You punch ");
	      else if (p->possessions[O_WEAPON_HAND]->type == CUTTING) 
		strcpy(Str1,"You cut ");
	      else if (p->possessions[O_WEAPON_HAND]->type == STRIKING) 
		strcpy(Str1,"You strike ");
	      else
		strcpy(Str1,"You attack ");

	      strcat(Str1, actionlocstr(p->meleestr[i+1]));

	      if (Verbosity == VERBOSE)
		mprint(Str1);

	      if (player_hit(p, 0 , p->meleestr[i+1], m))
		weapon_use(p, 2 * statmod(p->str), p->possessions[O_WEAPON_HAND], m);
	      else
		player_miss(p, m, NORMAL_DAMAGE);
	      break;

	    case 'l': case 'L':
	      strcpy(Str1, "You lunge ");
	      strcat(Str1, actionlocstr(p->meleestr[i+1]));

	      if (Verbosity == VERBOSE)
		mprint(Str1);

	      if (player_hit(p, p->level + p->dex, p->meleestr[i+1],m))
		weapon_use(p, p->level, p->possessions[O_WEAPON_HAND], m);
	      else
		player_miss(p, m, NORMAL_DAMAGE);
	      break;
	    }
	}
    }
}

/* This function is used to undo all items temporarily, should
always be used in pairs with on being TRUE and FALSE, and may cause
anomalous stats and item-usage if used indiscriminately */

void toggle_item_use( ppl p, int on )
{
  static int used[MAXITEMS];

  int i;

  setpflag(p, SUPPRESS_PRINTING);

  if (on)
    {
      for(i = 0; i < MAXITEMS; ++i)
	{
	  used[i] = FALSE;
	  if (p->possessions[i] != NULL)
	    {
	      if ((used[i] = p->possessions[i]->used) == TRUE)
		{
		  p->possessions[i]->used = FALSE;
		  item_use(p->possessions[i]);
		}
	    }
	}
    }
  else
    {
      for(i = 1; i < MAXITEMS; ++i) 
	if (used[i])
	  {
	    p->possessions[i]->used = TRUE;
	    item_use(p->possessions[i]);
	  }

      calc_melee(p);
      showflags(p);
      dataprint();
    }

  resetpflag(p, SUPPRESS_PRINTING);
}

/* end */
