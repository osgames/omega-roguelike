/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/* *** Declarations that deal with flow. *** */


typedef bool (*grid_test_fp) (int x, int y);
bool setup_flow_for_moveto(list_t * spots, grid_test_fp grid_test);;

