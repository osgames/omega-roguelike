/* omega copyright (c) 1987,1988,1989 by Laurence Raphael Brothers */

/* this file includes main() and some top-level functions */
/* omega.c */

#include "game_time.h"
#include "glob.h"
#include "ticket.h"
#include "liboutil/list.h"
#include "command.h" /* Just for cmd_quit() */

#include <signal.h>
#include <fcntl.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
/* Note: in order to avoid a memory bug I've been told about, I'm
   explicitly initializing every global to something. */

#ifndef NOGETOPT
# include <unistd.h>
#endif

/* most globals originate in omega.c */

char *Omegalib;		/* contains the path to the library files */
char *Omegavar;		/* contains the path to the hiscore and log files */

#ifdef DEBUG
FILE *DG_debug_log; /* debug log file pointer */
int DG_debug_flag = 0; /* debug flag -- set by -d commandline option */
#endif

char SaveFileName[80];

/* Objects and Monsters are allocated and initialized in init.c */

/* one of each spell */
struct spell Spells[NUMSPELLS+1];

/* locations of city sites [0] - found, [1] - x, [2] - y */
int CitySiteList[NUMCITYSITES][3];

int Lev_Length=MAXLENGTH;
int Lev_Width=MAXWIDTH;

long GameStatus=0L;                   /* Game Status bit vector */
int ScreenLength = 0;                 /* How large is level window */
int ScreenWidth = 0;                 /* How large is level window */
struct player Player;                 /* the player */
struct terrain Country[COUNTRY_WIDTH][COUNTRY_LENGTH];/* The countryside */
int Villagenum = 0;                   /* Current Village number */
int MaxDungeonLevels = 0;             /* Deepest level allowed in dungeon */
int Current_Dungeon= -1;              /* What is Dungeon now */
int Current_Environment= E_CITY;      /* Which environment are we in */
int Dirs[2][num_logical_dirs];                       /* 9 xy directions */
int EDirs[2][num_logical_dirs];                      /* 9 xy directions for explosions */
char Cmd='s';                         /* last player command */
struct monster *Arena_Monster=NULL;   /* Opponent in arena */
int Arena_Opponent=0;                 /* case label of opponent in l_arena()*/
int Arena_Victory = 0;                /* did player win in arena? */
int Imprisonment=0;                   /* amount of time spent in jail */
int Precipitation=0;                  /* Hours of rain, snow, etc */
pob Pawnitems[PAWNITEMS] = {NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};
/* items in pawn shop */
int Constriction=0;                   /* Dragonlord Attack State */
int Blessing=FALSE;                   /* Altar Blessing State */
int RitualRoom= -1;                   /* last room of ritual magic */
int Lawstone=0;                       /* magic stone counter */
int Chaostone=0;                      /* magic stone counter */
int Mindstone=0;                      /* magic stone counter */
int Searchnum = 1;                    /* number of times to search on 's' */
int Behavior;			      /* Player NPC behavior */
int Verbosity = VERBOSE;              /* verbosity level */
char Stringbuffer[STRING_BUFFER_SIZE][80];	/* last strings printed */
long Gymcredit = 0;                   /* credit at rampart gym */
int Spellsleft = 0;                   /* research allowance at college */
int Studiesleft = 0;                  /* study allowance at monastery */
int HiMagic = 0;                      /* current level for l_throne */
int Pawndate = 0;                     /* Pawn Shop item generation date */

int LegionSalary = 0;                 /* legion salary */
int LegionAccount = 0;                /* legion bank account */
int LegionObligations = 0;

char dole_too_soon = FALSE;
char ritual_too_soon = FALSE;
char club_hint_too_soon = FALSE;
char tavern_hint_too_soon = FALSE;

#ifdef NEW_BANK
bank_account *bank = NULL;            /* ptr to a list of bank accounts */
#else
long Balance = 0;                     /* bank account */
char Password[64];                    /* autoteller password */
#endif

long FixedPoints = 0;                 /* points are frozen after adepthood*/

char Str1[STRING_LEN],Str2[STRING_LEN],Str3[STRING_LEN],Str4[STRING_LEN];
   /* Some string space, random uses */

list_t * Condoitems = 0;                        /* Items in condo */
list_t * Bagitems = 0;                          /* Items in bag of holding */

/* high score names, levels, behavior */
int Shadowlordbehavior,Archmagebehavior,Primebehavior,Commandantbehavior;
int Championbehavior,Priestbehavior[7],Hibehavior,Dukebehavior;
int Chaoslordbehavior,Lawlordbehavior,Justiciarbehavior;
int Grandmasterbehavior;
int Grandmasterlevel;
char Grandmaster[80];
char Shadowlord[80],Archmage[80],Prime[80],Commandant[80],Duke[80];
char Champion[80],Priest[7][80],Hiscorer[80],Hidescrip[80];
char Chaoslord[80],Lawlord[80],Justiciar[80];
int Shadowlordlevel,Archmagelevel,Primelevel,Commandantlevel,Dukelevel;
int Championlevel,Priestlevel[7],Hilevel,Justiciarlevel;
long Hiscore = 0L;
int Chaoslordlevel = 0,Lawlordlevel = 0,Chaos = 0,Law = 0;

/* New globals which used to be statics */
int saved = FALSE;
int twiddle = FALSE;
int onewithchaos = FALSE;
int winnings = 0;

int scroll_ids[30];
int potion_ids[30];
int stick_ids[30];
int cloak_ids[30];
int ring_ids[30];
int boot_ids[30];

int deepest[E_MAX + 1];
int level_seed[E_MAX + 1];	/* random number seed that generated level */

/* This may be implementation dependent */
/* SRANDFUNCTION is defined in odefs.h */
/* environment is the environment about to be generated, or -1 for the first */
/* time, or -2 if we want to restore the random number point */
void initrand(int environment, int level)
{
  static int store;
  int seed;

  if (environment >= 0)
    store = RANDFUNCTION();
  /* Pseudo Random Seed */
  if (environment == E_RANDOM)
    seed = (int) time(NULL);
  else if (environment == E_RESTORE)
    seed = store;
  else
    seed = level_seed[environment] + 1000*level;
  SRANDFUNCTION(seed);
}


int game_restore(char *savefile)
{
  int ok;
  ok = restore_game(savefile);
  if (! ok) {
    endgraf();
    printf("Try again with the right save file, luser!\n");
    exit(1);
  }
  change_to_user_perms();
  remove(savefile);
  change_to_game_perms();
  return(TRUE);
}

void signalquit(int ignore)
{
  cmd_quit(no_dir, TRUE);
}

int main(int argc, char *argv[])
{
  int continuing = 0;
  int count;
  int scores_only = 0;
#ifndef NOGETOPT
  int i;

  while(( i= getopt( argc, argv, "dsh")) != -1)
  {
     switch (i)
     {
       case 'd':
#ifdef DEBUG
         DG_debug_flag++;
#endif
         break;
       case 's':
         scores_only = 1;
         break;
       case 'h':
#ifdef DEBUG
         printf("Usage: omega [-shd] [savefile]\n");
#else
         printf("Usage: omega [-sh] [savefile]\n");
#endif
         printf("Options:\n");
         printf("  -s  Display high score list\n");
         printf("  -h  Display this message\n");
#ifdef DEBUG
         printf("  -d  Enable debug mode\n");
#endif
         exit(0);
         break;
       case '?':
         /* error parsing args... ignore? */
         printf("'%c' is an invalid option, ignoring\n", optopt );
         break;
     }
  }

  if (optind >= argc ) {
    /* no save file given */
#if defined( BSD ) || defined( SYSV )
    sprintf( SaveFileName, "Omega%d", getuid() );
#else
    strcpy( SaveFileName,"Omega");
#endif
  } else {
    /* savefile given */
    continuing = 1;
    strcpy(SaveFileName,argv[optind]);
  }

#else
  /* alternate code for people who don't support getopt() -- no enhancement */
  if (argc ==2) {
    strcpy( SaveFileName, argv[1]);
    continuing = 1;
  } else {
    strcpy( SaveFileName,"Omega");
  }
#endif

  /* always catch ^c and hang-up signals */

#ifdef SIGINT
  signal(SIGINT,signalquit);
#endif
#ifdef SIGHUP
  signal(SIGHUP,signalsave);
#endif

#ifndef MSDOS
  if (CATCH_SIGNALS) {
    signal(SIGQUIT,signalexit);
    signal(SIGILL,signalexit);
#ifdef DEBUG
    if( DG_debug_flag ) {
#endif
    signal(SIGTRAP,signalexit);
    signal(SIGFPE,signalexit);
    signal(SIGSEGV,signalexit);
#ifdef DEBUG
    }
#endif
#ifdef SIGIOT
    signal(SIGIOT,signalexit);
#endif
#ifdef SIGABRT
    signal(SIGABRT,signalexit);
#endif
#ifdef SIGEMT
    signal(SIGEMT,signalexit);
#endif
#ifdef SIGBUS
    signal(SIGBUS,signalexit);
#endif
#ifdef SIGSYS
    signal(SIGSYS,signalexit);
#endif
    }
#endif

#ifndef FIXED_OMEGALIB
  if (!(Omegalib = getenv("OMEGALIB")))
#endif
    Omegalib = OMEGALIB;

#ifndef FIXED_OMEGALIB
  if (!(Omegavar = getenv("OMEGAVAR")))
#endif
    Omegavar = OMEGAVAR;

  /* if filecheck is 0, some necessary data files are missing */
  if (filecheck() == 0) exit(0);

  /* all kinds of initialization */
  init_perms();
  initgraf();
  initdirs();
  initrand(E_RANDOM, 0);
  initspells();

#ifdef DEBUG
  /* initialize debug log file */
  DG_debug_log = fopen( "/tmp/omega_dbg_log", "a" );
  assert( DG_debug_log ); /* WDT :) */
  setvbuf( DG_debug_log, NULL, _IOLBF, 0);
  fprintf(DG_debug_log, "##############  new game started ##############\n");
#endif

  for (count = 0; count < STRING_BUFFER_SIZE; count++)
    strcpy(Stringbuffer[count],"<nothing>");

  lev_init();

  omega_title();
  showscores();

  if (scores_only ) {
    endgraf();
    exit(0);
  }

  /* game restore attempts to restore game if there is an argument */
  if (continuing)
  {
     game_restore(SaveFileName);
     mprint("Your adventure continues....");
  }
  else
  {
    /* monsters initialized in game_restore if game is being restored */
    /* items initialized in game_restore if game is being restored */
    inititem(TRUE);

    init_time();
    bank_init();

    continuing = initplayer(); /* RM: 04-19-2000 loading patch */
  }

  if (!continuing)
  {
    init_world();  /* RM: 04-19-2000 loading patch */
    mprint("'?' for help or commandlist, 'Q' to quit.");
  }
  if (optionp(SHOW_COLOUR))
    colour_on();
  else
    colour_off();

  set_notice_flags
    (
      notice_new_level
    | notice_equip
    | notice_x_mounted
    | notice_food
    | notice_vision
    | notice_py_data
    | notice_time
    | notice_moon_phase
       );

  /* game cycle */
  while (TRUE)
    {
      ticket_dispatch();
    }
}

#ifndef MSDOS
void signalexit(int ignored)
{
  clearmsg();
  mprint("Yikes!");
  morewait();
  mprint("Sorry, caught a core-dump signal.");
  signalsave(0);
  endgraf();
  printf("Bye!\n");
  exit(0);
}
#endif

/* Start up game with new dungeons; start with player in city */
void init_world(void)
{
  int /*i,*/ env;

  for (env = 0; env <= E_MAX; env++)
    { level_seed[env] = RANDFUNCTION(); }

  load_country(); /* Always loaded, but restore loads it too. */
  change_environment_push(E_COUNTRYSIDE, 0);
  change_environment_push(E_CITY, 0);
}

/* set variable item names */
void inititem(int reset)
{
  int i;

  if (reset) {
    shuffle(scroll_ids, 30);
    shuffle(potion_ids, 20);
    shuffle(stick_ids, 20);
    shuffle(boot_ids, 20);
    shuffle(cloak_ids, 20);
    shuffle(ring_ids, 20);
  }
  for(i=0;i<NUMSCROLLS;i++)
    Objects[SCROLLID+i].objstr = scrollname(i);
  for(i=0;i<NUMPOTIONS;i++)
    Objects[POTIONID+i].objstr = potionname(i);
  Objects[OB_POTION_DEATH].objstr = potionname(18);
  Objects[OB_POTION_LIFE].objstr = potionname(19);
  for(i=0;i<NUMSTICKS;i++)
    Objects[STICKID+i].objstr = stickname(i);
  for(i=0;i<NUMBOOTS;i++)
    Objects[BOOTID+i].objstr = bootname(i);
  for(i=0;i<NUMCLOAKS;i++)
    Objects[CLOAKID+i].objstr = cloakname(i);
  for(i=0;i<NUMRINGS;i++)
    Objects[RINGID+i].objstr = ringname(i);
}
