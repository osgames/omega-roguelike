/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/* Macros to help manage saving and restoring. */

/* These macros let us save objects almost as simply as naming the
   fields in them.   Both assume the file descriptor is named `fd'.
   SAVE_SCALAR assumes there is a integer variable called `ok' - Tehom 
*/
#define SAVE_SCALAR(TYPE, SCAL)\
  ok &= (fwrite((char *)&SCAL,sizeof(TYPE),1,fd) == 1)

#define RESTORE_SCALAR(TYPE, SCAL)\
  fread((char *)&SCAL,sizeof(TYPE),1,fd)


/* In addition to the `fd' and `ok' variables above, the various _FIELD
   macros assume there is a pointer variable `ptr' pointing to a struct
   containing FIELD. */

#define SAVE_FIELD(TYPE, FIELD)\
  ok &= (fwrite((char *)&ptr->FIELD,sizeof(TYPE),1,fd) == 1)

#define RESTORE_FIELD(TYPE, FIELD)\
  fread((char *)&ptr->FIELD,sizeof(TYPE),1,fd)

#define SAVE_STRING_FIELD(FIELD)\
  ok &= (fprintf(fd,"%s\n",ptr->FIELD) >= 0)

#define RESTORE_STRING_FIELD(FIELD)\
  { filescanstring(fd, tempstr); ptr->FIELD = salloc(tempstr); }

#define SAVE_BY_CALL(FUNC, OBJECT)\
  ok &= FUNC(fd,OBJECT)

#define RESTORE_BY_CALL(FUNC, OBJECT)\
  FUNC(fd, &OBJECT, version)

#define BEGIN_SAVER(NAME, TYPE)\
int NAME(FILE *fd, TYPE * ptr)\
{\
  int ok = 1;

#define END_SAVER \
  return ok;\
}

#define BEGIN_RESTORER(NAME, TYPE)\
void NAME(FILE *fd, TYPE * * ptr_ptr, int version)\
{

#define END_RESTORER \
} 


int save_check(FILE *fd, char * check_string);
void restore_check(FILE *fd, char * check_string);
