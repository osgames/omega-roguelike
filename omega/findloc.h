/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/* *** Declarations dealing with finding and setting x,y locations by
   various criterions. *** */

struct coords_alloced_t;
typedef struct coords_alloced_t
{ int x; int y; struct coords_alloced_t * next; }
coords_alloced_t; 


void delete_coords(void * element);
list_t * parsecitysite(void);
char * get_full_sitename(locf_t index);

int save_knowable_locs(FILE* fd, list_t * special_locs);
void restore_knowable_locs(FILE *fd, int version);

int make_loc_known(int index);
int make_loc_known_at(int x, int y);
void make_all_knowable_locs_known(void);

int get_special_loc(int which, int * x, int * y);
int get_random_multiloc(int which, int * x, int * y);
void add_knowable_user_loc(int x, int y, char* namestring);
void try_add_knowable_loc(int index, int x, int y, int known);
void add_game_special_loc(int index, int x, int y);

int find_central_space(int *x, int *y);
int find_random_country_loc(int *x, int *y);
int find_country_site( int base_terrain_type, int aux, int * px, int * py);
int spaceok (int x_idx, int y_idx, void* dummy);
int water_space (int x_idx, int y_idx, void* dummy);
int space_in_another_room (int x_idx, int y_idx, void* data);
int findspace( int *,int *,int );
int findspace_aux(
		   int *x,
		   int *y,
		   int (*spaceok) (int x_idx, int y_idx, void* data),
		   void* data);
int water_space (int x_idx, int y_idx, void* dummy);
int find_floorspace( int *x, int *y );

int set_spot_from_player(int *x, int *y);
