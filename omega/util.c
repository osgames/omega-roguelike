/* copyright (c) 1987,1988,1989 by Laurence Raphael Brothers */
/* utils.c */

/* Random utility functions called from all over */

#include <sys/types.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <unistd.h>
int setreuid(uid_t ruid, uid_t euid);
#include <stdlib.h>

#include "game_time.h"
#include "glob.h"
#include "lev.h"
#include "liboutil/list.h"
#include "memory.h"

/* utility for liboutil/list.c */
void * malloc_nofail(size_t size)
{
  return checkmalloc(size);
}

void setPlayerXY(int x, int y)
{
  Player.x = x;
  Player.y = y;
  notice_flags |= notice_loc;
}

/* x and y on level? */
int inbounds(int x, int y)
{
  return((x>=0)&&(y>=0)&&(x<Lev_Width)&&(y<Lev_Length));
}

/* RANDFUNCTION is defined in odefs.h */
unsigned random_range(unsigned k)
{
  return( k==0 ? 0 : (int) (RANDFUNCTION() % k) ) ;
  /*return( k==0 ? 0 : (int) ((RANDFUNCTION()%10000)*k)/10000); */
}



/* always hit on a natural 0; never hit on a natural 19 */
int hitp(int hit, int ac)
{
  int roll = random_range(20);
  if (roll == 0) return(TRUE);
  else if (roll == 19) return(FALSE);
  else return((roll < (hit - ac)) ? TRUE : FALSE );
}


/* number of moves from x1,y1 to x2,y2 */
int distance(int x1, int y1, int x2, int y2)
{
  return(max(abs(x2-x1),abs(y2-y1)));
}


/* can you shoot, or move monsters through a spot? */
int unblocked(int x, int y)
{
  if ((! inbounds(x,y)) || get_monster(Level, x, y) ||
      (Level->site[x][y].locchar == WALL) ||
      (Level->site[x][y].locchar == PORTCULLIS) ||
      (Level->site[x][y].locchar == STATUE) ||
      (Level->site[x][y].locchar == HEDGE) ||
      (Level->site[x][y].locchar == CLOSED_DOOR) ||
      loc_statusp(x,y,SECRET) ||
      ((x==Player.x) && (y==Player.y)))
    return(FALSE);
  else
    return(TRUE);
}


/* do monsters want to move through a spot */
int m_unblocked(pmt m, int x, int y)
{
  if ((! inbounds(x,y)) || ((x==Player.x) && (y==Player.y)))
    return(FALSE);
  else if (get_monster(Level, x, y) || SPACE == Level->site[x][y].locchar)
    return(FALSE);
  else if (m_statusp(m,ONLYSWIM))
    return(Level->site[x][y].locchar == WATER);
  else if (loc_statusp(x,y,SECRET)) {
    if (m->movef == M_MOVE_SMART) {
      if (los_p(x, y, Player.x, Player.y)) {
	mprint("You see a secret door swing open!");
	lreset(x, y, SECRET);
	lset(x, y, CHANGED);
      }
      else
	mprint("You hear a door creak open, and then close again.");
	/* smart monsters would close secret doors behind them if the */
	/* player didn't see them using it */
      return(TRUE);
    }
    else
      return(m_statusp(m,INTANGIBLE));
  }
  else if ((Level->site[x][y].locchar == FLOOR) ||
	   (Level->site[x][y].locchar == OPEN_DOOR))
    return(TRUE);
  else if ((Level->site[x][y].locchar == PORTCULLIS) ||
	   (Level->site[x][y].locchar == WALL) ||
	   (Level->site[x][y].locchar == STATUE))
    return(m_statusp(m,INTANGIBLE));
  else if (Level->site[x][y].locchar==WATER)
    return(m_statusp(m,SWIMMING) ||
	   m_statusp(m,ONLYSWIM) ||
	   m_statusp(m,INTANGIBLE) ||
	   m_statusp(m,FLYING));
  else if (Level->site[x][y].locchar == CLOSED_DOOR) {
    if (m->movef==M_MOVE_SMART) {
      mprint("You hear a door creak open.");
      Level->site[x][y].locchar = OPEN_DOOR;
      lset(x, y, CHANGED);
      return(TRUE);
    }
    else if (random_range(m->dmg) > random_range(100)) {
      mprint("You hear a door shattering.");
      Level->site[x][y].locchar = RUBBLE;
      lset(x, y, CHANGED);
      return(TRUE);
    }
    else return(m_statusp(m,INTANGIBLE));
  }
  else if (Level->site[x][y].locchar == LAVA)
    return((m_immunityp(m,FLAME) &&
	    m_statusp(m,SWIMMING)) ||
	   m_statusp(m,INTANGIBLE) ||
	   m_statusp(m,FLYING));
  else if (Level->site[x][y].locchar == FIRE)
    return(m_statusp(m,INTANGIBLE) ||
	   m_immunityp(m,FLAME));
  else if ((Level->site[x][y].locchar == TRAP) ||
	   (Level->site[x][y].locchar == HEDGE) ||
	   (Level->site[x][y].locchar == ABYSS))
    return((m->movef == M_MOVE_CONFUSED) ||
	   m_statusp(m,INTANGIBLE) ||
	   m_statusp(m,FLYING));
  else return(TRUE);
}



/* can you see through a spot? */
int view_unblocked(int x, int y)
{
  if (! inbounds(x,y)) return(FALSE);
  else if ((Level->site[x][y].locchar == WALL) ||
	   (Level->site[x][y].locchar == STATUE) ||
	   (Level->site[x][y].locchar == HEDGE) ||
	   (Level->site[x][y].locchar == FIRE) ||
	   (Level->site[x][y].locchar == CLOSED_DOOR) ||
	   loc_statusp(x,y,SECRET))
    return(FALSE);
  else
    return(TRUE);
}


/* 8 moves in Dirs */
void initdirs(void)
{
  Dirs[0][0] = 1;
  Dirs[0][1] = 1;
  Dirs[0][2] = -1;
  Dirs[0][3] = -1;
  Dirs[0][4] = 1;
  Dirs[0][5] = -1;
  Dirs[0][6] = 0;
  Dirs[0][7] = 0;
  Dirs[0][8] = 0;
  Dirs[1][0] = 1;
  Dirs[1][1] = -1;
  Dirs[1][2] = 1;
  Dirs[1][3] = -1;
  Dirs[1][4] = 0;
  Dirs[1][5] = 0;
  Dirs[1][6] = 1;
  Dirs[1][7] = -1;
  Dirs[1][8] = 0;
}



/* do_los moves pyx along a lineofsight from x1 to x2 */
/* x1 and x2 are pointers because as a side effect they are changed */
/* to the final location of the pyx */
void do_los(Symbol pyx, int *x1, int *y1, int x2, int y2)
{
  int dx,dy,ox,oy;
  int major, minor;
  int error, delta, step;
  int blocked;

  if (x2 - *x1 < 0) dx = 5;
  else if (x2 - *x1 > 0) dx = 4;
  else dx = -1;
  if (y2 - *y1 < 0) dy = 7;
  else if (y2 - *y1 > 0) dy = 6;
  else dy = -1;
  if (abs(x2 - *x1) > abs(y2 - *y1)) {
    major = dx;
    minor = dy;
    step = abs(x2 - *x1);
    delta = 2*abs(y2 - *y1);
  }
  else {
    major = dy;
    minor = dx;
    step = abs(y2 - *y1);
    delta = 2*abs(x2 - *x1);
  }
  if (major == -1)	/* x1,y2 already == x2,y2 */
    return;
  error = 0;
  do {
    ox = *x1; oy = *y1;
    *x1 += Dirs[0][major];
    *y1 += Dirs[1][major];
    error += delta;
    if (error > step) {	/* don't need to check that minor >= 0 */
      *x1 += Dirs[0][minor];
      *y1 += Dirs[1][minor];
      error -= 2*step;
    }
    blocked = !unblocked(*x1,*y1);
    if (error < 0 && (*x1 != x2 || *y1 != y2) && blocked) {
      *x1 -= Dirs[0][minor];
      *y1 -= Dirs[1][minor];
      error += 2*step;
      blocked = !unblocked(*x1,*y1);
    }
    Level->site[*x1][*y1].showchar = pyx;
    plotchar(pyx,*x1,*y1);
    plotspot(ox,oy,TRUE);
    usleep(50000);
  } while ((*x1 != x2 || *y1 != y2) && !blocked);
  plotspot(*x1,*y1,TRUE);
  levelrefresh();
}


/* This is the same as do_los, except we stop before hitting nonliving
obstructions */
void do_object_los(Symbol pyx, int *x1, int *y1, int x2, int y2)
{
  int dx,dy,ox,oy;
  int major, minor;
  int error, delta, step;
  int blocked;

  if (x2 - *x1 < 0) dx = 5;
  else if (x2 - *x1 > 0) dx = 4;
  else dx = -1;
  if (y2 - *y1 < 0) dy = 7;
  else if (y2 - *y1 > 0) dy = 6;
  else dy = -1;
  if (abs(x2 - *x1) > abs(y2 - *y1)) {
    major = dx;
    minor = dy;
    step = abs(x2 - *x1);
    delta = 2*abs(y2 - *y1);
  }
  else {
    major = dy;
    minor = dx;
    step = abs(y2 - *y1);
    delta = 2*abs(x2 - *x1);
  }
  if (major == -1)	/* x1,y2 already == x2,y2 */
    return;
  error = 0;
  do {
    ox = *x1; oy = *y1;
    *x1 += Dirs[0][major];
    *y1 += Dirs[1][major];
    error += delta;
    if (error > step) {	/* don't need to check that minor >= 0 */
      *x1 += Dirs[0][minor];
      *y1 += Dirs[1][minor];
      error -= 2*step;
    }
    blocked = !unblocked(*x1,*y1);
    if (error < 0 && (*x1 != x2 || *y1 != y2) && blocked) {
      *x1 -= Dirs[0][minor];
      *y1 -= Dirs[1][minor];
      error += 2*step;
      blocked = !unblocked(*x1,*y1);
    }
    plotspot(ox,oy,TRUE);
    if (unblocked(*x1,*y1)) {
      plotchar(pyx,*x1,*y1);
      Level->site[*x1][*y1].showchar = pyx;
      usleep(50000);
    }
  } while ((*x1 != x2 || *y1 != y2) && !blocked);
  if (blocked && !get_monster(Level, *x1, *y1))
    {
      *x1 = ox;
      *y1 = oy;
    }
  plotspot(*x1,*y1,TRUE);
  levelrefresh();
}


/* los_p checks to see whether there is an unblocked los from x1,y1 to x2,y2 */
int los_p(int x1, int y1, int x2, int y2)
{
  int dx,dy;
  int major, minor;
  int error, delta, step;
  int blocked;

  if (x2-x1 < 0) dx = 5;
  else if (x2-x1 > 0) dx = 4;
  else dx = -1;
  if (y2-y1 < 0) dy = 7;
  else if (y2-y1 > 0) dy = 6;
  else dy = -1;
  if (abs(x2-x1) > abs(y2-y1)) {
    major = dx;
    minor = dy;
    step = abs(x2 - x1);
    delta = 2*abs(y2 - y1);
  }
  else {
    major = dy;
    minor = dx;
    step = abs(y2 - y1);
    delta = 2*abs(x2 - x1);
  }
  if (major == -1)	/* x1,y2 already == x2,y2 */
    return TRUE;
  error = 0;
  do {
    x1 += Dirs[0][major];
    y1 += Dirs[1][major];
    error += delta;
    if (error > step) {	/* don't need to check that minor >= 0 */
      x1 += Dirs[0][minor];
      y1 += Dirs[1][minor];
      error -= 2*step;
    }
    blocked = !unblocked(x1,y1);
    if (error < 0 && (x1 != x2 || y1 != y2) && blocked) {
      x1 -= Dirs[0][minor];
      y1 -= Dirs[1][minor];
      error += 2*step;
      blocked = !unblocked(x1,y1);
    }
  } while ((x1 != x2 || y1 != y2) && !blocked);
  return((x1==x2) && (y1==y2));
}


/* view_los_p sees through monsters */
int view_los_p(int x1, int y1, int x2, int y2)
{
  int dx,dy;
  int major, minor;
  int error, delta, step;
  int blocked;

  if (x2-x1 < 0) dx = 5;
  else if (x2-x1 > 0) dx = 4;
  else dx = -1;
  if (y2-y1 < 0) dy = 7;
  else if (y2-y1 > 0) dy = 6;
  else dy = -1;
  if (abs(x2-x1) > abs(y2-y1)) {
    major = dx;
    minor = dy;
    step = abs(x2 - x1);
    delta = 2*abs(y2 - y1);
  }
  else {
    major = dy;
    minor = dx;
    step = abs(y2 - y1);
    delta = 2*abs(x2 - x1);
  }
  if (major == -1)	/* x1,y2 already == x2,y2 */
    return TRUE;
  error = 0;
  do {
    x1 += Dirs[0][major];
    y1 += Dirs[1][major];
    error += delta;
    if (error > step) {
      x1 += Dirs[0][minor];
      y1 += Dirs[1][minor];
      error -= 2*step;
    }
    blocked = !view_unblocked(x1,y1);
    if (error < 0 && (x1 != x2 || y1 != y2) && blocked) {
      x1 -= Dirs[0][minor];
      y1 -= Dirs[1][minor];
      error += 2*step;
      blocked = !view_unblocked(x1,y1);
    }
  } while ((x1 != x2 || y1 != y2) && !blocked);
  return((x1==x2) && (y1==y2));
}

/* returns the command direction from the index into Dirs */
char inversedir(int dirindex)
{
  switch (dirindex) {
    case 0:return('n');
    case 1:return('u');
    case 2:return('b');
    case 3:return('y');
    case 4:return('l');
    case 5:return('h');
    case 6:return('j');
    case 7:return('k');
    default:return('\0');
  }
}

long calc_points(void)
{
  int i;
  long points=0;

  if (gamestatusp(SPOKE_TO_DRUID)) points += 50;
  if (gamestatusp(COMPLETED_CAVES)) points += 100;
  if (gamestatusp(COMPLETED_SEWERS)) points += 200;
  if (gamestatusp(COMPLETED_CASTLE)) points += 300;
  if (gamestatusp(COMPLETED_ASTRAL)) points += 400;
  if (gamestatusp(COMPLETED_VOLCANO)) points += 500;
  if (gamestatusp(KILLED_DRAGONLORD)) points += 100;
  if (gamestatusp(KILLED_EATER)) points += 100;
  if (gamestatusp(KILLED_LAWBRINGER)) points += 100;

  points += Player.xp/50;

  points += Player.cash/500;

  for (i=0;i<MAXITEMS;i++)
    if (Player.possessions[i] != NULL)
      points += Player.possessions[i]->level*(Player.possessions[i]->known+1);

  for (i=0;i<MAXPACK;i++)
    if (Player.pack[i] != NULL)
      points += Player.pack[i]->level*(Player.pack[i]->known+1);

  for (i=0;i<NUMRANKS;i++) {
    if (Player.rank[i] == 5) points += 500;
    else points += 20*Player.rank[i];
  }

  if (Player.hp < 1)
    points = (points / 2);

  else if (Player.rank[ADEPT])
    points *= 10;

  return(points);
}

char * getarticle (char * str)
{
  if ((str[0]=='a') || (str[0]=='A') ||
      (str[0]=='e') || (str[0]=='E') ||
      (str[0]=='i') || (str[0]=='I') ||
      (str[0]=='o') || (str[0]=='O') ||
      (str[0]=='u') || (str[0]=='U') ||
      (((str[0]=='h') || (str[0]=='H')) &&
       ((str[1]=='i') || (str[1]=='e'))))
    return("an ");
  else
    return("a ");
}

char * ordinal (int number)
{
  if (number == 11 || number == 12 || number == 13)
    return("th");
  else switch (number % 10)
    {
    case 1: return("st");
    case 2: return("nd");
    case 3: return("rd");
    default: return("th");
    }
}


/* is ch a vowel? */
int is_vowel (char ch)
{
  if ('a' == ch) return TRUE;
  if ('A' == ch) return TRUE;
  if ('e' == ch) return TRUE;
  if ('E' == ch) return TRUE;
  if ('i' == ch) return TRUE;
  if ('I' == ch) return TRUE;
  if ('o' == ch) return TRUE;
  if ('O' == ch) return TRUE;
  if ('u' == ch) return TRUE;
  if ('U' == ch) return TRUE;

  return FALSE;
}

/* is prefix a prefix of s? */
int strprefix(char *prefix, char *s)
{
  int len = strlen(prefix);
  return
    (strncasecmp(prefix, s, len) == 0);
}

/* WSS - Maybe I'm not understanding something here, but isn't "strmem" just */
/* WSS - a lame version of the standard library function strchr? */

/* is character c a member of string s */
int strmem(char c, char *s)
{
  int i,found=FALSE;
  for(i=0;((i<strlen(s)) && (! found));i++)
    found = (s[i] == c);
  return(found);
}

void calc_weight(void)
{
  int i,weight=0;

  for(i=1;i<MAXITEMS;i++)
    if (Player.possessions[i] != NULL)
      weight += Player.possessions[i]->weight *	Player.possessions[i]->number;
  if ((Player.possessions[O_WEAPON_HAND] != NULL) &&
      (Player.possessions[O_READY_HAND] == Player.possessions[O_WEAPON_HAND]))
    weight -= Player.possessions[O_READY_HAND]->weight *
      Player.possessions[O_READY_HAND]->number;
  for(i=0;i<MAXPACK;i++)
    if (Player.pack[i] != NULL)
      weight += Player.pack[i]->weight *
	Player.pack[i]->number;
  Player.itemweight = weight;
  dataprint();
}

char joinString[256], temp[256];
char *strjoin(char*one, char*two)
{
  assert(one != two);
  if (joinString == one) goto DirectJoin;
  if (joinString == two) {strcpy(temp,two); two = temp;}
  strcpy(joinString,one);
 DirectJoin:
  strcat(joinString,two);
  return joinString;
}

int get_lunarity (void)
{
  int lunarity;
  int moon_state;

  lunarity = 0;
  moon_state = time_phase() / 2;

  if (DRUID == Player.patron)
    {
      if (3 == moon_state || 9 == moon_state)
        lunarity = 1;
      else if (0 == moon_state || 6 == moon_state)
        lunarity = -1;
    }
  else if (Player.alignment > 10)
    {
      if (6 == moon_state)
        lunarity = 1;
      else if (0 == moon_state)
        lunarity = -1;
    }
  else if (Player.alignment < -10)
    {
      if (0 == moon_state)
        lunarity = 1;
      else if (6 == moon_state)
        lunarity = -1;
    }

  return lunarity;
}



object * copy_obj (object * obj)
{
   object * new;
   new = new_object();

   *new = *obj;

   if ((obj->id == CORPSEID) && (obj->level & ALLOC))
     new->objstr = new->cursestr = new->truename = salloc(obj->objstr);

   return new;
}

/* DAG frees object; if flag true, final free, free any allocated corpse string */
void free_obj (pob obj, int flag)
{
  if (flag && (obj->id == CORPSEID) && (obj->level & ALLOC))
    free(obj->objstr);

  delete_object(obj);
}

void free_object (pob obj)
{
  free_obj(obj, TRUE);
}


void free_object_v(void * data)
{
  pob obj = data;
  free_obj ( obj, TRUE);
}

LIST_DECLARE_FREER(fully_free_object_list, free_object_v);


void free_monster (monster * mon)
{
  fully_free_object_list(&mon->possession_list);

  /* DAG free the monstring & corpsestr if allocated */
  if (m_statusp(mon, ALLOC))
    {
      free(mon->monstring);
      free(mon->corpsestr);
    }

  delete_monster(mon);
}

void free_monster_v(void * data)
{
  monster * mon = data;
  if (mon->ticket) ticket_remove(mon->ticket);
  free_monster(mon);
}

LIST_DECLARE_FREER(fully_free_monster_list, free_monster_v);

void mon_alloc_strings( monster * mon, char * name, char * corpse_string )
{
  mon->monstring = salloc(name);
  mon->corpsestr = salloc(corpse_string);
  m_status_set( mon, ALLOC );
}




/* malloc function that checks its return value - if NULL, tries to free */
/* some memory... */
void * checkmalloc (size_t size)
{
  void * ptr;
  int released;

  ptr = malloc(size);
  if (ptr) return ptr;

  released = release_some_lev_memory();
  if(released)
    {
      ptr = malloc(size);
      if (ptr) return ptr;
    }

  print1("Out of memory!  Saving and quitting.");
  morewait();
  save(FALSE, TRUE);
  endgraf();
  exit(0);
}

/* alloc just enough string space for str, strcpy, and return pointer */
char *salloc(char *str)
{
  char *s=checkmalloc((unsigned)(strlen(str)+1));
  strcpy(s,str);
  return(s);
}


#ifdef MSDOS
/* ****Moved here from another file**** */
/* reads a string from a file. If it is a line with more than 80 char's,
   then remainder of line to \n is consumed */
void filescanstring(FILE *fd, char *fstr)
{
  int i= -1;
  int byte='x';
  while ((i<80) && (byte != '\n') && (byte != EOF)) {
    i++;
    byte=fgetc(fd);
    fstr[i] = byte;
  }
  if (byte != '\n')
    while((byte!='\n') && (byte != EOF))
      byte=fgetc(fd);
  fstr[i]=0;
}
#endif

char cryptkey(char *fname)
{
    int pos, key = 0;

    for (pos = 0; fname[pos]; pos++)
	key += 3*(fname[pos] - ' ');
    return (key&0xff);
}

int game_uid;
int user_uid;

void init_perms(void)
{
#if (defined(BSD) || defined(SYSV)) && !defined(__DJGPP__)
    user_uid = getuid();
    game_uid = geteuid();
#endif
}

/*
#ifdef BSD
void setreuid(int, int);
#endif
*/

void change_to_user_perms(void)
{
#if (defined( BSD ) || defined( SYSV )) && !defined(__EMX__) && !defined(__DJGPP__)
#ifdef BSD
    setreuid(game_uid, user_uid);
#else /* SYSV */
    seteuid(user_uid);
#endif /* BSD */
#endif /* BSD || SYSV */
}

void change_to_game_perms(void)
{
#if (defined( BSD ) || defined( SYSV )) && !defined(__EMX__) && !defined(__DJGPP__)
#ifdef BSD
    setreuid(user_uid, game_uid);
#else /* SYSV */
    seteuid(game_uid);
#endif /* BSD */
#endif /* BSD || SYSV */
}

#ifdef NO_USLEEP
void usleep(int usecs)
{
  fd_set null;
  struct timeval timeout;

  FD_ZERO(&null);
  timeout.tv_usec = usecs;
  timeout.tv_sec = 0;
  select(0, &null, &null, &null, &timeout);
}
#endif
