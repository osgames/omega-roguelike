/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/* *** Functions to interactively pick from lists and arrays. *** */

#include "extern.h"
#include "pick.h"

/*** Pick from a generalized list ***/

bool pick_from_list(char * prompt,
		      char * found_none_message,
		      void * p_data,
		      char * buffer,
		      int (*update_count)(void * p_data, char * prefix, int hint),
		      void (*show_known)(void * p_data),
		      char * (*selected_name)(void * p_data))
{
  int pos = 0;
  char byte;
  int count;
  char * prefix = buffer;
  prefix[0] = '\0';
  count = update_count(p_data, prefix, unq_init);
  switch(count)
    {
    default:
      print1(prompt);
      print2("");
      break;

    case 0:
      print3(found_none_message);
      return FALSE;

    case 1:
      print1("Only one possibility:");
      print2(selected_name(p_data));
      break;
    }

  while(TRUE)
    {
      byte = mgetc();
      switch(byte)
	{
	default:
	  {
	    /* If we've already found it, ignore new characters. */
	    if(count == 1)
	      { continue; }

	    prefix[pos++] = byte;
	    prefix[pos] = '\0';

	    count = update_count(p_data, prefix, uniq_prefix_longer);
	    switch(count)
	      {
	      default:
		/* Print just the new character. */
		nprint2(prefix + pos - 1);
		break;

	      case 1:
		/* Found it just now.  Print the whole thing. */
		{
		  char * fullname =
		    selected_name(p_data);
		  nprint2(fullname + pos - 1);
		}
		break;

	      case 0:
		/* No matches.  Undo the add. */
		pos--;
		prefix[pos] = '\0';
		count = update_count(p_data, prefix, uniq_prefix_shorter);
		break;
	      }
	  }
	  break;

#if 0  /* New, currently untested. */
	case '\t':
	  if(count > 1)
	    {
	      /* Get the name of an entry.  Since we're only
		 interested until they diverge, any one will do. */
	      int original_pos = pos;
	      int original_count = count;
	      char * name = selected_name(p_data);
	      /* Add characters while they are common to all
		 entries. */
	      while(name && (count == original_count))
		{
		  prefix[pos] = name[pos];
		  pos++;
		  prefix[pos] = '\0';
		  count = update_count(p_data, prefix, uniq_prefix_longer);
		}
	      /* If we've overshot, back up.  We need only back up 1
		 space at most. */
	      if(count < original_count)
		{
		  prefix[--pos] = '\0';
		  count = update_count(p_data, prefix, uniq_prefix_shorter);
		}
	
	      /* Print any extra characters we got. */
	      if(pos > original_pos)
		{ nprint2(prefix + original_pos - 1); }
	    }
	  break;
#endif

	case BACKSPACE:
        case DELETE:
	  {
	    if (pos > 0)
	      {
		prefix[--pos] = '\0';
		print2(prefix);
		count = update_count(p_data, prefix, uniq_prefix_shorter);
	      }
	    else
	      { print2(""); }
	  }
	  break;
	
	case ESCAPE:
	  return FALSE;

	case '?':
	  { show_known(p_data); }
	  break;
	
	case '\n':
	  if(count == 1)
	    { return TRUE; }
	  else
	    {
	      print3("That is an ambiguous abbreviation!");
	      return FALSE;
	    }
	  break;
	}
    }
}


/*** Pick from an array ***/

typedef struct sorted_uniqueness_helper_t
{
  int    first;
  int    last;
  bool   (*element_test)(int index);
  char * (*element_name)(int index);
  void   (*menuprint_element)(int index);
  char * type_of_elements;
  int    last_index;
} sorted_uniqueness_helper_t;


/* arraypick helper functions. */
char * arraypick_selected_name(void * v_data)
{
  sorted_uniqueness_helper_t * p_data = v_data;
  return p_data->element_name(p_data->first);
}

/* Menuprint all that apply */
void arraypick_show_known(void * v_data)
{
  sorted_uniqueness_helper_t * p_data = v_data;
  set_notice_flags(notice_drew_menu);
  menuclear();
  if(p_data->first > p_data->last)
    {
      menuprint("\nNo ");
      menuprint(p_data->type_of_elements);
      menuprint(" match that prefix!");
    }
  else
    {
      int i;
      menuprint("\nPossible ");
      menuprint(p_data->type_of_elements);
      menuprint(":\n");
      for (i = p_data->first; i <= p_data->last; i++)
	{
	  if(p_data->element_test(i))
	    {
	      if(p_data->menuprint_element)
		{
		  p_data->menuprint_element(i);
		}
	      else
		{
		  menuprint(p_data->element_name(i));
		  menuprint("\n");
		}
	    }
	}
    }
  showmenu();
}

int arraypick_update_count(void * v_data, char * prefix, int hint)
{
  sorted_uniqueness_helper_t * p_data = v_data;
  switch(hint)
    {
    default:
      error("Bad uniq-hint");
      break;

    case unq_init:
      {
	int f = 0;
	while
	  (
	   (f <= p_data->last_index) &&
	   !p_data->element_test(f))
	  { f++; }
	p_data->first = f;
      }
      p_data->last = p_data->last_index;
      break;

    case uniq_prefix_shorter:
      {
	int f, l;
	f = p_data->first;
	while ((f >= 0) && strprefix(prefix, p_data->element_name(f)))
	  {
	    if(p_data->element_test(f))
	      { p_data->first = f; }
	    f--;
	  }
	
	l = p_data->last;
	while ((l <= (p_data->last_index)) &&
	       strprefix(prefix, p_data->element_name(l)))
	  {
	    if(p_data->element_test(l))
	      { p_data->last = l; }
	    l++;
	  }
      }
      break;

    case uniq_prefix_longer:
      {
	int f, l;
	f = p_data->first;
	while ((f < (p_data->last_index)) &&
	       (!p_data->element_test(f)
		||
		!strprefix(prefix, p_data->element_name(f))))
	  { f++; }
	p_data->first = f;

	l = p_data->last;
	while ((l >= f) &&
	       (!p_data->element_test(l)
		||
		!strprefix(prefix, p_data->element_name(l))))
	  { l--; }
	p_data->last = l;
      }
      break;
    }

  return p_data->last + 1 - p_data->first;
}


/* For picking from an array pre-sorted by name.  */
int arraypick(
	      char * prompt,
	      char * found_none_message,
	      bool (*element_test)(int index),
	      char * (*element_name)(int index),
	      void   (*menuprint_element)(int index),
	      char * type_of_elements,
	      int    last_index)
{
  sorted_uniqueness_helper_t data =
  { 0, 0, element_test, element_name, menuprint_element,
    type_of_elements, last_index, };
  char buffer[81];
  bool success =
    pick_from_list(prompt, found_none_message, &data, buffer,
		   arraypick_update_count,
		   arraypick_show_known,
		   arraypick_selected_name);
  if(success)
    {
      return data.first;
    }
  else
    { return ABORT; }
}
