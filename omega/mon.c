/* omega copyright (c) 1987,1988,1989 by Laurence Raphael Brothers */
/* mon.c */
/* various functions to do with monsters */

#include "glob.h"
#include "lev.h"
#include "liboutil/list.h"
#include "memory.h"
#include "ticket.h"
#include "util.h"
#include <limits.h>

/*               Revised function              */
/* WDT: code contributed by David J. Robertson */
/* consider one monster's action */

void m_pulse (pmt m)
{
  int attacked = FALSE;
  int range;
  player_t * p;

  assert(m);

  /* dead monsters can't walk */
  if (m_statusp(m, M_GONE)) return;

  p = &Player;

  /* distance from monster to player */
  if (m->dlevel == p->dlevel)
    range = distance(m->x, m->y, Player.x, Player.y);
  else if (m->dlevel->environment == p->dlevel->environment)
    range = 100 * abs(m->dlevel->depth - p->dlevel->depth); /* big and appropriately increasing */
  else
    range = INT_MAX;

  /* need to wake up? */
  if (!m_statusp(m, AWAKE))
    {
      if (range > m->wakeup) return;

      m_status_set(m, AWAKE);
      set_notice_flags(notice_disturbed_mild);
    }

  /* monster forgets player who leaves the level */
  if (m->dlevel != p->dlevel)
    m_status_set(m, WANDERING);

  /* consider monster action */
  if (m_statusp(m, WANDERING))
    {
      /* wandering monsters move without considering the player */

      if (m_statusp(m, MOBILE))
	m_random_move(m);

      if (range <= m->sense && (m_statusp(m, HOSTILE) || m_statusp(m, NEEDY)))
	m_status_reset(m, WANDERING);
    }
  else if (m_statusp(m, HOSTILE))
    {
      if (range < m->sense)
	{
	  if (range > 1)
	    {
	      /* consider a ranged attack */
	      if (m->strikef != M_NO_OP)
		if (los_p(m->dlevel, m->x, m->y, Player.x, Player.y))
		  if (Player.status[INVISIBLE] == 0 || mstatusp(m, SEE_INVIS))
		    if (random_range(2) == 1)
		      {
			monster_strike(m);
			attacked = TRUE;
		      }

	      /* consider a special attack */
	      if (attacked == FALSE)
		if (m->specialf != N_NO_OP)
		  if (los_p(m->dlevel, m->x, m->y, Player.x, Player.y))
		    if (Player.status[INVISIBLE] == 0 || mstatusp(m, SEE_INVIS))
		      if (random_range(2) == 1)
			{
			  monster_special(m);
			  attacked = TRUE;
			}

	      /* maybe move with respect to player */
	      if (attacked == FALSE)
		if (m_statusp(m, MOBILE))
		  if (random_range(4) != 1)
		    monster_move(m);
	    }
	  else
	    {
	      /* attack player from neighboring space */
	      set_notice_flags(notice_was_attacked);
	      tacmonster(m);
	    }
	}
    }
  else if (m_statusp(m, NEEDY))
    {
      /* maybe follow player */

      if (range > 1)
	if (m_statusp(m, MOBILE))
	  if (random_range(2) == 1)
	    monster_move(m);
    }
  else if (m_statusp(m,GREEDY))
    {
      /* picks up 'treasure' */

      list_t * olist;
      olist = m->dlevel->site[m->x][m->y].object_list;

      if (olist && list_size(olist) > 0)
	m_pickup(m, list_remove_head(olist));

      if (list_size(olist) == 0)
	{
	  list_delete(olist);
	  m->dlevel->site[m->x][m->y].object_list = 0;
	}
    }
  else
    {
#ifdef USE_OPCURSES
      printf("m_pulse() default %s\n", m->monstring);
#endif

      /* how often do we get here? */
      if (m_statusp(m, MOBILE))
	if (random_range(2) == 1)
	  monster_random_move(m);
    }
}

int is_monster_guard (monster_t * mon)
{
  if (GUARD == mon->id) return TRUE;
  if (HISCORE_NPC == mon->id && NPC_JUSTICIAR == mon->aux2) return TRUE;
  return FALSE;
}

/* give object o to monster m */
void m_pickup (monster_t * mon, object_t * obj)
{
  if (0 == mon->possession_list)
    mon->possession_list = list_new();

  list_prepend(mon->possession_list, obj);
}

void give_mon_object_by_id (monster_t * mon, int oid)
{
  assert(oid >= 0);
  assert(oid < TOTALITEMS);

  if (Objects[oid].uniqueness < UNIQUE_TAKEN)
    {
      object_t * obj;

      obj = new_object();
      *obj = Objects[oid];

      if (Objects[oid].uniqueness == UNIQUE_UNMADE)
	Objects[oid].uniqueness = UNIQUE_MADE;

      m_pickup(mon, obj);
    }
}

object_t * m_remove_random_item (monster_t * mon)
{
  int idx;
  int which;
  object * obj;
  list_iterator_t * it;

  if (list_size(mon->possession_list) == 0) return NULL;

  /* remove one of the monster's items */
  which = 1 + random_range(list_size(mon->possession_list));
  it = list_iterator_create(mon->possession_list);

  for (idx = 0; idx < which; ++idx)
    obj = list_iterator_next(it);

  list_iterator_remove(it);
  list_iterator_delete(it);

  if (0 == list_size(mon->possession_list))
    {
      list_delete(mon->possession_list);
      mon->possession_list = 0;
    }

  return obj;
}

void m_dropstuff (monster_t * mon)
{
  while (list_size(mon->possession_list))
    add_object_to_level(mon->dlevel, list_remove_head(mon->possession_list), mon->x, mon->y);

  list_delete(mon->possession_list);
  mon->possession_list = NULL;
}

/* Implement the (non) death of Death */
static void m_kill_death (monster_t * mon)
{
  mprint("Death lies sprawled out on the ground......");
  mprint("Death laughs ironically and gets back to its feet.");
  mprint("It gestures and another scythe appears in its hands.");

  switch (random_range(10))
    {
    case 0:
      mprint("Death performs a little bow and goes back on guard.");
      break;

    case 1:
      mprint("'A hit! A palpable hit!' Death goes back on the attack.");
      break;

    case 2:
      mprint("'Ah, if only it could be so simple!' snickers Death.");
      break;

    case 3:
      mprint("'You think Death can be slain?  What a jest!' says Death.");
      break;

    case 4:
      mprint("'Your point is well taken.' says Death, attacking again.");
      break;

    case 5:
      mprint("'Oh, come now, stop delaying the inevitable.' says Death.");
      break;

    case 6:
      mprint("'Your destiny ends here with me.' says Death, scythe raised.");
      break;

    case 7:
      mprint("'I almost felt that.' says Death, smiling.");
      break;

    case 8:
      mprint("'Timeo Mortis?' asks Death quizzically, 'Not me!'");
      break;

    case 9:
      mprint("Death sighs theatrically. 'They never learn.'");
      break;
    }

  strengthen_death(mon);
}

static object_t * find_justiciar_badge (level_t * lev, int x, int y)
{
  object_t * obj;
  list_iterator_t * it;

  it = list_iterator_create(get_object_list(lev, x, y));
  while (list_iterator_has_next(it))
    {
      obj = list_iterator_next(it);
      if (OB_JUSTICIAR_BADGE == obj->id)
        {
          list_iterator_remove(it);
          goto finish;
        }
    }

  obj = NULL;

 finish:
  list_iterator_delete(it);
  return obj;
}

static monster_t * find_promotable_guard (level_t * lev)
{
  monster_t * mon;
  list_iterator_t * it;

  it = list_iterator_create(get_monster_list(lev));
  while (list_iterator_has_next(it))
    {
      mon = list_iterator_next(it);
      if (GUARD == mon->id)
        goto finish;
    }

  mon = NULL;

 finish:
  list_iterator_delete(it);
  return mon;
}

static void change_into_justiciar (monster_t * guard)
{
  int x, y;
  ticket_t * ticket;

  /* save important stuff */
  x = guard->x;
  y = guard->y;
  ticket = guard->ticket;

  /* change regular guard into a justiciar */
  make_hiscore_npc(guard, NPC_JUSTICIAR);

  /* restore important stuff */
  guard->x = x;
  guard->y = y;
  guard->ticket = ticket;

  m_status_reset(guard, AWAKE);
  m_status_reset(guard, HOSTILE);
}

static void kill_justiciar (monster_t * mon)
{
  object_t * badge = NULL;
  monster_t * guard = NULL;

  /* just a tad complicated. Promote a new justiciar if any */
  /* guards are left in the city, otherwise Destroy the Order! */

  Player.alignment -= 100;

  if (gamestatusp(DESTROYED_ORDER))
    {
      /* how could this happen? */
      mprint("A Servant of Chaos materializes, grabs the corpse,");
      mprint("snickers a bit, and vanishes.");
      return;
    }

  /* Find the Justiciar's Badge which should have been dropped by m_dropstuff() */
  badge = find_justiciar_badge(mon->dlevel, mon->x, mon->y);

  /* promote one of the city guards to be justiciar */
  guard = find_promotable_guard(mon->dlevel);

  mprint("In the distance you hear a trumpet. A Servant of Law");

  if (guard)
    {
      /* Pick up the Justiciar's Badge */
      if (badge)
        {
          mprint("materializes, sheds a tear, picks up the badge, and leaves.");
          m_pickup(guard, badge);
        }
      else
        {
          mprint("materializes, sheds a tear, and leaves.");
        }

      change_into_justiciar(guard);

      strcpy(Justiciar, nameprint());
      Justiciarbehavior = 2911;

      mprint("A new justiciar has been promoted!");
    }
  else
    {
      mprint("materializes, sheds a tear, and leaves.");
      morewait();
    }

  alert_guards(); /* will cause order to be destroyed if no guards or justiciar*/
}

static void kill_hiscore_npc (monster_t * mon)
{
  switch (mon->aux2)
    {
    case NPC_HISCORER:
      mprint("You hear a faroff dirge. You feel a sense of triumph.");
      break;

    case NPC_ODIN: case NPC_SET: case NPC_ATHENA: case NPC_HECATE: case NPC_DRUID: case NPC_DESTINY:
      mprint("You hear a faroff sound like angels crying....");
      strcpy(Priest[mon->aux2], nameprint());
      Priestbehavior[mon->aux2] = 2933;
      break;

    case NPC_SHADOWLORD:
      mprint("A furtive figure dashes out of the shadows, takes a look at");
      mprint("the corpse, and runs away!");
      strcpy(Shadowlord, nameprint());
      Shadowlordbehavior = 2912;
      break;

    case NPC_COMMANDANT:
      mprint("An aide-de-camp approaches, removes the corpse's insignia,");
      mprint("and departs.");
      strcpy(Commandant, nameprint());
      Commandantbehavior = 2912;
      break;

    case NPC_ARCHMAGE:
      mprint("An odd glow surrounds the corpse, and slowly fades.");
      strcpy(Archmage, nameprint());
      Archmagebehavior = 2933;
      break;

    case NPC_PRIME_SORCEROR:
      mprint("A demon materializes, takes a quick look at the corpse,");
      mprint("and teleports away with a faint popping noise.");
      strcpy(Prime, nameprint());
      Primebehavior = 2932;
      break;

    case NPC_ARENA_CHAMPION:
      mprint("A sports columnist rushes forward and takes a quick photo");
      mprint("of the corpse and rushes off muttering about a deadline.");
      strcpy(Champion, nameprint());
      Championbehavior = 2913;
      break;

    case NPC_DUKE:
      mprint("You hear a fanfare in the distance, and feel dismayed.");
      strcpy(Duke, nameprint());
      Dukebehavior = 2911;
      break;

    case NPC_CHAOS_LORD:
      if (Player.alignment > 10)
        mprint("You feel smug.");
      else if (Player.alignment < 10)
        mprint("You feel ashamed.");
      strcpy(Chaoslord, nameprint());
      Chaoslordbehavior = 2912;
      break;

    case NPC_LAW_LORD:
      if (Player.alignment < 10)
        mprint("You feel smug.");
      else if (Player.alignment > 10)
        mprint("You feel ashamed.");
      strcpy(Lawlord, nameprint());
      Lawlordbehavior = 2911;
      break;

    case NPC_JUSTICIAR:
      kill_justiciar(mon);
      break;

    case NPC_GRANDMASTER:
#ifdef TRADEMARK_VIOLATION /* paraphrase of harlan ellison... ? PGM */
      mprint("It doesn't rain this day, anywhere in the known universe.");
#else
      mprint("The universal equilibrium slides down a notch.");
#endif
      strcpy(Grandmaster, nameprint());
      Grandmasterbehavior = 2933;
      break;
    }
}

/* remove a monster */
void m_remove (monster_t * mon)
{
  remove_monster_from_level(mon);
  erase_monster(mon);
}

void m_death (monster_t * m, player_t * killer)
{
  player_t * p;
  p = &Player;

  /* Tell the player about it if he can see the monster */
  if (m->dlevel == p->dlevel)
    if (los_p(m->dlevel, p->x, p->y, m->x, m->y))
      {
	if (COMMON == m->uniqueness)
	  sprintf(Str1, "The %s is dead! ", m->monstring);
	else
	  sprintf(Str1, "%s is dead! ", m->monstring);

	mprint(Str1);
      }

  if (killer != NULL)
    {
      gain_experience(killer, m->xpv);
      calc_melee(killer);
    }

  /* drop possessions */
  m_dropstuff(m);

  /* Death is a special case, because it doesn't really die. */
  if (m->id == DEATH)
    {
      m_kill_death(m);
      return;
    }

  /* won this round of arena combat */
  if (m == Arena_Monster)
    Arena_Victory = TRUE;

  /* (maybe) leave a corpse behind */
  if (random_range(2) || m->uniqueness != COMMON)
    {
      object * corpse;
      corpse = new_object();
      make_corpse(corpse, m);
      add_object_to_level(m->dlevel, corpse, m->x, m->y);
    }

  /* this redraws before justiciar's badge is picked up, so it might draw incorrectly -Tehom */
  m_remove(m);

  /* implement effects of killing various monsters */
  switch (m->id)
    {
    case HISCORE_NPC:
      kill_hiscore_npc(m);
      save_hiscore_npc(m->aux2);
      break;

    case GUARD:
      Player.alignment -= 10;
      if (Current_Environment == E_CITY || Current_Environment == E_VILLAGE)
        alert_guards();
      break;

    case GOBLIN_KING:
      if (!gamestatusp(ATTACKED_ORACLE))
        {
          mprint("You seem to hear a woman's voice from far off:");
          mprint("'Well done! Come to me now....'");
        }
      setgamestatus(COMPLETED_CAVES);
      break;

    case GREAT_WYRM:
      if (!gamestatusp(ATTACKED_ORACLE))
        {
          mprint("A female voice sounds from just behind your ear:");
          mprint("'Well fought! I have some new advice for you....'");
        }
      setgamestatus(COMPLETED_SEWERS);
      break;

    case EATER:
      setgamestatus(KILLED_EATER);
      break;

    case LAWBRINGER:
      setgamestatus(KILLED_LAWBRINGER);
      break;

    case DRAGON_LORD:
      setgamestatus(KILLED_DRAGONLORD);
      break;

    case DEMON_EMP:
      setgamestatus(COMPLETED_VOLCANO);
      if (!gamestatusp(ATTACKED_ORACLE))
        {
          mprint("You feel a soft touch on your shoulder...");
          mprint("You turn around but there is no one there!");
          mprint("You turn back and see a note: 'See me soon.'");
          mprint("The note vanishes in a burst of blue fire!");
        }
      break;

    case ELEM_MASTER:
      if (!gamestatusp(ATTACKED_ORACLE))
        {
          mprint("Words appear before you, traced in blue flame!");
          mprint("'Return to the Prime Plane via the Circle of Sorcerors....'");
        }
      break;
    }

  /* special attacks if a monster is killed in court of */
  /* the high mage or in the dragon's lair */
  if (M_SP_COURT == m->specialf || M_SP_LAIR == m->specialf)
    {
      m_status_set(m, HOSTILE);
      monster_special(m);
    }
}

void m_damage (monster_t * mon, player_t * damager, int dmg, int dtype)
{
  player_t * p;
  p = &Player;

  m_status_set(mon, AWAKE);
  m_status_set(mon, HOSTILE);

  if (m_immunityp(mon, dtype))
    {
      if (mon->dlevel == p->dlevel)
	if (los_p(p->x, p->y, mon->x, mon->y))
	  {
	    if (COMMON == mon->uniqueness)
	      sprintf(Str1, "The %s ignores the attack!", mon->monstring);
	    else
	      sprintf(Str1, "%s ignores the attack!", mon->monstring);

	    mprint(Str1);
	  }
    }
  else
    {
      mon->hp -= dmg;
      if (mon->hp <= 0)
	m_death(mon, damager);
    }
}

void monster_move (monster_t * m)
{
  switch (m->movef)
    {
    case M_MOVE_LEASH:    m_move_leash(m);    break;
    case M_MOVE_SMART:    m_smart_move(m);    break;
    case M_MOVE_ANIMAL:   m_move_animal(m);   break;
    case M_MOVE_FOLLOW:   m_follow_move(m);   break;
    case M_MOVE_NORMAL:   m_normal_move(m);   break;
    case M_MOVE_RANDOM:   m_random_move(m);   break;
    case M_MOVE_SPIRIT:   m_spirit_move(m);   break;
    case M_MOVE_FLUTTER:  m_flutter_move(m);  break;
    case M_MOVE_SCAREDY:  m_scaredy_move(m);  break;
    case M_MOVE_CONFUSED: m_confused_move(m); break;
    case M_MOVE_TELEPORT: m_teleport(m);      break;
    default:              m_no_op(m);         break;
    }
}

void monster_strike (monster_t * m)
{
  player_t * p;
  p = &Player;

  if (player_on_sanctuary(p))
    {
      print1("The aegis of your deity protects you!");
      return;
    }

  set_notice_flags(notice_was_attacked);

  /* It's lawful to wait to be attacked */
  if (m->attacked == 0)
    ++(p->alignment);

  m->attacked = 1;

  switch (m->strikef)
    {
    case M_STRIKE_BLIND:    m_blind_strike(m); break;
    case M_STRIKE_FBALL:    m_fireball(m);     break;
    case M_STRIKE_FBOLT:    m_firebolt(m);     break;
    case M_STRIKE_LBALL:    m_lball(m);        break;
    case M_STRIKE_SONIC:    m_strike_sonic(m); break;
    case M_STRIKE_MISSILE:  m_nbolt(m);        break;
    case M_STRIKE_SNOWBALL: m_snowball(m);     break;
    default:                m_no_op(m);        break;
    }
}

void monster_special (monster_t * m)
{
  player_t * p;
  p = &Player;

  if (m_statusp(m,HOSTILE))
    {
      /* since many special functions are really attacks, cancel them all if on sanctuary */

      if (player_on_sanctuary(p))
	{
	  print1("The aegis of your deity protects you!");
	  return;
	}

      set_notice_flags(notice_was_attacked);
    }

  switch (m->specialf)
    {
    case M_SP_AV:            m_sp_av(m);            break;
    case M_SP_LW:            m_sp_lw(m);            break;
    case M_SP_MB:            m_sp_mb(m);            break;
    case M_SP_MP:            m_sp_mp(m);            break;
    case M_SP_BOG:           m_sp_bogthing(m);      break;
    case M_SP_HUGE:          m_huge_sounds(m);      break;
    case M_SP_LAIR:          m_sp_lair(m);          break;
    case M_SP_WERE:          m_sp_were(m);          break;
    case M_SP_ANGEL:         m_sp_angel(m);         break;
    case M_SP_COURT:         m_sp_court(m);         break;
    case M_SP_DEMON:         m_sp_demon(m);         break;
    case M_SP_EATER:         m_sp_eater(m);         break;
    case M_SP_GHOST:         m_sp_ghost(m);         break;
    case M_SP_PRIME:         m_sp_prime(m);         break;
    case M_SP_RAISE:         m_sp_raise(m);         break;
    case M_SP_SPELL:         m_sp_spell(m);         break;
    case M_SP_SWARM:         m_sp_swarm(m);         break;
    case M_SP_THIEF:         m_thief_f(m);          break;
    case M_SP_ESCAPE:        m_sp_escape(m);        break;
    case M_SP_MIRROR:        m_sp_mirror(m);        break;
    case M_SP_SUMMON:        m_summon(m);           break;
    case M_SP_EXPLODE:       m_sp_explode(m);       break;
    case M_SP_FLUTTER:       m_flutter_move(m);     break;
    case M_SP_SERVANT:       m_sp_servant(m);       break;
    case M_SP_BLACKOUT:      m_sp_blackout(m);      break;
    case M_SP_ILLUSION:      m_illusion(m);         break;
    case M_SP_MERCHANT:      m_sp_merchant(m);      break;
    case M_SP_SEDUCTOR:      m_sp_seductor(m);      break;
    case M_SP_SURPRISE:      m_sp_surprise(m);      break;
    case M_SP_AGGRAVATE:     m_aggravate(m);        break;
    case M_SP_ACID_CLOUD:    m_sp_acid_cloud(m);    break;
    case M_SP_DRAGONLORD:    m_sp_dragonlord(m);    break;
    case M_SP_DEMONLOVER:    m_sp_demonlover(m);    break;
    case M_SP_POISON_CLOUD:  m_sp_poison_cloud(m);  break;
    case M_SP_WHISTLEBLOWER: m_sp_whistleblower(m); break;
    default:                 m_no_op(m);            break;
    }
}

void monster_talk (monster_t * m)
{
  switch (m->talkf)
    {
    case M_TALK_EF:         m_talk_ef(m);         break;
    case M_TALK_GF:         m_talk_gf(m);         break;
    case M_TALK_IM:         m_talk_im(m);         break;
    case M_TALK_MP:         m_talk_mp(m);         break;
    case M_TALK_BEG:        m_talk_beg(m);        break;
    case M_TALK_MAN:        m_talk_man(m);        break;
    case M_TALK_EVIL:       m_talk_evil(m);       break;
    case M_TALK_HINT:       m_talk_hint(m);       break;
    case M_TALK_DRUID:      m_talk_druid(m);      break;
    case M_TALK_GUARD:      m_talk_guard(m);      break;
    case M_TALK_HORSE:      m_talk_horse(m);      break;
    case M_TALK_HYENA:      m_talk_hyena(m);      break;
    case M_TALK_MIMSY:      m_talk_mimsy(m);      break;
    case M_TALK_NINJA:      m_talk_ninja(m);      break;
    case M_TALK_PRIME:      m_talk_prime(m);      break;
    case M_TALK_ROBOT:      m_talk_robot(m);      break;
    case M_TALK_THIEF:      m_talk_thief(m);      break;
    case M_TALK_STUPID:     m_talk_stupid(m);     break;
    case M_TALK_ANIMAL:     m_talk_animal(m);     break;
    case M_TALK_BURBLE:     m_talk_burble(m);     break;
    case M_TALK_GREEDY:     m_talk_greedy(m);     break;
    case M_TALK_HUNGRY:     m_talk_hungry(m);     break;
    case M_TALK_PARROT:     m_talk_parrot(m);     break;
    case M_TALK_SCREAM:     m_talk_scream(m);     break;
    case M_TALK_SILENT:     m_talk_silent(m);     break;
    case M_TALK_SLITHY:     m_talk_slithy(m);     break;
    case M_TALK_TITTER:     m_talk_titter(m);     break;
    case M_TALK_SERVANT:    m_talk_servant(m);    break;
    case M_TALK_ARCHMAGE:   m_talk_archmage(m);   break;
    case M_TALK_ASSASSIN:   m_talk_assassin(m);   break;
    case M_TALK_MAHARAJA:   m_talk_maharaja(m);   break;
    case M_TALK_MERCHANT:   m_talk_merchant(m);   break;
    case M_TALK_SEDUCTOR:   m_talk_seductor(m);   break;
    case M_TALK_DEMONLOVER: m_talk_demonlover(m); break;
    default:                m_no_op(m);           break;
    }
}

/* makes one of the highscore npcs */
void make_hiscore_npc (monster_t * npc, int npcid)
{
  int st = -1;
  player_t * p;

  p = &Player;

  *npc = Monsters[HISCORE_NPC];
  npc->aux2 = npcid;

  /* each of the high score npcs can be created here */
  switch (npcid)
    {
    case NPC_HISCORER:
      strcpy(Str2, Hiscorer);
      determine_npc_behavior(npc, Hilevel, Hibehavior);
      break;

    case NPC_SET:
    case NPC_ODIN:
    case NPC_DRUID:
    case NPC_ATHENA:
    case NPC_HECATE:
    case NPC_DESTINY:
      strcpy(Str2, Priest[npcid]);
      determine_npc_behavior(npc, Priestlevel[npcid], Priestbehavior[npcid]);
      st = SYMBOLID + npcid; /* appropriate holy symbol... */

      if (npcid == DRUID)
        npc->talkf = M_TALK_DRUID;

      if (p->patron == npcid)
        m_status_reset(npc, HOSTILE);
      break;

    case NPC_SHADOWLORD:
      strcpy(Str2, Shadowlord);
      determine_npc_behavior(npc, Shadowlordlevel, Shadowlordbehavior);
      break;

    case NPC_COMMANDANT:
      strcpy(Str2, Commandant);
      determine_npc_behavior(npc, Commandantlevel, Commandantbehavior);
      if (p->rank[LEGION])
        m_status_reset(npc, HOSTILE);
      break;

    case NPC_ARCHMAGE:
      strcpy(Str2, Archmage);
      determine_npc_behavior(npc, Archmagelevel, Archmagebehavior);
      st = OB_KOLWYNIA; /* kolwynia */
      npc->talkf = M_TALK_ARCHMAGE;
      m_status_reset(npc, WANDERING);
      m_status_reset(npc, HOSTILE);
      break;

    case NPC_PRIME_SORCEROR:
      strcpy(Str2, Prime);
      determine_npc_behavior(npc, Primelevel, Primebehavior);
      npc->talkf = M_TALK_PRIME;
      npc->specialf = M_SP_PRIME;
      if (p->alignment < 0)
        m_status_reset(npc, HOSTILE);
      break;

    case NPC_ARENA_CHAMPION:
      strcpy(Str2, Champion);
      determine_npc_behavior(npc, Championlevel, Championbehavior);
      if (p->rank[ARENA])
        m_status_reset(npc, HOSTILE);
      break;

    case NPC_DUKE:
      strcpy(Str2, Duke);
      determine_npc_behavior(npc, Dukelevel, Dukebehavior);
      break;

    case NPC_CHAOS_LORD:
      strcpy(Str2, Chaoslord);
      determine_npc_behavior(npc, Chaoslordlevel, Chaoslordbehavior);
      if (p->alignment < 0 && random_range(2))
        m_status_reset(npc, HOSTILE);
      break;

    case NPC_LAW_LORD:
      strcpy(Str2, Lawlord);
      determine_npc_behavior(npc, Lawlordlevel, Lawlordbehavior);
      if (p->alignment > 0)
        m_status_reset(npc, HOSTILE);
      break;

    case NPC_JUSTICIAR:
      strcpy(Str2, Justiciar);
      determine_npc_behavior(npc, Justiciarlevel, Justiciarbehavior);
      st = OB_JUSTICIAR_BADGE;	/* badge */
      npc->talkf = M_TALK_GUARD;
      npc->specialf = M_SP_WHISTLEBLOWER;
      m_status_reset(npc, WANDERING);
      m_status_reset(npc, HOSTILE);
      break;

    case NPC_GRANDMASTER:   /* PGM TODO */
      strcpy(Str2, Grandmaster);
      determine_npc_behavior(npc, Grandmasterlevel, Grandmasterbehavior);
      break;
    }

  m_pickup_by_id(npc, st);

  npc->monstring = salloc(Str2);
  strcpy(Str1,"The body of ");
  strcat(Str1,Str2);
  npc->corpsestr = salloc(Str1);
  m_status_set(npc, ALLOC);
}

/* sets npc behavior given level and behavior code */
void determine_npc_behavior (monster_t * npc, int level, int behavior)
{
  int combatype,competence,talktype;
  npc->hp = (level+1)*20;
  npc->status = AWAKE+MOBILE+WANDERING;
  combatype = (behavior % 100) / 10;
  competence = (behavior % 1000) / 100;
  talktype = behavior / 1000;
  npc->level = competence;
  if (npc->level < 2*difficulty()) npc->status += HOSTILE;
  npc->xpv = npc->level*20;
  switch (combatype) {
  case 1: /* melee */
    npc->meleef = M_MELEE_NORMAL;
    npc->dmg = competence*5;
    npc->hit = competence*3;
    npc->speed = 3;
    break;
  case 2: /*missile*/
    npc->meleef = M_MELEE_NORMAL;
    npc->strikef = M_STRIKE_MISSILE;
    npc->dmg = competence*3;
    npc->hit = competence*2;
    npc->speed = 4;
    break;
  case 3: /* spellcasting */
    npc->meleef = M_MELEE_NORMAL;
    npc->dmg = competence;
    npc->hit = competence;
    npc->specialf = M_SP_SPELL;
    npc->speed = 6;
    break;
  case 4: /* thievery */
    npc->meleef = M_MELEE_NORMAL;
    npc->dmg = competence;
    npc->hit = competence;
    npc->specialf=M_SP_THIEF;
    npc->speed = 3;
    break;
  case 5: /* flee */
    npc->dmg = competence;
    npc->hit = competence;
    npc->meleef = M_MELEE_NORMAL;
    npc->specialf = M_MOVE_SCAREDY;
    npc->speed = 3;
    break;
  }
  if (npc->talkf == M_TALK_MAN)
    switch (talktype) {
    case 1: npc->talkf = M_TALK_EVIL; break;
    case 2: npc->talkf = M_TALK_MAN; break;
    case 3: npc->talkf = M_TALK_HINT; break;
    case 4: npc->talkf = M_TALK_BEG; break;
    case 5: npc->talkf = M_TALK_SILENT; break;
    default: mprint("Say Whutt? (npc talk weirdness)"); break;
    }
  npc->uniqueness = UNIQUE_MADE;
}

/* makes an ordinary npc (maybe undead) */
void make_log_npc(pmt npc)
{
  int i,n;
  int behavior,status,level;
  FILE *fd;

  /* in case the log file is null */
  behavior = 2718;
  level = 1;
  status = 2;
  strcpy(Str2,"Malaprop the Misnamed");

  strcpy(Str1,Omegavar);
  strcat(Str1,"omega.log");
  fd = checkfopen(Str1,"r");
  n = 1;
  while(fgets(Str1,STRING_LEN, fd)) {
    if (random_range(n) == 0) {	/* this algo. from Knuth 2 - cute, eh? */
      sscanf(Str1,"%d %d %d",&status,&level,&behavior);
      for (i = 0; (Str1[i] < 'a' || Str1[i] > 'z') &&
	(Str1[i] < 'A' || Str1[i] > 'Z'); i++)
	;
      strcpy(Str2, Str1 + i);
      Str2[strlen(Str2) - 1] = '\0';	/* 'cos fgets reads in the \n */
    }
    n++;
  }
  fclose(fd);
  if (status==1) {
    if (level < 3) {
      *npc = Monsters[GHOST];
      strcpy(Str1,"ghost named ");
    }
    else if (level < 7) {
      *npc = Monsters[HAUNT];
      strcpy(Str1,"haunt named ");
    }
    else if (level < 12) {
      *npc = Monsters[SPECTRE];
      strcpy(Str1,"spectre named ");
    }
    else {
      *npc = Monsters[LICHE];
      strcpy(Str1,"lich named ");
    }
    strcat(Str1,Str2);
    npc->monstring = salloc(Str1);
    strcpy(Str3,"the mortal remains of ");
    strcat(Str3,Str2);
    npc->corpsestr = salloc(Str3);
    m_status_set( npc, ALLOC );
  }
  else {
    *npc = Monsters[NPC];
    npc->hp = level*20;
    npc->monstring=salloc(Str2);
    strcpy(Str3,"the corpse of ");
    strcat(Str3,Str2);
    npc->corpsestr = salloc(Str3);
    m_status_set( npc, ALLOC );
  }
  determine_npc_behavior(npc,level,behavior);
}

static int m_walkon_aux (monster_t * m, char * description, int istrap)
{
  player_t * p;
  p = &Player;

  if (m->dlevel != p->dlevel) return FALSE;
  if (m_statusp(m, M_INVISIBLE)) return FALSE;
  if (los_p(p->dlevel, p->x, p->y, m->x, m->y) == FALSE) return FALSE;

  if (m->uniqueness == COMMON)
    sprintf(Str1, "The %s ", m->monstring);
  else
    sprintf(Str1, "%s ", m->monstring);

  strcat(Str1, description);
  mprint(Str1);

  if (trap)
    {
      m->dlevel->site[m->x][m->y].locchar = TRAP;
      lset(m->dlevel, m->x, m->y, CHANGED);
    }

  return TRUE;
}

void m_trap_dart (monster_t * m)
{
  if (m_statusp(m, FLYING) || m_statusp(m, INTANGIBLE)) return;
  m_walkon_aux(m, "was hit by a dart!", TRUE);
  m_damage(m, NULL, 2 * difficulty(), NORMAL_DAMAGE);
}

void m_trap_pit (monster_t * m)
{
  if (m_statusp(m, FLYING) || m_statusp(m, INTANGIBLE)) return;
  m_walkon_aux(m, "fell into a pit!", TRUE);
  m_status_reset(m, MOBILE);
  m_damage(m, NULL, 5 * difficulty(), NORMAL_DAMAGE);
}

void m_trap_door (monster_t * m)
{
  if (m_statusp(m, FLYING) || m_statusp(m, INTANGIBLE)) return;
  m_walkon_aux(m, "fell into a trap door!", TRUE);
  m_remove(m);
}

void m_trap_abyss (monster_t * m)
{
  if (m_statusp(m, FLYING) || m_statusp(m, INTANGIBLE)) return;
  m_walkon_aux(m, "fell into the infinite abyss!", FALSE);

  /* is a trap, but changes floor differently */
  m->dlevel->site[m->x][m->y].locchar = ABYSS;
  m->dlevel->site[m->x][m->y].p_locf = L_ABYSS;
  lset(m->dlevel, m->x, m->y, CHANGED);

  m_remove(m);
}

void m_trap_snare (monster_t * m)
{
  if (m_statusp(m, FLYING) || m_statusp(m, INTANGIBLE)) return;
  m_walkon_aux(m, "was caught in a snare!", TRUE);
  m_status_reset(m, MOBILE);
}

void m_trap_blade (monster_t * m)
{
  if (m_statusp(m, FLYING) || m_statusp(m, INTANGIBLE)) return;
  m_walkon_aux(m, "was hit by a blade trap!", TRUE);
  m_damage(m, NULL, 7 * (difficulty() + 1), NORMAL_DAMAGE);
}

void m_trap_fire (monster_t * m)
{
  if (m_statusp(m, FLYING) || m_statusp(m, INTANGIBLE)) return;
  m_walkon_aux(m, "set off a fire trap!", FALSE);

  /* is a trap, but changes floor differently */
  lev->site[Player.x][Player.y].locchar = FIRE;
  lev->site[Player.x][Player.y].p_locf = L_FIRE;
  lset(lev, Player.x, Player.y, CHANGED);

  if (m_immunityp(m, FLAME)) return;
  m_damage(m, NULL, 5 * (difficulty() + 1), FLAME);
}

void m_fire (monster_t * m)
{
  if (m_statusp(m, INTANGIBLE)) return;

  if (m_immunityp(m, FLAME))
    {
      m_walkon_aux(m, "moved unaffected into fire!", FALSE);
      return;
    }

  m_walkon_aux(m, "was blasted by fire!", FALSE);
  m_damage(m, NULL, random_range(100), FLAME);
}

void m_trap_teleport (monster_t * m)
{
  if (m_statusp(m, FLYING) || m_statusp(m, INTANGIBLE)) return;
  m_walkon_aux(m, "was teleported away!", TRUE);
  m_teleport(m);
}

void m_trap_disintegrate (monster_t * m)
{
  if (m_statusp(m, FLYING) || m_statusp(m, INTANGIBLE)) return;
  m_walkon_aux(m, "was disintegrated!", TRUE);
  disintegrate(m->x, m->y);
}

void m_trap_sleepgas (monster_t * m)
{
  if (m_statusp(m, FLYING) || m_statusp(m, INTANGIBLE)) return;
  if (m_immunityp(m, SLEEP)) return;
  m_walkon_aux(m, "fell asleep!", TRUE);
  m_status_reset(m, AWAKE);
}

void m_trap_acid (monster_t * m)
{
  if (m_statusp(m, FLYING) || m_statusp(m, INTANGIBLE)) return;
  m_walkon_aux(m, "was sprayed by acid!", TRUE);
  m_damage(m, NULL, random_range(difficulty() * difficulty()), ACID);
}

void m_trap_manadrain (monster_t * m)
{
  if (m_statusp(m, FLYING) || m_statusp(m, INTANGIBLE)) return;
  m_walkon_aux(m, "was bathed in a weird rainbow light!", TRUE);
  if (m->specialf != M_SP_SPELL) return;
  m->specialf = M_NO_OP;
}

void m_water (monster_t * m)
{
  if (m_statusp(m, FLYING) || m_statusp(m, INTANGIBLE)) return;

  if (m_statusp(m, SWIMMING) || m_statusp(m, ONLYSWIM))
    {
      m_walkon_aux(m, "swims through the water!", FALSE);
      return;
    }

  m_walkon_aux(m, "fell in the water and drowned!", FALSE);
  m_death(m, NULL);
}

void m_abyss (monster_t * m)
{
  if (m_statusp(m, FLYING) || m_statusp(m, INTANGIBLE)) return;
  m_walkon_aux(m, "fell into the infinite abyss!", FALSE);
  m_remove(m);
}

void m_lava (monster_t * m)
{
  if (m_statusp(m, FLYING) || m_statusp(m, INTANGIBLE)) return;

  if (m_immunityp(m, FLAME))
    {
      m_walkon_aux(m, "moved unaffected into a pool of lava!", FALSE);
      return;
    }

  m_walkon_aux(m, "died in a pool of lava!", FALSE);
  m_death(m, NULL);
}

enum altar_reaction { ignore_monster, help_monster, hurt_monster };

static void m_altar_reaction (enum altar_reaction reaction, int visible)
{
  switch (reaction)
    {
    case ignore_monster:
      if (visible)
	mprint("but nothing much seems to happen");
      break;

    case hurt_monster:
      if (visible)
	{
	  mprint("Your deity is angry!");
	  mprint("A bolt of godsfire strikes the monster....");
	}
      disrupt(m->x, m->y, 50 * p->rank[PRIESTHOOD]);
      break;

    case help_monster:
      if (visible)
	{
	  mprint("The deity of the altar smiles on the monster....");
	  mprint("A shaft of light zaps the altar...");
	}
      m->hp = 2 * Monsters[m->id].hp;
      break;
    }
}

void m_altar (monster_t * m)
{
  int deity;
  int visible;
  player_t * p;
  enum altar_reaction reaction = ignore_monster;

  p = &Player;

  deity = lev->site[m->x][m->y].aux;
  visible = m_walkon_aux(m, "walks next to an altar...", FALSE);

  if (m_statusp(m, HOSTILE))
    {
      if (m->id == HISCORE_NPC && m->aux2 == deity) /* high priest of same deity */
	{
	  reaction = help_monster;
	}
      else if (m->id == ANGEL || m->id == HIGH_ANGEL || m->id == ARCHANGEL) /* angel of the same deity */
	{
	  if (m->aux1 == deity)
	    reaction = help_monster;
	}
      else if (p->patron == deity)
	{
	  reaction = hurt_monster;
	}
      else if (p->patron == ODIN || p->patron == ATHENA)
	{
	  if (deity == SET || deity == HECATE)
	    reaction = help_monster;
	}
      else if (p->patron == SET || p->patron == HECATE)
	{
	  if (deity == ODIN || deity == ATHENA)
	    reaction = help_monster;
	}
    }

  m_altar_reaction(reaction, visible);
}

char * mancorpse (void)
{
  switch (random_range(21))
    {
    case 0:  return "dead janitor";
    case 1:  return "dead beggar";
    case 2:  return "dead barbarian";
    case 3:  return "dead hairdresser";
    case 4:  return "dead accountant";
    case 5:  return "dead lawyer";
    case 6:  return "dead indian chief";
    case 7:  return "dead tinker";
    case 8:  return "dead tailor";
    case 9:  return "dead soldier";
    case 10: return "dead spy";
    case 11: return "dead doctor";
    case 12: return "dead miner";
    case 13: return "dead noble";
    case 14: return "dead serf";
    case 15: return "dead neer-do-well";
    case 16: return "dead vendor";
    case 17: return "dead dilettante";
    case 18: return "dead surveyor";
    case 19: return "dead hacker";
    default: return "dead jongleur";
    }
}

char * angeltype (int mid, int deity)
{
   if (ANGEL == mid)
   {
     switch (deity)
     {
     case ODIN:    return "angel of Odin";
     case SET:     return "angel of Set";
     case HECATE:  return "angel of Hecate";
     case ATHENA:  return "angel of Athena";
     case DESTINY: return "angel of Destiny";
     case DRUID:   return "angel of the Balance";
     }
   }
   else if (HIGH_ANGEL == mid)
   {
     switch (deity)
     {
     case ODIN:    return "high angel of Odin";
     case SET:     return "high angel of Set";
     case HECATE:  return "high angel of Hecate";
     case ATHENA:  return "high angel of Athena";
     case DESTINY: return "high angel of Destiny";
     case DRUID:   return "high angel of the Balance";
     }
   }
   else /* ARCHANGEL */
   {
     switch (deity)
     {
     case ODIN:    return "archangel of Odin";
     case SET:     return "archangel of Set";
     case HECATE:  return "archangel of Hecate";
     case ATHENA:  return "archangel of Athena";
     case DESTINY: return "archangel of Destiny";
     case DRUID:   return "archangel of the Balance";
     }
   }

  /* And, if none of those work, this function's broken -- I'm gonna die. */
  return "angel of Death";
}

void strengthen_death (monster_t * m)
{
  m->xpv   += min(10000, m->xpv + 1000);
  m->hit   += min(1000, m->hit + 10);
  m->dmg    = min(10000, 2 * m->dmg);
  m->ac    += min(1000, m->ac + 10);
  m->speed  = max(m->speed - 1, 1);
  m->movef  = M_MOVE_SMART;
  m->hp     = min(30000, 10 * m->dmg + 100);

  m_pickup_by_id(m, OB_SCYTHE_DEATH);
}

void m_no_op (monster_t * m)
{
}

/* end */
