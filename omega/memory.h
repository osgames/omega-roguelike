/*
 * memory.h
 * omega : Copyright (c) 1987-2001 by Laurence Raphael Brothers
 */

#ifndef INCLUDED_MEMORY_H
#define INCLUDED_MEMORY_H

#include "defs.h"

monster * monster_utility (monster * m);
#define new_monster() monster_utility(0)
#define delete_monster(m) monster_utility(m)

object * object_utility (object * o);
#define new_object() object_utility(0)
#define delete_object(o) object_utility(o)

account * account_utility (account * a);
#define new_account() account_utility(0)
#define delete_account(a) account_utility(a)

void print_allocation_info (void);

#endif /* INCLUDED_MEMORY_H */
