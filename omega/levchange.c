/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/*  Manage environment change and level change. */

#include "glob.h"
#include "lev.h"
#include "load.h"
#include "game_time.h"
#include "save.h"
#include "findloc.h"

/*** Internal declarations. ***/
static void change_environment_aux(int new_idx, int entry_style);


/*** The environment stack  ***/
enum { max_env_stack = 4, };

env_params_t env_stack[max_env_stack];
int env_stack_current = -1;

env_params_t * current_env_params(void)
{
  assert(env_stack_current >= 0);
  assert(env_stack_current < max_env_stack);
  return &env_stack[env_stack_current];
}


/*** Level change interface ***/

/* Change from one level to another level in the same dungeon.  Legacy
   interface. */ 
void change_level (char fromlevel, char tolevel, char rewrite_level)
{
  int style;
  /* Only occurs when called from flux */
  if(rewrite_level)
    {
      style = lem_rewrite;
    }
  else
  switch(tolevel - fromlevel)
    {
    default: 
      style = lem_teleport;
      break;

      case 1:
	style = lem_enter;
	break;

      case -1:
	style = lem_emerge;
	break;
    }
    
  change_level_aux ( tolevel, style);
}

/* Change to TOLEVEL in the current dungeon.  Only fires if player is
   in fact in the dungeon.  (Could easily force a change to said
   dungeon, but let's not)
*/
void change_level_aux ( char tolevel, int entry_style)
{
  env_params_t * p_env_params = current_env_params();
  if(p_env_params->env_idx == Current_Dungeon)
    {
      p_env_params->subtype = tolevel;
      put_player_in_level ( p_env_params, entry_style);
    }
}



/***  Environment change interface  ***/

/* Put us in NEW_ENVIRONMENT if it encloses the current one.  TRUE if
   we entered or were already in that environment, FALSE otherwise.

   This does not do any game effects or messages.  That is the
   caller's responsibility.  */
int try_to_emerge_to_environment(char new_environment)
{
  int i;
  /*  Find a match, if possible.  */
  for(i = env_stack_current; i >= 0; i--)
    {
      env_params_t * p_rpos = &env_stack[i];
      /* Found it.  */
      if(p_rpos->env_idx == new_environment)
	{
	  /* Emerge to it if we're not already there. */
	  if(i < env_stack_current )
	    { change_environment_aux(i, lem_emerge); }
	  return TRUE;
	}
    }
  return FALSE;
}     


void teleport_to_random_in_country(void)
{
  int x, y;
  find_random_country_loc( &x, &y);
  pop_environment_to_country(x, y);
}

/* Force environment to the shallowest level, reset location.  */
void pop_environment_to_country(int x, int y)
{
  if(env_stack_current > 0)
  {
    change_environment_aux(0, lem_other);
  }
  setPlayerXY(x, y);
}     

/* Pop one level of environment, doing game effects.  */
void change_environment_emerge(void)
{
  if(env_stack_current > 0)
    {
      change_environment_aux(env_stack_current - 1, lem_emerge);
      player_emerge_to_current_environment();
    }
}

/*  Change to a new environment, pushing the old one.  */
void change_environment_push(char new_environment, int subtype)
{
  int new_env_stack_current = env_stack_current + 1;
  env_params_t * p_new_env_params = &env_stack[new_env_stack_current];

  /*  Set up what it is.  */
  p_new_env_params->env_idx = new_environment;
  p_new_env_params->subtype     = subtype;

  /*  Load it.  */
  change_environment_aux( new_env_stack_current, lem_enter );
  player_enter_current_environment();
}


/*** Internal environment change ***/

/* Change to the environment/subtype of the env_params_t
   corresponding to new_idx, which must already have those fields set.

   There are no messages here, because we may change to an environment
   for different reasons (enter_site vs l_house_exit vs send_to_jail).
   entry_style says whether and how to set the player's position.
*/
void change_environment_aux(int new_idx, int entry_style)
{
  static int changing_environments = FALSE;
  assert(env_stack_current < max_env_stack);
  assert(new_idx < max_env_stack);
  assert(new_idx >= 0);
  assert(new_idx != env_stack_current);

  /* Don't allow re-entrance on this code. */
  assert(!changing_environments);
  changing_environments = TRUE;

  /* Settle the old environment, if it exists.  For env_stack_current == -1,
     there is no older environment.  */
  if(env_stack_current >= 0)
    { 
      env_params_t * p_old_env_params = current_env_params();
      p_old_env_params->x = Player.x;
      p_old_env_params->y = Player.y;
    }

  /* Unwind all intervening environments. */
  for( ; env_stack_current > new_idx; env_stack_current--)
    { player_leave_current_environment(); }

  /* env_stack_current still needs to be set because new env may be
     deeper.  */
  env_stack_current = new_idx;
  
  /* ** Now we're in the new environment ** */
  {
    env_params_t * p_new_env_params = &env_stack[new_idx];
    Current_Environment = p_new_env_params->env_idx;

    /*  Load it. */
    put_player_in_level(p_new_env_params, entry_style);
  }

  /* We're done */
  changing_environments = FALSE;
}


/**** Save and restore ****/

static int save_env_params(FILE *fd, env_params_t * p_env_params)
{
  int ok = 1;
  SAVE_SCALAR(int, p_env_params->env_idx);
  SAVE_SCALAR(int, p_env_params->subtype);
  SAVE_SCALAR(int, p_env_params->x);
  SAVE_SCALAR(int, p_env_params->y);
  return ok;
}

static void restore_env_params(FILE *fd, env_params_t * p_env_params)
{
  RESTORE_SCALAR(int, p_env_params->env_idx);
  RESTORE_SCALAR(int, p_env_params->subtype);
  RESTORE_SCALAR(int, p_env_params->x);
  RESTORE_SCALAR(int, p_env_params->y);
  p_env_params->p_env_loader = 
    env_loader_find( p_env_params->env_idx, env_loader_table );
}

/* The env stack object.  */
int save_env_params_stack(FILE *fd)
{
  int ok = 1;
  int i;
  ok &= save_check(fd, "envs");
  SAVE_SCALAR(int, env_stack_current);  
  for(i = 0; i <= env_stack_current; i++)
    {
      ok &= save_env_params(fd,&env_stack[i]);
    }
  return ok;
}     

void restore_env_params_stack(FILE *fd)
{
  int i;
  restore_check(fd, "envs");
  RESTORE_SCALAR(int, env_stack_current);  
  for(i = 0; i <= env_stack_current; i++)
    {
      restore_env_params(fd, &env_stack[i]);
    }
}

