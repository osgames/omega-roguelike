/* omega copyright (c) 1987-2001 by Laurence Raphael Brothers */
/* ticket.h */

#ifndef INCLUDED_TICKET_H
#define INCLUDED_TICKET_H

#include "liboutil/list.h"

struct ticket;
typedef struct ticket ticket_t;

typedef void (* ticket_handler_t) (ticket_t *);

/**********************************************/

ticket_t * ticket_new_oneshot (ticket_handler_t handler);
ticket_t * ticket_new_periodic (ticket_handler_t handler, unsigned long period);

/**********************************************/

void ticket_add_argument (ticket_t * ticket, void * arg);
int ticket_has_next_argument (ticket_t * ticket);
void * ticket_next_argument (ticket_t * ticket);
void ticket_reset_arguments (ticket_t * ticket);
list_t * ticket_get_argument_list (ticket_t * ticket);

/**********************************************/

unsigned long get_current_ticket_time (void);
unsigned long get_ticket_duration (ticket_t * ticket);

/**********************************************/

ticket_t * ticket_find (ticket_t * ticket);

void ticket_remove (ticket_t * ticket);
void ticket_schedule (ticket_t * ticket, unsigned long when_happens);
void ticket_dispatch (void);

void ticket_process_for_seconds (unsigned long seconds);
void ticket_process_for_minutes (unsigned long minutes);
void ticket_process_for_hours (unsigned long hours);

void ticket_process_until_quit (void);
void ticket_quit (void);

#endif /* INCLUDED_TICKET_H */
