;;; declare-omega.el --- Elisp declarations supporting loaders.el

;; Copyright (C) 2001 by Tom Breton

;; Author: Tom Breton <tob@world.std.com>
;; Keywords: lisp

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; This is a separate file so that if this data is later supported by
;; some other tool, this code can be shared or not.

;;; Code:


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;To make the symbols that are shared with C.

(defmacro def-symbols (name list &optional docstring)
   ""
   
   `
   (progn
      ,@(loop
	   for sym in list 
	   collect 
	   (if
	      (symbolp sym)
	      `(defconst ,sym ',sym)
	      `(defconst ,(second sym) ',(second sym))))
       
      (defconst ,name ',list)))


;;;;;;;;;;
;;Simple makers

(defmacro make-cmap-readtable (&rest list)
   ""
   
   `'(,@list))

;;ancestor is often nil
(defmacro make-readtable (ancestor &rest list)
   ""
   
   `'(,ancestor ,@list))
'
(defmacro make-readtable (ancestor &rest list)
   ""
   
   `'(ancestor ,@list))

(defmacro make-shuffled-sites-table (size default &rest list)
   ""
   
   `'(,size ,default ,@list))

(defmacro make-monster-set (&rest list)
   ""
   
   `'(,@list))

;;Random-tables.  NB, this is not used when they are defined within
;;quoted material.  The fact of lmap_make_random_site conveys
;;`make-random-table' within actions.
(defmacro make-random-table (&rest list)
   ""
   
   `'(,@list))

;;;;;;;;;;;;;;
;;More complex structures and makers.

(defstruct 
   (subroom-t
      (:constructor make-subroom-t
	 (  type 
	    data
	    &key 
	    (ulc 'sl_level_ulc)
	    (lrc 'sl_level_lrc)
	    (min_x ulc)
	    (min_y ulc)
	    (max_x lrc)
	    (max_y lrc))))

   "Subroom data"
   min_x  min_y  max_x  max_y
   type
   data)


(defstruct (omega-readmap-entry-t (:type list))
   ""
   action
   param1
   test)



(defstruct 
   (shuffled-site-table-t 
      (:constructor make-shuffled-sites
	 (sites-expected
	    default
	    table
	    &aux (size-2 sites-expected))))
   
   "Data to make shuffled sites"
   size-2
   default
   table)

(defstruct 
   (map-subtype-loader-t
      (:constructor make-map-subtype-loader
	 (  
	    &key 
	    (name "") 
	    map_idx
	    (prepare_action '())
	    (subrooms '())
	    (lmap-table '())
	    &allow-other-keys))
      )
   
   ""
   name
   map_idx
   subrooms
   prepare_action
   lmap-table
   )


(defstruct 
   (map-loader-t
      (:constructor make-map-loader-t
	 (
	    subenvs
	    &key
	    (post-process 'post_process_no_op)
	    (shuffled-sites '())
	    &allow-other-keys)))
   
   ""
   subenvs
   post-process
   shuffled-sites)


(defun* make-map-loader-only 
   ( &rest args
      &key
      subenvs
      (difficulty 3)
      &allow-other-keys)
   "Make a bare maploader"
   (let*
      (
	 (subenv-list
	    (if (null subenvs)
	       (list 
		  (apply #'make-map-subtype-loader args))
	 
	       (mapcar
		  #'(lambda (sub-args)
		       (apply #'make-map-subtype-loader 
			  (append sub-args args)))
		  subenvs)))
	 
	 (map-loader
	    (apply #'make-map-loader-t subenv-list args)))
      
      map-loader))

(defun* make-map-loader 
   (
      index
      &rest args
      &key
      subenvs
      (difficulty 3)
      &allow-other-keys)
   ""

   (let
      ((map-loader
	  (apply #'make-map-loader-only args)))
	 
      (apply #'make-env-loader-t
	 index 
	 map-loader 
	 :base-difficulty difficulty
	 args)))


'  ;;Example.
(make-map-loader
   'E_NEVER_NEVER_LAND
   :name "All"
   :subenvs
   (list
      (list :name "None" :map_idx 11)
      (list :map_idx 12)))



;;;;;;;;;;;;;
;;Countryside

(defstruct (cmap-entry-t (:type list))
   ""
  sym
  base-terrain-type
  current-terrain-type
  aux)

(defstruct (coun-loader-t)
   ""
   cmap)


;;;;;;;;;
;;Dungeon

;;For future expansion.  
(defstruct (mon-random-t)
   ""
   mon-id 
   (chance  100))


(defstruct 
   (levplan-t
      (:constructor
	 make-levplan-t
	 (levplan-sym &key chance)))
   ""
   levplan-sym
   (chance 1))


(defstruct 
   (dun-subtype-loader-t
      (:constructor
	 make-dun-subtype-loader-t
	 (
	    &key
	    name
	    lmap-table
	    level-type-list
	    monster-table
	    &allow-other-keys)))
   
   ""
   (name "A mysterious dungeon")
   (lmap-table lmap-dungeon-normal)
   (level-type-list '(dls_room_level))
   (monster-table 
      (list (make-mon-random-t :mon-id 'RANDOM))))


(defstruct 
   (dun-loader-t
      (:constructor
	 make-dun-loader-t
	 (
	    subenv-list
	    &key
	    width
	    length
	    max-levels
	    base-population
	    &allow-other-keys)))
   ""
   (width 64)
   (length 64)
   (max-levels 10)
   subenv-list
   (base-population 13))




(defun* make-dun-loader 
   (
      index
      &rest args
      &key
      subenvs
      (difficulty 3)
      &allow-other-keys)
   ""
   (let
      ((subenv-list
	    (if (null subenvs)
	       (list 
		  (apply #'make-dun-subtype-loader-t args))
	 
	       (mapcar
		  #'(lambda (sub-args)
		       (apply #'make-dun-subtype-loader-t
			  (append sub-args args)))
		  subenvs))))

      ;;Interpret a `difficulty' formula.  For future expansion.
      (destructuring-bind
	 (base-difficulty depth-scale phase-scale)
	 ;;destructuring-bind can't do keys with defaults, but this can.
	 (apply
	    (function*
	       (lambda (base-difficulty &key (depth-scale 0) (phase-scale 0))
		  (list base-difficulty depth-scale phase-scale)))
	    (if
	       (listp difficulty)
	       difficulty
	       (list difficulty)))
      
	 (let
	    ((dun-loader
		(apply #'make-dun-loader-t 
		   subenv-list 
		   args)))
	 
	 
	    (apply #'make-env-loader-t
	       index 
	       dun-loader
	       :cache-type 'cache_in_dungeon
	       :set-randomizer t
	       :base-difficulty base-difficulty
	       args)))))



;;;;;;;;
;;Tacmap

;;For future expansion.  
(defstruct (tacmap-subenv-t)
   ""
   ;;monster-table
   ;;readtable
   ;;terrain-string 
   sym)


(defstruct (tacmap-loader-t)
   ""
   subenvs)




;;;;;;;;;;;;;;;
;;Overall loader type.

(defstruct 
   (env-loader-t 
      (:constructor
	 make-env-loader-t
	 (
	    index
	    data
	    &key
	    (flag-setter 'no_flags)
	    (set-randomizer nil)
	    (cache-type 'cache_dont)
	    (base-difficulty 3)
	    &allow-other-keys)))
   
   ""
   index
   data
   set-randomizer
   cache-type
   base-difficulty
   flag-setter)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;To allow handholder to recognize/collect/rewrite types.

(defconst omega-makers 
   '
   (make-monster-set 
      make-monster-set 
      make-readtable
      make-shuffled-sites-table 
      make-cmap-readtable
      make-map-loader-only
      make-map-loader
      make-dun-loader
      make-env-loader-t)
   "The available makers" )


(defconst omega-lists 
   '
   (omega_env_loaders omega-random-tables)

   "The top-level lists" )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;To see that loaders.el uses only a known set of heads:

(defun get-head-recurse (sexp)
   ""
   (if
      (listp sexp)
      (let*
	 ((head (first sexp)))
	 (if
	    (or
	       (eq head 'quote)
	       (apropos-macrop head))
	    (list head)
	    (remove-duplicates
	       (apply #'append
		  (list head)
		  (mapcar
		     #'get-head-recurse
		     (cdr sexp))))))))



(defun check-loaders-el ()
   ""
   
   (interactive)
   (let
      (
	 collected
	 (stream (get-buffer "loaders.el"))
	 done)
      (with-current-buffer stream

	 (goto-char (point-min))
	 (while
	    (not done)
	    ;;Easiest way to find out when read can't read anything more
	    ;;is to let it try and catch its error.
	    (condition-case err
	       (setq a (get-head-recurse (read stream)))
	       (error (setq done t)))

	    ;;This has to be outside the condition-case, otherwise it
	    ;;gets messed up on the final iteration.
	    (unless done
	       (push a collected))))
      (remove-duplicates
	 (apply #'append
	    collected))))


;;(check-loaders-el)

(provide 'declare-omega)

;;; declare-omega.el ends here