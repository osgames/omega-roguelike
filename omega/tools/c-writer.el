;;; c-writer.el --- Code to help write data as other languages, esp C

;; Copyright (C) 2001 by Tom Breton

;; Author: Tom Breton <tehom@panix.com>
;; Keywords: tools, c

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:


;; Important: Your load-path must include this directory, or the
;; include files won't load.  Add this line to .emacs, with the
;; obvious substitution:

;;    (add-to-list 'load-path "WHEREVER-IT-IS/")


;; Overall approach

;; In the first pass, we find the names of everything that will be
;; declared separately in C.  In the second pass, we write it all.


;; The C-structure analogs are declared via the macros.  Each one
;; corresponds to a C type, objects of which will be exported to C.
;; Their structure corresponds to inclusion (direct or referential) in
;; C.


;; There are 4 possible "linkages":
;; 
;; * No linkage, which writes nothing itself, but build structures for
;; other linkages to use.  This is the default.  Everything that
;; doesn't declare a linkage has no linkage.

;; * Anonymous objects.  They write structures, but do not store their
;; names.  Thus their structures can't be referenced by objects other
;; than the object that immediately encloses them.

;; Anonymously linked types are declared via:

;; `cwrit-anon'

;; * Collections, which store the name and thus can be shared
;; arbitrarily.

;; Collections are declared via:

;; `cwrit-collect'

;; * Indexed linkage.  These return the indexes of objects, and
;; finally write a table of pointers to those objects, in the indexed
;; order.

;; `cwrit-index'.

;;;

;;; Data declarations:

;; `cwrit-struct'
;; `cwrit-table-of'
;; `cwrit-array-of'
;; `cwrit-table-of-array-of'


;;; Nesting:

;; Wherever cwrit form expects a handler, the handler can be a nested
;; directly or can be a symbol referring to a handler.  If a symbol,
;; if will be evalled at runtime.

;;;

;; To invoke this code, once all structures are declared:

;; clear all collections and indexers involved.

;; call `c-writer-nametrack-symbol-list' on all "root" objects
;; (objects that, in sum, hold the entirety of what you want to export
;; to C).  All root objects should either be collections
;; (`cwrit-collect') or singletons (`make-cwrit-singleton').

;; call `c-writer-insert-names' on all collections.

;; That's the heart of it.  You will probably want to insert some
;; other stuff, like includes and comments.

;;;; 

;;; Code:
(require 'cl)

;;;;;;;;
;;Functions that write C snippets

(defun c-writer-char-string (char)
   ""
   (cond
      ((= char ?\" ) "'\\\"'" )
      ((= char ?\' ) "'\\\''" )
      ((and (>= char ?\ ) (<= char 127) )
	 (format "'%c'" char))
      (t (int-to-string char))))

(defun c-writer-object-string (sym)
   ""
   (prin1-to-string sym t))

(defun c-writer-array-size-string (arrayname type)
   ""
   
   (format "sizeof\(%s\)/sizeof\(%s\)" arrayname type))


(defun c-writer-block-string (separator-0 &rest list)
   ""
   (let*
      (  (separator-1 (or separator-0 "\n  "))
	 (separator (concat "," separator-1)))

      (concat
	 "{"
	 separator-1
	 (mapconcat
	    #'c-writer-object-string
	    list
	    separator)
	 separator
	 "}")))

(defun c-writer-build-bool-string (flag)
   (if flag "TRUE" "FALSE"))

(defun c-writer-build-pointer-string (array-ref)
   ""
   (if array-ref (concat "&" array-ref) "NULL"))

(defun c-writer-build-table-string (array-name el-type-name)
   ""
   
   (if
      array-name
      (c-writer-block-string " "
	 array-name
	 (c-writer-array-size-string 
	    array-name el-type-name))
      
      "{ NULL, 0, }"))


(defun c-writer-make-unique-name (prefix)
   ""
   
   (let*
      ((name (format "%s_%d" prefix *gensym-counter*)))
      (incf *gensym-counter*)
      name))

;;;;;;;;;
;;Utility

(defun cwrit-resolve-to-object (sym)
   ""
   (let
      ((circling nil))
      (while 
	 (and (symbolp sym) (not circling))
	 (let
	    ((new-sym (eval sym)))
	    (if (eq new-sym sym) 
	       (setq circling t)
	       (setq sym new-sym)))))
   
   sym)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;To manage data topology and name-tracking
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Structures to manage data topology and name-tracking

(defstruct cwrit-useable
   "Base object to track and retrieve name-dependent data"
   nametracker
   ref-string-builder
   C-type-name
   subhandler)


(defstruct (cwrit-content (:include cwrit-useable)) 
   "Object to build declarable content"
   content-string-builder ;;Usually the same as `ref-string-builder'
   array-p)


(defstruct (cwrit-anonymous (:include cwrit-useable))
   "Object to manage an anonymous object type."
   prefix)


;;Helper type for collections.
(defstruct (c-writer-collected-entry-t (:type list))
   "OBJECT is an object of the source type.  NAME is a string."
   object 
   name)

(defstruct (cwrit-nametrack (:include cwrit-useable))
   "Object to manage collected objects"
   list
   test
   prefix)


(defstruct (cwrit-indexer (:include cwrit-content))
   "Object to manage an indexer.
LIST is a list of \(OBJECT INDEX\)"
   list 
   count)


(defstruct cwrit-singleton
   "Object the describes a singleton that should be exported.
OBJECT need not be meaningful, but shouldn't be `nil'."
   name
   object
   handler)


;;;;;;;;;;;;;;;;;;;;;;;;;
;;Functions for collections

(defun c-writer-find-entry (object collection)
   ""
   
   (assoc* 
      object 
      (cwrit-nametrack-list collection) 
      :test (cwrit-nametrack-test collection)))


(defun c-writer-find-name (object collection)
   ""

   (let
      ((entry (c-writer-find-entry object collection)))
   
      (second entry)))

(defun* c-writer-store-if-unique (object collection &optional true-name)
   ""
   (check-type collection cwrit-nametrack)
   (let
      ((entry (c-writer-find-entry object collection)))
       
      (if
	 entry
	 (when
	    true-name
	    (setf (second entry) true-name))

	 (push
	    (make-c-writer-collected-entry-t 
	       :object 
	       object
	       :name 
	       (or 
		  true-name
		  (c-writer-make-unique-name 
		     (cwrit-nametrack-prefix collection))))
	    (cwrit-nametrack-list collection)))))

;;On the objects as a whole.

(defun c-writer-nametrack-symbol-list (collection list)
   ""
   
   (mapcar
      #'(lambda (x)
	   (c-writer-track-name
	      (eval x) 
	      collection
	      (symbol-name x)))
      list))


;;;;;;;;;;;;


(defun cwrit-clear-handler (handler)
   ""
   
   (typecase handler

      (cwrit-nametrack
	 (setf (cwrit-nametrack-list handler) nil))

      (cwrit-indexer
	 (setf (cwrit-indexer-count handler) 0)
	 (setf (cwrit-indexer-list handler) nil))

      ;;Default for useables: recurse to the subhandler.
      (cwrit-useable
	 (let
	    ((subhandler (cwrit-useable-subhandler handler)))
	    (when subhandler
	       (cwrit-clear-handler 
		  (cwrit-resolve-to-object subhandler)))))

      ;;Singletons just contain a handler.
      (cwrit-singleton
	 (let
	    (
	       (subhandler 
		  (cwrit-resolve-to-object
		     (cwrit-singleton-handler handler))))
	    (cwrit-clear-handler subhandler)))
      (t)))


;;;;;;;;;;;;;;;;;;;;
;; Declare C objects

(defun cwrit-declare (object name handler)
   ""
   
   (let*
      (
	 (content 
	    (cwrit-resolve-to-object handler))
	 (contents
	    (funcall
	       (cwrit-content-content-string-builder content)
	       object
	       content)))
      
      (insert
	 (cwrit-get-C-type-name content)
	 " "
	 name
	 (if (cwrit-content-array-p content) "[]" "")
	 " =\n"
	 contents
	 ";\n\n")))

(defun cwrit-declare-collection (collection)
   ""
   (mapcar 
      #'(lambda (x)
	   (cwrit-declare
	      (c-writer-collected-entry-t-object x)
	      (c-writer-collected-entry-t-name x)
	      (cwrit-useable-subhandler collection)))
      
      (cwrit-nametrack-list collection)))


(defun cwrit-declare-thing (declarable)
   ""
   (typecase declarable

      (cwrit-nametrack (cwrit-declare-collection declarable))

      (cwrit-singleton
	 (cwrit-declare 
	    (cwrit-singleton-object  declarable) 
	    (cwrit-singleton-name    declarable)
	    (cwrit-singleton-handler declarable)))))

;;;;
;;To make forward declarations from collections (Singletons are not
;;supported for this)


(defun cwrit-forward-declare (object name handler)
   ""
   
   (let*
      (
	 (content 
	    (cwrit-resolve-to-object handler)))
      
      (insert
	 "extern "
	 (cwrit-get-C-type-name content)
	 " "
	 name
	 (if (cwrit-content-array-p content) "[]" "")
	 ";\n\n")))

(defun cwrit-forward-declare-collection (collection)
   ""
   (mapcar 
      #'(lambda (x)
	   (cwrit-forward-declare
	      (c-writer-collected-entry-t-object x)
	      (c-writer-collected-entry-t-name x)
	      (cwrit-useable-subhandler collection)))
      
      (cwrit-nametrack-list collection)))


;;;;
;;Helper functions for the {second,third} pass.  


(defun cwrit-get-C-type-name (handler)
   ""
   (let*
      ((content (cwrit-resolve-to-object handler))
	 (name-data
	    (cwrit-useable-C-type-name content)))
      (typecase name-data
	 (string name-data)
	 (function
	    (funcall name-data content))
	 (t (error "Can't get a type name")))))



(defun c-writer-track-name (object handler &optional true-name)
   "Track an object.
Does nothing if OBJECT is `nil'."
   (when object
      (let
	 ((useable (cwrit-resolve-to-object handler)))
	 
	 (funcall
	    (cwrit-useable-nametracker useable)
	    object
	    useable
	    true-name))))


(defun c-writer-mapcar-track-name (object handler)
   "Equivalent to a `mapcar' with `c-writer-track-name'
Does nothing if OBJECT is `nil'."
   (when object
      (let* 
	 (
	    (useable (cwrit-resolve-to-object handler))
	    (nametrack-func (cwrit-useable-nametracker useable)))
	 
	 (mapcar
	    #'(lambda (x)
		 (funcall nametrack-func x useable)) 
	    object))))

;;This fires even if OBJECT is `nil' because we may want `nil'
;;translated at a lower level, eg for a table of an array
;;corresponding to an empty list.

(defun c-writer-get-string (object handler)
   "Get the string to reference OBJECT.
Fires even if OBJECT is `nil'."

   (let
      ((useable (cwrit-resolve-to-object handler)))
	 
      (funcall
	 (cwrit-useable-ref-string-builder useable)
	 object
	 useable)))


(defun c-writer-mapcar-get-string (object handler)
   "Equivalent to a `mapcar' with `c-writer-get-string'.
Fires even if OBJECT is `nil'."
   
   (let* 
      (
	 (useable (cwrit-resolve-to-object handler))
	 (string-build-func (cwrit-useable-ref-string-builder useable)))
	 
      (mapcar
	 #'(lambda (x)
	      (funcall string-build-func x useable)) 
	 object)))

;;;;;;;;;;;;;;;;
;;Test function.

(defun c-writer-test (object handler)
   ""
   (c-writer-track-name object handler "Early_name")

   (if
      (cwrit-nametrack-p handler)
      (cwrit-declare-collection handler)
      (cwrit-declare
	 object "Late_name" handler)))


;;;;;;;;;;;;;;;;;;;
;;Functions to create collections etc describing objects.


(defun* cwrit-anon (subhandler &optional (prefix "object"))
   ""


   (make-cwrit-anonymous
      :subhandler subhandler
      :C-type-name
      #'(lambda (handler)
	   (cwrit-get-C-type-name
	      (cwrit-useable-subhandler handler)))
      :prefix    prefix
      :nametracker
      #'(lambda (object handler &optional true-name)
	   (c-writer-track-name object 
	      (cwrit-useable-subhandler handler)))
	 
      :ref-string-builder
      #'(lambda (object handler)
	   (let
	      (
		 (tab-name 
		    (c-writer-make-unique-name 
		       (cwrit-anonymous-prefix handler))))

	      (cwrit-declare object tab-name 
		 (cwrit-useable-subhandler handler))
	      tab-name))))


(defun* cwrit-collect (subhandler &optional (test #'eq) (prefix "object"))
   ""


   (make-cwrit-nametrack
      :subhandler subhandler
      :C-type-name
      #'(lambda (handler)
	   (cwrit-get-C-type-name
	      (cwrit-useable-subhandler handler)))
      :test    test
      :prefix  prefix
      :nametracker
      #'(lambda (object handler &optional true-name)
	   (c-writer-store-if-unique object handler true-name)
	   (c-writer-track-name object 
	      (cwrit-useable-subhandler handler)))
      :ref-string-builder
      #'(lambda (object handler)
	   (c-writer-find-name object handler))))

'
(defun cwrit-index (subhandler)
   ""

   (make-cwrit-indexer
      :subhandler subhandler
      :nametracker
      #'(lambda (object handler &optional true-name)
	   (unless
	      (assoc object (cwrit-indexer-list handler))
	      (push 
		 (list object (cwrit-indexer-count handler)) 
		 (cwrit-indexer-list handler))
	      (incf (cwrit-indexer-count handler))
	      (c-writer-track-name object 
		 (cwrit-useable-subhandler handler))))
      :ref-string-builder
      #'(lambda (object handler)
	   (let*
	      ((found (assoc object (cwrit-indexer-list handler))))
	      (assert found nil "Somehow the object wasn't indexed!")
	      (int-to-string (second found))))
      :content-string-builder
      #'(lambda (object handler)
	   (let*
	      ((list 
		  (reverse (cwrit-indexer-list handler))))
	      ;;Surely list is in proper order now.
	      (apply #'c-writer-block-string "\n  "
		 (mapcar
		    #'(lambda (cell)
			 (c-writer-build-pointer-string
			    (c-writer-get-string 
			       (first cell) 
			       (cwrit-useable-subhandler handler))))
		    list))))
      :C-type-name 
      #'(lambda (handler)
	   (concat
	      (cwrit-get-C-type-name 
		 (cwrit-useable-subhandler handler))
	      " * "))

      :array-p t))

(defun index-content-string-builder (object handler)
   (let
      ((list 
	  (reverse (cwrit-indexer-list handler))))
      ;;Surely list is in proper order now.
      (apply #'c-writer-block-string "\n  "
	 (mapcar
	    #'(lambda (cell)
		 (c-writer-build-pointer-string
		    (c-writer-get-string 
		       (first cell) 
		       (cwrit-useable-subhandler handler))))
	    list))))


(defun index-nametracker (object handler &optional true-name)
   (unless
      (assoc object (cwrit-indexer-list handler))
      (push 
	 (list object (cwrit-indexer-count handler)) 
	 (cwrit-indexer-list handler))
      (incf (cwrit-indexer-count handler))
      (c-writer-track-name object 
	 (cwrit-useable-subhandler handler))))

(defun index-ref-string-builder (object handler)
   (let
      ((found (assoc object (cwrit-indexer-list handler))))
      (assert found nil "Somehow the object wasn't indexed!")
      (int-to-string (second found))))

(defun cwrit-index (subhandler)
   ""

   (make-cwrit-indexer
      :subhandler subhandler
      :nametracker #'index-nametracker
      :ref-string-builder #'index-ref-string-builder
      :content-string-builder #'index-content-string-builder
      :C-type-name 
      #'(lambda (handler)
	   (concat
	      (cwrit-get-C-type-name 
		 (cwrit-useable-subhandler handler))
	      " * "))

      :array-p t))



(defun c-writer-build-struct 
   (C-type-name let-tracked body)
   "Build a suitable funcpair.
C-TYPE-NAME is a string representing the type-name in C.
BODY is code that returns a string representing the object.
LET-TRACKED is a list suitable as the first arg of `let*'.  It is
converted by sublis to proper form for the different passes."
   (let*
      (
	 (assigns 
	    (sublis
	       '(  (cwrit-track    . c-writer-track-name)
		   (cwrit-maptrack . c-writer-mapcar-track-name))
	       let-tracked))
	 
	 (name-gathering 
	    (sublis
	       '(  (cwrit-track    . c-writer-get-string)
		   (cwrit-maptrack . c-writer-mapcar-get-string))
	       let-tracked))
	 (string-builder
	    (eval
	       `
	       #'(lambda (object &optional handler)
		    (let*
		       ,name-gathering
		       ,body)))))

      (make-cwrit-content
	 :C-type-name C-type-name
	 :nametracker
	 (eval
	    `#'(lambda (object &optional handler true-name)
		  (let*
		     ,assigns)))
       
	 :ref-string-builder     string-builder
	 :content-string-builder string-builder)))


(defmacro cwrit-struct 
   (C-type-name let-tracked body)
   ""

   `
   (c-writer-build-struct
      ,C-type-name ',let-tracked ',body))


'
(defvar test-0
   (cwrit-struct
      "type_t"
      ()
      (c-writer-block-string "\n  "
	 (first   object)
	 (second  object)
	 (third   object)))
   
   "" )

'
(c-writer-test
   '(a b c)
   test-0)

(defun* cwrit-array-of (subhandler)
   ""

   (let
      ((string-builder
	  #'(lambda (object handler)
	       (let
		  ((contents
		      (c-writer-mapcar-get-string object
			 (cwrit-useable-subhandler handler))))
		  (apply #'c-writer-block-string "\n  "
		     contents)))))

      (make-cwrit-content
	 :subhandler subhandler
	 :C-type-name 
	 #'(lambda (handler)
	      (cwrit-get-C-type-name 
		 (cwrit-useable-subhandler handler)))
	 :array-p t
	 :nametracker
	 #'(lambda (object handler &optional true-name)
	      (c-writer-mapcar-track-name object 
		 (cwrit-useable-subhandler handler)))

	 :ref-string-builder     string-builder
	 :content-string-builder string-builder)))



'  
(defvar test-1 
   (cwrit-collect
      (cwrit-array-of
	 (cwrit-struct
	    "type_t"
	    ()
	    (c-writer-block-string "\n  "
	       (first   object)
	       (second  object)
	       (third   object))))
      #'equal "actlist")

   "" )

'
(c-writer-test
   (second (car lmap_dlair))
   test-1)


(defun* cwrit-table-of
   (C-type-name subhandler)
   ""

   (let
      ((string-builder
	  #'(lambda (object handler)
	       (let*
		  (
		     (subhandler
			(cwrit-useable-subhandler handler))
			
		     (body
			(if object
			   (c-writer-get-string object subhandler)
			   nil))
		     (el-type-name
			(cwrit-get-C-type-name subhandler)))
			  
		  (c-writer-build-table-string 
		     body
		     el-type-name)))))
	 
      (make-cwrit-content
	 :subhandler subhandler
	 :C-type-name C-type-name
	 :nametracker
	 #'(lambda (object handler &optional true-name)
	      (c-writer-track-name 
		 object 
		 (cwrit-useable-subhandler handler)))
	 :ref-string-builder     string-builder
	 :content-string-builder string-builder)))


(defun* cwrit-table-of-array-of (C-type-name handler)
   ""

   (cwrit-table-of
      C-type-name
      (cwrit-anon
	 (cwrit-array-of handler))))

;;;;;;;;;;;;;;;

;;Set up font locking nicely.  Derived from:
;;    (tehom-build-font-lock-expression 
;;       '(
;;        "cwrit-collect"
;;        "cwrit-anon"
;;        "cwrit-index"
;;        "cwrit-array-of"
;;        "cwrit-table-of"
;;        "cwrit-struct")
;;       font-lock-keyword-face)


(font-lock-add-keywords 'emacs-lisp-mode
   '
   (
      ("(\\(cwrit-\\(a\\(non\\|rray-of\\)\\|collect\\|index\\|struct\\|table-of\\)\\)\\>" 1 font-lock-keyword-face)
      ))

(provide 'c-writer)

;;; c-writer.el ends here