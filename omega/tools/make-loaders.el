;;; make-loaders.el --- Make map loaders for Omega

;; Copyright (C) 2001 by Tom Breton

;; Author: Tom Breton;; 
;; Keywords: tools, c

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; Important: Your load-path must include this directory, or the
;; include files won't load.  Add this line to .emacs, with the
;; obvious substitution:

;;    (add-to-list 'load-path "WHEREVER-IT-IS/omega-roguelike/omega/tools/")

;;; How to use:

;; Load "declare-omega.el"
;; Load "loaders.el"
;; Load this file
;; In a buffer editing `loaders.c', call `omega-insert-loaders'.  
;; Save the buffer.
;; make omega

;;; Code:

(require 'cl)
(require 'c-writer "./c-writer.el")
(require 'declare-omega "./declare-omega.el")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Omega structure data.

(defvar entries-coll
   (cwrit-collect
      (cwrit-array-of
	 (cwrit-struct "lmap_reify_data_t"
	    (
	       (action (omega-readmap-entry-t-action object))
	       (param1 (omega-readmap-entry-t-param1 object))
	       (param1-string
		  ;;`lmap_make_random_site' indexes random-tables
		  (if
		     (eq action 'lmap_make_random_site)
		     (cwrit-track 
			(cwrit-resolve-to-object param1)
			random-act-indexer) 
		     param1)))
	    (let
	       (
		  (test
		     (or 
			(omega-readmap-entry-t-test object)
			'always)))
	    (c-writer-block-string "\n  "
	       test
	       action
	       param1-string))))
      #'equal
      "act_list"))



'
(c-writer-test
   (second (car lmap_dlair))
   entries-coll)

(defvar actlist-table-f2
   (cwrit-table-of "lmap_reify_data_table_t"
      'entries-coll))

'
(c-writer-test
   (second (car lmap_dlair))
   actlist-table-f2)


(defvar lmap-coll
   (cwrit-collect
      (cwrit-array-of
	 (cwrit-struct
	    "lmap_read_data_t"
	    ((actlist-name 
		(cwrit-track (second object) actlist-table-f2)))
	    (c-writer-block-string "\n  "
	       (c-writer-char-string (aref (car object) 0))
	       actlist-name)))
      #'equal 
      "lmap")
   
   "" )

(defvar lmap-table-f2
   (cwrit-table-of "lmap_table_t"
      lmap-coll))


;;This collection has to be forward-declared first, so the objects of
;;the same type can be referenced.
(defvar readtable-coll 
   (cwrit-collect
      (cwrit-struct "readtable_t"
	 ((table-string 
	     (cwrit-track (cdr object) lmap-table-f2))
	    (ancestor-string
	       (c-writer-build-pointer-string
		  (let
		     ((o (car object)))
		     (if
			o
			(cwrit-track 
			   (cwrit-resolve-to-object o) 
			   readtable-coll))))

;; 	       (if 
;; 		  (car object)
;; 		  (c-writer-build-pointer-string
;; 		     (cwrit-track 
;; 			(cwrit-resolve-to-object (car object)) 
;; 			readtable-coll))
;; 		  "NULL")

	       ))
	 
	 (c-writer-block-string "\n      "
	    table-string
	    ancestor-string)))
   
   "" )


'  
(c-writer-test
   lmap_dlair
   readtable-coll)

(defvar subroom-coll 
   (cwrit-collect
      (cwrit-array-of
	 (cwrit-struct "subroom_data_t"
	    (  (type (subroom-t-type object))
	       (data-0 (subroom-t-data object))
	       (data
		  (if (eq type 'subroom_map)
		     (typecase data-0
			;;Later we may allow action-lists too.
			(map-loader-t
			   (cwrit-track data-0 map-loader-indexer)))
		     data-0)))

	    (c-writer-block-string "\n  "
	       (subroom-t-min_x object)
	       (subroom-t-min_y object)
	       (subroom-t-max_x object)
	       (subroom-t-max_y object)
	       type
	       data))))
   
   "" )

(defvar subroom-table-f2
   (cwrit-table-of
      "subroom_table_t"
      'subroom-coll))


'  ;;Test also depends on later declarations.
(c-writer-test
      (list
	 ;;Hedgemaze
	 (make-subroom-t 
	    subroom_map 
	    (make-map-loader-only 
	       lmap_hedgemaze
	       :map_idx MAP_hedges
	       :prepare_action
	       '
	       (
		  (always lmap_clear_loc_flags (SEEN | LIT)))
	       :post-process post_process_calm_monster)
	    :rect-num 1)
	 ;;Jail
	 (make-subroom-t 
	    subroom_map 
	    jail_map_loader
	    :rect-num 2))
   ;;Try both.
   subroom-coll
   ;;subroom-table-f2
   )


;;To support randomization
(defvar pseudoid-table-f2 
   (cwrit-table-of-array-of "pseudoid_table_t"
      (cwrit-struct "pseudoid_t"
	 ()
	 object))
   
   "" )

'
(c-writer-test '(1 2 3) pseudoid-table-f2)


(defvar random-table-f2
   (cwrit-struct "random_table_t"
      ((table-string
	  (cwrit-track object pseudoid-table-f2)))
      (let
	 (
	    (prob-count 
	       (loop
		  for i in object
		  sum i)))
	 (c-writer-block-string " "
	    table-string
	    prob-count)))
   "" )

'
(c-writer-test '(1 2 3) random-table-f2)

;;
(defmacro cwrit-randomized (C-type-name chance-accessor 
			      data-accessor data-handler)
   "Make a handler that emits C-TYPE-NAME. 
The emitted C structure will consist of a random table and a pointer
to an array of the same size.
DATA-HANDLER must take a list.
The created handler will take a list of objects that CHANCE-ACCESSOR
and  DATA-ACCESSOR can handle."
   
   `(cwrit-struct ,C-type-name
       ((tab-string 
	   (cwrit-track 
	      (mapcar ,chance-accessor object) 
	      random-table-f2))
	  (array-name
	     (cwrit-track 
		(mapcar ,data-accessor object)
		,data-handler)))

       (c-writer-block-string " "
	  tab-string
	  array-name)))

;;Randomized reify actions.
(defvar random-act-coll
   (cwrit-collect
      (cwrit-randomized "random_action_t" 
	 #'first
	 #'second 
	 entry-list-list-anon))

   "" )

'  ;;Tested w/o `cwrit-collect'
(c-writer-test
   random_village_house_locf
   random-act-coll)


;;Randomized level-plans
(defvar levplan-array-f2 
   (cwrit-anon
      (cwrit-array-of
	 (cwrit-struct "int"
	    ()
	    object)))
   "" )

;;A bare symbol means a level-type with unit chance.
(defvar random-levplan-f2
   (cwrit-randomized "random_levplan_t" 
      #'(lambda (i)
	   (if
	      (symbolp i)
	      1
	      (levplan-t-chance i)))
      #'(lambda (i)
	   (if
	      (symbolp i)
	      i
	      (levplan-t-levplan-sym i)))
      levplan-array-f2)

   "" )

;;For future expansion
;;Randomized monsters, including pseudo-ids.
(defvar monid-array-f2 
   (cwrit-anon
      (cwrit-array-of
	 (cwrit-struct "int"
	    ()
	    object)))
   "" )


;;For future expansion
;;This, like random-levplan-f2, could accept symbols as having unit chance.
(defvar random-dun-monster-f2
   (cwrit-randomized "random_dun_monster_table_t" 
      #'mon-random-t-chance
      #'mon-random-t-mon-id
      monid-array-f2)

   "" )



(defvar random-act-indexer 
   (cwrit-index
      'random-act-coll)
   "" )

(defvar random-act-table 
   (cwrit-table-of "random_action_table_t"
      (cwrit-anon 
	 'random-act-indexer))
   "" )


(defvar entry-list-list-anon 
   (cwrit-anon
      (cwrit-array-of
	 'actlist-table-f2))
   
   
   "" )



;;(first x) is shuffle_size
;;(second x) is the default actionlist.
;;(cddr x) is the list of actionlists.
(defvar shuffled-coll 
   (cwrit-collect 
      (cwrit-struct "shuffled_site_table_t"
	 (  (tab-name         
	       (cwrit-track (cddr   object) entry-list-list-anon))
	    (default-act-name 
	       (cwrit-track (second object) actlist-table-f2)))

	 (c-writer-block-string "\n  "
	    tab-name
	    (c-writer-array-size-string tab-name 
	       "lmap_reify_data_table_t")
	    (car object)
	    default-act-name)))
   

   "" )

'
(c-writer-test
   village_shuffled_sites
   shuffled-coll)


(defvar submap-loader-f2 
   (cwrit-struct
      "subenv_data_t"
      (
	 (subroom-table-string
	    (cwrit-track
	       (map-subtype-loader-t-subrooms object)
	       subroom-table-f2))
	 (lmap-table-string
	    (c-writer-build-pointer-string
	       (let ((o (map-subtype-loader-t-lmap-table object)))
		  (if 
		     o
		     (cwrit-track 
			(cwrit-resolve-to-object o) 
			readtable-coll)))))

	 (initter-table-string
	    (cwrit-track
	       (map-subtype-loader-t-prepare_action object)
	       actlist-table-f2)))
   
      (c-writer-block-string "\n    "
	 (prin1-to-string
	    (map-subtype-loader-t-name object))
	 (map-subtype-loader-t-map_idx object)
	 subroom-table-string
	 initter-table-string
	 lmap-table-string
	 ))

   "" )

(defvar subenv-loader-array-coll
   (cwrit-collect
      (cwrit-array-of
	 'submap-loader-f2))

   "" )


(defvar map-subtype-loader-table-f2
   (cwrit-table-of "subenv_data_table_t"
      'subenv-loader-array-coll)
   
   "" )


(defvar map-loader-coll 
   (cwrit-collect
      (cwrit-struct
	 "maploader_t"
	 (
	    (subenv-table-string
	       (cwrit-track
		  (map-loader-t-subenvs object)
		  map-subtype-loader-table-f2))
      
	    (shuffle-name
	       (cwrit-track
		  (map-loader-t-shuffled-sites object)
		  shuffled-coll)))
   
	 (c-writer-block-string "\n  "
	    subenv-table-string
	    (map-loader-t-post-process object)
	    (c-writer-build-pointer-string shuffle-name))))

   "" )


(defvar map-loader-indexer
   (cwrit-index
      'map-loader-coll))

(defvar map-loader-table
   (cwrit-table-of "maploader_table_t"
      (cwrit-anon 
	 'map-loader-indexer))

   "" )


;;;;;;;;;;;;;;
;;Country

(defvar cmap-el-array-coll
   (cwrit-collect 
      (cwrit-array-of
	 (cwrit-struct "cmap_read_data_t"
	    ()
	    (c-writer-block-string "\n  "
	       (let
		  ((char (cmap-entry-t-sym object)))
		  (if
		     (numberp char)
		     (c-writer-char-string char)
		     (concat
			(c-writer-object-string
			   (cmap-entry-t-sym object))
			"&0xff")))
	       (cmap-entry-t-base-terrain-type    object)
	       (cmap-entry-t-current-terrain-type object)
	       (cmap-entry-t-aux                  object)))))
   

   "" )

(defvar cmap-table-f2
   (cwrit-table-of "cmap_loader_t"
      'cmap-el-array-coll)
   "" )

(defvar coun-loader-anon
   (cwrit-anon
      (cwrit-struct "cmap_loader_t"
	 (
	    (table-string
	       (cwrit-track (coun-loader-t-cmap object) cmap-table-f2)))
	 table-string))
   
   "" )


;;;;;;;;;;;;;;;
;;Dungeon

(defvar dun-subenv-loader-anon
   (cwrit-struct "dun_subenv_data_t"
      (
	 (readtable-string 
	    (c-writer-build-pointer-string
	       (let ((o (dun-subtype-loader-t-lmap-table object)))
		  (if
		     o
		     (cwrit-track 
			(cwrit-resolve-to-object o) 
			readtable-coll)))))
	 
	 (levplan-string 
	    (cwrit-track
	       (dun-subtype-loader-t-level-type-list object)
	       random-levplan-f2))
	 
	 ;;For future expansion.  Punt for now.
	 (mon-table-string
	    "{ { { NULL, 0, }, 0, }, NULL, }" 
;; 	       (cwrit-track
;; 		  (dun-subtype-loader-t-monster-table object)
;; 		  random-dun-monster-f2)
	       ))
	 
      (c-writer-block-string "\n  "
	 (prin1-to-string
	    (dun-subtype-loader-t-name object))
	 readtable-string
	 levplan-string
	 mon-table-string))
   
   "" )


(defvar dun-subenv-loader-table-f2 
   (cwrit-table-of-array-of  "dun_subenv_data_table_t"
      'dun-subenv-loader-anon)
   "" )

(defvar dun-loader-anon
   (cwrit-anon
      (cwrit-struct "dun_loader_t"
	 (
	    (subenv-string 
	       (cwrit-track
		  (dun-loader-t-subenv-list object)
		  dun-subenv-loader-table-f2)))
	 
	 (c-writer-block-string "\n  "
	    (dun-loader-t-width object)
	    (dun-loader-t-length object)
	    (dun-loader-t-max-levels object)
	    subenv-string
	    (dun-loader-t-base-population object))))

 "" )




;;;;;;;;;;;;;;;
;;Tacmap

(defvar tacmap-subenv-array-coll
   (cwrit-collect
      (cwrit-array-of
	 (cwrit-struct "tacmap_subenv_data_t"
	    ()
	    (c-writer-block-string "\n  "
	       (tacmap-loader-t-sym object)))))
   
   "" )

(defvar tacmap-table-f2
   (cwrit-table-of "tacmap_loader_t"
      tacmap-subenv-array-coll)
   "" )

(defvar tacmap-loader-anon
   (cwrit-anon
      (cwrit-struct "tacmap_loader_t"
	 (
	    (table-string
	       (cwrit-track (tacmap-loader-t-subenvs object) tacmap-table-f2)))
	 table-string))
   
   "" )


;;;;;;;;;;;;;;
;;Loader


(defvar base-env-loader-coll 
   (cwrit-struct "env_loader_t"
      (	   
	 (data (env-loader-t-data object))
	 (data-ref 
	    (typecase data
	       (map-loader-t    (cwrit-track data map-loader-coll))
	       (coun-loader-t   (cwrit-track data coun-loader-anon))
	       (dun-loader-t    (cwrit-track data dun-loader-anon))
	       (tacmap-loader-t (cwrit-track data tacmap-loader-anon)))))


      (c-writer-block-string "\n  "
	 (env-loader-t-index  object)
	 (typecase data
	    (map-loader-t    'enst_map)
	    (coun-loader-t   'enst_coun)
	    (dun-loader-t    'enst_dun)
	    (tacmap-loader-t 'enst_tacmap))
	 (c-writer-block-string " "
	    (c-writer-build-pointer-string
	       data-ref))
	 (c-writer-build-bool-string
	    (env-loader-t-set-randomizer  object))
	 (env-loader-t-cache-type      object)
	 (env-loader-t-base-difficulty object)
	 (env-loader-t-flag-setter object)))
   
   "" )


(defvar env-loaders-table-f2 
   (cwrit-table-of "env_loader_table_t"
      (cwrit-anon
	 (cwrit-array-of
	    'base-env-loader-coll)))
   

   "" )


;;;;;;;;;;;;;;;;;;;
;;The writer itself

(defun omega-insert-headers ()
   ""
   (insert
      "/*  **************************************************  */\n"
      "/*  Not for hand editing.  Edit loaders.el instead.    */\n" 
      "/*  This is an intermediate file.  When Omega has an    */\n"
      "/*  interpreter, this file will go away.                */\n"
      "\n"
      "/*  Automatically generated by make-loaders.el, by Tehom  */\n"
      "/*  Created on "(current-time-string)" by "user-full-name"  */\n"
      "#include \"defs.h\"\n"
      "#include \"load.h\"\n"
      "\n"
      "\n"))


(defun omega-insert-loaders ()
   ""
   
   (interactive)

   (let*
      (
	 (max-lisp-eval-depth 444)
	 (omega_env_loaders-list
	    (mapcar #'eval omega_env_loaders))
	 (ordered-collectors
	    (list 
	       entries-coll 
	       lmap-coll 
	       readtable-coll
	       subroom-coll 
	       random-act-coll
	       shuffled-coll 
	       subenv-loader-array-coll
	       map-loader-coll
	       cmap-el-array-coll
	       (make-cwrit-singleton 
		  :name    "aux_loaders_table"
		  :object  t ;;Anything but `nil'
		  :handler map-loader-table)

	       (make-cwrit-singleton 
		  :name    "random_act_table"
		  :object  t ;;Anything but `nil'
		  :handler random-act-table)

	       (make-cwrit-singleton 
		  :name    "env_loader_table"
		  :object  omega_env_loaders-list
		  :handler env-loaders-table-f2))))


      ;;Empty all the collectors.
      (mapcar
	 #'cwrit-clear-handler
	 ordered-collectors)

      
      ;;Collect all the entries, associating them with names, indexes,
      ;;etc as appropriate.  Descending from the roots accomplishes
      ;;this.
      
      (c-writer-nametrack-symbol-list 
	 map-loader-coll
	 omega-list-auxilliary-loader-syms)

      (c-writer-nametrack-symbol-list 
	 base-env-loader-coll
	 omega_env_loaders)

      ;;Obsolescent.  This lmap is no longer specially named.
      (c-writer-track-name
	 lmap-dungeon-normal
	 readtable-coll
	 "dun_lmap")
      
      ;;Now that we know all the names etc, insert everything.

      ;;Set up buffer.
      (erase-buffer)
      (goto-char (point-max))

      ;;Print headers.
      (omega-insert-headers)
      
      ;;Do some forward declarations
      (cwrit-forward-declare-collection
	 readtable-coll)
      
      ;;Declare everything.
      (mapcar
	 #'cwrit-declare-thing
	 ordered-collectors)))


