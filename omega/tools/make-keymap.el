;;; make-keymap.el --- Make keymaps for Omega

;; Copyright (C) 2001 by Tom Breton (Tehom)
;;licensed under the terms of the * Omega license as part of Omega. 

;; Author: Tom Breton <tehom@panix.com>
;; Keywords: tools, c


;;; Commentary:

;; Sample keylists are in keys_extended.el and keys_original.el.
;; Presumably if you need to use this, you'd make your own keylist.  

;; This package is mostly used by editing a keylist, then creating a
;; new C file, and calling `omega-insert-keytable-data' to insert
;; compiler-ready C code.

;; This file provides `omega-insert-key-command-data' to help you
;; create proper new key entries.  If you need to delete key entries,
;; you'll have to do it manually.


;; If `omega-insert-keytable-data' gives you a message that there is
;; an invalid prefix character, it means that you've defined keys such
;; that a key is used both as a command and as a prefix.  Change it.

;;; Code:

;;;;;;
;;; Requirements.

(require 'cl)
(require 'tehom-4)
(require 'c-writer)


;;;;;;
;;; Types.


(defstruct (keymap-data-t (:type list))

   "OBJECT is a keymap.  NUMBER is a whole number.  ARRAY-NAME and
TABLE-NAME are strings"
   object 
   number 
   array-name
   table-name)



(defstruct (key-entry-t (:type list))
   ""
   command
   dir-arg)

;;;;;;
;;; Customization

(defcustom omega-keystroke-filename 
   "~/keystrokes" 
   "Name of the file that temporarily stores keystrokes.")


;;;;;;
;;; Variables.

;;The entries could be interned and initted as keymaps for being part
;;of the list, rather than being separate.  But there's only 2.
(defvar omega-dungeon-keymap (make-sparse-keymap) "Keymap in dungeon" )
(defvar omega-country-keymap (make-sparse-keymap) "Keymap in country" )

(defvar omega-keymaps-list '() 
   "An alist of keymap-data-t, indexed by a keymap object" )

(defvar omega-count 0  
   "Count of the number of keymaps as they are written." )

(defvar omega-keymaps-awaiting-output '() 
   "Stack of keymaps that should be output" )

;;;;;;;;;;;;;;;;;;;;;
;;; Data

(defconst omega-root-keymaps-data 
   '(
       (omega-dungeon-keymap "dungeon_command_table")
       (omega-country-keymap "country_command_table"))
   "The keymaps we'll assign to.  Not used as keymaps in emacs.")


;;These know whether they take a direction, so we don't interact to
;;get a direction when it won't be used.
(defconst omega-commands-data
   '(
       (cmd_abortshadowform       nil)
       (cmd_activate              nil)
       (cmd_bash_item             nil)
       (cmd_bash_location         nil)
       (cmd_bufferprint           nil)
       (cmd_callitem              nil)
       (cmd_charid                nil)
       (cmd_check_memory          nil)
       (cmd_city_move             nil)
       (cmd_closedoor             nil)
       (cmd_countrysearch         nil)
       (cmd_disarm                nil)
       (cmd_dismount_steed        nil)
       (cmd_display_pack          nil)
       (cmd_do_inventory_control  nil)
       (cmd_downstairs            nil)
       (cmd_drop                  nil)
       (cmd_eat                   nil)
       (cmd_editstats             nil)
       (cmd_enter_site            nil)
       (cmd_escape                nil)
       (cmd_examine               nil)
       (cmd_fire                  nil)
       (cmd_frobgamestatus        nil)
       (cmd_give                  nil)
       (cmd_help                  nil)
       (cmd_hunt                  nil)
       (cmd_inventory_control     nil)
       (cmd_magic                 nil)
       (cmd_nap                   nil)
       (cmd_no_op                 nil)
       (cmd_opendoor              nil)
       (cmd_peruse                nil)
       (cmd_pickpocket            nil)
       (cmd_pickup                nil)
       (cmd_player_dump           nil)
       (cmd_print_allocation_info nil)
       (cmd_quaff                 nil)
       (cmd_quit                  nil)
       (cmd_redraw                nil)
       (cmd_rename_player         nil)
       (cmd_rest                  nil)
       (cmd_run                   t)
       (cmd_save                  nil)
       (cmd_search                nil)
       (cmd_setoptions            nil)
       (cmd_show_key              nil)
       (cmd_show_license          nil)
       (cmd_single_move           t)
       (cmd_stay                  nil)
       (cmd_tacoptions            nil)
       (cmd_talk                  nil)
       (cmd_tunnel                nil)
       (cmd_upstairs              nil)
       (cmd_vault                 nil)
       (cmd_version               nil)
       (cmd_wish                  nil)
       (cmd_wizard                nil)
       (cmd_wizard_draw           nil)
       (cmd_xredraw               nil)
       (cmd_zapwand               nil))
   "The available commands")



;;;;;
;;Variables to check membership.

(defconst omega-root-keymaps
   (mapcar
      #'(lambda (a)
	   (car a))
      omega-root-keymaps-data)

   "The keymaps we'll assign to.  Not used as keymaps in emacs.")

(defconst omega-commands
   (mapcar 
      #'(lambda (a)
	   (car a))
      omega-commands-data))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Collections of the above, for easy interaction.

(defconst omega-commands-alist
   (mapcar 
      #'(lambda (a)
	   (list* (symbol-name (car a)) a))
      omega-commands-data)
   "" )


(defconst omega-root-keymaps-alist 
   (mapcar 
      #'(lambda (a)
	   (list (symbol-name a) a))
      omega-root-keymaps)
   
   "" )


(defconst omega-directions-alist 
   '(
       ([home ]	3)
       ([left ]	5)
       ([end  ]	2)
       ([up   ]	7)
       ([begin]	no_dir)
       ([down ]	6)
       ([prior]	1)
       ([right]	4)
       ([next ]	0))
   
   "Keys that equate to directions" )



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Functions.


;;;;;;;;;;;;;;;;;;;;;;;;
;; Interaction functions

;;I wanted to do something that would get raw ASCII input
;;interactively from within emacs.  But that doesn't seem possible.
;;Even dribble doesn't really see that stuff.

;;So instead, from the command line, cat the key-sequence into a
;;temporary file named in `omega-keystroke-filename'.  Press ^D^D when
;;done.  That means you can't use ^D^D as a key sequence.  if you need
;;it, create the string manually.  Then return to emacs, and call
;;`omega-insert-key-command-data' with a prefix, or call
;;`omega-get-key-sequence-from-file'.  Under a windowing system, it's
;;probably easiest to set up so that emacs and a command line window
;;are near each other on the screen, and the switch back and forth as
;;needed.


(defun omega-get-key-sequence-from-file ()
   ""

   (with-temp-buffer
      (insert-file-contents-literally omega-keystroke-filename)
      (buffer-string)))

;;This could let the user use the file?  Nah.
(defun omega-get-key-sequence-from-interaction ()
   ""
   (call-interactively
      #'(lambda (a)
	   (interactive "sKey sequence: ")
	   a)))

;;Not used any more.
(defun omega-choose-root-keymap ()
   ""
   (tehom-completing-read-assoc 
      "Used in: " omega-root-keymaps-alist nil t "omega-"))


(defun omega-choose-command ()
   ""
   (tehom-completing-read-assoc 
      "Command: " omega-commands-alist nil t "cmd_"))


(defun omega-get-direction ()
   ""
   (let*
      ((key 
	  (read-key-sequence 
	     "Which direction? (Press a keypad key)  "))
	 (data
	    (assoc key omega-directions-alist)))
   
      (second data)))


;;Entry point.

;;Use this to create data for the tables.
(defun omega-insert-key-command-data (&optional use-file)
   ""
   (interactive "P")
   (let*
      ((key-sequence
	  (if use-file
	     (omega-get-key-sequence-from-file)
	     (omega-get-key-sequence-from-interaction)))
	 (command-0 (omega-choose-command))
	 (dir
	    (if (second command-0)
	       (omega-get-direction)
	       'no_dir))

	 (command (car command-0))
	 (object
	    (list key-sequence dir command)))

      (insert (prin1-to-string object))
      object))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;  Key sequence inserters.  

(defun set-omega-key (keymap-sym key-sequence command &optional dir-0)
   ""

   (unless (memq command omega-commands) 
      (error "Not a valid command"))
   (unless (memq keymap-sym omega-root-keymaps) 
      (error "Can't assign omega commands to that"))
   
   (let*
      (
	 (dir
	    (if (eq dir-0 'no_dir)
	       -1
	       dir-0))
	 (entry 
	    (make-key-entry-t 
	       :command command
	       :dir-arg dir)))
      
      (define-key (symbol-value keymap-sym) key-sequence entry)))
;;Example:
;; '
;; (set-omega-key 'omega-dungeon-keymap "C" 'cmd_zapwand 0)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Exporters

(defun omega-insert-all-in-C ()
   "Do everything needed to insert code in C for the keymaps in the
current buffer."
   
   (interactive)

   (setq omega-keymaps-list '())
   (setq omega-count 0)
   (setq omega-keymaps-awaiting-output '())
   
   ;;Queue the root keymaps.  They should have special names.
   (dolist (keymap omega-root-keymaps-data)
      (omega-queue-keymap 
	 (symbol-value (car keymap))
	 (second keymap)))

   ;;Insert description
   (insert
      "/* Keymap data generated automatically by make-keymap, by Tehom */\n\n"
      )

   ;;Insert headers.
   (insert 
      "#include \"defs.h\"\n"
      "#include \"extern.h\"\n"
      "#include \"keys.h\"\n\n")

   ;;Describe the "Escape" command.
   (omega-insert-how-to-escape)

   (omega-insert-all-keymaps)
   (omega-insert-keytable-array-in-C))


(defun omega-insert-how-to-escape ()
   ""
   
   (let*
      (
	 (escape-key
	    (where-is-internal
	       (make-key-entry-t 
		  :command 'cmd_escape
		  :dir-arg -1)

	       omega-dungeon-keymap t))
	 
	 (key-desc
	    (key-description escape-key))
	 
	 (how-declaration
	    (format
	       "char * how_to_escape = \"%s to abort\";\n\n"
	       key-desc)))
      
      (insert how-declaration)))


(defun omega-insert-keytable-array-in-C ()
   "Write the keytable array."
   
   (let*
      ((list (reverse omega-keymaps-list)))
      
      (insert "cmd_lookup_table_t * cmd_lookup_tables[] =\n")

      (insert "{")

      (dolist (entry list)
	 (insert (format "&%s,\n" (keymap-data-t-table-name entry))))

      (insert "};\n\n" )

      (insert
	 "const int num_cmd_lookup_tables =\n"
	 "    "
	 (c-writer-array-size-string 
	    "cmd_lookup_tables"
	    "cmd_lookup_table_t *")
	 ";\n\n")))




(defun omega-insert-all-keymaps ()
   "Write keymaps until we've written them all."
   
   (while omega-keymaps-awaiting-output
      (omega-insert-some-keymap-in-C)))


(defun omega-insert-some-keymap-in-C ()
   "Write the next available keymap in the current buffer."
   
   (let*
      ((keymap-data (pop omega-keymaps-awaiting-output)))
      (omega-insert-keymap-in-C keymap-data)))


(defun omega-queue-keymap (keymap &optional table-name-0)
   "Queue a new keymap"
   
   (let*
      ((old-count omega-count)
	 ;;Can drop out.
	 (array-name "UNUSED")
	 (table-name 
	    (if table-name-0 table-name-0
	       (format "cmd_lookup_table_%d" old-count)))
	 (new-keymap-data
	    (list keymap old-count array-name table-name)))
      
      (push new-keymap-data omega-keymaps-list)
      (push new-keymap-data omega-keymaps-awaiting-output)
      (incf omega-count)
      old-count))


(defun omega-get-keymap-number (keymap)
   "Return a keymap's number, queueing it if needed."
   
   (let*
      ((keymap-data
	  (assoc keymap omega-keymaps-list)))
      (if keymap-data
	 (keymap-data-t-number keymap-data)
	 (omega-queue-keymap keymap))))


(defun omega-insert-pure-key-entry-in-C (letter command-sym dir-arg)
   "Write a key entry, raw."
   
   (insert
      "   "
      (c-writer-block-string
	 " "

	 (c-writer-char-string letter) 
	 dir-arg 
	 (symbol-name command-sym))
      
      ",\n"))



(defun omega-insert-key-entry-in-C (letter key-entry)
   "Write a key entry, handling all conditions."
   (if (keymapp key-entry)
       (let
	  ((number (omega-get-keymap-number key-entry)))
	  (omega-insert-pure-key-entry-in-C letter 'cmd_keymap number))

      (omega-insert-pure-key-entry-in-C 
	 letter
	 (key-entry-t-command key-entry)
	 (key-entry-t-dir-arg key-entry))))

(defun omega-insert-keymap-in-C (keymap-data)
   "Write KEYMAP as C code in the current buffer."

   (let
      (
	 (keymap      (keymap-data-t-object     keymap-data))
	 (array-name
	    (c-writer-make-unique-name "cmd_lookup_array"))
	 (table-name  (keymap-data-t-table-name keymap-data)))
      
      
      (insert
	 "cmd_lookup_t "array-name"[] =\n"  
	 "{\n")
      (loop
	 for k being the key-codes of keymap
	 using (key-bindings b)
	 do
	 (omega-insert-key-entry-in-C k b))
      (insert "}\;\n")
      
      (insert
	 "cmd_lookup_table_t "table-name" =\n" 
	 (c-writer-block-string
	    " "

	    array-name 
	    (c-writer-array-size-string 
	       array-name
	       "cmd_lookup_t")
	    )
	 ";\n\n")))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Command data


;;Sets some commands.

(defun omega-insert-keytable-data ()
   ""
   (interactive)
   (setq omega-dungeon-keymap (make-sparse-keymap))
   (setq omega-country-keymap (make-sparse-keymap))

   (mapcar
      #'(lambda (data)
	   (set-omega-key 
	      'omega-dungeon-keymap 
	      (first data)
	      (third data)
	      (second data)))
      omega-dungeon-commands)
   

   (mapcar
      #'(lambda (data)
	   (set-omega-key 
	      'omega-country-keymap 
	      (first data)
	      (third data)
	      (second data)))
      omega-country-commands)

   (omega-insert-all-in-C))   


;;; make-keymap.el ends here
