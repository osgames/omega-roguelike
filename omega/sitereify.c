/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/* *** Functions to build sites according to data *** */

#include "glob.h"
#include "lev.h"
#include "util.h"
#include "memory.h"
#include "load.h"
#include "lut.h"
#include "findloc.h"

/***  Internal declarations. */
static void dispatch_reification( sitemaker_params_t * p_site_parms,
				  lmap_reify_data_t * data );



/*  ***********************************  */
/* Special functions that help make sites.  */

/* ** General use. ** */

int mon_id_is_angel(int mid)
{
  return (ANGEL == mid || HIGH_ANGEL == mid || ARCHANGEL == mid);
}


/* ** Make sites or critters. ** */

static void make_food_bin (int i, int j)
{
  int idx;

  for (idx = 0; idx < 10; ++idx)
    {
      object * obj;
      obj = new_object();
      *obj = Objects[OB_GRAIN];
      add_object_to_level(Level, obj, i, j);
    }
}
/* Was "make_archmage".  If a general, non-throne-sitting archmage is
   ever wanted, we can split off make_archmage and let them be
   different logical monsters. */
static monster * make_sitting_archmage (int i, int j)
{
  monster * mon;

  mon = new_monster();
  make_hiscore_npc(mon, NPC_ARCHMAGE);

  mon->specialf = M_SP_COURT;
  m_status_reset(mon, (HOSTILE | MOBILE));

  return mon;
}

/* make the prime sorceror */
static monster * make_prime_sorceror (int i, int j)
{
  monster * mon;
  mon = new_monster();
  make_hiscore_npc(mon, NPC_PRIME_SORCEROR);
  give_mon_object_by_id(mon, OB_STARGEM);
  mon->specialf = M_SP_PRIME;

  return mon;
}

static monster * make_justiciar(int i, int j)
{
  monster * mon;
  mon = new_monster();
  make_hiscore_npc(mon, NPC_JUSTICIAR);
  return mon;
}

static monster * make_high_priest(int i, int j, int deity)
{
  monster * mon;
  mon = new_monster();
  make_hiscore_npc(mon, deity);
  return mon;
}

/* makes a log npc for houses and hovels */
static monster * make_house_npc (int i, int j)
{
  monster * mon;

  mon = new_monster();
  make_log_npc(mon);
  give_mon_object_by_id(mon, mon->startthing);
  return mon;
}

/* makes a hiscore npc for mansions */

int get_houseable_hiscore_npc_id(void)
{ return NPC_ODIN + random_range(NPC_LAW_LORD - NPC_ODIN); }

static monster * make_mansion_npc (int i, int j)
{
  monster * mon;
  mon = new_monster();
  make_hiscore_npc(mon, get_houseable_hiscore_npc_id());
  return mon;
}

int GetArenaOpponent(int opponentNumber)
{
  int i;
  switch(opponentNumber) {
  case 0:  return GEEK;
  case 1:  return HORNET;
  case 2:  return HYENA;
  case 3:  return GOBLIN;
  case 4:  return GRUNT;
  case 5:  return TOVE;
  case 6:  return APPR_NINJA;
  case 7:  return SALAMANDER;
  case 8:  return ANT;
  case 9:  return MANTICORE;
  case 10: return SPECTRE;
  case 11: return BANDERSNATCH;
  case 12: return LICHE;
  case 13: return AUTO_MAJOR;
  case 14: return JABBERWOCK;
  case 15: return JOTUN;
  default:
    if ((Player.rank[ARENA] < 5) && (Player.rank[ARENA] > 0))
      {
	vs_Arena_champion_p = TRUE;
	return HISCORE_NPC;
      }
    else {
      do
	{ i = random_range(ML9 - ML0) + ML0; }
      while (i == NPC ||
	     i == HISCORE_NPC ||
	     i == ZERO_NPC ||
	     (Monsters[i].uniqueness != COMMON) ||
	     (Monsters[i].dmg == 0));
      return i;
    }
  }
}

static monster * make_arena_monster(int x, int y )
{
  int monsterlevel;
  int i;

  /* Kluuge: If the Arena champion is already in the arena, don't make
     another monster, lest we make 2 champions, "identical twins". */
  if(vs_Arena_champion_p) { return NULL; }

  /* Arena_Monster has to be global, for realizing that this
      particular monster's death means Arena Victory. */
  Arena_Monster = new_monster();
  *Arena_Monster = Monsters[GetArenaOpponent(Arena_Opponent)];
  if ( vs_Arena_champion_p )
    Arena_Monster->level = 20;

  monsterlevel = Arena_Monster->level;
  if (vs_Arena_champion_p)
    {
      const int max_meleestr_reps = 7;
      strcpy(Str1,Champion);
      strcat(Str1,", the arena champion");
      strcpy(Str2,"The corpse of ");
      strcat(Str2,Str1);
      mon_alloc_strings( Arena_Monster, Str1, Str2 );

      /* The meleestr never gets deallocated.  See util::free_monster. */
      Arena_Monster->meleestr =
	(char *) checkmalloc(((max_meleestr_reps*4) + 1)*sizeof(char));
      strcpy(Arena_Monster->meleestr,"");
      for(i=0;(i<Championlevel/5) && (i < max_meleestr_reps);i++)
	{
	  strcat(Arena_Monster->meleestr,"L?R?");
	}
    }
  else
    {
      char * generic_name = Arena_Monster->monstring;
      strcpy(Str1,nameprint());
      strcat(Str1," the ");
      strcat(Str1, generic_name );
      strcpy(Str2,"The corpse of ");
      strcat(Str2,Str1);
      mon_alloc_strings( Arena_Monster, Str1, Str2 );
    }
  Arena_Monster->uniqueness = UNIQUE_MADE;
  Arena_Monster->attacked = TRUE;
  m_status_set(Arena_Monster, MOBILE);
  m_status_set(Arena_Monster, HOSTILE);
  m_status_set(Arena_Monster, AWAKE);

  Arena_Monster->sense = 50;

  /* DAG pump up the stats of the arena monster; from env.c */
  /* DAG should we even do this for the champion? */
  if(vs_Arena_champion_p)
    {
      Arena_Monster->hp    = Championlevel*Championlevel*5;
      Arena_Monster->hit   = Championlevel*4;
      Arena_Monster->ac    = Championlevel*3;
      Arena_Monster->dmg   = 100+Championlevel*2;
      Arena_Monster->xpv   = Championlevel*Championlevel*5;
      Arena_Monster->speed = 3;
    }
  else
    {
      Arena_Monster->hp  += Arena_Monster->level*10;
      Arena_Monster->hit += Arena_Monster->hit;
      Arena_Monster->dmg += Arena_Monster->dmg/2;
    }

  base_Arena_reward += max(25,monsterlevel * 50);

  give_mon_object_by_id(Arena_Monster, OB_GARAGE_OPENER);

  return Arena_Monster;
}

/*  ***********************************  */
/*  Dispatch actions.  */

/* Unused, but could simplify maps. */
void copy_loc(plc source, plc target)
{
  target->p_locf     = source->p_locf    ;
  target->lstatus    = source->lstatus   ;
  target->roomnumber = source->roomnumber;
  target->locchar    = source->locchar   ;
  target->aux        = source->aux       ;
}

void make_logical_monster(sitemaker_params_t * p_site_parms, int index)
{
  int x = p_site_parms->x;
  int y = p_site_parms->y;
  monster * mon;
  switch(index)
    {
    default:
      printf("Unsupported logical monster: %d on (%d, %d)",
	     index, x, y );
      return;

    case mon_random:
      mon = make_creature(resolve_monster_id(RANDOM));
      break;

    case mon_elite_dragon:
      {
	mon = make_creature(DRAGON);
	mon->hit *= 2;
	mon->dmg *= 2;
      }
      break;

    case mon_undead_guard:
      {
	mon = make_creature(GUARD);
	mon->monstring = "undead guardsman";
	mon->meleef    = M_MELEE_SPIRIT;
	mon->movef     = M_MOVE_SPIRIT;
	mon->strikef   = M_STRIKE_MISSILE;
	mon->immunity = EVERYTHING - pow2(NORMAL_DAMAGE);
	mon->hp  *= 2;
	mon->hit *= 2;
	mon->dmg *= 2;
	mon->ac  *= 2;
	m_status_set(mon, HOSTILE);
	m_status_set(mon, AWAKE);
      }
      break;

    case mon_justiciar:
      mon = make_justiciar(x, y);
      break;

    case mon_house_npc:
      mon = make_house_npc(x, y);
      break;

    case mon_mansion_npc:
      mon = make_mansion_npc(x, y);
      break;

    case mon_high_priest:
      mon = make_high_priest(x, y, p_site_parms->p_env_params->subtype);
      break;

    case mon_prime_sorceror:
      mon = make_prime_sorceror(x, y);
      break;

    case mon_archmage:
      mon = make_sitting_archmage(x, y);
      break;

    case mon_arena_monster:
      mon = make_arena_monster( x, y );
      break;
    }

  if(mon)
    { add_monster_xy(Level, x, y, mon); }
}

void do_site_action(sitemaker_params_t * p_site_parms,
		    lmap_reify_data_table_t act)
{
  int index;
  for(index = 0; index < act.num_els; index++)
    {
      dispatch_reification( p_site_parms, &act.array[index]);
    }
}


void make_random_site(sitemaker_params_t * p_site_parms, int index)
{
  if((index < 0) || (index >= random_act_table.num_els))
    {
      printf("Invalid random site: %d on (%d, %d)",
	     index, p_site_parms->x, p_site_parms->y );
      return;
    }
  {
    random_action_t * p_table = random_act_table.array[index];
    int choice = random_pick(&p_table->t);
    do_site_action(p_site_parms, p_table->data_array[choice] );
  }
}


/*  Sites too anomalous to belong in the other functions and too
    special to belong in dispatch_reification.
*/
void make_logical_site(sitemaker_params_t * p_site_parms, int index)
{
  int x = p_site_parms->x;
  int y = p_site_parms->y;
  switch(index)
    {
    default:
      printf("Unsupported logical site: %d on (%d, %d)", index, x, y );
      break;

    case site_food_bin:
      make_food_bin(x, y);
      break;

    case site_random_altar:
      {
	plc p_site = &Level->site[x][y];
	p_site->locchar = ALTAR;
	p_site->p_locf = L_ALTAR;
	p_site->aux = random_range(10);
      }
      break;

    case site_alert_statue:
      {
	int j;
	Level->site[x][y].locchar = STATUE;
	if (random_range(100) < difficulty())
	  for (j=0;j<num_dirs;j++)
	    {
	      int ox = x+Dirs[0][j];
	      int oy = y+Dirs[1][j];
	      if(inbounds(ox, oy))
		{
		  plc p_site = &Level->site[ox][oy];
		  if (p_site->p_locf != L_NO_OP)
		    { p_site->locchar = FLOOR; }
		  p_site->p_locf = L_STATUE_WAKE;
		}
	    }
      }
      break;

    case site_all_stops:
      {
	int j;
	/* The cardinal directions and the (0,0) direction. */
	for (j = first_cardinal_dir; j < num_logical_dirs; j++)
	  {
	    int ox = x+Dirs[0][j];
	    int oy = y+Dirs[1][j];
	    if(inbounds(ox, oy))
	      { lset(ox,oy,STOPS); }
	  }
      }
      break;
    }
}

/*  **  Shuffled sites  **  */

/* Static data */
static int num_shuffled_sites_used=0;
static int permutation[64];


void init_shuffler(shuffled_site_table_t * table)
{
  assert(table);

  num_shuffled_sites_used = 0;
  SET_TO_MIN(table->shuffle_size, 64);
  shuffle(permutation, table->shuffle_size);
}

void make_shuffled_site(sitemaker_params_t * p_site_parms)
{
  shuffled_site_table_t * table =
    p_site_parms->shuffled_sites;
  if(!table)
    {
      printf("Shuffle table does not exist!" );
      return;
    }

  {
    sitemaker_params_t new_params = *p_site_parms;
    new_params.flags |= flag_position_varies;
    new_params.shuffled_sites = NULL; /* Shuffles shouldn't recurse.*/

    if(num_shuffled_sites_used >= table->shuffle_size)
      { /* Use the default if we overflow logically.  */
	do_site_action( &new_params, table->default_action); }
    else
      {
	int index = permutation[num_shuffled_sites_used];
	num_shuffled_sites_used++;
	if(index >= table->num_act_lists)
	  { /* Use the default if we overflow physically. */
	    do_site_action( &new_params, table->default_action);
	  }
	else
	  { /* Use the respective entry. */
	    do_site_action( &new_params, table->act_list[index]);
	  }
      }
  }
}


void dispatch_reification( sitemaker_params_t * p_site_parms,
				lmap_reify_data_t * data )
{
  env_params_t  * p_env_params = p_site_parms->p_env_params;
  int flags     = p_site_parms->flags;
  int param1    = data->param1;
  int x         = p_site_parms->x;
  int y         = p_site_parms->y;
  int populated = !(flags & flag_empty);
  location * p_site = &Level->site[x][y];

  if(!maskedtester_satisfied(flags, data->test))
    { return; }

  switch( data->action)
    {
    default:

      printf("Unsupported action: %d on (%d, %d)",
	     data->action, x, y );

      /** Actions to make monsters. **/

    case lmap_make_monster:
      if(populated && !p_site->creature)
      {
	make_site_monster(x, y, param1);
      }
      break;

    case lmap_make_logical_monster:
      if(populated && !p_site->creature)
      {
	make_logical_monster(p_site_parms, param1);
      }
      break;


    /** Actions to make treasure. **/

    case lmap_make_treasure:
      if(populated)
	{ make_site_treasure(x,y,param1); }
      break;

    case lmap_make_specific_treasure:
      if(populated)
	{ make_specific_treasure(x, y, param1); }
      break;


    /** Actions to make the location itself **/

    case lmap_set_locf:
      p_site->p_locf = param1;
      /* The site may be "knowable", ie Player can move to it by name.
	 A site is automatically known if it's in constant position.  */
      try_add_knowable_loc(param1, x, y, !(flags & flag_position_varies));
      break;

    case lmap_set_locchar:
      p_site->locchar = param1;
      break;

    case lmap_set_aux:
      p_site->aux = param1;
      break;

    case lmap_set_aux_to_subtype:
      p_site->aux = p_env_params->subtype;
      break;

    case lmap_set_loc_flags:
      lset(x,y,param1);
      break;

    case lmap_clear_loc_flags:
      lreset(x,y,param1);
      break;

    case lmap_set_roomnumber:
      p_site->roomnumber = param1;
      break;


    /** Actions to dispatch other actions. **/

    case lmap_make_shuffled_site:
      make_shuffled_site(p_site_parms);
      break;

    case lmap_make_random_site:
      make_random_site( p_site_parms, param1);
      break;

    case lmap_make_logical_site:
      make_logical_site( p_site_parms, param1);
      break;


    /**  Actions to remember map locations, for game use or placing
	submaps. **/

    case lmap_set_special_loc:
      change_special_loc(param1, x, y);
      break;

    case lmap_retain_min:
      special_loc_retain_min(param1, x, y);
      break;

    case lmap_retain_max:
      special_loc_retain_max(param1, x, y);
      break;

    case lmap_set_multi_pos:
      add_game_special_loc(param1, x, y);
      break;

    /** Copy the previous location.  (Unused but potentially useful) **/
    case lmap_copy_prev_loc:
      {
	plc target = &Level->site[x][y];
	if(x > 0)
	  {
	    plc source = &Level->site[x-1][y];
	    copy_loc(source, target);
	  }
	else
	  if(y > 0)
	    {
	      plc source = &Level->site[Level->level_width-1][y-1];
	      copy_loc(source, target);
	    }
	  else
	    { mprint("No lmap_copy_prev_loc at (0,0)."); }
      }
      break;
    }
}


void set_mon_safety(pmt mon, int flags)
{
  if(flags & flag_set_safety)
    {
      if(flags & flag_hostile)
	{
	  m_status_set(mon, HOSTILE);
	}
      else
	{
	  m_status_reset(mon, HOSTILE);	
	}
    }
}

/*  General post-processing */
/* But it always seems to deal only with monsters. */
void conform_site(sitemaker_params_t * p_site_parms)
{
  env_params_t  * p_env_params = p_site_parms->p_env_params;
  int flags     = p_site_parms->flags;
  int x         = p_site_parms->x;
  int y         = p_site_parms->y;
  int populated = !(flags & flag_empty);
  location * p_site = &Level->site[x][y];

  if(populated)
    {
      monster * mon = p_site->creature;
      if(mon)
	{
	  set_mon_safety(mon, flags);
	  /* Some monsters want to know their start location.  */
	  if((GUARD == mon->id) || (mon->movef == M_MOVE_LEASH))
	    {
	      mon->aux1 = x;
	      mon->aux2 = y;
	    }
	  else /* Angels in some locations parameterize on subtype.  */
	    {
	      int mid = mon->id;
	      if(mon_id_is_angel(mid) && (p_env_params->env_idx == E_TEMPLE))
		{
		  int deity = p_env_params->subtype;
		  /* aux1 field of an angel is its deity */
		  mon->aux1 = deity;
		  mon->monstring = angeltype( mid, deity );
		}
	    }
	}
    }
}

def_lookup_table_funcs(lmap_read_data_t, lmap, sym);


void reify_lmap_sym( char site,
		     readtable_t * p_table,
		     sitemaker_params_t * p_site_parms)
{
  while(p_table)
    {
      lmap_read_data_t* entry = lmap_find( site, p_table->data );
      if(entry)
	{
	  do_site_action(p_site_parms, entry->act );
	  conform_site(p_site_parms);
	  break;
	}
      p_table = p_table->superclass;
    }
}

