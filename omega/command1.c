/* omega copyright (C) by Laurence Raphael Brothers, 1987,1988,1989 */
/* command1.c */

/* Command lookup.  Rewritten 2001 by Tehom (Tom Breton).  Licensed
    under the terms of the * Omega license as part of Omega.
*/

#include "glob.h"
#include "keys.h"
#include "scr.h"  /* For the notice system. */


/***  Local declarations.  ***/
static unsigned long p_process_aux (void);
static void notice_stuff();

/*** Global definitions. ***/

/* Common states (command.h). */
/*  repeats_allowed is just a flag here, only matters whether it's 0.  */
const post_command_state_t base_state      = { 0, 0, };
const post_command_state_t unchanged_state = { 1, 0, };

/* Notice (glob.h). */
int32 notice_flags = 0;

/*** Local definitions ***/

/* Pseudo lookups, for error conditions. */
const cmd_lookup_t error_cmd_lookup = { '\0', no_dir, cmd_error, };
const cmd_lookup_t lookup_no_op     = { '\0', no_dir, cmd_no_op, };

/* Command string */
enum { Cmd_string_length = 20, };
static int Cmd_string_pos = 0;
static char Cmd_string[Cmd_string_length+1];





/*  *** Command lookup functions ***  */

/* Pseudo-command invoked if there's a problem with command lookup. */
post_command_state_t cmd_error(int dir, int first_time)
{
  sprintf(Str2, "%s : unknown command", Cmd_string);
  print3_nostore(Str2);
  return base_state; 
}

def_lookup_table_funcs(cmd_lookup_t, cmd_lookup, key);

/*  This would need to be managed wisely if we added more tables.  */
cmd_lookup_table_t get_command_table(void)
{
  if (Current_Environment == E_COUNTRYSIDE)
    {
      return country_command_table;
    }
  else
    {
      return dungeon_command_table;
    }
}

/* get_command is guaranteed to return a meaningful pointer.

   This works "as if" cmd_keymap looked up a new command.  It's not
   just more efficient, but important for getting direction commands
   properly.  It potentially allows some more advanced possibilities
   like "What does this key do?".
*/
const cmd_lookup_t * get_command(cmd_lookup_table_t table)
{
  Cmd_string_pos = 0;
  while(TRUE)
  {
    cmd_lookup_t * cmd_found;
    char Cmd = mgetc();
    if(Cmd_string_pos < Cmd_string_length)
      {
	Cmd_string[Cmd_string_pos] = Cmd;
	Cmd_string_pos++;
	Cmd_string[Cmd_string_pos] = '\0';
      }
    clear_if_necessary();
    cmd_found = cmd_lookup_find(Cmd, table );

    if(!cmd_found)
      { return &error_cmd_lookup; }
    if(cmd_found->command != cmd_keymap)
      { return cmd_found; }
    else
    {
      int next_table = cmd_found->dir;
      if((next_table < 0) || (next_table >= num_cmd_lookup_tables))
      { return &error_cmd_lookup; }
      table = *cmd_lookup_tables[cmd_found->dir];
    }
  }
  /*NOTREACHED*/
}

/*  *** Pseudo-command-loops ***  */

/*  Get a key that corresponds to a direction, or escape. */
int get_direction(char* prompt, int prompt_immediately )
{
  const cmd_lookup_t * move_cmd;
  int need_prompt = prompt_immediately;
  
  while(TRUE)
    {
      if(need_prompt)
	{
	  print3(prompt);
	  nprint3(how_to_escape);
	}

      move_cmd = get_command(dungeon_command_table);
      assert(move_cmd);
      
      /*  Escape always gets us out.  */
      if(move_cmd->command == cmd_escape)
	{ return no_dir; }

      { /*  Any command that has a meaningful direction gives that
	    direction. */ 
	int dir = move_cmd->dir;
	if(dir_OK_p(dir))
	  { return dir; }
      }
      
      need_prompt = TRUE;

      /* Can we print this on line 2 without obscuring anything
	 important? */ 
      /*print2("That's not a direction! ");*/
    }
}


/* move the cursor around, like for firing a wand, sets x and y to target */
int setspot (int * x, int * y)
{
  mprint("Targeting.... ? for help");
  omshowcursor(*x, *y);

  while(TRUE)
    {
      const cmd_lookup_t * move_cmd = get_command(dungeon_command_table);
      assert(move_cmd);

      /*  Escape always gets us out.  */
      if(move_cmd->command == cmd_escape)
	{ 
	  *x = *y = ABORT;
	  return FALSE;
	}

      /* Kluuge:  Fake a command set.  */
      if(move_cmd->command == cmd_rest)
	{
	  return TRUE;
	}

      /*  Any command that has a meaningful direction gives that
	  direction. */
      {
	int dir = move_cmd->dir;
	if(dir_OK_p(dir))
	  { 
	    movecursor(x, y, Dirs[0][dir], Dirs[1][dir]); 
	    set_notice_flags(notice_loc);
	  }
	else
	  /*  Anything else shows help.  */
	  {
	    /*  This message is obsolete. */
	    clearmsg();
	    mprint("Use vi keys or numeric keypad to move cursor to target.");
	    mprint("Hit the '.' key when done.  ");
	    mprint( how_to_escape );
	  }
      }
    }
}


/*  *** Command loop functions ***  */

/*  Handle pseudo-commands the player does not choose (as such). */
post_command_state_t force_move (void)
{
  /* Poor-man's paralysis. */
  if (pflagp(SKIP_PLAYER))
    {
      resetpflag(SKIP_PLAYER);
      RETURN_post_command_state(0, 10);
    }

  if ( Player.status[BERSERK] )
    { return cmd_goberserk(no_dir, TRUE); }

  /* Handle sleeping? */
  
  /*  Here is the place for state-like pseudocommands, if we ever have
      them.*/


  return unchanged_state;
}


/* Handle pseudo-commands the player has pre-arranged as if chosen (by
    options and such). */
post_command_state_t auto_act (void)
{
  /* Notice stuff, so player can see what he's doing, and so that
      we've already cleared notice-conditions that we could indirectly
      act on, lest they be acted on twice.  */
  
  notice_stuff();

  /*  Maybe pickup stuff.  */
  if( (notice_flags & notice_stuff_at_feet) != 0 )
    {
      notice_flags &= ~notice_stuff_at_feet;
      if((Current_Environment != E_COUNTRYSIDE) 
	 && optionp(PICKUP)
	 && list_size(Level->site[Player.x][Player.y].object_list))
	{ return cmd_pickup(no_dir, TRUE); }
    }
  
  return unchanged_state;
}


/* Variables shared by the command loop and cmd_repeat_cmd, a command
   that affects it. */
static const cmd_lookup_t * old_repeat_cmd = &lookup_no_op;
static const cmd_lookup_t * repeat_cmd;

/* This restores the last repeating command, unless it's been stopped
   by changing levels. */
post_command_state_t cmd_repeat_cmd(int dir, int first_time)
{
  repeat_cmd = old_repeat_cmd;
  RETURN_post_command_state(50, 0);
}

static void manage_goal_move(const cmd_lookup_t * cmd)
{
  const command_f_t command = cmd->command;
  if
    (
     (command == cmd_city_move) ||
     (command == cmd_moveto_spot))
    { setpflag(MOVING_TO_GOAL); }
  else
    { resetpflag(MOVING_TO_GOAL); }
}

post_command_state_t choose_act (void)
{
  static int repeat_times = 0;
  post_command_state_t state;

  /* Kluuge:  Make monsters be printed before player's turn. */
  notice_flags |= notice_vision;
  notice_stuff();

  if(notice_flags & notice_dont_repeat)
    {
      notice_flags &= ~notice_dont_repeat;

      if(repeat_times > 0)
	{
	  /* Protect the old repeat state, for possible resumption.
	     It needs protection because the process of calling
	     `cmd_repeat_cmd' overwrites repeat_cmd.  */
	  old_repeat_cmd = repeat_cmd;
	}

      repeat_times = 0; 
    }

  /* Do a command.  Either... */
  if (repeat_times > 0)
    {
      if (! optionp(JUMPMOVE))
	{ omshowcursor(Player.x,Player.y); }

      /* ...use the previous command again. */
      manage_goal_move(repeat_cmd);
      state = repeat_cmd->command(repeat_cmd->dir, FALSE);
      --repeat_times;
      return state;
    }
  else
    {
      /* ...or get a first-time command.  */
      cmd_lookup_table_t table = get_command_table();
      omshowcursor(Player.x,Player.y);
      repeat_cmd = get_command(table);
      assert(repeat_cmd);
      
      manage_goal_move(repeat_cmd);
      state = repeat_cmd->command(repeat_cmd->dir, TRUE);

      /* Here is the only place the *number* of repeats allowed
	 is considered. */
      repeat_times = state.repeats_allowed;
      return state;
    }
}

/* Deal with a new player turn.  */
unsigned long p_process (void)
{
  unsigned long duration = p_process_aux();

  /* A final check. */
  notice_stuff();
  
  return duration;
}

/* Deal with a new player turn. */
unsigned long p_process_aux (void)
{
  post_command_state_t state;

  /*  Once per turn, look for forced pseudo-commands.  */
  state = force_move();

  if(state.repeats_allowed == 0)
    {
      set_notice_flags(notice_dont_repeat);
    }
  if(state.duration > 0)
    {
      return state.duration;
    }


  while(TRUE)
    {
      /* Look for automatic commands.  */
      state = auto_act();

      if(state.repeats_allowed == 0)
	{
	  set_notice_flags(notice_dont_repeat);
	}
      if(state.duration > 0)
	{
	  /* If a forced move took time, we've done our action. */
	  return state.duration;
	}

      /* Look for deliberate commands, including repeating. */
      state = choose_act();

      if(state.repeats_allowed == 0)
	{
	  set_notice_flags(notice_dont_repeat);
	}
      if(state.duration > 0)
	{
	  /* If a deliberate move took time, we've done our action. */
	  return state.duration;
	}
    }
  /*NOTREACHED*/
}


/*  *** The notice system *** */

void set_notice_flags(long flags)
{
  notice_flags |= flags;
}

static void notice_stuff()
{
  enum
  {
    flags_handled =
      notice_lev_altered
    | notice_loc
    | notice_new_level
    | notice_pov_moved
    | notice_invalid_screen
    | notice_drew_menu
    | notice_equip
    | notice_x_mounted
    | notice_food
    | notice_vision
    | notice_py_data
    | notice_time
    | notice_moon_phase
    | notice_disturbed_mild
    | notice_hurt
    | notice_was_attacked
  };
  
  /* Bail out quick if there's nothing to do.  */
  if((notice_flags & flags_handled) == 0)
    { return; }

  /* The level has changed in some way.  */
  if(notice_flags & notice_lev_altered)
    {
      set_notice_flags(notice_vision);
    }

  if(notice_flags & (notice_loc | notice_new_level))
    {
      if (Current_Environment != E_COUNTRYSIDE)
	{
	  /* We may have changed rooms.  If we changed levels, we
	     surely did.  */ 
	  roomcheck_2((notice_flags & notice_new_level) != 0);
	}
      else
	{
	  /* We may have gotten lost. */
	  if(pflagp(LOST))
	    { set_notice_flags(notice_dont_repeat); }
	  /*  We may have changed terrain.  */
	  terrain_check();
	}

      if(notice_flags & notice_new_level)
	{
	  /* There is no "spot we were on last turn".  Kluuge: pretend
	      the current spot is that spot.  */
	  setlastxy(Player.x, Player.y);
	  /* Repeating last command is not safe, its setup may depend
	     on Level. */ 
	  repeat_cmd = old_repeat_cmd = &lookup_no_op;
	  
	  notice_flags |=
	    /* Player may want to stop whatever he's doing.  */
	    notice_dont_repeat |
	    /* Screen is invalid. */
	    notice_invalid_screen;
	}
      
      /* We may be standing on new stuff.  */
      /* The screen may need to scroll to our position. */
      /* We may see new things. */
      notice_flags |= notice_stuff_at_feet | notice_pov_moved |
	notice_vision; 
    }

  if(notice_flags & (notice_pov_moved | notice_invalid_screen) )
    {
      screencheck(Player.x, Player.y);
    }
  
  if(notice_flags & notice_drew_menu )
    {
      /* For MSDOS and Amiga, it's possible we should also call
	 show_screen(); as in cmd_setoptions.  */
      xredraw();
    }
  
  
  if(notice_flags & (notice_equip | notice_x_mounted) )
    { calc_melee(); }

  /* Imperfect.  Both food and mounted can force showflags. */
  if(notice_flags & notice_x_mounted) 
    { showflags(); }

  if(notice_flags & notice_food  )
    { foodcheck(); }

  if(notice_flags & notice_vision    )
    { drawvision(Player.x,Player.y); }

  if(notice_flags & notice_py_data  )
    { dataprint(); }

  if(notice_flags & notice_time )
    { display_time(); }

  if(notice_flags & notice_moon_phase )
    { display_moon_phase(); }

  /* Hard-coded test, but these disturbances should eventually be
     managed by player option. */
  if(notice_flags & (notice_hurt | notice_was_attacked))
    {
      notice_flags |= notice_dont_repeat;
    }
  /* notice_disturbed_mild is not acted on. */
  
  /*  Turn off the flags we dealt with.  */
  notice_flags &= ~ flags_handled;
}

