/* omega copyright (c) 1987-2001 by Laurence Raphael Brothers */
/* lev.h */

#ifndef INCLUDED_LEV_H
#define INCLUDED_LEV_H

/* get the list of objects at a particular location on a particular level */
list_t * get_object_list (level * lev, int x, int y);

/* get the list of monsters on a particular level */
list_t * get_monster_list (level * lev);

/* get the monster (if any) at a particular location on a particular level */
monster * get_monster (level * lev, int x, int y);

/* add a particular object to a particular place on a particular level */
void add_object_to_level (level * lev, object * obj, int x, int y);

#if 0
void add_monster_to_level (level * lev, monster * mon, int x, int y);
#endif

void remove_monster_from_level (level * lev, monster * mon);

/* move monster from one location to another on a level */
void move_monster (level * lev, monster * mon, int x, int y);

/* create a monster of type mid and place it on the current level at x,y */
monster * make_site_monster(int x, int y, int mid);

#endif /* INCLUDED_LEV_H */
