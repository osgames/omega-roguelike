/*
  loctype.h - location types
  Copyright (C) 2003 by Willian Sheldon Simms

  licensed as a part of Omega under the terms of the Omega
  license which can be found in the Omega source distribution
  in the directory omega-roguelike/omega/lib/license.txt
*/

#ifndef INCLUDED_LOCTYPE_H
#define INCLUDED_LOCTYPE_H

/*
  These location types are to be used for drawing maps and categorizing
  map locations. They are not meant to be duplicates or replacements of
  the L_??? values in defs.h, although there is considerable overlap.
 */

/*
  There are 47 different kinds of each of certain types of locations
  like walls, hedges, and water. Each such kind is represented by an
  enumeration constant consisting (textually) of a base word (e.g. WALL)
  followed by abbreviated indications of which corners and sides are
  also of the base word type. The 8 such abbreviations are TL, TR, BL,
  BR, T, B, L, R; which stand for "top left", "top right", "bottom left",
  and so on.

  For example, LOCTYPE_WALL_L is a wall which has a location of base type
  wall to it's left. The location to the left must then necessarily have
  a location base type of LOCTYPE_WALL concatenated with, at least, the
  abbreviation R.

  These enumeration constants can be unwieldy, but as they will appear
  only rarely in the rest of the code, that shouldn't be a problem.
 */

typedef enum loctype_t loctype_t;
enum loctype_t
{
  /*
   * WALL LOCATION TYPES
   */

  LOCTYPE_WALL_TL_TR_BL_BR_T_B_L_R,
  /* wall completely surrounded by walls */

  LOCTYPE_WALL_TL_TR_BL_T_B_L_R,
  LOCTYPE_WALL_TL_TR_BR_T_B_L_R,
  LOCTYPE_WALL_TL_BL_BR_T_B_L_R,
  LOCTYPE_WALL_TR_BL_BR_T_B_L_R,
  /* three diagonal neighbors are walls */

  LOCTYPE_WALL_TL_TR_T_B_L_R,
  LOCTYPE_WALL_TL_BL_T_B_L_R,
  LOCTYPE_WALL_TL_BR_T_B_L_R,
  LOCTYPE_WALL_TR_BL_T_B_L_R,
  LOCTYPE_WALL_TR_BR_T_B_L_R,
  LOCTYPE_WALL_BL_BR_T_B_L_R,
  /* two diagonal neighbors are walls */

  LOCTYPE_WALL_TL_T_B_L_R,
  LOCTYPE_WALL_TR_T_B_L_R,
  LOCTYPE_WALL_BL_T_B_L_R,
  LOCTYPE_WALL_BR_T_B_L_R,
  /* one diagonal neighbor is a wall */

  LOCTYPE_WALL_T_B_L_R,
  /* no diagonal neighbors are walls */

  LOCTYPE_WALL_TL_TR_T_L_R,
  LOCTYPE_WALL_TL_BL_T_B_L,
  LOCTYPE_WALL_TR_BR_T_B_R,
  LOCTYPE_WALL_BL_BR_B_L_R,
  /* neighbors on three complete sides are walls */

  LOCTYPE_WALL_TL_T_B_L,
  LOCTYPE_WALL_TL_T_L_R,
  LOCTYPE_WALL_TR_T_B_R,
  LOCTYPE_WALL_TR_T_L_R,
  LOCTYPE_WALL_BL_T_B_L,
  LOCTYPE_WALL_BL_B_L_R,
  LOCTYPE_WALL_BR_T_B_R,
  LOCTYPE_WALL_BR_B_L_R,
  /* as the last, with one extra non-wall diagonal neighbor */

  LOCTYPE_WALL_T_B_L,
  LOCTYPE_WALL_T_B_R,
  LOCTYPE_WALL_T_L_R,
  LOCTYPE_WALL_B_L_R,
  /* no diagonal neighbors, and only three adjacent neighbors are walls */

  LOCTYPE_WALL_TL_T_L,
  LOCTYPE_WALL_TR_T_R,
  LOCTYPE_WALL_BL_B_L,
  LOCTYPE_WALL_BR_B_R,
  /* 'solid' corner walls */

  LOCTYPE_WALL_B_R,
  LOCTYPE_WALL_B_L,
  LOCTYPE_WALL_T_R,
  LOCTYPE_WALL_T_L,
  /* 'hollow' corner walls */

  LOCTYPE_WALL_T_B,
  LOCTYPE_WALL_L_R,
  /* horizonzal and vertical walls */

  LOCTYPE_WALL_T,
  LOCTYPE_WALL_B,
  LOCTYPE_WALL_L,
  LOCTYPE_WALL_R,
  /* wall ends */

  LOCTYPE_WALL,
  /* 'column' wall */

  /*
   * HEDGE LOCATION TYPES
   */

  LOCTYPE_HEDGE_TL_TR_BL_BR_T_B_L_R,
  /* hedge completely surrounded by hedges */

  LOCTYPE_HEDGE_TL_TR_BL_T_B_L_R,
  LOCTYPE_HEDGE_TL_TR_BR_T_B_L_R,
  LOCTYPE_HEDGE_TL_BL_BR_T_B_L_R,
  LOCTYPE_HEDGE_TR_BL_BR_T_B_L_R,
  /* three diagonal neighbors are hedges */

  LOCTYPE_HEDGE_TL_TR_T_B_L_R,
  LOCTYPE_HEDGE_TL_BL_T_B_L_R,
  LOCTYPE_HEDGE_TL_BR_T_B_L_R,
  LOCTYPE_HEDGE_TR_BL_T_B_L_R,
  LOCTYPE_HEDGE_TR_BR_T_B_L_R,
  LOCTYPE_HEDGE_BL_BR_T_B_L_R,
  /* two diagonal neighbors are hedges */

  LOCTYPE_HEDGE_TL_T_B_L_R,
  LOCTYPE_HEDGE_TR_T_B_L_R,
  LOCTYPE_HEDGE_BL_T_B_L_R,
  LOCTYPE_HEDGE_BR_T_B_L_R,
  /* one diagonal neighbor is a hedge */

  LOCTYPE_HEDGE_T_B_L_R,
  /* no diagonal neighbors are hedges */

  LOCTYPE_HEDGE_TL_TR_T_L_R,
  LOCTYPE_HEDGE_TL_BL_T_B_L,
  LOCTYPE_HEDGE_TR_BR_T_B_R,
  LOCTYPE_HEDGE_BL_BR_B_L_R,
  /* neighbors on three complete sides are hedges */

  LOCTYPE_HEDGE_TL_T_B_L,
  LOCTYPE_HEDGE_TL_T_L_R,
  LOCTYPE_HEDGE_TR_T_B_R,
  LOCTYPE_HEDGE_TR_T_L_R,
  LOCTYPE_HEDGE_BL_T_B_L,
  LOCTYPE_HEDGE_BL_B_L_R,
  LOCTYPE_HEDGE_BR_T_B_R,
  LOCTYPE_HEDGE_BR_B_L_R,
  /* as the last, with one extra non-hedge diagonal neighbor */

  LOCTYPE_HEDGE_T_B_L,
  LOCTYPE_HEDGE_T_B_R,
  LOCTYPE_HEDGE_T_L_R,
  LOCTYPE_HEDGE_B_L_R,
  /* no diagonal neighbors, and only three adjacent neighbors are hedges */

  LOCTYPE_HEDGE_TL_T_L,
  LOCTYPE_HEDGE_TR_T_R,
  LOCTYPE_HEDGE_BL_B_L,
  LOCTYPE_HEDGE_BR_B_R,
  /* 'solid' corner hedges */

  LOCTYPE_HEDGE_B_R,
  LOCTYPE_HEDGE_B_L,
  LOCTYPE_HEDGE_T_R,
  LOCTYPE_HEDGE_T_L,
  /* 'hollow' corner hedges */

  LOCTYPE_HEDGE_T_B,
  LOCTYPE_HEDGE_L_R,
  /* horizonzal and vertical hedges */

  LOCTYPE_HEDGE_T,
  LOCTYPE_HEDGE_B,
  LOCTYPE_HEDGE_L,
  LOCTYPE_HEDGE_R,
  /* hedge ends */

  LOCTYPE_HEDGE,
  /* 'column' hedge */

  /*
   * WATER LOCATION TYPES
   */

  LOCTYPE_WATER_TL_TR_BL_BR_T_B_L_R,
  /* water completely surrounded by water */

  LOCTYPE_WATER_TL_TR_BL_T_B_L_R,
  LOCTYPE_WATER_TL_TR_BR_T_B_L_R,
  LOCTYPE_WATER_TL_BL_BR_T_B_L_R,
  LOCTYPE_WATER_TR_BL_BR_T_B_L_R,
  /* three diagonal neighbors are water */

  LOCTYPE_WATER_TL_TR_T_B_L_R,
  LOCTYPE_WATER_TL_BL_T_B_L_R,
  LOCTYPE_WATER_TL_BR_T_B_L_R,
  LOCTYPE_WATER_TR_BL_T_B_L_R,
  LOCTYPE_WATER_TR_BR_T_B_L_R,
  LOCTYPE_WATER_BL_BR_T_B_L_R,
  /* two diagonal neighbors are water */

  LOCTYPE_WATER_TL_T_B_L_R,
  LOCTYPE_WATER_TR_T_B_L_R,
  LOCTYPE_WATER_BL_T_B_L_R,
  LOCTYPE_WATER_BR_T_B_L_R,
  /* one diagonal neighbor is a water */

  LOCTYPE_WATER_T_B_L_R,
  /* no diagonal neighbors are water */

  LOCTYPE_WATER_TL_TR_T_L_R,
  LOCTYPE_WATER_TL_BL_T_B_L,
  LOCTYPE_WATER_TR_BR_T_B_R,
  LOCTYPE_WATER_BL_BR_B_L_R,
  /* neighbors on three complete sides are water */

  LOCTYPE_WATER_TL_T_B_L,
  LOCTYPE_WATER_TL_T_L_R,
  LOCTYPE_WATER_TR_T_B_R,
  LOCTYPE_WATER_TR_T_L_R,
  LOCTYPE_WATER_BL_T_B_L,
  LOCTYPE_WATER_BL_B_L_R,
  LOCTYPE_WATER_BR_T_B_R,
  LOCTYPE_WATER_BR_B_L_R,
  /* as the last, with one extra non-water diagonal neighbor */

  LOCTYPE_WATER_T_B_L,
  LOCTYPE_WATER_T_B_R,
  LOCTYPE_WATER_T_L_R,
  LOCTYPE_WATER_B_L_R,
  /* no diagonal neighbors, and only three adjacent neighbors are water */

  LOCTYPE_WATER_TL_T_L,
  LOCTYPE_WATER_TR_T_R,
  LOCTYPE_WATER_BL_B_L,
  LOCTYPE_WATER_BR_B_R,
  /* 'solid' corner canals */

  LOCTYPE_WATER_B_R,
  LOCTYPE_WATER_B_L,
  LOCTYPE_WATER_T_R,
  LOCTYPE_WATER_T_L,
  /* 'hollow' corner canals */

  LOCTYPE_WATER_T_B,
  LOCTYPE_WATER_L_R,
  /* horizonzal and vertical 'canals' */

  LOCTYPE_WATER_T,
  LOCTYPE_WATER_B,
  LOCTYPE_WATER_L,
  LOCTYPE_WATER_R,
  /* 'canal' ends */

  LOCTYPE_WATER,
  /* pool of water */

  /*
   * FLOOR LOCATION TYPES
   */

  LOCTYPE_FLOOR,
  LOCTYPE_FLOOR_SMOOTH,
  LOCTYPE_FLOOR_SCORCHED,

  /*
   * DOOR LOCATION TYPES
   */

  LOCTYPE_DOOR_OPEN,
  LOCTYPE_DOOR_LOCKED,
  LOCTYPE_DOOR_UNLOCKED,

  /*
   * TRAP LOCATION TYPES
   */

  LOCTYPE_TRAP_PIT,
  LOCTYPE_TRAP_ACID,
  LOCTYPE_TRAP_DART,
  LOCTYPE_TRAP_DOOR,
  LOCTYPE_TRAP_FIRE,
  LOCTYPE_TRAP_ABYSS,
  LOCTYPE_TRAP_BLADE,
  LOCTYPE_TRAP_SIREN,
  LOCTYPE_TRAP_SLEEP,
  LOCTYPE_TRAP_SNARE,
  LOCTYPE_TRAP_TELEPORT,
  LOCTYPE_TRAP_MANADRAIN,
  LOCTYPE_TRAP_PORTCULLIS,
  LOCTYPE_TRAP_DISINTEGRATE,
  LOCTYPE_TRAP_UNIDENTIFIED,

  /*
   * ALTAR LOCATION TYPES
   */

  LOCTYPE_ALTAR_SET,
  LOCTYPE_ALTAR_ODIN,
  LOCTYPE_ALTAR_DRUID,
  LOCTYPE_ALTAR_MAJOR,
  LOCTYPE_ALTAR_MINOR,
  LOCTYPE_ALTAR_ATHENA,
  LOCTYPE_ALTAR_HECATE,
  LOCTYPE_ALTAR_DESTINY,
  LOCTYPE_ALTAR_UNKNOWN,

  /*
   * STONE LOCATION TYPES
   */

  LOCTYPE_STONE_LAW,
  LOCTYPE_STONE_MIND,
  LOCTYPE_STONE_VOID,
  LOCTYPE_STONE_CHAOS,
  LOCTYPE_STONE_BALANCE,
  LOCTYPE_STONE_SACRIFICE,

  /*
   * CITY AND TOWN LOCATION TYPES
   */

  LOCTYPE_DPW,
  LOCTYPE_GYM,
  LOCTYPE_BANK,
  LOCTYPE_CLUB,
  LOCTYPE_CRAP,
  LOCTYPE_JAIL,
  LOCTYPE_PAWN,
  LOCTYPE_ARENA,
  LOCTYPE_CONDO,
  LOCTYPE_DINER,
  LOCTYPE_ORDER,
  LOCTYPE_SEWER,
  LOCTYPE_CASINO,
  LOCTYPE_CASTLE,
  LOCTYPE_HEALER,
  LOCTYPE_LEGION,
  LOCTYPE_ORACLE,
  LOCTYPE_TAVERN,
  LOCTYPE_ARMORER,
  LOCTYPE_BROTHEL,
  LOCTYPE_CHARITY,
  LOCTYPE_COLLEGE,
  LOCTYPE_GRANARY,
  LOCTYPE_LIBRARY,
  LOCTYPE_STABLES,
  LOCTYPE_THIEVES,
  LOCTYPE_TOURIST,
  LOCTYPE_FASTFOOD,
  LOCTYPE_ALCHEMIST,
  LOCTYPE_MONASTERY,
  LOCTYPE_SORCERORS,
  LOCTYPE_CARTOGRAPHER,

  /*
   * OTHER LOCATION TYPES
   */

  LOCTYPE_TRIFID,
  LOCTYPE_RUBBLE,

  LOCTYPE_LIFT,
  LOCTYPE_ESCALATOR,
  LOCTYPE_STAIRS_UP,
  LOCTYPE_STAIRS_DOWN,

  LOCTYPE_VOID,
  LOCTYPE_LAVA,
  LOCTYPE_ABYSS,
  LOCTYPE_SWAMP,

  LOCTYPE_SAFE,
  LOCTYPE_THRONE,
  LOCTYPE_STATUE,

  LOCTYPE_FIRE_STORM,
  LOCTYPE_ELECTRICAL_STORM,

  LOCTYPE_POOL,
  LOCTYPE_POOL_EERIE,
  LOCTYPE_POOL_BOILING,

  LOCTYPE_PORTCULLIS_RAISER,

  LOCTYPE_PORTCULLIS_VERTICAL_OPEN,
  LOCTYPE_PORTCULLIS_VERTICAL_CLOSED,

  LOCTYPE_PORTCULLIS_HORIZONTAL_OPEN,
  LOCTYPE_PORTCULLIS_HORIZONTAL_CLOSED,

  LOCTYPE_LAST
};

#endif /* INCLUDED_LOCTYPE_H */
