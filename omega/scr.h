/* omega copyright (c) 1987-2001 by Laurence Raphael Brothers */
/* scr.h */

#ifndef INCLUDED_SCR_H
#define INCLUDED_SCR_H

void display_time (void);
void display_moon_phase (void);

#endif /* INCLUDED_SCR_H */
