/* omega copyright (c) 1987,1988,1989 by Laurence Raphael Brothers */
/* gen1.c */
/* level generator functions */

#include <time.h>
#include "game_time.h"
#include "glob.h"
#include "lev.h"   /* make_site_monster() */
#include "liboutil/list.h"
#include "util.h"
#include "findloc.h"

static void corridor_crawl(level * lev, int * fx, int * fy, int sx, int sy, int n, Symbol loc, char rsi)
{
  int idx;
  int lfx, lfy;

  lfx = *fx;
  lfy = *fy;

  for (idx = 0; idx < n; ++idx)
    {
      lfx += sx;
      lfy += sy;

      if (lfx >= lev->level_width) continue;
      if (lfx < 0) continue;

      if (lfy >= lev->level_length) continue;
      if (lfy < 0) continue;

      lev->site[lfx][lfy].locchar = loc;

      if (lev->site[lfx][lfy].roomnumber == RS_WALLSPACE)
	lev->site[lfx][lfy].roomnumber = rsi;

      if (loc == WATER) 
	lev->site[lfx][lfy].p_locf = L_WATER;
      else if (loc == FLOOR) 
	lev->site[lfx][lfy].p_locf = L_NO_OP;
      else if (loc == RUBBLE)
	lev->site[lfx][lfy].p_locf = L_RUBBLE;
    }

  *fx = lfx;
  *fy = lfy;
}  

/* keep going in one orthogonal direction or another until we hit our */
/* destination */

void straggle_corridor (level * lev, int fx, int fy, int tx, int ty, Symbol loc, char rsi)
{
  int dx,dy;

  while ((fx != tx) || (fy != ty))
    {
      dx = tx - fx;
      dy = ty - fy;

      if (random_range(abs(dx)+abs(dy)) < abs(dx))
	corridor_crawl(lev, &fx, &fy, sign(dx), 0, random_range(abs(dx)) + 1, loc, rsi);
      else
	corridor_crawl(lev, &fx, &fy, 0, sign(dy), random_range(abs(dy)) + 1, loc, rsi);
  }
}

void makedoor (level * lev, int x, int y)
{
  if (random_range(20) <= (lev->depth / 10))
    {
      lev->site[x][y].locchar = FLOOR;
      lset(lev, x, y, SECRET);
    }
  else if (random_range(20) <= (lev->depth / 2))
    {
      lev->site[x][y].locchar = CLOSED_DOOR;

      if (random_range(20) <= (lev->depth / 10))
	lset(lev, x, y, SECRET);
      if (random_range(40) <= (lev->depth))
	lev->site[x][y].aux = LOCKED;
      else
	lev->site[x][y].aux = UNLOCKED;
    }
  else
    {
      lev->site[x][y].locchar = OPEN_DOOR;
      lev->site[x][y].aux = UNLOCKED;
    }

  if (loc_statusp(lev, x, y, SECRET) == FALSE)
    {
      lset(lev, x,   y+1, STOPS);
      lset(lev, x+1, y,   STOPS);
      lset(lev, x-1, y,   STOPS);
      lset(lev, x,   y-1, STOPS);
      lset(lev, x,   y,   STOPS);
    }

  /* prevents water corridors from being instant death in sewers */
  lev->site[x][y].p_locf = L_NO_OP; 
}


char *roomname(int index)
{
  switch(index) {
  case RS_ZORCH:strcpy(Str4,"A place zorched by powerful magic.");break;
  case RS_COURT:strcpy(Str4,"The Court of the ArchMage."); break;
  case RS_CIRCLE:strcpy(Str4,"The Astral Demesne of the Circle of Sorcerors");
    break;
  case RS_MAGIC_ISLE: strcpy(Str4,"An island positively reeking of magic");
    break;
  case RS_STARPEAK: strcpy(Str4,"Near the oddly glowing peak of a mountain");
    break;
  case RS_VOLCANO: strcpy(Str4,"Deep within the bowels of the earth"); break;
  case RS_HIGHASTRAL: strcpy(Str4,"The High Astral Plane"); break;
  case RS_EARTHPLANE: strcpy(Str4,"The Plane of Earth"); break;
  case RS_WATERPLANE: strcpy(Str4,"The Plane of Water"); break;
  case RS_FIREPLANE: strcpy(Str4,"The Plane of Fire"); break;
  case RS_AIRPLANE: strcpy(Str4,"The Plane of Air"); break;
  case RS_KITCHEN: strcpy(Str4,"A kitchen"); break;
  case RS_BATHROOM: strcpy(Str4,"A bathroom"); break;
  case RS_BEDROOM: strcpy(Str4,"A bedroom"); break;
  case RS_DININGROOM: strcpy(Str4,"A dining room"); break;
  case RS_SECRETPASSAGE: strcpy(Str4,"A secret passage"); break;
  case RS_CLOSET: strcpy(Str4,"A stuffy closet"); break;
  case RS_ARENA: strcpy(Str4,"The Rampart Arena"); break;
  case RS_DROWNED_SEWER: strcpy(Str4,"A water-filled sewer node"); break;
  case RS_DRAINED_SEWER: strcpy(Str4,"An unused sewer node"); break;
  case RS_SEWER_DUCT: strcpy(Str4,"A winding sewer duct"); break;
  case RS_DESTINY: strcpy(Str4,"The Halls of Fate"); break;
  case RS_DRUID: strcpy(Str4,"The Great Henge"); break;
  case RS_HECATE: strcpy(Str4,"The Church of the Far Side"); break;
  case RS_SET: strcpy(Str4,"The Temple of the Black Hand"); break;
  case RS_ATHENA: strcpy(Str4,"The Parthenon"); break;
  case RS_ODIN: strcpy(Str4,"The Shrine of the Noose"); break;
  case RS_ADEPT: strcpy(Str4,"The Adept's Challenge"); break;
  case RS_WYRM: strcpy(Str4,"The Sunken Cavern of the Great Wyrm."); break;
  case RS_OCEAN: strcpy(Str4,"The Underground Ocean."); break;
  case RS_PONDS: strcpy(Str4,"A series of subterranean pools and streams."); break;
  case RS_DRAGONLORD: strcpy(Str4,"The Lair of the DragonLord."); break;
  case RS_GOBLINKING: strcpy(Str4,"The Caves of the Goblins."); break;
  case RS_CAVERN: strcpy(Str4,"A vast natural cavern."); break;
  case RS_CORRIDOR: strcpy(Str4,"A dimly lit corridor."); break;
  case RS_WALLSPACE: strcpy(Str4,"A niche hollowed out of the wall."); break;
  /* following are above ROOMBASE */
  case RS_GARDEROBE: strcpy(Str4,"An abandoned garderobe."); break;
  case RS_CELL: strcpy(Str4,"A dungeon cell."); break;
  case RS_TILED: strcpy(Str4,"A tiled chamber."); break;
  case RS_CRYSTAL_CAVE: strcpy(Str4,"A crystal cavern."); break;
  case RS_BEDROOM2: strcpy(Str4,"Someone's bedroom."); break;
  case RS_STOREROOM: strcpy(Str4,"An old storeroom."); break;
  case RS_CHARRED: strcpy(Str4,"A room with charred walls."); break;
  case RS_MARBLE_HALL: strcpy(Str4,"A marble hall."); break;
  case RS_EERIE_CAVE: strcpy(Str4,"An eerie cave."); break;
  case RS_TREASURE: strcpy(Str4,"A ransacked treasure-chamber."); break;
  case RS_SMOKEY: strcpy(Str4,"A smoke-filled room."); break;
  case RS_APARTMENT: strcpy(Str4,"A well-appointed apartment."); break;
  case RS_ANTECHAMBER: strcpy(Str4,"An antechamber."); break;
  case RS_HAREM: strcpy(Str4,"An unoccupied harem."); break;
  case RS_MULTIPURPOSE: strcpy(Str4,"A multi-purpose room."); break;
  case RS_STALACTITES: strcpy(Str4,"A room filled with stalactites."); break;
  case RS_GREENHOUSE: strcpy(Str4,"An underground greenhouse."); break;
  case RS_WATERCLOSET: strcpy(Str4,"A water closet."); break;
  case RS_STUDY: strcpy(Str4,"A study."); break;
  case RS_LIVING_ROOM: strcpy(Str4,"A living room."); break;
  case RS_DEN: strcpy(Str4,"A comfortable den."); break;
  case RS_ABATOIR: strcpy(Str4,"An abatoir."); break;
  case RS_BOUDOIR: strcpy(Str4,"A boudoir.");break;
  case RS_STAR_CHAMBER: strcpy(Str4,"A star chamber.");break;
  case RS_MANMADE_CAVE: strcpy(Str4,"A manmade cavern."); break;
  case RS_SEWER_CONTROL: strcpy(Str4,"A sewer control room");break;
  case RS_SHRINE: strcpy(Str4,"A shrine to High Magic"); break;
  case RS_MAGIC_LAB: strcpy(Str4,"A magic laboratory"); break;
  case RS_PENTAGRAM: strcpy(Str4,"A room with inscribed pentagram");break;
  case RS_OMEGA_DAIS: strcpy(Str4,"A chamber with a blue crystal omega dais");
    break;
     /* WDT: removed period from description. */
  default: strcpy(Str4,"A room of mystery and allure"); break;
  }
  return(Str4);
}

void install_traps (level * lev)
{
  int idx, jdx;
  unsigned int trap_chance;

  assert(lev != NULL);

  trap_chance = lev->depth / 6;

  for (idx = 0; idx < lev->level_width; ++idx)
    for (jdx = 0; jdx < lev->level_length; ++jdx)
      {
	if (lev->site[idx][jdx].locchar != FLOOR) continue;
	if (lev->site[idx][jdx].p_locf != L_NO_OP) continue;
	if (random_range(500) > trap_chance) continue;

	lev->site[i][j].p_locf = TRAP_BASE + random_range(NUMTRAPS);
      }
}


/* x, y, is top left corner, l is length of side, rsi is room string index */
/* baux is so all rooms will have a key field. */
void build_square_room(int x, int y, int l, char rsi, int baux)
{
  int i,j;

  for(i=x;i<=x+l;i++)
    for(j=y;j<=y+l;j++){
      Level->site[i][j].roomnumber = rsi;
      Level->site[i][j].buildaux = baux;
    }
  for(i=x+1;i<x+l;i++)
    for(j=y+1;j<y+l;j++) {
      Level->site[i][j].locchar = FLOOR;
      Level->site[i][j].p_locf = L_NO_OP;
    }
}


void build_room(int x, int y, int l, char rsi, int baux)
{
  build_square_room(x,y,l,rsi,baux);
}

void cavern_level (void)
{
  int idx;
  int fx, fy, tx, ty;
  int top, left, edge;
  char rsi;

  Level->numrooms = 1;

  if ((Current_Dungeon == E_CAVES) && (Level->depth == MaxDungeonLevels))
    rsi = RS_GOBLINKING;
  else
    rsi = RS_CAVERN;

  top = random_range(Level->level_length / 2);
  left = random_range(Level->level_width / 2);
  edge = random_range(Level->level_width / 8) + (Level->level_width / 8);

  build_square_room(top, left, edge, rsi, 0);
  
  for (idx = 0; idx < 16; ++idx)
    {
      find_floorspace(&tx, &ty);
      fx = 1 + random_range(Level->level_width - 2);
      fy = 1 + random_range(Level->level_length - 2);
      straggle_corridor(fx, fy, tx, ty, FLOOR, RS_CORRIDOR);
    }

  while (random_range(3) == 1)
    {
      find_floorspace(&tx, &ty);
      fx = 1 + random_range(Level->level_width - 2);
      fy = 1 + random_range(Level->level_length - 2);
      straggle_corridor(fx, fy, tx, ty, WATER, RS_PONDS);
  }

  if (Current_Dungeon == E_CAVES)
    {
      if ((Level->depth == MaxDungeonLevels) && (!gamestatusp(COMPLETED_CAVES)))
        {
          find_floorspace(&tx, &ty);
          make_site_monster(tx, ty, GOBLIN_KING);
        }
    }
  else if (Current_Environment == E_VOLCANO)
    {
      if (Level->depth == MaxDungeonLevels)
        {
          find_floorspace(&tx, &ty);
          make_site_monster(tx, ty, DEMON_EMP);
        }
    }
}


void sewer_level (void)
{
  int idx;
  int tx, ty;
  int top, left, edge;
  char rsi;
  Symbol lchar;

  Level->numrooms = 3 + random_range(3);
  rsi = RS_DRAINED_SEWER;

  for (idx = 0; idx < Level->numrooms; ++idx)
    {
      do
        {
          top = 1 + random_range(Level->level_length - 10);
          left = 1 + random_range(Level->level_width - 10);
          edge = 4;
        }
      while ((Level->site[left][top].roomnumber == rsi)
             || (Level->site[left + edge][top].roomnumber == rsi)
             || (Level->site[left][top + edge].roomnumber == rsi)
             || (Level->site[left + edge][top + edge].roomnumber == rsi));

      if (random_range(5))
        {
          lchar = FLOOR;
          rsi = RS_DRAINED_SEWER;
        }
      else
        {
          lchar = WATER;
          rsi = RS_DROWNED_SEWER;
        }

      build_room(left, top, edge, rsi, idx);
      sewer_corridor(left, top, -1, -1, lchar);
      sewer_corridor(left + edge, top, 1, -1, lchar);
      sewer_corridor(left, top + edge, -1, 1, lchar);
      sewer_corridor(left + edge, top + edge, 1, 1, lchar);
    }

  if (Current_Dungeon == E_SEWERS)
    {
      if ((Level->depth == MaxDungeonLevels) && !gamestatusp(COMPLETED_SEWERS))
        {
          find_floorspace(&tx,&ty);
          make_site_monster(tx, ty, GREAT_WYRM);
        }
    }
}

void sewer_corridor(int x, int y, int dx, int dy, Symbol locchar)
{
  int continuing = TRUE;
  makedoor(x,y);
  x+=dx;
  y+=dy;
  while(continuing) {
    Level->site[x][y].locchar = locchar;
    if (locchar == WATER)
      Level->site[x][y].p_locf = L_WATER;
    else Level->site[x][y].p_locf = L_NO_OP;
    Level->site[x][y].roomnumber = RS_SEWER_DUCT;
    x+=dx;
    y+=dy;
  if (locchar == WATER)
    continuing = (inbounds(x,y) && 
		  ((Level->site[x][y].locchar == WALL) ||
		   (Level->site[x][y].locchar == WATER)));
  else
    continuing = (inbounds(x,y) && 
		  ((Level->site[x][y].roomnumber == RS_WALLSPACE) ||
		   (Level->site[x][y].roomnumber == RS_SEWER_DUCT)));
  }
  if (inbounds(x,y))
    makedoor(x,y);
}

void install_specials (level * lev)
{
  int x, y;
  int which;

  assert(lev != NULL);

  for (x = 0; x < lev->level_width; ++x)
    for(y = 0; y < lev->level_length; ++y)
      {
	if (lev->site[x][y].locchar != FLOOR) continue;
	if (lev->site[x][y].p_locf != L_NO_OP) continue;
	if (random_range(300) >= difficulty()) continue;

	which = random_range(100);

	if (which < 10)
	  {
	    lev->site[x][y].locchar = ALTAR;
	    lev->site[x][y].p_locf = L_ALTAR;
	    lev->site[x][y].aux = random_range(10);
	  }
	else if (which < 20)
	  {
	    lev->site[x][y].locchar = WATER;
	    lev->site[x][y].p_locf = L_MAGIC_POOL;
	  }
	else if (which < 35)
	  {
	    lev->site[x][y].locchar = RUBBLE;
	    lev->site[x][y].p_locf = L_RUBBLE;
	  }
	else if (which < 40)
	  {
	    lev->site[x][y].locchar = LAVA;
	    lev->site[x][y].p_locf = L_LAVA;
	  }
	else if (which < 45)
	  {
	    lev->site[x][y].locchar = FIRE;
	    lev->site[x][y].p_locf = L_FIRE;
	  }
	else if ((which < 50) && (lev->environment != E_ASTRAL))
	  {
	    lev->site[x][y].locchar = LIFT;
	    lev->site[x][y].p_locf = L_LIFT;
	  }
	else if ((which < 55) && (lev->environment != E_VOLCANO))
	  {
	    lev->site[x][y].locchar = HEDGE;
	    lev->site[x][y].p_locf = L_HEDGE;
	  }
	else if (which < 57)
	  {
	    lev->site[x][y].locchar = HEDGE;
	    lev->site[x][y].p_locf = L_TRIFID;
	  }
	else if (which < 70)
	  {
	    lev->site[x][y].locchar = STATUE;
	    if (random_range(100) < difficulty())
	      {
		int idx;
		for (idx = 0; idx < 8; ++idx)
		  {
		    if (lev->site[x + Dirs[0][idx]][y + Dirs[1][idx]].p_locf != L_NO_OP)
		      lev->site[x + Dirs[0][idx]][y + Dirs[1][idx]].locchar = FLOOR;
		    lev->site[x + Dirs[0][idx]][y + Dirs[1][idx]].p_locf = L_STATUE_WAKE;
		  }
	      }
	  }
	else
	  {
	    if (lev->environment == E_VOLCANO)
	      {
		lev->site[x][y].locchar = LAVA;
		lev->site[x][y].p_locf = L_LAVA;
	      }
	    else if (lev->environment == E_ASTRAL)
	      {
		if (lev->depth == 1)
		  {
		    lev->site[x][y].locchar = RUBBLE;
		    lev->site[x][y].p_locf = L_RUBBLE;
		  }
		else if (lev->depth == 2)
		  {
		    lev->site[x][y].locchar = FIRE;
		    lev->site[x][y].p_locf = L_FIRE;
		  }
		else if (lev->depth == 3)
		  {
		    lev->site[x][y].locchar = WATER;
		    lev->site[x][y].p_locf = L_WATER;
		  }
		else if (lev->depth == 4)
		  {
		    lev->site[x][y].locchar = ABYSS;
		    lev->site[x][y].p_locf = L_ABYSS;
		  }
	      }
	    else
	      {
		lev->site[x][y].locchar = WATER;
		lev->site[x][y].p_locf = L_WATER;
	      }
	  }

	try_add_knowable_loc(lev, lev->site[x][y].p_locf, x, y, FALSE);
      }
}


/* For each level, there should be one stairway going up and one down.
fromlevel determines whether the player is placed on the up or the down
staircase. The aux value is currently unused elsewhere, but is set
to the destination level. */
void make_stairs_new(void)
{
  int i,j;
  if (Level->environment == E_ASTRAL)
    {
      find_floorspace(&i,&j);
      change_special_loc(sl_py_entrance, i, j);
    }
  else
    {
      find_floorspace(&i,&j);
      change_special_loc(sl_py_entrance, i, j);
      Level->site[i][j].locchar = STAIRS_UP;
      Level->site[i][j].p_locf = L_STAIRS_UP;
      lset(i,j,STOPS);
      try_add_knowable_loc(Level, Level->site[i][j].p_locf, i, j, FALSE);
    }


  if (Level->depth < MaxDungeonLevels)
    {
      find_floorspace(&i,&j);
      change_special_loc(sl_py_emerge, i, j);
      Level->site[i][j].locchar = STAIRS_DOWN;
      Level->site[i][j].p_locf = L_STAIRS_DOWN;
      lset(i,j,STOPS);
      try_add_knowable_loc(Level, Level->site[i][j].p_locf, i, j, FALSE);
    }
}


/* tactical map generating functions */

void make_country_screen(Symbol terrain)
{
  Level->environment = E_TACTICAL_MAP;
  Level->generated = TRUE;

  Level->level_length = TACTICAL_LENGTH;
  Level->level_width = TACTICAL_WIDTH;
  Lev_Length = Level->level_length;
  Lev_Width  = Level->level_width;

  switch(terrain) {
  case FOREST: make_forest(); break;
  case JUNGLE: make_jungle(); break;
  case SWAMP: make_swamp(); break;
  case RIVER: make_river(); break;
  case MOUNTAINS: case PASS: make_mountains(); break;
  case ROAD: make_road(); break;
  default: make_plains(); break;
  }
}

void make_general_map(char *terrain)
{
  int i, j;
  int size = strlen(terrain);
  char curr;

  for (i=0;i<Level->level_width;i++)
    for (j=0;j<Level->level_length;j++) {
      /* Use showchar to cache the character. */
      if ((i == 0 && j == 0) || !random_range(5))
	curr = terrain[random_range(size)];
      else if (j == 0 || (random_range(2) && i > 0))
	curr = Level->site[i - 1][j].showchar;
      else
	curr = Level->site[i][j - 1].showchar;
      Level->site[i][j].showchar = curr;
      switch (curr) {
	case (FLOOR&0xff):
	  Level->site[i][j].locchar = Level->site[i][j].showchar = FLOOR;
	  Level->site[i][j].p_locf = L_NO_OP;
	  break;
	case (HEDGE&0xff):
	  Level->site[i][j].locchar = Level->site[i][j].showchar = HEDGE;
	  Level->site[i][j].p_locf = L_HEDGE;
	  break;
	case (WATER&0xff):
	  Level->site[i][j].locchar = Level->site[i][j].showchar = WATER;
	  Level->site[i][j].p_locf = L_WATER;
	  break;
	case (RUBBLE&0xff):
	  Level->site[i][j].locchar = Level->site[i][j].showchar = RUBBLE;
	  Level->site[i][j].p_locf = L_RUBBLE;
	  break;
      }
      Level->site[i][j].lstatus = SEEN+LIT;
      Level->site[i][j].roomnumber = RS_COUNTRYSIDE;
      if ((i == 0) || (j == 0) || (i == (Level->level_width)-1) || (j == (Level->level_length)-1))
	Level->site[i][j].p_locf = L_TACTICAL_EXIT;
    }
}

void make_plains(void)
{
  make_general_map(".");
}

void make_road(void)
{
  int x, y;
  int middle_x = (Level->level_width)/2;
  make_general_map("\"\"~4....");
  for (x = middle_x - 3; x <= middle_x + 3; x++)
    for (y = 0; y < Level->level_length; y++) {
      Level->site[x][y].locchar = Level->site[x][y].showchar = FLOOR;
      if (y != 0 && y != ((Level->level_length) - 1))
	Level->site[x][y].p_locf = L_NO_OP;
    }
}



void make_forest(void)
{
  make_general_map("\".");
  straggle_corridor(0,random_range(Level->level_length),Level->level_width,random_range(Level->level_length),
    WATER,RS_COUNTRYSIDE);
}


void make_jungle(void)
{
  make_general_map("\"\".");
}


void make_river(void)
{
  int i,y,y1;
  make_general_map("\".......");
  y = random_range(Level->level_length);
  y1 = random_range(Level->level_length);
  straggle_corridor(0,y,Level->level_width,y1,WATER,RS_COUNTRYSIDE);
  for(i=0;i<7;i++) {
    if (y > (Level->level_length)/2) y--;
    else y++;
    if (y1 > (Level->level_length)/2) y1--;
    else y1++;
    straggle_corridor(0,y,Level->level_width,y1,WATER,RS_COUNTRYSIDE);
  }
}


void make_mountains(void)
{
  int i,x,y,x1,y1;
  make_general_map("4...");
  x = 0;
  y = random_range(Level->level_length);
  x1 = Level->level_width;
  y1 = random_range(Level->level_length);
  straggle_corridor(x,y,x1,y1,WATER,RS_COUNTRYSIDE);
  for(i=0;i<7;i++) {
    x = random_range(Level->level_width);
    x1 = random_range(Level->level_width);
    y = 0;
    y1 = Level->level_length;
    straggle_corridor(x,y,x1,y1,WATER,RS_COUNTRYSIDE);
  }
}


void make_swamp(void)
{
  make_general_map("~~\".");
}

/* builds a room. Then, for each successive room, sends off at least one
corridor which is guaranteed to connect up to another room, thus guaranteeing
a fully connected level. */

void room_level(void)
{
  int idx;
  int fx, fy, tx, ty;
  int top, left, edge;
  char rsi;

  Level->numrooms = 9 + random_range(8);

  do
    {
      top = 1 + random_range(Level->level_length - 10);
      left = 1 + random_range(Level->level_width - 10);
      edge = 4 + random_range(5);
    }
  while ((Level->site[left][top].roomnumber != RS_WALLSPACE)
         || (Level->site[left + edge][top].roomnumber != RS_WALLSPACE)
         || (Level->site[left][top + edge].roomnumber != RS_WALLSPACE)
         || (Level->site[left + edge][top + edge].roomnumber != RS_WALLSPACE));

  if (Current_Dungeon == E_SEWERS)
    {
      if (random_range(2))
        rsi = RS_SEWER_CONTROL;
      else
        rsi = ROOMBASE + random_range(NUMROOMNAMES);
    }
  else
    rsi = ROOMBASE + random_range(NUMROOMNAMES);

  build_room(left, top, edge, rsi, 1);

  for (idx = 2; idx <= Level->numrooms; ++idx)
    {
      do
        {
          top = 1 + random_range(Level->level_length - 10);
          left = 1 + random_range(Level->level_width - 10);
          edge = 4 + random_range(5);
        }
      while ((Level->site[left][top].roomnumber != RS_WALLSPACE)
             || (Level->site[left + edge][top].roomnumber != RS_WALLSPACE)
             || (Level->site[left][top + edge].roomnumber != RS_WALLSPACE)
             || (Level->site[left + edge][top + edge].roomnumber != RS_WALLSPACE));

      if (Current_Dungeon == E_SEWERS)
        {
          if (random_range(2))
            rsi = RS_SEWER_CONTROL;
          else
            rsi = ROOMBASE + random_range(NUMROOMNAMES);
        }
      else
        rsi = ROOMBASE + random_range(NUMROOMNAMES);

      build_room(left, top, edge, rsi, idx);

      /* Find a corridor which is guaranteed to connect, or,
       * if there is no starting point for it, then our current room
       * might have been big enough to wipe out all previous rooms on
       * the level (it's rare but possible).  In that case just consider
       * this the new first room, and continue.  -- Richard Braakman */

      if (!findspace(&tx, &ty, idx))
        continue;

      /* figure out where to start corridor from */
      if (ty <= top && tx <= (left + edge))
        {
          fx = 1 + left + random_range(edge - 1);
          fy = top;
        }
      else if (tx >= (left + edge) && ty <= (top + edge))
        {
          fx = left + edge;
          fy = 1 + top + random_range(edge - 1);
        }
      else if (ty >= (top + edge) && tx >= left)
        {
          fx = 1 + left + random_range(edge - 1);
          fy = top + edge;
        }
      else
        {
          fx = left;
          fy = 1 + top + random_range(edge - 1);
        }

      room_corridor(fx, fy, tx, ty, idx);

      /* corridor which may not go anywhere */
      if (random_range(2))
        {
          findspace(&tx, &ty, idx);

          if (ty <= top && tx <= (left + edge))
            {
              fx = 1 + left + random_range(edge - 1);
              fy = top;
            }
          else if (tx >= (left + edge) && ty <= (top + edge))
            {
              fx = left + edge;
              fy = 1 + top + random_range(edge - 1);
            }
          else if (ty >= (top + edge) && tx >= left)
            {
              fx = 1 + left + random_range(edge - 1);
              fy = top + edge;
            }
          else
            {
              fx = left;
              fy = 1 + top + random_range(edge - 1);
            }

          room_corridor(fx, fy, tx, ty, idx);
        }
    }

  if (Current_Dungeon == E_SEWERS)
    {
      if (Level->depth == MaxDungeonLevels)
        {
          find_floorspace(&tx, &ty);
          make_site_monster(tx, ty, GREAT_WYRM);
        }
    }
  else if (Current_Dungeon == E_PALACE)
    {
      if (Level->depth == MaxDungeonLevels)
        {
          find_floorspace(&tx, &ty);
          make_site_monster(tx, ty, MAHARAJA);
        }
    }
  else if (Current_Environment == E_VOLCANO)
    {
      if (Level->depth == MaxDungeonLevels && !gamestatusp(COMPLETED_VOLCANO))
        {
          find_floorspace(&tx, &ty);
          make_site_monster(tx, ty, DEMON_EMP);
        }
    }
  else if (Current_Environment == E_CASTLE)
    {
      if (Level->depth == MaxDungeonLevels)
        {
          find_floorspace(&tx,&ty);
          Level->site[tx][ty].locchar = STAIRS_DOWN;
          Level->site[tx][ty].p_locf = L_ENTER_COURT;
        }
    }
}

/* goes from f to t unless it hits a site which is not a wall and doesn't
   have buildaux field == baux */
void room_corridor(int fx, int fy, int tx, int ty, int baux)
{
  int dx,dy,continuing = TRUE;

  dx = sign(tx-fx);
  dy = sign(ty-fy);

  makedoor(fx,fy);

  fx+=dx;
  fy+=dy;

  while(continuing) {
    Level->site[fx][fy].locchar = FLOOR;
    Level->site[fx][fy].roomnumber = RS_CORRIDOR;
    Level->site[fx][fy].buildaux = baux;
    dx = sign(tx-fx);
    dy = sign(ty-fy);
    if ((dx != 0) && (dy != 0)) {
      if (random_range(2)) dx = 0;
      else if (random_range(2)) dy = 0;
    }
    fx+=dx;
    fy+=dy;
    continuing = (((fx != tx) || (fy != ty)) &&
		  ((Level->site[fx][fy].buildaux == 0) ||
		   (Level->site[fx][fy].buildaux == baux)));
  }
  makedoor(fx,fy);
}

void maze_level (void)
{
  int mid;
  int y_idx;
  int x_idx;
  int tx, ty;

  char rsi = RS_VOLCANO;

  if (E_ASTRAL == Current_Environment)
    {
      switch (Level->depth)
        {
        case 1: rsi = RS_EARTHPLANE; break;
        case 2: rsi = RS_AIRPLANE; break;
        case 3: rsi = RS_WATERPLANE; break;
        case 4: rsi = RS_FIREPLANE; break;
        case 5: rsi = RS_HIGHASTRAL; break;
        }
    }

  maze_corridor(1 + random_range(Level->level_width - 1),
                1 + random_range(Level->level_length - 1),
                1 + random_range(Level->level_width - 1),
                1 + random_range(Level->level_length - 1),
                rsi,
                0);

  if (E_ASTRAL == Current_Dungeon)
    {
      for (x_idx = 0; x_idx < Level->level_width; ++x_idx)
        {
          for (y_idx = 0; y_idx < Level->level_length; ++y_idx)
            {
              if (WALL == Level->site[x_idx][y_idx].locchar)
                {
                  switch (Level->depth)
                    {
                    case 1:
                      Level->site[x_idx][y_idx].aux = 500;
                      break;

                    case 2:
                      Level->site[x_idx][y_idx].locchar = WHIRLWIND;
                      Level->site[x_idx][y_idx].p_locf = L_WHIRLWIND;
                      break;

                    case 3:
                      Level->site[x_idx][y_idx].locchar = WATER;
                      Level->site[x_idx][y_idx].p_locf = L_WATER;
                      break;

                    case 4:
                      Level->site[x_idx][y_idx].locchar = FIRE;
                      Level->site[x_idx][y_idx].p_locf = L_FIRE;
                      break;

                    case 5:
                      Level->site[x_idx][y_idx].locchar = ABYSS;
                      Level->site[x_idx][y_idx].p_locf = L_ABYSS;
                      break;
                    }
                }
            }
        }

      switch (Level->depth)
        {
        case 1: mid = LORD_EARTH; break; /* Elemental Lord of Earth */
        case 2: mid = LORD_AIR; break; /* Elemental Lord of Air */
        case 3: mid = LORD_WATER; break; /* Elemental Lord of Water */
        case 4: mid = LORD_FIRE; break; /* Elemental Lord of Fire */
        case 5: mid = ELEM_MASTER; break; /* Elemental Master */
        default: mid = ELEM_MASTER; assert(FALSE); /* bomb if this happens */
        }

      if (5 == Level->depth)
        {
          find_floorspace(&tx, &ty);
          Level->site[tx][ty].p_locf = L_ENTER_CIRCLE;
          Level->site[tx][ty].locchar = STAIRS_DOWN;
        }

      if (!gamestatusp(COMPLETED_ASTRAL))
        {
          find_floorspace(&tx, &ty);
          make_site_monster(tx, ty, mid);
        }
    }
  else if (E_VOLCANO == Current_Environment)
    {
      if (MaxDungeonLevels == Level->depth && !gamestatusp(COMPLETED_VOLCANO))
        {
          find_floorspace(&tx, &ty);
          make_site_monster(tx, ty, DEMON_EMP);
        }
    }
}


/* keep drawing corridors recursively for 2^5 endpoints */
void maze_corridor(int fx, int fy, int tx, int ty, char rsi, char num)
{
  if (num < 6) {
    straggle_corridor(fx,fy,tx,ty,FLOOR,rsi);
    maze_corridor(tx,ty,
		  random_range((Level->level_width)-1)+1,
		  random_range((Level->level_length)-1)+1,
		  rsi,num+1);

  }
}

/* end */
