/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/* *** Game effects and messages caused by changing levels. *** */

#include "glob.h"
#include "game_time.h"
#include "memory.h" /* For new_object */
#include "load.h"
#include "findloc.h"

/*  Flags for Arena status.  Also see Arena_Victory.  */
int vs_Arena_champion_p = FALSE;
int base_Arena_reward    = 0;


/* Enter the current environment, gamewise. */
void player_enter_current_environment(void)
{
  env_params_t * p_tos = current_env_params();

  switch (p_tos->env_idx)
    {
    case E_NEVER_NEVER_LAND:
    default:
      print1("There must be some mistake. You don't look like Peter Pan.");
      print2("(But here you are in Never-Never Land)");
      break;

    case E_ARENA:
      Arena_Victory = FALSE;
      print1("You have a challenger: ");
      print2(Arena_Monster->monstring);
      /* hehehehe cackled the dungeon master.... */
      print3("Your opponent holds the only way you can leave!");
      break;

    case E_ABYSS:
      abyss_file();
      lose_all_items();
      break;

    case E_CIRCLE:
      if (Objects[OB_STARGEM].uniqueness == UNIQUE_TAKEN)
        {
          print1("A bemused voice says:");
          print2("'Why are you here? You already have the Star Gem!'");
          morewait();
        }
      else if (Player.rank[CIRCLE] > 0)
        {
          print1("You hear the voice of the Prime Sorceror:");
          print2("'Congratulations on your attainment of the Circle's Demesne.'");
          morewait();
          print1("For the honor of the Circle, you may take the Star Gem");
          print2("and destroy it on the acme of Star Peak.");
          morewait();
          print1("Beware the foul LawBringer who resides there...");
          print2("By the way, some of the members of the Circle seem to");
          morewait();
          print1("have become a bit jealous of your success --");
          print2("I'd watch out for them too if I were you.");
          morewait();
        }
      else if (Player.alignment > 0)
        {
          print1("A mysterious ghostly image materializes in front of you.");
          print2("It speaks: 'Greetings, fellow abider in Law. I am called");
          morewait();
          print1("The LawBringer. If you wish to advance our cause, obtain");
          print2("the mystic Star Gem and return it to me on Star Peak.");
          morewait();
          print1("Beware the power of the evil Circle of Sorcerors and the");
          print2("forces of Chaos which guard the gem.'");
          morewait();
          print1("The strange form fades slowly.");
          morewait();
        }
      break;

    case E_COURT:
      break;

    case E_MANSION:
      break;

    case E_HOUSE:
      break;

    case E_HOVEL:
      break;

    case E_DLAIR:
      if (gamestatusp(KILLED_DRAGONLORD))
	{
	  mprint("The Lair is now devoid of inhabitants and treasure.");
	  morewait();
	}
      break;

    case E_STARPEAK:
      if (gamestatusp(KILLED_LAWBRINGER))
	{
	  mprint("The peak is now devoid of inhabitants and treasure.");
	  morewait();
	}
      break;

    case E_MAGIC_ISLE:
      if (gamestatusp(KILLED_EATER))
	{
	  mprint("The isle is now devoid of inhabitants and treasure.");
	  morewait();
	}
      break;

    case E_TEMPLE:
      break;

  case E_CITY:
    print1("You pass through the massive gates of Rampart, the city.");
    break;

  case E_VILLAGE:
    print1("You enter a small rural village.");
    break;

    case E_CAVES:
      print1("You enter a dark cleft in a hillside;");
      print2("You note signs of recent passage in the dirt nearby.");
      if (pflagp(MOUNTED))
        {
          morewait();
          print1("Seeing as you might not be coming back, you feel compelled");
          print2("to let your horse go, rather than keep him hobbled outside.");
          resetpflag(MOUNTED);
	  set_notice_flags( notice_x_mounted );
        }
      break;

    case E_VOLCANO:
      print1("You pass down through the glowing crater.");
      if (pflagp(MOUNTED))
        {
          morewait();
          print1("Seeing as you might not be coming back, you feel compelled");
          print2("to let your horse go, rather than keep him hobbled outside.");
          resetpflag(MOUNTED);
	  set_notice_flags( notice_x_mounted );
        }
      break;

    case E_ASTRAL:
      print1("You are in a weird flickery maze.");
      if (pflagp(MOUNTED))
        {
          print2("Your horse doesn't seem to have made it....");
          resetpflag(MOUNTED);
	  set_notice_flags( notice_x_mounted );
        }
      break;

    case E_CASTLE:
      print1("You cross the drawbridge. Strange forms move beneath the water.");
      if (pflagp(MOUNTED))
        {
          morewait();
          print1("Seeing as you might not be coming back, you feel compelled");
          print2("to let your horse go, rather than keep him hobbled outside.");
          resetpflag(MOUNTED);
	  set_notice_flags( notice_x_mounted );
        }
      break;

    case E_SEWERS:

      break;

    case E_COUNTRYSIDE:
      break;

    case E_TACTICAL_MAP:
      if (nighttime()) {
	print3("Night's gloom shrouds your sight.");
      }
      print1("You are now on the tactical screen; exit off any side to leave");
      break;

    case E_PALACE:
      print1("You enter the dungeons of the ruined palace.");
      if (pflagp(MOUNTED))
        {
          morewait();
          print1("Seeing as you might not be coming back, you feel compelled");
          print2("to let your horse go, rather than keep him hobbled outside.");
          resetpflag(MOUNTED);
	  set_notice_flags( notice_x_mounted );
        }
      break;

    }
}




/* Emerge TO the current environment, gamewise. */
void player_emerge_to_current_environment(void)
{
  env_params_t * p_tos = current_env_params();

  switch (p_tos->env_idx)
    {
    default:
    case E_CITY:
      print1("You emerge onto the street.");
      break;

    case E_VILLAGE:
      print1("You emerge onto the road.");
    break;

    case E_COUNTRYSIDE:
      print1("You return to the fresh air of the open countryside.");
      break;
    }
}


void leave_arena(void)
{
  if (! Arena_Victory) {
    print1("The crowd boos your craven behavior!!!");
    if (Player.rank[ARENA] > 0) {
      print2("You are thrown out of the Gladiator's Guild!");
      morewait();
      clearmsg();
      if (Gymcredit > 0) print1("Your credit at the gym is cut off!");
      Gymcredit = 0;
      Player.rank[ARENA] = -1;
    }
    else
      { morewait(); }
  }
  else {
    Arena_Opponent++;
    if (vs_Arena_champion_p) {
      print1("The crowd roars its approval!");
      if (Player.rank[ARENA]) {
	print2("You are the new Arena Champion!");
	Championlevel = Player.level;
	strcpy(Champion,Player.name);
	Player.rank[ARENA] = 5;
	morewait();
	Championbehavior = fixnpc(4);
	save_hiscore_npc(11);
	print1("You are awarded the Champion's Spear: Victrix!");
	morewait();
	{
	  pob newitem = new_object();
	  *newitem = Objects[OB_VICTRIX];
	  gain_item(newitem);
	}
      }
      else {
	print1("As you are not an official gladiator,");
	nprint1("you are not made Champion.");
	morewait();
      }
    }
    clearmsg();
    print1("Good fight! ");
    {
      int prize;
      nprint1("Your prize is: ");
      prize = base_Arena_reward;
      if (Player.rank[ARENA] > 0) prize *= 2;
      mnumprint(prize);
      nprint1("Au.");
      Player.cash+=prize;
    }
    morewait();
    if ((Player.rank[ARENA]<4) &&
	(Arena_Opponent>5) &&
	(Arena_Opponent % 3 == 0)) {
      if (Player.rank[ARENA]>0) {
	Player.rank[ARENA]++;
	morewait();
	print1("You've been promoted to a stronger class!");
	print2("You are also entitled to additional training.");
	Gymcredit+=Arena_Opponent*1000;
      }
    }
  }
}


/* Emerge FROM the current environment, gamewise. */
void player_leave_current_environment(void)
{
  env_params_t * p_tos = current_env_params();

  switch(p_tos->env_idx)
    {
    default:
      /* Do nothing. */
      break;

    case E_ARENA:
      leave_arena();
      break;
    }
}

/* Game-relevant things that are done when leaving any environment,
    regardless where we're going or what else is happening
    game-wise. */
void nullify_current_level_effects(void)
{
  /* reset sanctuary if there was one */
  Player.sx = -1;
  Player.sy = -1;

  monster_tickets_off(FALSE);

  /* In case the player gets lost _on_ a site. */
  if (pflagp(LOST))
    {
      resetpflag(LOST);	
      mprint("You know where you are now.");
    }
}

/** Functions that use loading to alter the level but do not really
    make a new level.  **/

void repair_jail(void)
{
  reload_map(&jail_map_loader, sl_jail_ulc, 0);
}

void resurrect_guards(void)
{
  reload_map(&city_map_loader_resurrect_guards, sl_level_ulc, 0);
  setgamestatus(UNDEAD_GUARDS);
}

void destroy_order(void)
{
  setgamestatus(DESTROYED_ORDER);
  if (Current_Environment != E_CITY)
    {
      error("Zounds! A Serious Mistake!");
      return;
    }

  reload_map(&city_map_loader_destroy_order, 7, 0);
}



