/* omega copyright (c) 1987-2001 by Laurence Raphael Brothers */
/* ticket.c */

#include <assert.h>

#include "game_time.h"
#include "liboutil/allocator.h"
#include "liboutil/list.h"
#include "liboutil/pqueue.h"
#include "ticket.h"

#define MAXIMUM_NUMBER_ACTIVE_TICKETS 4096

void * malloc_nofail (size_t size);

/**********************************************/

struct ticket
{
  list_t * args;
  list_iterator_t * it;
  ticket_t * next;
  unsigned long period;
  unsigned long scheduled;  /* Time the ticket is scheduled for; equal to
                               its priority in the priority queue */
  ticket_handler_t handler;
};

static unsigned long current_time = 0;
static priority_queue_t * ticket_queue = 0;

/**********************************************/

make_static_allocator(ticket_t, (256 * sizeof(ticket_t)), next, malloc_nofail);

#define alloc_ticket() ticket_t_utility(0);
#define free_ticket(t) ticket_t_utility(t);

/**********************************************/

ticket_t * ticket_new_periodic (ticket_handler_t handler, unsigned long period)
{
  ticket_t * nt;
  nt = alloc_ticket();

  nt->it = 0;
  nt->next = 0;
  nt->args = 0;
  nt->period = period;
  nt->handler = handler;

  return nt;
}

ticket_t * ticket_new_oneshot (ticket_handler_t handler)
{
  return ticket_new_periodic(handler, 0);
}

void delete_ticket (ticket_t * ticket)
{
  /* Caution! arguments are not freed! If arguments need to be freed, */
  /* the ticket handler function should free them explicitly */

  if (ticket->args)
    {
      while (list_size(ticket->args))
        list_remove_head(ticket->args);

      list_delete(ticket->args);
    }

  if (ticket->it)
    list_iterator_delete(ticket->it);

  ticket->it = 0;
  ticket->args = 0;
  ticket->period = 0;
  ticket->handler = 0;

  free_ticket(ticket);
}

/**********************************************/

void ticket_add_argument (ticket_t * ticket, void * arg)
{
  if (0 == ticket->args)
    ticket->args = list_new();

  list_append(ticket->args, arg);
}

int ticket_has_next_argument (ticket_t * ticket)
{
  int result;

  if (0 == ticket->it)
    ticket->it = list_iterator_create(ticket->args);

  result = list_iterator_has_next(ticket->it);

  if (!result)
    {
      list_iterator_delete(ticket->it);
      ticket->it = 0;
    }

  return result;
}

void * ticket_next_argument (ticket_t * ticket)
{
  return list_iterator_next(ticket->it);
}

void ticket_reset_arguments (ticket_t * ticket)
{
  if (ticket->it != 0)
    {
      list_iterator_delete(ticket->it);
      ticket->it = 0;
    }
}

list_t * ticket_get_argument_list (ticket_t * ticket)
{
  return ticket->args;
}

/**********************************************/

unsigned long get_current_ticket_time (void)
{
  return current_time;
}

unsigned long get_ticket_duration (ticket_t * ticket)
{
  return ticket->scheduled - current_time;
}

/**********************************************/

ticket_t * ticket_find (ticket_t * ticket)
{
  ticket_t * removed;

  assert(ticket);
  removed = pq_remove_member(ticket_queue, ticket);
  return removed;
}

void ticket_remove (ticket_t * ticket)
{
  ticket_t * removed;

  assert(ticket);
  removed = pq_remove_member(ticket_queue, ticket);

  assert(removed);
  delete_ticket(ticket);
}

void ticket_schedule (ticket_t * ticket, unsigned long duration)
{
  if (0 == ticket_queue)
    ticket_queue = pq_new(MAXIMUM_NUMBER_ACTIVE_TICKETS);

  ticket->scheduled = current_time + duration;
  pq_insert(ticket_queue, ticket->scheduled, ticket);
}

void ticket_dispatch (void)
{
  ticket_t * ticket;
  ticket_handler_t handle_ticket;

  current_time = pq_get_current_priority(ticket_queue);
  ticket = pq_remove(ticket_queue);
  assert(ticket);

  handle_ticket = ticket->handler;
  handle_ticket(ticket);

  if (ticket->period)
    ticket_schedule(ticket, ticket->period);
  else
    delete_ticket(ticket);
}

void ticket_process_for_seconds (unsigned long seconds)
{
  int goal_time;
  int remaining_time;

  seconds = seconds * TICKS_PER_SECOND;
  goal_time = current_time + seconds;

  do
    {
      ticket_dispatch();
      remaining_time = goal_time - current_time;
    }
  while (remaining_time <= seconds);
}

void ticket_process_for_minutes (unsigned long minutes)
{
  int goal_time;
  int remaining_time;

  minutes = minutes * 60 * TICKS_PER_SECOND;
  goal_time = current_time + minutes;

  do
    {
      ticket_dispatch();
      remaining_time = goal_time - current_time;
    }
  while (remaining_time <= minutes);
}

void ticket_process_for_hours (unsigned long hours)
{
  int goal_time;
  int remaining_time;

  hours = hours * 60 * 60 * TICKS_PER_SECOND;
  goal_time = current_time + hours;

  do
    {
      ticket_dispatch();
      remaining_time = goal_time - current_time;
    }
  while (remaining_time <= hours);
}

static int keep_processing;

void ticket_process_until_quit (void)
{
  keep_processing = 1;

  while (keep_processing)
    ticket_dispatch();
}

void ticket_quit (void)
{
  keep_processing = 0;
}
