/* omega copyright (c) 1987,1988,1989 by Laurence Raphael Brothers */
/* mspec.c */
/* monster special functions */

#include "game_time.h"
#include "glob.h"
#include "lev.h"
#include "liboutil/list.h"
#include "util.h"

void m_sp_mp (monster_t * m, player_t * p)
{
  if (m->dlevel != p->dlevel) return;
  if (m->attacked && (random_range(3) == 1))
    {
      mprint("The mendicant priest curses you...");

      if ((p->patron == SET || p->patron == HECATE) && p->alignment < -20)
	{
	  mprint("but the curse backfires!");
	  m_damage(m, NULL, 10, UNSTOPPABLE);
	}
      else
	{
	  p_damage(p, 10, UNSTOPPABLE, "a mendicant priest's curse");

	  if (p->alignment < -20)
	    mprint("you feel humiliated!");
	  else
	    mprint("you feel ashamed!");
	}

      p->alignment -= 5;
      m_vanish(m); /* why should he be able to disappear? */
      return;
    }

  if (m_statusp(m, NEEDY)) return;

  mprint("The mendicant priest makes a mystical gesture....");

  if (p->alignment < -20)
    mprint("You feel humiliated!");
  else
    mprint("You feel impressed!");

  if (p->alignment > 20)
    p->hp = max(p->hp, p->maxhp);

  p->alignment += 5;
  m_vanish(m);
}

void m_sp_ng (monster_t * m, player_t * p)
{
  if (m->dlevel != p->dlevel) return;
  if (distance(m->x, m->y, p->x, p->y) >= 2) return;

  if ((random_range(5) == 1) || (p->status[VULNERABLE] > 0))
    {
      mprint("The night gaunt grabs you and carries you off!");
      mprint("Its leathery wings flap and flap, and it giggles insanely.");
      mprint("It tickles you cunningly to render you incapable of escape.");
      mprint("Finally, it deposits you in a strange place.");
      p_teleport(0);
    }
}

void m_sp_poison_cloud (monster_t * m, player_t * p)
{
  if (m->dlevel != p->dlevel) return;
  if (distance(m->x, m->y, p->x, p->y) >= 3) return;

  mprint("A cloud of poison gas surrounds you!");

  if (p->status[BREATHING] > 0)
    mprint("You can breathe freely, however.");
  else
    p_poison(7);
}

void m_sp_explode (monster_t * m, player_t * p)
{
  if (m_statusp(m, M_GONE)) return;
  if (m->dlevel != p->dlevel) return;
  if (m->hp >= Monsters[m->id].hp) return;
  if (distance(m->x, m->y, p->x, p->y) >= 2) return;

  fball(m->x, m->y, m->x, m->y, m->hp);
}

void m_sp_demon (monster_t * m, player_t * p)
{
  if (m->dlevel != p->dlevel) return;

  if (random_range(2) == 1)
    {
      int afraid = FALSE;

      if (m->id != INCUBUS)
	if (p->status[AFRAID] == 0)
	  if (is_monster_visible(p,m) == TRUE)
	    if (distance_to_monster(p,m) < (25 - p->level))
	      if ((m->level + random_range(25)) > (p->level + 5))
		{
		  afraid = TRUE;
		  mprint("You are stricken with fear!");
		  if (p_immune(p,FEAR) == TRUE)
		    mprint("You master your reptile brain and stand fast.");
		  else
		    p->status[AFRAID] += m->level;
		}

      if (afraid == FALSE)
	m_sp_spell(m);
    }

  if (m->hp < (5 * m->level))
    if (m->hp > 1)
      if (m->level >= 3 && m->level <= 10)
	{
	  int mid;

	  mprint("The demon uses its waning lifeforce to summon help!");
	  m->hp = 1;

	  switch (m->level)
	    {
	    case 4:
	    case 5:  mid = L_FDEMON;     break; /* lesser frost demon */
	    case 6:  mid = FROST_DEMON;  break;
	    case 7:  mid = OUTER_DEMON;  break; /* outer circle demon */
	    case 8:  mid = DEMON_SERP;   break; /* demon serpent */
	    case 9:  mid = INNER_DEMON;  break; /* inner circle demon */
	    case 10: mid = DEMON_PRINCE; break;
	    default: mid = NIGHT_GAUNT;  break;
	    }

	  summon(-1, mid);
	  summon(-1, mid);
	  summon(-1, mid);
	}
}

void m_sp_acid_cloud (monster_t * m, player_t * p)
{
  if (m->dlevel != p->dlevel) return;
  if (m_statusp(m, HOSTILE) == FALSE) return;
  if (distance(m->x, m->y, p->x, p->y) >= 3) return;

  acid_cloud();
}

void m_sp_escape(monster_t * m, player_t * p)
{
  if (m->dlevel != p->dlevel) return;
  if (m_statusp(m, HOSTILE) == FALSE) return;

  m_vanish(m);
}

void m_sp_ghost (monster_t * m, player_t * p)
{
  if (m->dlevel != p->dlevel) return;
  if (m_statusp(m,HOSTILE) == FALSE) return;

  mprint("The ghost moans horribly....");
  p_damage(p, 1, FEAR, "a ghost-inspired heart attack");

  mprint("You've been terrorized!");

  if (p_immune(FEAR) == FALSE)
    p->status[AFRAID] += m->level;
  else
    mprint("You master your reptile brain and stand fast.");
}

/* random spell cast by monster */
void m_sp_spell (monster_t * m, player_t * p)
{
  char action[80];

  if (m->dlevel != p->dlevel) return;
  if (m_statusp(m,HOSTILE) == FALSE) return;
  if (is_player_visible(m,p) == FALSE) return;

  if (m->uniqueness == COMMON)
    sprintf(action, "The %s casts a spell...", m->monstring);
  else
    sprintf(action, "%s casts a spell...", m->monstring);
  mprint(action);

  if (magic_resist(m->level)) return;

  switch (random_range(m->level + 7))
    {
    case 0: 
      nbolt(m->x, m->y, p->x, p->y, m->hit, 10);
      break;

    case 1:
      mprint("It seems stronger..."); 
      m->hp += random_range(m->level * m->level);
      break;

    case 2:
      haste(p,-1);
      break;

    case 3:
      cure(p,-1);
      break;

    case 4:
      /* WDT: I'd like to make this (and "case 5" below) dependant on
       * the monster's IQ in some way -- dumb but powerful monsters
       * deserve what they get :).  No rush. */
      if (m_immunityp(m, ELECTRICITY) || distance(m->x, m->y, p->x, p->y) > 2)
	lball(m->x, m->y, p->x, p->y, 20);
      else
	lbolt(m->x, m->y, p->x, p->y, m->hit, 20);
      break;

    case 5:
      if (m_immunityp(m, COLD) || distance(m->x, m->y, p->x, p->y) > 2)
	snowball(m->x, m->y, p->x, p->y, 30);
      else
	icebolt(m->x, m->y, p->x, p->y, m->hit, 30);
      break;

    case 6:
      enchant(p,-1);
      break;

    case 7:
      bless(p, 0 - m->level);
      break;

    case 8:
      p_poison(p, m->level);
      break;

    case 9:
      sleep_player(p, m->level / 2);
      break;

    case 10:
      fbolt(m->x, m->y, p->x, p->y, 3 * m->hit, 50);
      break;

    case 11:
      acquire(p, 0 - m->level);
      break;

    case 12:
      dispel(p,-1);
      break;

    case 13:
      disrupt(p->x, p->y, 50);
      break;

    case 14:
      if (m->uniqueness == COMMON)
	sprintf(Str2, "a %s", m->monstring);
      else
	strcpy(Str2, m->monstring);
      level_drain(p, m->level, Str2);
      break;

    case 15:
    case 16:
      disintegrate(p->x, p->y);
      break;
    }
}

/* monsters with this have some way to hide, camouflage, etc until they attack */
void m_sp_surprise (monster_t * m, player_t * p)
{
  if (p->status[TRUESIGHT] > 0) return;
  if (m_statusp(m, HOSTILE) == FALSE) return;
  if (m_statusp(m, M_INVISIBLE) == FALSE) return;

  if (p->status[ALERT] > 0)
    {
      mprint("You alertly sense the presence of an attacker!");
      m_status_reset(m, M_INVISIBLE);
      return;
    }	

  m->monchar = Monsters[m->id].monchar;

  switch (random_range(4))
    {
    case 0: 
      mprint("You are surprised by a sudden treacherous attack!");
      break;

    case 1: 
      mprint("You are shocked out of your reverie by the scream of battle!");
      break;

    case 2: 
      mprint("Suddenly, from out of the shadows, a surprise attack!");
      break;

    case 3: 
      mprint("A shriek of hatred causes you to momentarily freeze up!");
      break;
    }

  morewait();
  setpflag(p, SKIP_PLAYER);
  m_status_reset(m, M_INVISIBLE);
}

void m_sp_whistleblower (monster_t * m, player_t * p)
{
  if (m_statusp(m, HOSTILE))
    {
      alert_guards();
      m->specialf = M_MELEE_NORMAL;
    }
}

void m_sp_seductor (monster_t * m, player_t * p)
{
  if (m_statusp(m, HOSTILE) == FALSE)
    {
      if (distance(m->x, m->y, p->x, p->y) < 2)
	m_talk_seductor(m,p);

      return;
    }

  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s runs away screaming for help...", m->monstring);
  else
    sprintf(Str2, "The %s runs away screaming for help...", m->monstring);

  mprint(Str2);
  m_remove(m);

  summon(-1,-1);
  summon(-1,-1);
  summon(-1,-1);
}

void m_sp_demonlover (monster_t * m, player_t * p)
{
  if (distance(m->x, m->y, p->x, p->y) < 2)
    m_talk_demonlover(m,p);
}

void m_sp_eater (monster_t * m, player_t * p)
{
  if (p->rank[COLLEGE])
    m_status_set(m, HOSTILE);

  if (m_statusp(m, HOSTILE))
    {
      if (m->hp < 10)
	{
	  mprint("The Eater explodes in a burst of mana!");
	  manastorm(m->x, m->y, 1000);
	}
      else if (is_player_visible(m,p))
	{
	  mprint("A strange numbing sensation comes over you...");
	  morewait();

	  p->mana = p->mana / 2;

	  if (random_range(4) == 1)
	    dispel(-1);
	  else
	    enchant(-1);

	  p->pow -= 2;

	  if (p->pow < 0)
	    p_death(p, "the Eater of Magic");
	}
    }
}

void m_sp_dragonlord (monster_t * m, player_t * p)
{
  if (m_statusp(m, HOSTILE) == FALSE)
    {
      if (distance(p->x, p->y, m->x, m->y) < 2)
	mprint("You are extremely impressed at the sight of the Dragonlord.");
      return;
    }

  if (distance(p->x, p->y, m->x, m->y) >= 2)
    {
      Constriction = 0;

      if (is_player_visible(m,p))
	{
	  int idx;

	  if (p->immunity[FEAR] <= 0 && p->status[AFRAID] <= 0)
	    {
	      mprint("You are awestruck at the sight of the Dragonlord.");
	      p->status[AFRAID]+=5;
	    }

	  for (idx = 0; idx < random_range(3); ++idx)
	    m_sp_spell(m,p);
	}

      return;
    }

  if (p->status[IMMOBILE] == 0)
    {
      mprint("A gust of wind from the Dragonlord's wings knocks you down!");
      p_damage(p, 25, NORMAL_DAMAGE, "a gust of wind");
      setpflag(p, SKIP_PLAYER);
      p->status[IMMOBILE] += 2;
    }
  else if (Constriction == 0)
    {
      mprint("The Dragonlord grabs you with his tail!");
      Constriction = 25;
      p->status[IMMOBILE] += 1;
    }
  else if (random_range(2))
    {
      mprint("The coils squeeze tighter and tighter...");
      p_damage(p, Constriction, NORMAL_DAMAGE, "the Dragonlord");
      p->status[IMMOBILE] += 1;
      Constriction *= 2;
    }
  else
    {
      mprint("The Dragonlord hurls you to the ground!");
      p_damage(p, 2 * Constriction, NORMAL_DAMAGE, "the Dragonlord");
      Constriction = 0;
    }

  m_sp_spell(m,p);
}

void m_sp_blackout (monster_t * m, player_t * p)
{
  if (distance(m->x, m->y, p->x, p->y) < 4 && p->status[BLINDED] == 0)
    {
      switch(m->id)
        {
        case MURK:
	  if (is_monster_visible(p,m))
	    mprint("The fungus emits a burst of black spores. You've been blinded!");
	  else
	    mprint("The air is filled with a burst of black spores. You've been blinded!");
          break;

        case SHADOW:
          mprint("A shadowy fog obscures your vision. You've been blinded!");
          break;

        case SHADOW_SLAY:
          mprint("A terrifying darkness comes over you. You've been blinded!");
          break;

        default:
          mprint("A bug in the program eats your eyeballs. You've been blinded!");
          assert(FALSE);
          break;
        }

      if (p->status[TRUESIGHT] > 0)
        mprint("The blindness quickly passes.");
      else
        p->status[BLINDED] += 4;
    }

  if (loc_statusp(m->x, m->y, LIT))
    {
      mprint("The area is plunged into darkness.");
      torch_check(TRUE);
      torch_check(TRUE);
      torch_check(TRUE);
      torch_check(TRUE);
      torch_check(TRUE);
      torch_check(TRUE);
      spreadroomdark(m->dlevel, m->x, m->y, m->dlevel->site[m->x][m->y].roomnumber);
      levelrefresh();
    }
}

void m_sp_bogthing (monster_t * m, player_t * p)
{
  if (distance(p->x, p->y, m->x, m->y) >= 2) return;

  if (p->status[AFRAID] == 0)
    {
      mprint("As the bogthing touches you, you feel a frisson of terror....");

      if (p->immunity[FEAR] > 0)
	mprint("which you shake off.");
      else
	p->status[AFRAID] = 2;
    }
  else
    {
      mprint("The bogthing's touch causes you scream in agony!");
      p_damage(p, 50, UNSTOPPABLE, "fright");
      mprint("Your struggles grow steadily weaker....");

      --(p->con);
      --(p->str);

      if (p->con < 3 || p->str < 3)
	p_death(p, "congestive heart failure");
    }
}

void m_sp_were (monster_t * m, player_t * p)
{
  int mid;

  if (m_statusp(m, HOSTILE) == FALSE && time_phase() != 6) return;

  /* limit to animals (no were-robot or were-ghost please) */

  switch (random_range(28))
    {
    case 0:  mid = HORNET;      break;
    case 1:  mid = SHEEP;       break;
    case 2:  mid = TSETSE;      break;
    case 3:  mid = SEWER_RAT;   break;
    case 4:  mid = QUAIL;       break;
    case 5:  mid = BADGER;      break;
    case 6:  mid = HAWK;        break;
    case 7:  mid = DEER;        break;
    case 8:  mid = CAMEL;       break;
    case 9:  mid = ANTEATER;    break;
    case 10: mid = BUNNY;       break;
    case 11: mid = PARROT;      break;
    case 12: mid = HYENA;       break;
    case 13: mid = WOLF;        break;
    case 14: mid = ANT;         break;
    case 15: mid = ELEPHANT;    break;
    case 16: mid = HORSE;       break;
    case 17: mid = SALAMANDER;  break;
    case 18: mid = PTERODACTYL; break;
    case 19: mid = LION;        break;
    case 20: mid = BEAR;        break;
    case 21: mid = MAMBA;       break;
    case 22: mid = CROC;        break;
    case 23: mid = DRAGONETTE;  break;
    case 24: mid = TRICER;      break;
    case 25: mid = UNICORN;     break;
    case 26: mid = ROUS;        break;
    case 27: mid = DRAGON;      break;
    default: return;
    }

  m->id           = Monsters[mid].id;
  m->hp          += Monsters[mid].hp;
  m->status      |= Monsters[mid].status;
  m->ac           = Monsters[mid].ac;
  m->dmg          = Monsters[mid].dmg;
  m->speed        = Monsters[mid].speed;
  m->immunity    |= Monsters[mid].immunity;
  m->xpv         += Monsters[mid].xpv;
  m->corpseweight = Monsters[mid].corpseweight;
  m->monchar      = Monsters[mid].monchar;
  m->talkf        = Monsters[mid].talkf;
  m->meleef       = Monsters[mid].meleef;
  m->strikef      = Monsters[mid].strikef;
  m->specialf     = Monsters[mid].specialf;
  m->immunity    |= pow2(NORMAL_DAMAGE);

  sprintf(Str1, "were-%s", Monsters[mid].monstring);
  m->monstring = salloc(Str1);

  sprintf(Str1, "dead were-%s", Monsters[mid].monstring);
  m->corpsestr = salloc(Str1);

  m_status_set(m, ALLOC);

  if (is_monster_visible(p,m))
    mprint("You witness a hideous transformation!");
  else
    mprint("You hear an anguished cry.");
}    

void m_sp_servant (monster_t * m, player_t * p)
{
  if (m->id == SERV_LAW && p->alignment < -10)
    m_status_set(m, HOSTILE);
  else if (m->id == SERV_CHAOS && p->alignment > 10)
    m_status_set(m, HOSTILE);
}

void m_sp_av (monster_t * m, player_t * p)
{
  int lossage;

  if (p->mana <= 0) return;

  lossage = 10 - distance(m->x, m->y, p->x, p->y);
  if (lossage > 0)
    {
      mprint("You feel a sudden loss of mana!");
      p->mana -= lossage;
      dataprint();
    }
}

void m_sp_lw (monster_t * m, player_t * p)
{
  if (random_range(2) == 1) return;

  if (m->dlevel->site[m->x][m->y].locchar == FLOOR)
    {
      m->dlevel->site[m->x][m->y].locchar = LAVA;
      m->dlevel->site[m->x][m->y].p_locf  = L_LAVA;
      lset(m->dlevel, m->x, m->y, CHANGED);
    }
  else if (m->dlevel->site[m->x][m->y].locchar == WATER)
    {
      m->dlevel->site[m->x][m->y].locchar = FLOOR;
      m->dlevel->site[m->x][m->y].p_locf  = L_NO_OP;
      lset(m->dlevel, m->x, m->y, CHANGED);
    }
}

void m_sp_angel (monster_t * m, player_t * p)
{
  int mid;

  switch (m->aux1)
    {
    case ATHENA: case ODIN: 
      if (p->patron == HECATE || p->patron == SET)
	m_status_set(m, HOSTILE);
      break;

    case SET: case HECATE: 
      if (p->patron == ODIN || p->patron == ATHENA)
	m_status_set(m, HOSTILE);
      break;

    case DESTINY:
      if (p->patron != DESTINY)
	m_status_set(m, HOSTILE);
      break;
    }

  if (m_statusp(m, HOSTILE) == FALSE) return;

  mprint("The angel summons a heavenly host!");

  switch (m->level)
    {
    default:
    case 6: mid = PHANTOM;    break;
    case 8: mid = ANGEL;      break;
    case 9: mid = HIGH_ANGEL; break;
    }

  summon(-1, mid);
  summon(-1, mid);
  summon(-1, mid);

  /* prevent angel from summoning infinitely */
  m->specialf = M_NO_OP;
}

/* Could completely fill up level */
void m_sp_swarm (monster_t * m, player_t * p)
{
  if (random_range(5) != 1) return;

  if (is_monster_visible(p,m))
    mprint("The swarm expands!");
  else
    mprint("You hear an aggravating humming noise.");

  summon(-1, SWARM);
}

/* raise nearby corpses from the dead.... */
void m_sp_raise (monster_t * m, player_t * p)
{
  int x, y;
  int message = FALSE;
  char msg[80];

  for (x = m->x - 2; x <= m->x + 2; ++x)
    {
      for (y = m->y - 2; y <= m->y + 2; ++y)
        {
          list_t * olist;
          list_iterator_t * it;

          if (x == p->x && y == p->y) continue;
          if (!inbounds(m->dlevel, x, y)) continue;
          if (get_monster(m->dlevel, x, y)) continue;

          olist = m->dlevel->site[x][y].object_list;
          if (list_size(olist) == 0) continue;

          it = list_iterator_create(olist);
          while (list_iterator_has_next(it))
            {
              object * obj;
              obj = list_iterator_next(it);

              if (obj->id == CORPSEID)
                {
                  monster * newmon;

		  message = TRUE;
                  newmon = make_creature(obj->charge);
                  m_status_set(newmon, HOSTILE);
                  add_monster_to_level(newmon->dlevel, newmon, x, y);

                  free_obj(obj, TRUE);
                  list_iterator_remove(it);
                  break; /* can only resurrect one per space */
                }
            }
          list_iterator_delete(it);

          if (list_size(olist) == 0)
            {
              list_delete(olist);
              m->dlevel->site[x][y].object_list = 0;
            }
        }
    }

  if (message)
    if (is_monster_visible(p,m))
      {
	sprintf(msg, "The %s makes a mystical gesture...", m->monstring);
	mprint(msg);
      }
}

void m_sp_mb (monster_t * m, player_t * p)
{
  if (distance(m->x, m->y, p->x, p->y) > 1) return;

  mprint("The manaburst explodes!");

  if (m_statusp(m, HOSTILE))
    {
      mprint("You get blasted!");
      p_damage(p, random_range(100), UNSTOPPABLE, "a manaburst");

      p->pow -= 3;
      p->iq  -= 1;
      p->con -= 1;
      p->str -= 2;
      p->dex -= 1;
      p->agi -= 1;

      dispel(-1);

      /* DAG -- need calc_melee/calcmana here, because of new stats */
      calc_melee(p);
      p->maxmana = calcmana(p);
      p->mana = min(p->mana, p->maxmana);

      mprint("You feel cold all over!");
    }
  else
    {
      ++(p->pow);
      ++(p->maxhp);

      p->mana = max(p->mana, calcmana(p));
      p->hp   = max(p->hp, p->maxhp);

      mprint("You feel toasty warm inside!");
    }

  m_remove(m);
}

void m_sp_mirror (monster_t * m, player_t * p)
{
  int i, x, y;

  if (is_monster_visible(p,m) == FALSE) return;

  if ((random_range(20) + 6) < m->level)
    {
      summon(-1, m->id);
      mprint("You hear the sound of a mirror shattering!");
    }
  else for (i = 0; i < 5; ++i)
    {
      x = m->x + random_range(13) - 6;
      y = m->y + random_range(13) - 6;

      if (inbounds(m->dlevel, x, y))
	{
	  m->dlevel->site[x][y].showchar = m->monchar;
	  putspot(x, y, m->monchar);
	}
    }
}

void m_illusion (monster_t * m, player_t * p)
{
  int i;

  do
    i = random_range(NUMMONSTERS);
  while (i == NPC || i == HISCORE_NPC || i == ZERO_NPC || Monsters[i].uniqueness != COMMON);

  if (p->status[TRUESIGHT])
    {
      m->monchar = Monsters[m->id].monchar;
      m->monstring = Monsters[m->id].monstring;
    }
  else if (random_range(5) == 1)
    {
      m->monchar = Monsters[i].monchar;
      m->monstring = Monsters[i].monstring;
    }
}

void m_huge_sounds (monster_t * m, player_t * p)
{
  if (m_statusp(m, AWAKE) == FALSE) return;
  if (is_monster_visibile(p,m) == TRUE) return;

  if (random_range(10) == 1)
    mprint("The dungeon shakes!");
}

void m_thief_f (monster_t * m, player_t * p)
{
  int i;

  if (random_range(3) != 1) return;
  if (distance(m->x, m->y, p->x, p->y) >= 2) return;

  if (p_immune(p, THEFT) || p->level > (2 * m->level + random_range(20)))
    {
      mprint("You feel secure.");
      return;
    }

  i = stolen_item();

  if (i == ABORT)
    {
      mprint("You feel fortunate.");
      return;
    }

  if (p->possessions[i]->used || p->dex < (m->level * random_range(10)))
    {
      mprint("You feel a sharp tug.... You hold on!");
      return;
    }

  mprint("You feel uneasy for a moment.");

  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s suddenly runs away for some reason.", m->monstring);
  else
    sprintf(Str2, "%s suddenly runs away for some reason.", m->monstring);

  mprint(Str2);

  m_pickup(m, p->possessions[i]);
  conform_unused_object(p->possessions[i]);
  p->possessions[i] = NULL;

  m_teleport(m);
  m->movef = M_MOVE_SCAREDY;
  m->specialf = M_MOVE_SCAREDY;
}

void m_summon (monster_t * m, player_t * p)
{
  if (distance(m->x, m->y, p->x, p->y) >= 2) return;
  if (random_range(3) != 1) return;

  summon(0,-1);
  summon(0,-1);
}	  

void m_aggravate (monster_t * m, player_t * p)
{
  if (m_statusp(m, HOSTILE) == FALSE) return;

  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s emits an irritating humming sound.", m->monstring);
  else
    sprintf(Str2, "%s emits an irritating humming sound.", m->monstring);

  mprint(Str2);

  aggravate(m->dlevel);
  m_status_reset(m, HOSTILE);
}

static void apply_make_village_hostile (void * vmon)
{
  monster * mon;
  mon = vmon;

  if (!m_statusp(mon, M_GONE))
    {
      m_status_set(mon, AWAKE);
      m_status_set(mon, HOSTILE);
      mon->specialf = M_NO_OP;
    }
}

void m_sp_merchant (monster_t * m, player_t * p)
{
  if (m_statusp(m, HOSTILE) == FALSE) return;
  if (m->dlevel->environment != E_VILLAGE) return;

  mprint("The merchant screams: 'Help! Murder! Guards! Help!'");
  mprint("You hear the sound of police whistles and running feet.");

  list_apply(m->dlevel->monster_list, apply_make_village_hostile);
}

/* The special function of the various people in the court of the archmage */
/* and the sorcerors' circle */
static void apply_make_court_hostile (void * vmon)
{
  monster * mon;
  mon = vmon;

  if (!m_statusp(mon, M_GONE))
    {
      m_status_set(mon, AWAKE);
      m_status_set(mon, HOSTILE);
      m_sp_spell(mon);

      if (M_SP_COURT == mon->specialf)
        mon->specialf = M_SP_SPELL;
    }
}

void m_sp_court (monster_t * m, player_t * p)
{
  if (m_statusp(m, HOSTILE) == FALSE) return;

  mprint("A storm of spells hits you!");
  morewait();

  list_apply(m->dlevel->monster_list, apply_make_court_hostile);
}

/* The special function of the dragons in the dragons' lair */
static void apply_make_dragons_hostile (void * vmon)
{
  monster * mon;
  mon = vmon;

  if (!m_statusp(mon, M_GONE) && M_SP_LAIR == mon->specialf)
    {
      m_status_set(mon, AWAKE);
      m_status_set(mon, HOSTILE);

      /* fbolt(mon->x, mon->y, p->x, p->y, 100, 100); */

      if (DRAGON_LORD == mon->id)
        mon->specialf = M_SP_DRAGONLORD;
      else
        mon->specialf = M_STRIKE_FBOLT;
    }
}

void m_sp_lair (monster_t * m, player_t * p)
{
  if (m_statusp(mon, HOSTILE) == FALSE) return;

  mprint("You notice a number of dragons waking up....");
  mprint("You are struck by a quantity of firebolts.");
  morewait();

  list_apply(m->dlevel->monster_list, apply_make_dragons_hostile);
}

void m_sp_prime (monster_t * m, player_t * p)
{
  if (m_statusp(m, HOSTILE) == FALSE) return;

  mprint("The prime sorceror gestures and a pentacular gate opens!");
  mprint("You are surrounded by demons!");

  summon(-1, DEMON_PRINCE);
  summon(-1, DEMON_PRINCE);
  summon(-1, DEMON_PRINCE);
  summon(-1, DEMON_PRINCE);

  m->specialf = M_SP_SPELL;
}
