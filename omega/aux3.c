/* omega copyright (C) by Laurence Raphael Brothers, 1987,1988,1989 */
/* aux3.c */
/* some functions called by com.c, also see aux1.c, aux2.c */ 
/* This is a real grab bag file. It contains functions used by
   aux1.c and omega.c, as well as elsewhere. It is mainly here so aux1.c
   and aux2.c are not huge */
  
#include "game_time.h"
#include "glob.h"
#include "lev.h"
#include "liboutil/list.h"
#include "memory.h"
#include "util.h"
#include "command.h" /* For cmd_rename_player */
#include "findloc.h" /* For get_special_loc  */


static void apply_heal_monster (void * vmon)
{
  monster * mon;
  mon = vmon;
  if (mon->hp > 0)
    mon->hp = Monsters[mon->id].hp;
}

static void twirl_handler (ticket_t * twirl_ticket)
{
  player_t * p;
  p = ticket_next_argument(twirl_ticket);

  while (ticket_has_next_argument(twirl_ticket))
    ;

  /* No more pecan twirl urge */
  resetpflag(p, WANT_PECAN_TWIRL);
}

void indoors_random_event (player_t * p)
{
  switch (random_range(1000))
    {
    default:
      return;

    case 0: 
      print3("You feel an unexplainable elation.");
      morewait();
      break;

    case 1:
      print3("You hear a distant rumbling.");
      morewait();
      break;

    case 2:
      print3("You realize your fly is open.");
      morewait();
      break;

    case 3:
      if (pflagp(p, WANT_PECAN_TWIRL) == FALSE)
	{
	  ticket_t * twirl_ticket;
	  print3("You have a sudden craving for a pecan twirl.");
	  morewait();
	  setpflag(WANT_PECAN_TWIRL);
	  twirl_ticket = ticket_new_oneshot(twirl_handler);
	  ticket_add_argument(twirl_ticket, p);
	  ticket_schedule(twirl_ticket, 5 * TICKS_PER_MINUTE);
	}
      break;

    case 4:
      print3("A mysterious healing flux settles over the level.");
      morewait();
      list_apply(p->devel->monster_list, apply_heal_monster);
      p->hp = max(p->hp, p->maxhp);
      break;

    case 5:
      print3("You discover an itch just where you can't scratch it.");
      morewait();
      break;

    case 6:
      print3("A cosmic ray strikes!");
      p_damage(p, 10, UNSTOPPABLE, "a cosmic ray");
      morewait();
      break;

    case 7:
      print3("You catch your second wind....");
      ++(p->maxhp);
      p->hp = max(p->hp, p->maxhp);
      p->mana = max(p->mana, calcmana());
      morewait();
      break;

    case 8:
      print3("You find some spare change in a hidden pocket.");
      morewait();
      p->cash += (p->level * p->level + 1);
      break;

    case 9:
      print3("You feel strangely lucky.");
      morewait();
      break;

    case 10:
      {
        object_t * obj;

        print3("You trip over something hidden in a shadow...");
        morewait();

        obj = create_object(difficulty()); /* FIXED!  12/30/98 */
        assert(obj); /* WDT I want to make sure... */

        add_object_to_level(p->dlevel, obj, p->x, p->y);
	set_notice_flags(notice_stuff_at_feet);
      }
      break;

    case 11: 
      print3("A mysterious voice echoes all around you....");
      morewait();
      hint();
      morewait();
      break;
    }

  /* For any non-null event: */
  set_notice_flags(notice_dont_repeat | notice_py_data );
  showflags(); /* Should be a notice flag too. */
}

void outdoors_random_event (player_t * p)
{
  int num,i,j;
  object_t * ob;

  switch (random_range(300))
    {
    default:
      return;

    case 0:
      switch(Country[p->x][p->y].current_terrain_type)
	{
	case TUNDRA: 
	  mprint("It begins to snow. Heavily.");
	  break;

	case DESERT:
	  mprint("A sandstorm swirls around you.");
	  break;

	default:
	  {
	    int date;
	    date = 30 * time_month() + time_day();
	    if (date > 75 && date < 330)
	      mprint("You are drenched by a sudden downpour!");
	    else
	      mprint("It begins to snow. Heavily.");
	  }
	}
      morewait();
      mprint("Due to the inclement weather conditions, you have become lost.");
      morewait();
      Precipitation += (random_range(12) + 1);
      setpflag(LOST);
      break;

    case 1:
      mprint("You enter a field of brightly colored flowers...");
      mprint("Wow, man! These are some pretty poppies...");
      morewait();
      mprint("poppies...");
      morewait();
      mprint("poppies...");
      morewait();
      print3("You become somewhat disoriented...");
      setpflag(LOST);
      break;

    case 2:
      mprint("You discover a sprig of athelas growing lonely in the wild.");
      morewait();
      mprint("Using your herbalist lore you cook a cake of lembas....");
      morewait();
      ob = new_object();
      *ob = Objects[OB_LEMBAS];
      gain_item(ob);
      break;

    case 3:
      if (Precipitation > 0)
	{
	  mprint("You are struck by a bolt of lightning!");
	  p_damage(p, random_range(25), ELECTRICITY, "a lightning strike");
	  morewait();
	}
      else
	mprint("You feel static cling");
      break;

    case 4:
      mprint("You find a fast-food establishment.");
      morewait();
      l_commandant();
      break;

    case 5:
      mprint("A weird howling tornado hits from out of the West!");
      morewait();
      mprint("You've been caught in a chaos storm!");
      morewait();
      num = random_range(300);
      if (num < 10)
	{
	  mprint("Your cell-structure was disrupted!");
	  p_damage(p, random_range(100), UNSTOPPABLE, "a chaos storm");
	  morewait();
	}
      else if (num < 20)
	{
	  int avg;

	  mprint("The chaos storm warps your frame!");
	  morewait();
	  mprint("Your statistical entropy has been maximized.");
	  morewait();
	  mprint("You feel average...");
	  morewait();
	  toggle_item_use(TRUE); /* FIXED! 12/30/98 */

	  avg = (p->maxstr + p->maxcon + p->maxdex + p->maxagi + p->maxiq + p->maxpow + 12) / 6;

	  p->str = p->maxstr = avg;
	  p->con = p->maxcon = avg;
	  p->dex = p->maxdex = avg;
	  p->agi = p->maxagi = avg;
	  p->iq  = p->maxiq  = avg;
	  p->pow = p->maxpow = avg;

	  toggle_item_use(FALSE); /* FIXED! 12/30/98 */
	}
      else if (num < 30)
	{
	  mprint("Your entire body glows with an eerie flickering light.");
	  morewait();
	  toggle_item_use(TRUE); /* FIXED! 12/30/98 */

	  for(i = 1; i <MAXITEMS; ++i)
	    if (p->possessions[i] != NULL)
	      {
		++(p->possessions[i]->plus);
		p->possessions[i]->blessing += 10;
		if (p->possessions[i]->objchar == STICK)
		  p->possessions[i]->charge += 10;
	      }

	  toggle_item_use(FALSE); /* FIXED! 12/30/98 */
	  cleanse(1);
	  mprint("You feel filled with energy!");
	  morewait();

	  p->maxpow += 5;
	  p->pow += 5;
	  p->mana = p->maxmana = 5 * calcmana();

	  mprint("You also feel weaker. Paradoxical, no?");
	  morewait();

	  p->con -= 5;
	  p->maxcon -= 5;
	  if (p->con < 3)
	    p_death(p, "congestive heart failure");
	}
      else if (num < 40)
	{
	  mprint("Your entire body glows black.");
	  morewait();
	  dispel(-1);
	  dispel(-1);
	  p->pow -= 10;
	  p->mana = 0;
	}
      else if (num < 60)
	{
	  mprint("The storm deposits you in a strange place....");
	  morewait();
	  teleport_to_random_in_country();
	}
      else if (num < 70)
	{
	  mprint("A tendril of the storm condenses and falls into your hands.");
	  morewait();
	  ob = new_object();
	  make_artifact(ob,-1);
	  gain_item(ob);
	}
      else if (num < 80)
	{
	  if (pflagp(p, MOUNTED) == TRUE)
	    {
	      mprint("Your horse screams as he is transformed into an");
	      morewait();
	      mprint("imaginary unseen dead tortoise.");
	      morewait();
	      mprint("You are now on foot.");
	      morewait();
	      resetpflag(p, MOUNTED);
	      set_notice_flags(notice_x_mounted);
	    }
	  else
	    {
	      mprint("You notice you are riding a horse. Odd. Very odd....");
	      morewait();
	      mprint("Now that's a horse of a different color!");
	      morewait();
	      setpflag(p, MOUNTED);
	      set_notice_flags(notice_x_mounted);
	    }
	}
      else if (num < 90)
	{
	  mprint("You feel imbued with godlike power....");
	  morewait();
	  wish(1);
	}
      else if (num < 100)
	{
	  mprint("The chaos storm has wiped your memory!");
	  morewait();
	  mprint("You feel extraordinarily naive....");
	  morewait();
	  mprint("You can't remember a thing! Not even your name.");
	  morewait();

	  p->xp = 0;
	  p->level = 0;

	  for (i = 0; i < NUMRANKS; ++i)
	    p->rank[i] = 0;

	  for (i = 0; i < NUMSPELLS; ++i)
	    p->spells[i].known = FALSE;

	  cmd_rename_player(no_dir, TRUE);
	}
      else
	{
	  mprint("You survive the chaos storm relatively unscathed.");
	  morewait();
	  mprint("It was a learning experience.");
	  morewait();
	  gain_experience(1000);
	}
      break;

    case 6: case 7: case 8: case 9: case 10:
      mprint("An encounter!");
      morewait();
      change_environment_push(E_TACTICAL_MAP, Country[p->x][p->y].current_terrain_type);
      break;

    case 11:
      mprint("You find a Traveller's Aid station with maps of the local area.");
      morewait();
      if (pflagp(p,LOST))
	{
	resetpflag(p,LOST);
	mprint("You know where you are now.");
	}

      for(i = p->x-5; i < (p->x + 6); ++i)
	for(j = p->y-5; j < (p->y + 6); ++j)
	  discover_country(i,j);

      set_notice_flags(notice_invalid_screen);
      break;

    case 12:
      if (pflagp(p, MOUNTED) == FALSE)
	{
	  mprint("You develop blisters....");
	  p_damage(p, 1, UNSTOPPABLE, "blisters");
	}
      break;

    case 13:
      mprint("You discover an itch just where you can't scratch it.");
      break;

    case 14:
      mprint("A cosmic ray strikes!");
      morewait();
      p_damage(p, 10, UNSTOPPABLE, "a cosmic ray");
      break;

    case 15:
      mprint("You feel strangely lucky.");
      break;

    case 16:
      mprint("The west wind carries with it a weird echoing voice....");
      hint();
      morewait();
      break;
    }

  /* For any non-null event: */
  set_notice_flags(notice_dont_repeat | notice_py_data );
  showflags(); /* Should be a notice flag too. */
}

char getlocation (void)
{
  int c = '\0';

  menuprint(" (enter location [HCL]) ");
  showmenu();

  while (c == '\0')
    switch (c = mcigetc())
      {
      case 'h': menuprint(" High.");   break;
      case 'c': menuprint(" Center."); break;
      case 'l': menuprint(" Low.");    break;
      default: break;
      }

  showmenu();
  return c - 'a' + 'A';
}

/* chance for player to resist magic somehow */
/* hostile_magic ranges in power from 0 (weak) to 10 (strong) */
int magic_resist (player_t * p, int hostile_magic)
{
  int attack;
  int resist;

  if (p->rank[COLLEGE] == 0 && p->rank[CIRCLE] == 0) goto no_counterspell;

  resist = (p->level / 2) + random_range(20);
  attack = hostile_magic + random_range(20);

  if (resist <= attack) goto no_counterspell;
  if (p->mana < (hostile_magic * hostile_magic)) goto no_counterspell;

  mprint("Thinking fast, you defend youself with a counterspell!");
  p->mana -= (hostile_magic * hostile_magic);
  dataprint();
  return TRUE;

 no_counterspell:

  resist = (p->level / 4) + p->status[PROTECTION] + random_range(20);

  if (resist <= attack) return FALSE;

  mprint("You resist the spell!");
  return TRUE;
}

void terrain_check (player_t * p)
{
  if (pflagp(p,LOST))
    {
      locprint("Lost");
      return;
    }
  
  switch(Country[p->x][p->y].current_terrain_type)
    {
    case RIVER:
      if (p->y < 6 && p->x > 20)
	locprint("Star Lake.");
      else if (p->y < 41)
	{
	  if (p->x < 10)
	    locprint("Aerie River.");
	  else
	    locprint("The Great Flood.");
	}
      else if (p->x < 42)
	locprint("The Swamp Runs.");
      else
	locprint("River Greenshriek.");
      break;

    case ROAD:
      locprint("A well-maintained road.");
      break;

    case PLAINS:
      locprint("A rippling sea of grass.");
      break;

    case TUNDRA:
      locprint("The Great Northern Wastes.");
      break;

    case FOREST:
      if (p->y < 10)
	locprint("The Deepwood.");
      else if (p->y < 18)
	locprint("The Forest of Erelon.");
      else if (p->y < 46)
	locprint("The Great Forest.");
      break;

    case JUNGLE:
      locprint("Greenshriek Jungle.");
      break;

    case DESERT:
      locprint("The Waste of Time.");
      break;

    case MOUNTAINS:
      if (p->y < 9 && p->x < 12)
	locprint("The Magic Mountains");
      else if (p->y < 9 && p->y > 2 && p->x < 40)
	locprint("The Peaks of the Fist.");
      else if (p->x < 52)
	locprint("The Rift Mountains.");
      else
	locprint("Borderland Mountains.");
      break;

    case PASS:
      locprint("A hidden pass.");
      break;

    case CHAOS_SEA:
      locprint("The Sea of Chaos.");
      break;

    case SWAMP:
      locprint("The Loathly Swamp.");
      break;

    case CITY:
      locprint("Outside Rampart, the city.");
      break;

    case VILLAGE:
      locprint("Outside a small village.");
      break;

    case CAVES:
      locprint("A deserted hillside.");
      break;

    case CASTLE:
      locprint("Near a fortified castle.");
      break;

    case TEMPLE:
      switch (Country[p->x][p->y].aux)
	{
	case ODIN:    locprint("A rough-hewn granite temple.");                break;
	case SET:     locprint("A black pyramidal temple made of sandstone."); break;
	case ATHENA:  locprint("A classical marble-columned temple.");         break;
	case HECATE:  locprint("A temple of ebony adorned with ivory.");       break;
	case DRUID:   locprint("A temple formed of living trees.");            break;
	case DESTINY: locprint("A temple of some mysterious blue crystal.");   break;
	}
      break;

    case MAGIC_ISLE:
      locprint("A strange island in the midst of the Sea of Chaos.");
      break;

    case STARPEAK:
      locprint("Star Peak.");
      break;

    case DRAGONLAIR:
      locprint("A rocky chasm.");
      break;

    case PALACE:
      locprint("The ruins of a once expansive palace.");
      break;

    case VOLCANO:
      locprint("HellWell Volcano.");
      break;

    default:
      locprint("I haven't any idea where you are!!!");
      break;
    }
}

char * countryid (Symbol terrain)
{
  switch (terrain & 0xff)
    {
    case MOUNTAINS & 0xff:
      strcpy(Str1,"Almost impassable mountains");
      break;

    case PLAINS & 0xff:
      strcpy(Str1,"Seemingly endless plains");
      break;

    case TUNDRA & 0xff:
      strcpy(Str1,"A frosty stretch of tundra");
      break;

    case ROAD & 0xff:
      strcpy(Str1,"A paved highway");
      break;

    case PASS & 0xff:
      strcpy(Str1,"A secret mountain pass");
      break;

    case RIVER & 0xff:
      strcpy(Str1,"A rolling river");
      break;

    case CITY & 0xff:
      strcpy(Str1,"The city of Rampart");
      break;

    case VILLAGE & 0xff:
      strcpy(Str1,"A rural village");
      break;

    case FOREST & 0xff:
      strcpy(Str1,"A verdant forest");
      break;

    case JUNGLE & 0xff:
      strcpy(Str1,"A densely overgrown jungle");
      break;

    case SWAMP & 0xff:
      strcpy(Str1,"A swampy fen");
      break;

    case VOLCANO & 0xff:
      strcpy(Str1,"A huge active volcano");
      break;

    case CASTLE & 0xff:
      strcpy(Str1,"An imposing castle");
      break;

    case STARPEAK & 0xff:
      strcpy(Str1,"A mysterious mountain.");
      break;

    case DRAGONLAIR & 0xff:
      strcpy(Str1,"A cavern filled with treasure.");
      break;

    case PALACE & 0xff:
      strcpy(Str1,"An ancient palace ruins.");
      break;

    case MAGIC_ISLE & 0xff:
      strcpy(Str1,"An island emanating magic.");
      break;

    case CAVES & 0xff:
      strcpy(Str1,"A hidden cave entrance");
      break;

    case TEMPLE & 0xff:
      strcpy(Str1,"A neoclassical temple");
      break;

    case DESERT & 0xff:
      strcpy(Str1,"A sere desert");
      break;

    case CHAOS_SEA & 0xff:
      strcpy(Str1,"The Sea of Chaos");
      break;

    default:
      strcpy(Str1,"I have no idea.");
      break;
    }

  return(Str1);
}

int discover_country (level_t * lev, int x, int y)
{
  if (inbounds(lev,x,y))
    {
      struct terrain * p_terr;
      p_terr = &Country[x][y];
      c_set(x, y, SEEN);

      if (p_terr->current_terrain_type != p_terr->base_terrain_type)
	{
	  p_terr->current_terrain_type = p_terr->base_terrain_type;
	  c_set(x, y, CHANGED);
	  set_notice_flags(notice_lev_altered);
	  return TRUE;
	}
    }

  return FALSE;
}

long countrysearch (player_t * p)
{
  int x, y;

  for (x = p->x - 1; x < (p->x + 2); ++x)
    for (y = p->y - 1; y < (p->y + 2); ++y)
      if (discover_country(p->dlevel,x,y))
	{
	  clearmsg();
	  mprint("Your search was fruitful!");
	  mprint("You discovered:");
	  mprint(countryid(Country[x][y].base_terrain_type));
	}

  return TICKS_PER_HOUR;
}

/* random effects from some of stones in villages */  
/* if alignment of stone is alignment of player, gets done sooner */
int stonecheck (player_t * p, int alignment)
{
  int * stone;
  int match = FALSE;
  int cycle = FALSE;
  int i;

  if (alignment == 1)
    {
      stone = &Lawstone;
      match = p->alignment > 0;
    }
  else if (alignment == -1)
    {
      stone = &Chaostone;
      match = p->alignment < 0;
    }
  else
    {
      stone = &Mindstone;
      match = FALSE;
    }

  *stone += random_range(4);
  if (match == TRUE)
    *stone += random_range(4);

  switch ((*stone)++)
    {
    case 0:  case 2:  case 4:  case 6:  case 8:  case 10: case 12:
    case 14: case 16: case 18: case 20: case 22: case 24: case 26:
    case 28: case 30: case 32: case 34: case 36: case 38: case 38: case 40:
      print1("The stone glows grey.");
      print2("Not much seems to happen this time."); 
      (*stone)--;
      break;

    case 1:
      print1("The stone glows black"); 
      print2("A burden has been removed from your shoulders.....");
      print3("Your pack has disintegrated!");
      for (i = 0; i < MAXPACK; ++i) 
	if (p->pack[i] != NULL)
	  {
	    free_obj(p->pack[i], TRUE);
	    p->pack[i] = NULL;
	  }
      p->packptr = 0;
      break;

  case 3:
    print1("The stone glows microwave");
    print2("A vortex of antimana spins about you!");
    morewait();
    dispel(-1);
    break;

    case 5:
      print1("The stone glows infrared");
      print2("A portal opens nearby and an obviously confused monster appears!");
      summon(-1,-1);
      morewait();
      break;

    case 7:
      print1("The stone glows brick red");
      print2("A gold piece falls from the heavens into your money pouch!");
      p->cash++;
      break;

    case 9:
      print1("The stone glows cherry red");
      print2("A flush of warmth spreads through your body.");
      augment(1);
      break;

    case 11:
      print1("The stone glows orange");
      print2("A flux of energy blasts you!"); 
      manastorm(p->x, p->y, random_range(p->maxhp) + 1);
      break;

    case 13:
      ("The stone glows lemon yellow");
      print2("You're surrounded by enemies! You begin to foam at the mouth.");
      p->status[BERSERK] += 10;
      break;

    case 15:
      print1("The stone glows yellow");
      print2("Oh no! The DREADED AQUAE MORTIS!");
      morewait();
      print2("No, wait, it's just your imagination.");
      break;

    case 17:
      print1("The stone glows chartreuse");
      print2("Your joints stiffen up.");
      p->agi -= 3;
      break;

    case 19:
      print1("The stone glows green");
      print2("You come down with an acute case of Advanced Leprosy.");
      p->status[DISEASED] = 1100;
      p->hp = 1;
      p->dex -= 5;
      break;

    case 21:
      print1("The stone glows forest green");
      print2("You feel wonderful!");
      p->status[HERO] += 10;
      break;

    case 23:
      print1("The stone glows cyan");
      print2("You feel a strange twisting sensation....");
      morewait();
      strategic_teleport(-1);
      break;

    case 25:
      print1("The stone glows blue");
      morewait();
      print1("You feel a tingle of an unearthly intuition:");
      morewait();
      hint();
      break;

    case 27:
      print1("The stone glows navy blue");
      print2("A sudden shock of knowledge overcomes you.");
      morewait();
      clearmsg();
      identify(1);
      knowledge(1);
      break;

    case 29:
      {
	int nsp = 0;

	for (i = 0; i < NUMSPELLS; ++i) 
	  if (p->spells[i].known)
	    ++nsp;

	print1("The stone glows blue-violet");
	print2("You feel forgetful.");

	for (i = 0; nsp > 0 && i < NUMSPELLS; ++i) 
	  if (p->spells[i].known)
	    if (--nsp == 0)
	      p->spells[i].known = FALSE;
      }
      break;

    case 31:
      print1("The stone glows violet");
      morewait();
      acquire(0);
      break;

    case 33:
      print1("The stone glows deep purple");
      print2("You vanish.");
      p->status[INVISIBLE] += 10;
      break;

    case 35: print1("The stone glows ultraviolet");
      print2("All your hair rises up on end.... A bolt of lightning hits you!");
      p_damage(p, random_range(p->maxhp), ELECTRICITY, "mystic lightning");
      break;

    case 37:
      print1("The stone glows roentgen");
      print2("You feel more experienced.");
      gain_experience(p, 250 * (p->level + 1));
      break;

    case 39:
      print1("The stone glows gamma"); 
      print2("Your left hand shines silvery, and your right emits a golden aura."); 
      morewait();
      enchant(1);
      bless(1);
      print3("Your hands stop glowing.");
      break;

    case 41: case 42: case 43: case 44: case 45: case 46: case 47: case 48: case 49:
      print1("The stone glows cosmic!");
      print2("The stone's energy field quiets for a moment...");
      *stone = 50;
      cycle = TRUE;
      break;

    default:
      print1("The stone glows polka-dot (?!?!?!?)");
      print2("You feel a strange twisting sensation....");
      morewait();
      *stone = 0;
      strategic_teleport(-1);
      break;
    }

  calc_melee(p);

  return cycle;
}

void alert_guards (player_t * p)
{
  int suppress = 0;
  int foundguard = FALSE;
  list_iterator_t * it;

  it = list_iterator_create(p->dlevel->monster_list);
  while (list_iterator_has_next(it))
    {
      monster * mon;
      mon = list_iterator_next(it);
      if (is_monster_guard(mon) && m_statusp(mon, M_GONE) == FALSE)
        {
          foundguard = TRUE;
          m_status_set(mon, AWAKE);
          m_status_set(mon, HOSTILE);
        }
    }
  list_iterator_delete(it);

  if (foundguard)
    {
      mprint("You hear a whistle and the sound of running feet!");
      if (p->dlevel->environment == E_CITY)
	{
	  /* pacify_guards restores this */
	  int x, y;
	  get_special_loc(sl_order_door, &x, &y);
	  p->dlevel->site[x][y].p_locf = L_NO_OP;
	}	
    }

  if (foundguard == FALSE && p->dlevel->environment == E_CITY && gamestatusp(p, DESTROYED_ORDER) == FALSE)
    {
      suppress = pflagp(SUPPRESS_PRINTING);
      resetpflag(SUPPRESS_PRINTING);
      print2("The last member of the Order of Paladins dies....");
      morewait();
      gain_experience(1000);
      p->alignment -= 250;

      if (gamestatusp(p, KILLED_LAWBRINGER) == FALSE)
        {
          print1("A chime sounds from far away.... The sound grows stronger....");
          print2("Suddenly the great shadowy form of the LawBringer appears over");
          print3("the city. He points his finger at you....");
          morewait();

          print1("\"Cursed art thou, minion of chaos! May thy strength fail thee");
          print2("in thy hour of need!\" You feel an unearthly shiver as the");
          print3("LawBringer waves his palm across the city skies....");
          morewait();

          p->str /= 2;
          dataprint();

          print1("You hear a bell tolling, and eerie moans all around you....");
          print2("Suddenly, the image of the LawBringer is gone.");
          print3("You hear a guardsman's whistle in the distance!");
          morewait();
          resurrect_guards();
        }
      else
        {
          print1("The Order's magical defenses have dropped, and the");
          print2("Legions of Chaos strike....");
          morewait();

          print1("The city shakes! An earthquake has struck!");
          print2("Cracks open in the street, and a chasm engulfs the Order HQ!");
          print3("Flames lick across the sky and you hear wild laughter....");

          morewait();
          gain_experience(p, 5000);
          destroy_order();
        }
    }

  if (suppress)
    resetpflag(p, SUPPRESS_PRINTING);
}

int maneuvers (player_t * p)
{
  int m;

  m = 2 + p->level / 7;

  if (p->rank[ARENA])
    m++;

  if (p->status[HASTED])
    m *= 2;

  if (p->status[SLOWED])
    m /= 2;

  m = min(8, max(1,m));

  return m;
}

/* for when haste runs out, etc. */
void default_maneuvers (player_t * p)
{
  int i;

  morewait();
  clearmsg();
  print1("Warning, resetting your combat options to the default.");
  print2("Use the 'F' command to select which options you prefer.");
  morewait();

  for (i = 0; i < maneuvers(); i += 2)
    {
      p->meleestr[i*2]         = 'A';
      p->meleestr[(i*2)+1]     = 'C';
      p->meleestr[(i+1)*2]     = 'B';
      p->meleestr[((i+1)*2)+1] = 'C';
    }

  p->meleestr[2 * maneuvers()] = 0;
}

/* end */
