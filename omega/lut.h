/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/* Macro templates to construct lookup table functions.

   def_lookup_table(TYPE, PREFIX, KEYNAME) creates:
     a type named PREFIX_table_t, which is a table.
     a lookup function named PREFIX_find, which takes the key and a
     table, and returns a pointer to TYPE.

 */
#if !defined __LUT_H
#define __LUT_H

#define def_lookup_table_type(type, prefix)\
typedef struct prefix##_table_t \
{ \
  type * array; \
  int    num_els; \
} \
prefix##_table_t; \
type* prefix##_find( char cmd, prefix##_table_t table )

/* Parameterized on table type. */
#define def_lookup_table_funcs_2(type, table_type, prefix, keyname)\
static int prefix##_comparison(void const * v_el1, void const * v_el2) \
{ \
  type * p_el1 = (type*) v_el1; \
  type * p_el2 = (type*) v_el2; \
 \
  return p_el1->keyname - p_el2->keyname; \
} \
 \
 \
type* prefix##_find( char cmd, table_type table ) \
{ \
  type dummy_element; \
  dummy_element.keyname = cmd; \
  return  (type*) \
    my_lfind \
    ((void *)&dummy_element, \
     (void *)table.array, \
     table.num_els, \
     sizeof(type), \
     prefix##_comparison); \
}

#define def_lookup_table_funcs(type, prefix, keyname)\
 def_lookup_table_funcs_2(type, prefix##_table_t, prefix, keyname)

#define def_lookup_table(type, prefix, keyname)\
  def_lookup_table_type(type, prefix); \
  def_lookup_table_funcs(type, prefix, keyname)


void *my_lfind( const void * key, const void * base, size_t num, size_t width,
                int (*fncomparison)(const void *, const void * ) );
/* Faster, but less safe.  Only useable if we can be sure the command
    array is sorted by keyname.  Requires stdlib.h, where lfind required
    search.h.
*/

/*
type* prefix##_find( char cmd, prefix##_table_t table ) \
{ \
  type dummy_element; \
  dummy_element.keyname = cmd; \
  return \
    bsearch \
    ((void *)&dummy_element, \
     (void *)table.array, \
     table.num_els, \
     sizeof(type), \
     prefix##_comparison); \
}
*/
#endif /*!defined __LUT_H*/
