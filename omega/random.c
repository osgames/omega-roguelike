/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/* *** Smart randomized choice *** */

#include "glob.h"
#include "util.h"
#include "load.h"

/* The freq_info_t support is unused as yet.  For later extension.  */

typedef int (*get_freq_f)(int id, int difficulty);
typedef struct freq_info_t
{
  pseudoid_set_table_t mset;
  get_freq_f get_freq;
  freq_table_table_t caches;
  int last_cache_used;
} freq_info_t;

/* Functions */

void freq_clear_table(random_table_t * p_table)
{
  int i;
  for(i = 0; i < p_table->t.num_els; i++)
    {
      p_table->t.array[i] = 0;
    }
}

void freq_calc_total_chances(random_table_t * p_table)
{
  int i;
  int total = 0;
  for(i = 0; i < p_table->t.num_els; i++)
    {
      total += p_table->t.array[i];
    }
  p_table->sum = total;
}

/* Set the values within TABLE according to ID.  */
void freq_setup_table_recurse(freq_table_t * p_table,
			      pseudoid_t id,
			      freq_info_t * p_info)
{
  /* If the id is real, act on it. */
  if(id >= 0)
    {
      assert(id < p_table->t.t.num_els);
      p_table->t.t.array[id] =
	p_info->get_freq(id, p_table->difficulty);
    }
  else
    /* Special id RANDOM allows everything. */
    if(id == RANDOM)
      {
	int i;
	for(i = 0; i < p_table->t.t.num_els; i++)
	  {
	    p_table->t.t.array[i] =
	      p_info->get_freq(i, p_table->difficulty);
	  }
      }
  else
    /* If the id refers to a set, recurse on the members. */
    {
      int set_id = -2 - id;
      assert(set_id >= 0);
      assert(set_id < p_info->mset.num_els);
      {
	pseudoid_set_t set = p_info->mset.array[set_id];
	int i;
	for(i = 0; i < set.num_els; i++)
	  {
	    pseudoid_t sub_id = set.array[i];
	    freq_setup_table_recurse(p_table, sub_id, p_info);
	  }
      }
    }
}

void freq_setup_table(freq_table_t * p_table,
		      pseudoid_t id,
		      int difficulty,
		      freq_info_t * p_info)
{
  freq_clear_table(&p_table->t);
  p_table->id = id;
  p_table->difficulty = difficulty;
  freq_setup_table_recurse(p_table, id, p_info);
  freq_calc_total_chances(&p_table->t);
}

/* Find or set up table among the caches. */
freq_table_t * freq_get_table(pseudoid_t id,
			      int difficulty,
			      freq_info_t * p_info)
{
  int i;
  for(i = 0; i < p_info->caches.num_els; i++)
    {
      freq_table_t * p_table = &p_info->caches.array[i];
      if((p_table->id == id) &&
	 (p_table->difficulty == difficulty))
	{ return p_table; }
    }
  /* Could look for one with different difficulty, which implies it's
     unlikely to be used in the near future. */

  /* If all else fails, pick one and reinitialize it. */
  {
    int table_num = p_info->last_cache_used;
    freq_table_t * p_table = &p_info->caches.array[table_num];
    p_info->last_cache_used ++;
    p_info->last_cache_used %= p_info->caches.num_els;
    freq_setup_table(p_table, id, difficulty, p_info);
    return p_table;
  }
}

int random_pick(random_table_t * p_table)
{
  int total_chances = p_table->sum;
  int roll = random_range(total_chances);
  int i;
  for(i = 0; i < p_table->t.num_els; i++)
    {
      roll -= p_table->t.array[i];
      if(roll < 0) { return i; }
    }
  error("Bad roll in freq_pick");
  return 0;
}


int freq_pick(pseudoid_t id,
	      int difficulty,
	      freq_info_t * p_info)
{
  freq_table_t * p_table = freq_get_table(id, difficulty, p_info);
  return random_pick(&p_table->t);
}

/* Allocate a probability table object. */
freq_info_t * make_freq_info(int num_els,
			     int num_caches,
			     pseudoid_set_table_t * p_mset,
			     get_freq_f get_freq)
{
  int i;
  freq_info_t  * p_freq = checkmalloc(sizeof(freq_info_t));
  freq_table_t * p_caches = checkmalloc(sizeof(freq_table_t) * num_caches);

  p_freq->mset            = *p_mset;
  p_freq->get_freq        = get_freq;
  p_freq->caches.array    = p_caches;
  p_freq->caches.num_els  = num_caches;
  p_freq->last_cache_used = 0;

  for(i = 0; i < num_caches; i++)
    {
      p_caches[i].t.t.array   = checkmalloc(sizeof(int) * num_els);
      p_caches[i].t.t.num_els = num_els;
      p_caches[i].id         = -1;
      p_caches[i].difficulty = -1;
    }

  return p_freq;
}

/**/
int freq_get_prob_of_level_diff(int level_differential)
{
  switch(level_differential)
    {
    default:
      if(level_differential > 0) { return 0; }
      return 3;

      case 2:
	return 1;

      case 1:
	return 10;

      case 0:
	return 1000;

      case -1:
	return 300;

      case -2:
	return 100;

      case -3:
	return 30;

      case -4:
	return 10;
    }
}


/* Support functions for monsters and objects. */

int get_monsterid_level(int id)
{
  assert(id >= 0);
  assert(id < NUMMONSTERS);

  return Monsters[id].level;
}

int get_monsterid_freq(int id, int difficulty)
{
  int mon_level = get_monsterid_level(id);
  return
    freq_get_prob_of_level_diff(mon_level - difficulty);
}

int get_objectid_level(int id)
{
  assert(id >= 0);
  assert(id < TOTALITEMS);
  return Objects[id].level;
}

int get_objectid_freq(int id, int difficulty)
{
  int obj_level = get_objectid_level(id);
  return
    freq_get_prob_of_level_diff(obj_level - difficulty);
}

