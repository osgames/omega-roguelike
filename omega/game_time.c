/* omega copyright (c) 1987,1988,1989 by Laurence Raphael Brothers */
/* game_time.c */

/* this file deals with the passage of time in omega */

#include <stdio.h>

#include "game_time.h"
#include "glob.h"
#include "lev.h"    /* get_monster() */
#include "scr.h"    /* display_time() */
#include "ticket.h" /* ticket_new_*(), ticket_schedule() */
#include "util.h"   /* random_range() */


static unsigned long elapsed_minutes;

static int clock_minute;
static int clock_hour;
static int clock_day;
static int clock_month;
static int clock_year;

static int moon_phase;

static int monsters_on = FALSE;  /* Only writable in monster_tickets_*. */

static ticket_t *minute_ticket;  /* Keep track of these to properly save time */
static ticket_t *moon_ticket;

unsigned long get_elapsed_time (void)
{
  return elapsed_minutes;
}

int time_year (void)
{
  return clock_year;
}

int time_month (void)
{
  return clock_month;
}

int time_day (void)
{
  return clock_day;
}

int time_hour (void)
{
  return clock_hour;
}

int time_minute (void)
{
  return clock_minute;
}

int time_phase (void)
{
  return moon_phase;
}

static int waste_time = 0;

void set_waste_time (int flag)
{
  if (waste_time && !flag)
    {
      display_time();
      display_moon_phase();
    }

  waste_time = flag;
}

/* nighttime is defined from 9 PM to 6AM */
int nighttime (void)
{
  return (time_hour() > 20 || time_hour() < 7);
}

/* this is called once per "moon day" (25 hours) */
static void moon_handler (ticket_t * ticket)
{
  int lunarity;

  /* 24 "moon day" lunar cycle */
  ++moon_phase;
  if (24 == moon_phase) moon_phase = 0;
  lunarity = get_lunarity();

  if (waste_time) return;

  display_moon_phase();

  if (1 == lunarity)
    mprint("As the moon rises you feel unusually vital!");
  else if (-1 == lunarity)
    mprint("The rise of the moon tokens a strange enervation!");
}

static void minute_handler (ticket_t * ticket)
{
  minute_status_check();

  ++elapsed_minutes;

  ++clock_minute;
  if (clock_minute < 60) goto display;

  clock_minute = 0;
  ++clock_hour;
  if (clock_hour < 24) goto display;

  clock_hour = 0;
  ++clock_day;
  if (clock_day < 30) goto display;

  clock_day = 0;
  ++clock_month;
  if (clock_month < 12) goto display;

  clock_month = 0;
  ++clock_year;

 display:

  if (waste_time) return;

  /* JDK HACK: Commented out to have a more precise time display while */
  /* fiddling with tickets */
  /*if (0 == (elapsed_minutes % 10))*/
  display_time();
}

static void ten_minute_handler (ticket_t * ticket)
{
  ten_minute_status_check(!waste_time);

  if (0 == Player.status[DISEASED] && Player.hp < Player.maxhp)
    Player.hp = min(Player.maxhp, Player.hp + Player.level + 1);

  if (waste_time) return;

  if (Current_Environment == Current_Dungeon)
    wandercheck();

  if (Current_Environment != E_COUNTRYSIDE && Current_Environment != E_ABYSS)
    indoors_random_event();
}

static void hour_handler (ticket_t * ticket)
{
  torch_check(!waste_time);
  --(Player.food);

  if (waste_time) return;

  foodcheck();
}

void player_handler (ticket_t * ticket)
{
  unsigned long duration;
  ticket_t * next_turn;

  /* Assure that monsters are moving if applicable.  NB, this does not
     make monsters move, it just ensures that they are set up to move.
     This is not done when loading a level, because that can happen in
     the middle of a player's turn, which would mess up time.  */
  if((Current_Environment != E_COUNTRYSIDE) && !waste_time)
    {
      monster_tickets_on();
    }

  duration = p_process();
  next_turn = ticket_new_oneshot(player_handler);
  ticket_schedule(next_turn, duration);
}

static void monster_handler (ticket_t * ticket)
{
  monster * mon;
  ticket_t * next_turn;

  ticket_reset_arguments(ticket);
  if (!ticket_has_next_argument(ticket)) assert(FALSE);
  mon = ticket_next_argument(ticket);
  assert(ticket == mon->ticket);

  assert(Current_Environment != E_COUNTRYSIDE);

  /* It may have been killed by the player. */
  if(!m_statusp(mon, M_GONE))
    {
      assert(mon->hp > 0);
      assert(mon == get_monster(Level, mon->x, mon->y));

      if (clock_minute % 10 == 0)
	if (mon->hp < Monsters[mon->id].hp)
	  ++(mon->hp);

      /* No ticket during m_pulse, in case we leave the environment
	 during it */
      mon->ticket = 0;
      m_pulse(mon);
    }

  /* It may have died or vanished from its own action. */
  if(!m_statusp(mon, M_GONE))
    {
      next_turn = ticket_new_oneshot(monster_handler);
      mon->ticket = next_turn;
      ticket_add_argument(next_turn, mon);
      ticket_schedule(next_turn, mon->speed);
    }
  else
    { /*  Free it now, not when it "dies", because now it's easy to
	  do it cleanly. */
      monster * rmon = list_remove_member(Level->monster_list, mon);
      free_monster(mon);
      assert(rmon == mon);
    }
  /* printf("scheduled monster %p with ticket %p\n", mon, mon->ticket); */
}

void set_monster_handler (monster * mon)
{
  ticket_t * ticket;
  int duration;
  if (!monsters_on)
    {
      mon->ticket = 0;
      return;
    }

  assert(Level);
  ticket = ticket_new_oneshot(monster_handler);
  mon->ticket = ticket;
  ticket_add_argument(ticket, mon);
  if (Level->clicks_contain_time)
    duration = mon->click;
  else
    duration = mon->speed;
  ticket_schedule(ticket, duration);
  /* printf("scheduled monster %p with ticket %p\n", mon, mon->ticket); */
}

void apply_remove_ticket (void * vmon)
{
  monster * mon;
  mon = vmon;
  ticket_remove(mon->ticket);
  mon->ticket = 0;
}

void apply_save_ticket (void * vmon)
{
  monster * mon;
  mon = vmon;
  mon->click = get_ticket_duration(mon->ticket);
  ticket_remove(mon->ticket);
  mon->ticket = 0;
}

void apply_insert_ticket (void * vmon)
{
  monster * mon;
  mon = vmon;
  if (0 == mon->ticket)
    set_monster_handler(mon);
}

/* Ensure that every monster has a ticket. */
void monster_tickets_on(void)
{
  if (!monsters_on)
    {
      monsters_on = TRUE;
      if(Level && get_monster_list(Level))
        {
          list_apply(get_monster_list(Level), apply_insert_ticket);
          /*mprint("Inserted monster tickets\n");*/
          Level->clicks_contain_time = FALSE;
        }
    }
}


/* Ensure that every monster doesn't have a ticket.  Optionally saves
 * monsters' next turns to mon->click. */
void monster_tickets_off(int save)
{
  if (monsters_on)
    {
      monsters_on = FALSE;
      if(Level && get_monster_list(Level))
        {
          if (save) {
            list_apply(get_monster_list(Level), apply_save_ticket);
            Level->clicks_contain_time = TRUE;
          }
          else
            list_apply(get_monster_list(Level), apply_remove_ticket);
          /*mprint("Removed all monster tickets\n");*/
        }
    }
}

void init_time (void)
{
  ticket_t * ticket;

  clock_year = 0;
  clock_month = random_range(12);
  clock_day = random_range(30);
  clock_hour = 12;
  clock_minute = 0;

  moon_phase = random_range(24);

  ticket = ticket_new_oneshot(player_handler);
  ticket_schedule(ticket, 0);

  minute_ticket = ticket_new_periodic(minute_handler, TICKS_PER_MINUTE);
  ticket_schedule(minute_ticket, TICKS_PER_MINUTE);

  ticket = ticket_new_periodic(ten_minute_handler, 10 * TICKS_PER_MINUTE);
  ticket_schedule(ticket, 10 * TICKS_PER_MINUTE);

  ticket = ticket_new_periodic(hour_handler, TICKS_PER_HOUR);
  ticket_schedule(ticket, TICKS_PER_HOUR);

  /* moon days (25 hours) are out of phase with sun days (24 hours) */
  moon_ticket = ticket_new_periodic(moon_handler, 25 * TICKS_PER_HOUR);
  ticket_schedule(moon_ticket, random_range(25) * TICKS_PER_HOUR);
}

int save_time (FILE * fd)
{
  int ok = 1;
  unsigned long duration;

  ok &= (fwrite(&elapsed_minutes, sizeof(unsigned long), 1, fd) > 0);
  ok &= (fwrite(&moon_phase, sizeof(int), 1, fd) > 0);
  ok &= (fwrite(&clock_minute, sizeof(int), 1, fd) > 0);
  ok &= (fwrite(&clock_hour, sizeof(int), 1, fd) > 0);
  ok &= (fwrite(&clock_day, sizeof(int), 1, fd) > 0);
  ok &= (fwrite(&clock_month, sizeof(int), 1, fd) > 0);
  ok &= (fwrite(&clock_year, sizeof(int), 1, fd) > 0);

  duration = get_ticket_duration(minute_ticket);
  ok &= (fwrite(&duration, sizeof(unsigned long), 1, fd) > 0);

  duration = get_ticket_duration(moon_ticket);
  ok &= (fwrite(&duration, sizeof(unsigned long), 1, fd) > 0);

  return ok;
}

int restore_time (FILE * fd)
{
  int ok = 1;
  unsigned long minute_duration, moon_duration;
  ticket_t *ticket;

  ok &= (fread(&elapsed_minutes, sizeof(unsigned long), 1, fd) > 0);
  ok &= (fread(&moon_phase, sizeof(int), 1, fd) > 0);
  ok &= (fread(&clock_minute, sizeof(int), 1, fd) > 0);
  ok &= (fread(&clock_hour, sizeof(int), 1, fd) > 0);
  ok &= (fread(&clock_day, sizeof(int), 1, fd) > 0);
  ok &= (fread(&clock_month, sizeof(int), 1, fd) > 0);
  ok &= (fread(&clock_year, sizeof(int), 1, fd) > 0);
  ok &= (fread(&minute_duration, sizeof(unsigned long), 1, fd) > 0);
  ok &= (fread(&moon_duration, sizeof(unsigned long), 1, fd) > 0);

  minute_ticket = ticket_new_periodic(minute_handler, TICKS_PER_MINUTE);
  ticket_schedule(minute_ticket, minute_duration);

  ticket = ticket_new_periodic(ten_minute_handler, 10 * TICKS_PER_MINUTE);
  ticket_schedule(ticket,
    (9 - (elapsed_minutes % 10)) * TICKS_PER_MINUTE + minute_duration);

  ticket = ticket_new_periodic(hour_handler, TICKS_PER_HOUR);
  ticket_schedule(ticket,
    (59 - (elapsed_minutes % 60)) * TICKS_PER_MINUTE + minute_duration);

  /* moon days (25 hours) are out of phase with sun days (24 hours) */
  moon_ticket = ticket_new_periodic(moon_handler, 25 * TICKS_PER_HOUR);
  ticket_schedule(moon_ticket, moon_duration);

  /* schedule restored player's ticket last in case any time handlers are */
  /* scheduled for the same time */
  ticket = ticket_new_oneshot(player_handler);
  ticket_schedule(ticket, 0);

  return ok;
}

