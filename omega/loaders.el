;; Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
;; Omega license as part of Omega.
;; ***********************************

;;Data describing how to load environments.

;; This file contains only data, no functions. 

;; This data is in sexp form.  The only functionality required is:

;; (make-random-table make-coun-loader-t make-env-loader-t
;;    make-tacmap-loader-t nil make-dun-loader make-levplan-t
;;    make-subroom-t make-map-loader list make-map-loader-only quote
;;    make-cmap-readtable make-shuffled-sites-table make-readtable
;;    defconst make-monster-set def-symbols) 

;;This can be verified with `check-loaders-el' in "declare-omega.el"


;;One way to transform this into C is to use tools/make-maptable.el 
;;Eventually an interpreter within Omega should read this directly.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;The data


;; Symbols that are meaningful in the Omega C code, made
;; self-referential for ease of use.  Many are unused.


(def-symbols action-tests
   (
      always
      ("If the level is empty" if_empty)
      ("If the level isn't empty" not_empty)
      if_hostile    
      not_hostile   
      if_flag3     	
      not_flag3    	
      if_flag4
      not_flag4))


(def-symbols post-processors
   (
      post_process_no_op          
      post_process_dlair          
      post_process_set_m_sp_court 
      post_process_house
      post_process_calm_monster
      post_process_destroy_order
      post_process_nitegloom)
   "The available post-processing functions")




(def-symbols flag-making-functions
   (safe_flags temple_flags dlair_flags speak_flags misle_flags no_flags
      circle_flags)
   "The available flag-making functions")



(def-symbols roomnames
   (
      RS_WALLSPACE
      RS_CORRIDOR
      RS_CAVERN
      RS_GOBLINKING
      RS_DRAGONLORD
      RS_PONDS
      RS_OCEAN
      RS_WYRM
      RS_ADEPT
      RS_DESTINY
      RS_ODIN
      RS_SET
      RS_ATHENA
      RS_HECATE
      RS_DRUID
      RS_COUNTRYSIDE
      RS_ARENA
      RS_SEWER_DUCT
      RS_DRAINED_SEWER
      RS_DROWNED_SEWER
      RS_KITCHEN
      RS_BEDROOM
      RS_BATHROOM
      RS_DININGROOM
      RS_SECRETPASSAGE
      RS_CLOSET
      RS_LOWERASTRAL
      RS_EARTHPLANE
      RS_WATERPLANE
      RS_AIRPLANE
      RS_FIREPLANE
      RS_HIGHASTRAL
      RS_VOLCANO
      RS_STARPEAK
      RS_MAGIC_ISLE
      RS_CIRCLE
      RS_ZORCH
      RS_COURT
      RS_GARDEROBE
      RS_CELL
      RS_TILED
      RS_CRYSTAL_CAVE
      RS_BEDROOM2
      RS_STOREROOM
      RS_CHARRED
      RS_MARBLE_HALL
      RS_EERIE_CAVE
      RS_TREASURE
      RS_SMOKEY
      RS_APARTMENT
      RS_ANTECHAMBER
      RS_HAREM
      RS_MULTIPURPOSE
      RS_STALACTITES
      RS_GREENHOUSE
      RS_WATERCLOSET
      RS_STUDY
      RS_LIVING_ROOM
      RS_DEN
      RS_ABATOIR
      RS_BOUDOIR
      RS_STAR_CHAMBER
      RS_MANMADE_CAVE
      RS_SEWER_CONTROL
      RS_SHRINE
      RS_MAGIC_LAB
      RS_PENTAGRAM
      RS_OMEGA_DAIS)
   "The available roonames")



(def-symbols map-name-symbols
   (
      MAP_country
      MAP_arena
      MAP_circle
      MAP_city
      MAP_abyss
      MAP_court
      MAP_dlair
      MAP_hedges
      MAP_house
      MAP_hovel
      MAP_mansion
      MAP_misle
      MAP_skorch
      MAP_speak
      MAP_starview
      MAP_stormwat
      MAP_temple
      MAP_thaumari
      MAP_whorfen
      MAP_woodmere
      MAP_jail
      MAP_order))



(def-symbols reify-actions
   (
      lmap_make_monster 
      lmap_make_logical_monster  
      lmap_set_locf 
      lmap_set_locchar 
      lmap_set_aux 
      lmap_set_aux_to_subtype 
      lmap_set_loc_flags 
      lmap_clear_loc_flags
      lmap_set_roomnumber 
      lmap_make_shuffled_site 
      lmap_make_random_site 
      lmap_make_logical_site 
      lmap_make_treasure 
      lmap_make_specific_treasure 
      lmap_set_special_loc
      lmap_retain_max
      lmap_retain_min
      lmap_set_multi_pos
      lmap_copy_prev_loc 
      ))

(def-symbols logical-monsters
   (
      mon_random 
      mon_elite_dragon 
      mon_undead_guard 
      mon_minor_undead 
      mon_major_undead 
      mon_house_npc 
      mon_mansion_npc 
      mon_justiciar 
      mon_high_priest 
      mon_prime_sorceror 
      mon_archmage 
      mon_arena_monster))

(def-symbols logical-sites
   (
      site_food_bin 
      site_random_altar
      site_alert_statue
      site_all_stops))

(def-symbols subroom-types
   (
      subroom_none 
      subroom_name 
      subroom_map))


(def-symbols level-plans
   (
      dls_room_level
      dls_cavern_level
      dls_sewer_level
      dls_maze_level))


(def-symbols env-families
   (enst_map enst_dun enst_coun enst_tacmap))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Sets of monsters (For later expansion) (For populate_level,
;;freq_table style)

;;The monster sets would be collected by index, and uses of their
;;indexes declared with the inverse of whatever formula
;;freq_setup_table_recurse uses to decode them.

(defconst mset-goblins
   (make-monster-set
      GOBLIN
      GOBLIN_CHIEF 
      GOBLIN_SHAMAN)
   "" )


(defconst mset-sewer-creatures
   (make-monster-set
      SEWER_RAT 
      AGGRAVATOR 
      BLIPPER 
      NIGHT_GAUNT 
      NASTY 
      MURK 
      CATOBLEPAS 
      ACID_CLOUD 
      DENEBIAN 
      CROC 
      TESLA 
      SHADOW 
      BOGTHING 
      WATER_ELEM 
      TRITON 
      ROUS))


(defconst mset-astral
   (make-monster-set
      THOUGHTFORM 
      FUZZY 
      BAN_SIDHE 
      GRUE 
      SHADOW 
      ASTRAL_VAMP 
      MANABURST 
      RAKSHASA 
      ILL_FIEND 
      MIRRORMAST 
      ELDER_GRUE 
      SHADOW_SLAY))


;;Angels + demons + servants.
(defconst mset-deep-astral
   (make-monster-set
      NIGHT_GAUNT 
      SERV_LAW 
      SERV_CHAOS 
      FROST_DEMON 
      OUTER_DEMON 
      DEMON_SERP 
      ANGEL 
      INNER_DEMON 
      FDEMON_L 
      HIGH_ANGEL 
      DEMON_PRINCE 
      ARCHANGEL))


          

(defconst mset-evil
   (make-monster-set
      HAUNT 
      INCUBUS 
      DRAGONETTE 
      FROST_DEMON 
      SPECTRE 
      LICHE 
      RAKSHASA 
      DEMON_SERP 
      NAZGUL 
      LOATHLY 
      ZOMBIE 
      INNER_DEMON 
      BAD_FAIRY  
      DRAGON  
      FDEMON_L
      SHADOW_SLAY  
      DEATHSTAR  
      VAMP_LORD  
      DEMON_PRINCE))



(defconst mset-fire-critter
   (make-monster-set
      LAVA_WORM 
      FIRE_ELEM 
      FLAME_DEV 
      FIRE_ELEM))

          
(defconst mset-magic-users
   (make-monster-set
      ENCHANTOR
      NECROMANCER
      THAUMATURGIST))

(defconst mset-elementals
   (make-monster-set
      FIRE_ELEM
      AIR_ELEM 
      WATER_ELEM
      EARTH_ELEM))


(defconst mset-angels 
   (make-monster-set
      ANGEL
      HIGH_ANGEL
      ARCHANGEL)
   "" )

(defconst mset-demons
   (make-monster-set
      NIGHT_GAUNT 
      L_FDEMON
      INCUBUS
      FROST_DEMON 
      OUTER_DEMON 
      DEMON_SERP 
      INNER_DEMON 
      FDEMON_L 
      DEMON_PRINCE ))

(defconst mset-robots 
   (make-monster-set

      ROBOT
      AUTO_MINOR
      AUTO_MAJOR)
   "" )


(defconst mset-dragons 
   (make-monster-set
      DRAGONETTE
      WYVERN
      DRAGON)
   
   "" )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Readtables

(defconst lmap_dlair
   (make-readtable nil
      ("@" ((lmap_set_locchar FLOOR)
	      (lmap_set_special_loc sl_py_entrance))
	 )
      ("D" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster DRAGON_LORD))
	 )
      ("d" ((lmap_set_locchar FLOOR)
	      (lmap_make_logical_monster mon_elite_dragon))
	 )
      ("W" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster KING_WYV))
	 )
      ("M" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster RANDOM))
	 )
      ("S" (
	      (lmap_set_locchar FLOOR)
	      (lmap_set_roomnumber RS_SECRETPASSAGE)
	      (lmap_set_loc_flags SECRET not_empty))
	 )
      ("$" ((lmap_set_locchar FLOOR)
	      (lmap_make_treasure 10))
	 )
      ;;  /* Happens to mark the split between rooms.  */
      ("s" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_TRAP_SIREN not_empty)
	      (lmap_set_special_loc sl_dlair_roomsplit))
	 )
      ("7" ((lmap_set_locchar PORTCULLIS not_empty)
	      (lmap_set_locchar FLOOR if_empty)
	      (lmap_set_locf L_PORTCULLIS))
	 )
      ("R" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_RAISE_PORTCULLIS))
	 )
      ("p" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_PORTCULLIS))
	 )
      ("T" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_PORTCULLIS_TRAP not_empty))
	 )
      ("X" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_TACTICAL_EXIT))
	 )
      ("#" ((lmap_set_locchar WALL)
	      (lmap_set_aux 150))
	 )
      ("." ((lmap_set_locchar FLOOR))
	 )))

(defconst lmap_jail 
   (make-readtable nil
      ("7" ((lmap_set_locchar FLOOR)
	     (lmap_set_locf L_PORTCULLIS))
	 )
      ("R" ((lmap_set_locchar FLOOR)
	     (lmap_set_locf L_RAISE_PORTCULLIS))
	 )
      ("T" ((lmap_set_locchar FLOOR)
	     (lmap_set_locf L_PORTCULLIS_TRAP)
	     (lmap_set_multi_pos sl_jail_cell))
	 )
      ("#" ((lmap_set_locchar WALL)
	     (lmap_set_aux 150))
	 )
      ("*" ((lmap_set_locchar WALL)
	     (lmap_set_aux 10))
	 )
      
      ("-" (
	     (lmap_set_locchar CLOSED_DOOR)
	     (lmap_set_locf L_JAIL)
	     (lmap_set_special_loc sl_jail_door)))

      ("." ((lmap_set_locchar FLOOR)))
      ))

(defconst lmap_hedgemaze 
   (make-readtable nil
      ("@" ((lmap_set_locchar FLOOR)
	      (lmap_set_special_loc sl_py_entrance))
	 )
      ("\"" ((lmap_make_random_site hedge_or_trifid))
	 )
      ("-" ((lmap_set_locchar CLOSED_DOOR))
	 )
      ("." ((lmap_set_locchar FLOOR))
	 )
      (">" ((lmap_set_locchar STAIRS_DOWN)
	      (lmap_set_locf L_SEWER))
	 )
      ("z" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_MAZE))
	 )
      ("O" ((lmap_set_locchar OPEN_DOOR)
	      (lmap_set_locf L_ORACLE))
	 )
      ("?" ((lmap_make_random_site random_maze_site_action))
	 )))



(defconst lmap_city 
   (make-readtable nil
      ("z" ((lmap_set_locchar FLOOR)
	   (lmap_set_locf L_MAZE))
       )
      ("@" ((lmap_set_locchar FLOOR)
	     (lmap_set_special_loc sl_py_entrance))
	 )
      ("g" ((lmap_set_locchar FLOOR)
	     (lmap_set_locf L_GARDEN))
	 )
      ("y" ((lmap_set_locchar FLOOR)
	     (lmap_set_locf L_CEMETARY))
	 )
      ;;   /* WDT: each of these places needs to be assigned
      ;;    * a function (or replaced with an ?x? in the map)
      ;;    */
      ("p" ((lmap_make_shuffled_site 0))
	 )
      ("!" ((lmap_make_shuffled_site 0))
	 )
      ("I" ((lmap_make_shuffled_site 0))
	 )
      ("E" ((lmap_make_shuffled_site 0))
	 )
      ("e" ((lmap_make_shuffled_site 0))
	 )
      ("x" ((lmap_make_shuffled_site 0))
	 )
      ("t" ((lmap_set_locchar FLOOR)
	     (lmap_set_locf L_TEMPLE))
	 )
      ;; #if 0 /* WDT: HACK!  The new city doesn?t use portcullis
      ;; traps  but has other 
      ;;        * uses for ?T?.  Um...  I?d rather have a use for them
      ;;        (that?s what 
      ;;        * the jail is supposed to be)  so this will stay only
      ;;        for now. with 
      ;;        * any luck we?ll have things fixed up before the next
      ;;        release. */ 
      
      ;;       ( "T"   (
      ;; 		(lmap_set_locchar FLOOR)
      ;; 		(lmap_set_locf  L_PORTCULLIS_TRAP)
      ;; 		(lmap_set_aux  NOCITYMOVE))  ) 

      ;; #endif /* end of hack */
      ("R" ((lmap_set_locchar FLOOR)
	     (lmap_set_locf L_RAISE_PORTCULLIS)
	     (lmap_set_aux NOCITYMOVE))
	 )
      ("7" ((lmap_set_locchar FLOOR)
	     (lmap_set_locf L_PORTCULLIS)
	     (lmap_set_aux NOCITYMOVE))
	 )
      ("C" ((lmap_set_locchar OPEN_DOOR)
	     (lmap_set_locf L_COLLEGE))
	 )
      ("s" ((lmap_set_locchar OPEN_DOOR)
	     (lmap_set_locf L_SORCERORS))
	 )
      ("M" ((lmap_set_locchar OPEN_DOOR)
	     (lmap_set_locf L_MERC_GUILD))
	 )
      ;; #ifdef INCLUDE_MONKS
      ;;   ( ?K ((lmap_set_locchar OPEN_DOOR) (lmap_set_locf  L_MONASTERY))) 
      ;; #else

      ("K" ((lmap_make_shuffled_site 0)))
      
      ;;#endif
      ("c" ((lmap_set_locchar OPEN_DOOR)
	     (lmap_set_locf L_CASTLE))
	 )

      ("?" ((lmap_retain_min sl_hedgemaze_ulc)))

      ("b" ((lmap_retain_min sl_jail_ulc)))
      
      ("d" ((lmap_retain_min sl_order_ulc)))

      ("P" ((lmap_set_locchar OPEN_DOOR)
	     (lmap_set_locf L_ORDER)
	     (lmap_set_special_loc sl_order_door))
	 
	 )
      ("H" ((lmap_set_locchar OPEN_DOOR)
	     (lmap_set_locf L_CHARITY))
	 )
      ("h" ((lmap_set_locchar FLOOR)
	     (lmap_make_monster HORSE))
	 )
      ("j" ((lmap_set_locchar FLOOR)
	     (lmap_make_logical_monster mon_justiciar)
	     (lmap_set_special_loc sl_justiciar_home)))
      ("J" (
	     (lmap_set_locchar CLOSED_DOOR)
	     (lmap_set_locf L_JAIL)
	     (lmap_set_special_loc sl_jail_door)))      

      ("A" ((lmap_set_locchar OPEN_DOOR)
	     (lmap_set_locf L_ARENA))
	 )
      ("B" ((lmap_set_locchar OPEN_DOOR)
	     (lmap_set_locf L_BANK))
	 )
      ("i" ((lmap_set_locchar OPEN_DOOR)
	     (lmap_set_locf L_TOURIST)
	     (lmap_make_logical_site site_all_stops))
	 )
      ("X" ((lmap_set_locchar FLOOR)
	     (lmap_set_locf L_COUNTRYSIDE))
	 )

      ("S" ((lmap_set_locchar FLOOR)
	     (lmap_set_aux NOCITYMOVE)
	     (lmap_set_loc_flags SECRET))
	 )
      ("G" ((lmap_set_locchar FLOOR)
	     (lmap_make_monster GUARD))
	 )
      ("u" ((lmap_set_locchar FLOOR)
	     (lmap_make_random_site random_minor_undead not_empty))
	 )
      ("U" ((lmap_set_locchar FLOOR)
	     (lmap_make_random_site random_major_undead not_empty))
	 )
      ("v" ((lmap_set_locchar FLOOR)
	     (lmap_set_locf L_VAULT)
	     (lmap_set_aux NOCITYMOVE)
	     (lmap_set_loc_flags SECRET))
	 )
      ;;Difference: "V" is inside, "v" is outside.
      ("V" ((lmap_set_locchar FLOOR)
	     (lmap_set_locf L_VAULT)
	     (lmap_make_treasure 5)
	     (lmap_set_aux NOCITYMOVE)
	     (lmap_set_loc_flags SECRET))
	 )
      ("w" (  (lmap_set_locchar WALL)
	      (lmap_set_special_loc sl_vault_door)))
      
      ("%" ((lmap_set_locchar FLOOR)
	     (lmap_set_locf L_TRAP_SIREN)
	     (lmap_make_treasure 5)
	     (lmap_set_aux NOCITYMOVE)
	     (lmap_set_loc_flags SECRET))
	 )
      ("$" ((lmap_set_locchar FLOOR)
	     (lmap_make_treasure 5))
	 )
      ("2" ((lmap_set_locchar ALTAR)
	     (lmap_set_locf L_ALTAR)
	     (lmap_set_aux ODIN))
	 )
      ("3" ((lmap_set_locchar ALTAR)
	     (lmap_set_locf L_ALTAR)
	     (lmap_set_aux SET))
	 )
      ("4" ((lmap_set_locchar ALTAR)
	     (lmap_set_locf L_ALTAR)
	     (lmap_set_aux ATHENA))
	 )
      ("5" ((lmap_set_locchar ALTAR)
	     (lmap_set_locf L_ALTAR)
	     (lmap_set_aux HECATE))
	 )
      ("6" ((lmap_set_locchar ALTAR)
	     (lmap_set_locf L_ALTAR)
	     (lmap_set_aux DESTINY))
	 )
      ("^" ((lmap_set_locchar FLOOR)
	     (lmap_make_random_site random_site_trap)
	     (lmap_set_loc_flags SECRET))
	 )
      ("\"" ((lmap_set_locchar HEDGE))
	 )
      ("~" ((lmap_set_locchar WATER)
	     (lmap_set_locf L_WATER))
	 )
      ("=" ((lmap_set_locchar WATER)
	     (lmap_set_locf L_MAGIC_POOL))
	 )
      ("*" ((lmap_set_locchar WALL)
	     (lmap_set_aux 10))
	 )
      ("#" ((lmap_set_locchar WALL)
	     (lmap_set_aux 500))
	 )
      ;;  /* currently meaningless in large city map. */
      ("T" ((lmap_set_locchar FLOOR))
	 )

      ;;Restored, why not? - Tehom
      (">" ((lmap_set_locchar STAIRS_DOWN)
	     (lmap_set_locf L_SEWER)))
      ("." ((lmap_set_locchar FLOOR))
	 )
      ("'" ((lmap_set_locchar FLOOR)
	      (lmap_set_aux NOCITYMOVE)
	      (lmap_set_loc_flags SECRET))
	 )
      ("-" ((lmap_set_locchar CLOSED_DOOR))
	 )
      ;;  /* WDT: should all Ds be changed to -  or should D be given
      ;;   * special treatment? */
      ("D" ((lmap_set_locchar CLOSED_DOOR))
	 )
      ("1" ((lmap_set_locchar STATUE))
	 )))


(defconst lmap_city_resurrect_guards 
   (make-readtable nil
       ("G" (
	      (lmap_make_logical_monster mon_undead_guard))))
   
   "Actions for resurrecting the city guards." )

(defconst lmap_city_destroy_order
   (make-readtable nil
       ("S" ((lmap_set_locchar FLOOR)
	       (lmap_clear_loc_flags SECRET)))
       ("j" ((lmap_set_locchar FLOOR)))
       ("P" ((lmap_set_locchar RUBBLE)
	     (lmap_set_locf L_RUBBLE)))
       ("#" ((lmap_set_locchar RUBBLE)
	      (lmap_set_locf L_RUBBLE)))
       ("." ((lmap_set_locchar FLOOR)))
       ("G" ((lmap_set_locchar FLOOR)))
       )
   
   "Actions for destroying the order headquarters." )


(defconst lmap_abyss 
   (make-readtable nil
      ("@" ((lmap_set_locchar FLOOR)
	      (lmap_set_special_loc sl_py_entrance))
	 )
      ("0" ((lmap_set_locchar VOID_CHAR)
	      (lmap_set_locf L_VOID))
	 )
      ("V" ((lmap_set_locchar VOID_CHAR)
	      (lmap_set_locf L_VOID_STATION))
	 )
      ("1" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_VOICE1))
	 )
      ("2" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_VOICE2))
	 )
      ("3" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_VOICE3))
	 )
      ("~" ((lmap_set_locchar WATER)
	      (lmap_set_locf L_WATER_STATION))
	 )
      (";" ((lmap_set_locchar FIRE)
	      (lmap_set_locf L_FIRE_STATION))
	 )
      ("\"" ((lmap_set_locchar HEDGE)
	       (lmap_set_locf L_EARTH_STATION))
	 )
      ("6" ((lmap_set_locchar WHIRLWIND)
	      (lmap_set_locf L_AIR_STATION))
	 )
      ("#" ((lmap_set_locchar WALL))
	 )
      ("." ((lmap_set_locchar FLOOR))
	 )))

(defconst lmap_speak  
   (make-readtable nil
      ("@" ((lmap_set_locchar FLOOR)
	      (lmap_set_special_loc sl_py_entrance))
	 )
      ("S" ((lmap_set_locchar FLOOR)
	      (lmap_set_loc_flags SECRET)
	      (lmap_set_roomnumber RS_SECRETPASSAGE))
	 )
      ("L" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster LAWBRINGER))
	 )
      ("s" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster SERV_LAW))
	 )
      ("M" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster RANDOM))
	 )
      ("$" ((lmap_set_locchar FLOOR)
	      (lmap_make_treasure 10))
	 )
      ("7" ((lmap_set_locchar PORTCULLIS not_empty)
	      (lmap_set_locchar FLOOR if_empty)
	      (lmap_set_locf L_PORTCULLIS))
	 )
      ("R" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_RAISE_PORTCULLIS))
	 )
      ("-" ((lmap_set_locchar CLOSED_DOOR))
	 )
      ("|" ((lmap_set_locchar OPEN_DOOR))
	 )
      ("p" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_PORTCULLIS))
	 )
      ("T" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_PORTCULLIS_TRAP not_empty))
	 )
      ("X" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_TACTICAL_EXIT))
	 )
      ("#" ((lmap_set_locchar WALL)
	      (lmap_set_aux 150))
	 )
      ("4" ((lmap_set_locchar RUBBLE)
	      (lmap_set_locf L_RUBBLE))
	 )
      ("." ((lmap_set_locchar FLOOR))
	 )))

(defconst lmap_misle  
   (make-readtable nil
      ("@" ((lmap_set_locchar FLOOR)
	      (lmap_set_special_loc sl_py_entrance))
	 )
      ("E" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster EATER))
	 )
      ("m" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster MIL_PRIEST))
	 )
      ("n" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster NAZGUL))
	 )
      ("X" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_TACTICAL_EXIT))
	 )
      ("#" ((lmap_set_locchar WALL)
	      (lmap_set_aux 150))
	 )
      ("4" ((lmap_set_locchar RUBBLE)
	      (lmap_set_locf L_RUBBLE))
	 )
      ("~" ((lmap_set_locchar WATER)
	      (lmap_set_locf L_CHAOS))
	 )
      ("=" ((lmap_set_locchar WATER)
	      (lmap_set_locf L_MAGIC_POOL))
	 )
      ("-" ((lmap_set_locchar CLOSED_DOOR))
	 )
      ("|" ((lmap_set_locchar OPEN_DOOR))
	 )
      ("." ((lmap_set_locchar FLOOR))
	 )))

;;    flag 3 = "player is high priest"
(defconst lmap_temple  
   (make-readtable nil
      ("@" ((lmap_set_locchar FLOOR)
	      (lmap_set_special_loc sl_py_entrance))
	 )
      ("8" ((lmap_set_locchar ALTAR)
	      (lmap_set_locf L_ALTAR)
	      (lmap_set_aux_to_subtype 0))
	 )
      ("H" ((lmap_set_locchar FLOOR)
	      (lmap_make_logical_monster mon_high_priest not_flag3))
	 )
      ("S" ((lmap_set_locchar FLOOR)
	      (lmap_set_loc_flags SECRET not_flag3))
	 )
      ("W" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_TEMPLE_WARNING if_hostile))
	 )
      ("m" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster MIL_PRIEST))
	 )
      ("d" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster DOBERMAN))
	 )
      ("X" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_TACTICAL_EXIT))
	 )
      ;;Walls, but overridden in druid temple.
      ("#"
 	 (
 	    (lmap_set_locchar WALL)
 	    (lmap_set_aux 150)))
      ("." ((lmap_set_locchar FLOOR))
	 )
      ("x" ((lmap_set_locchar FLOOR)
	      (lmap_make_random_site random_temple_site_action))
	 )

      ;;Same as floor, except in Destiny temple.
      ("?" ((lmap_set_locchar FLOOR)))
      
      ("-" ((lmap_set_locchar CLOSED_DOOR))
	 )
      ("|" ((lmap_set_locchar OPEN_DOOR))
	 )))

(defconst lmap_arena  
   (make-readtable nil
      ("@" ((lmap_set_locchar FLOOR)
	      (lmap_set_special_loc sl_py_entrance))
	 )
      ("A" ((lmap_set_locchar FLOOR)
	      (lmap_make_logical_monster mon_arena_monster))
	 )
      ("B" ((lmap_set_locchar FLOOR)
	      ;;   Add this to create a tag-team if flag 3 is set.
	      ;;( if_flag3  lmap_make_logical_monster  mon_arena_monster  ) 
	      )
	 )
      ("P" ((lmap_set_locchar PORTCULLIS)
	      (lmap_set_locf L_PORTCULLIS))
	 )
      ("X" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_ARENA_EXIT))
	 )
      ("#" ((lmap_set_locchar WALL))
	 )
      ("." ((lmap_set_locchar FLOOR))
	 )))

(defconst lmap_circle  
   (make-readtable nil
      ("@" ((lmap_set_locchar FLOOR)
	      (lmap_set_special_loc sl_py_entrance))
	 )
      ("P" ((lmap_set_locchar FLOOR)
	      (lmap_make_logical_monster mon_prime_sorceror))
	 )
      ("D" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster DEMON_PRINCE))
	 )
      ("s" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster SERV_CHAOS))
	 )
      ("e" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster ENCHANTOR))
	 )
      ("n" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster NECROMANCER))
	 )
      ("T" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster THAUMATURGIST))
	 )
      ("#" ((lmap_set_locchar WALL)
	      (lmap_set_aux 1000))
	 )
      ("L" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_CIRCLE_LIBRARY))
	 )
      ("?" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_TOME1))
	 )
      ("!" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_TOME2))
	 )
      ("S" ((lmap_set_locchar FLOOR)
	      (lmap_set_loc_flags SECRET))
	 )
      ("." ((lmap_set_locchar FLOOR))
	 )
      ("-" ((lmap_set_locchar CLOSED_DOOR))
	 )))

(defconst lmap_court  
   (make-readtable nil
      ("@" ((lmap_set_locchar FLOOR)
	      (lmap_set_special_loc sl_py_entrance))
	 )
      ("5" ((lmap_set_locchar CHAIR)
	      (lmap_set_locf L_THRONE)
	      (lmap_make_logical_monster mon_archmage)
	      (lmap_make_specific_treasure OB_SCEPTRE))
	 )
      ("e" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster ENCHANTOR))
	 )
      ("n" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster NECROMANCER))
	 )
      ("T" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster THAUMATURGIST))
	 )
      ("#" ((lmap_set_locchar WALL)
	      (lmap_set_aux 1000))
	 )
      ("G" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster GUARD))
	 )
      ("<" ((lmap_set_locchar STAIRS_UP)
	      (lmap_set_locf L_ESCALATOR))
	 )
      ("." ((lmap_set_locchar FLOOR))
	 )))

;;Map now puts stops next to entrance X.  
(defconst lmap_house  
   (make-readtable nil
      ("@" ((lmap_set_locchar FLOOR)
	      (lmap_set_special_loc sl_py_entrance))
	 )
      ("N" ((lmap_set_locchar FLOOR)
	      (lmap_set_roomnumber RS_BEDROOM)
	      (lmap_make_random_site maybe_house_npc not_empty))
	 )
      ("H" ((lmap_set_locchar FLOOR)
	      (lmap_set_roomnumber RS_BEDROOM)
	      (lmap_make_random_site maybe_mansion_npc not_empty))
	 )
      ("D" ((lmap_set_locchar FLOOR)
	      (lmap_set_roomnumber RS_DININGROOM))
	 )
      ("." ((lmap_set_locchar FLOOR))
	 )
      ("x" ((lmap_set_locchar FLOOR)
	      (lmap_set_loc_flags STOPS))
	 )
      ("c" ((lmap_set_locchar FLOOR)
	      (lmap_set_roomnumber RS_CLOSET))
	 )
      ("G" ((lmap_set_locchar FLOOR)
	      (lmap_set_roomnumber RS_BATHROOM))
	 )
      ("B" ((lmap_set_locchar FLOOR)
	      (lmap_set_roomnumber RS_BEDROOM))
	 )
      ("K" ((lmap_set_locchar FLOOR)
	      (lmap_set_roomnumber RS_KITCHEN))
	 )
      ("S" ((lmap_set_locchar FLOOR)
	      (lmap_set_loc_flags SECRET)
	      (lmap_set_roomnumber RS_SECRETPASSAGE))
	 )
      ("3" ((lmap_set_locchar SAFE)
	      (lmap_set_loc_flags SECRET)
	      (lmap_set_locf L_SAFE))
	 )
      ("^" ((lmap_set_locchar FLOOR)
	      (lmap_make_random_site random_site_trap))
	 )
      ("P" ((lmap_set_locchar PORTCULLIS)
	      (lmap_set_locf L_PORTCULLIS))
	 )
      ("R" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_RAISE_PORTCULLIS))
	 )
      ("p" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_PORTCULLIS))
	 )
      ("T" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_PORTCULLIS_TRAP))
	 )
      ("X" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_HOUSE_EXIT))
	 )
      ("|" ((lmap_set_locchar OPEN_DOOR)
	      (lmap_set_roomnumber RS_CORRIDOR)
	      (lmap_set_loc_flags STOPS))
	 )
      ("+" ((lmap_set_locchar CLOSED_DOOR)
	      (lmap_set_roomnumber RS_CORRIDOR)
	      (lmap_set_aux LOCKED)
	      (lmap_set_loc_flags STOPS))
	 )
      ("d" ((lmap_set_locchar FLOOR)
	      (lmap_set_roomnumber RS_CORRIDOR)
	      (lmap_make_monster DOBERMAN))
	 )
      ("a" ((lmap_set_locchar FLOOR)
	      (lmap_set_roomnumber RS_CORRIDOR)
	      (lmap_set_locf L_TRAP_SIREN))
	 )
      ("A" ((lmap_set_locchar FLOOR)
	      (lmap_set_roomnumber RS_CORRIDOR)
	      (lmap_make_monster AUTO_MINOR))
	 )))


(defconst lmap_village  
   (make-readtable nil
      ("@" ((lmap_set_locchar FLOOR)
	      (lmap_set_special_loc sl_py_entrance))
	 )
      ("f" ((lmap_set_locchar FLOOR)
	      (lmap_make_logical_site site_food_bin not_empty))
	 )
      ("g" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_GRANARY))
	 )
      ("h" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster HORSE))
	 )
      ("S" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_STABLES))
	 )
      ("H" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster MERCHANT))
	 )
      ("C" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_COMMONS))
	 )
      ("s" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster SHEEP))
	 )
      ("x" ((lmap_make_shuffled_site 0))
	 )
      ("X" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_COUNTRYSIDE))
	 )
      ("G" ((lmap_set_locchar FLOOR)
	      (lmap_make_monster GUARD))
	 )
      ("^" ((lmap_set_locchar FLOOR)
	      (lmap_set_locf L_TRAP_SIREN))
	 )
      ("\"" ((lmap_set_locchar HEDGE)
	       (lmap_set_locf L_HEDGE))
	 )
      ("~" ((lmap_set_locchar WATER)
	      (lmap_set_locf L_WATER))
	 )
      ("+" ((lmap_set_locchar WATER)
	      (lmap_set_locf L_CHAOS))
	 )
      ("'" ((lmap_set_locchar HEDGE)
	      (lmap_set_locf L_TRIFID))
	 )

      ("#" ((lmap_set_locchar WALL)
	      (lmap_set_aux 100))
	 )
      ("." ((lmap_set_locchar FLOOR))
	 )
      ("-" ((lmap_set_locchar CLOSED_DOOR))
	 )
      ("1" ((lmap_set_locchar STATUE))
	 )))

;;
;; % means "making a special site".
;; < means stairway up.  That doesn't set entry point, C does that.
;; B means boss or other bottom-level special.

(defconst lmap-dungeon-normal
   (make-readtable nil

      (">" ((lmap_set_locchar STAIRS_DOWN)
	      (lmap_set_locf L_STAIRS_DOWN)))
      ("<" ((lmap_set_locchar STAIRS_UP)
	      (lmap_set_locf L_STAIRS_UP)))
      ("#" ((lmap_set_locchar WALL)
	      (lmap_set_aux 150)))
      ("." ((lmap_set_locchar FLOOR)))
      ;;There is no default boss.
      ("B" ())
      )

   "Base lmap for dungeons" )

;;All of these could inherit from a common readtable.

;;;;;;;;;;;;;;;;;
;; Shuffled sites

;;(first x) is shuffle_size, the number of entries we expect, which C
;;will truncate to 64. 
;;(second x) is the default actionlist.
;;(cddr x) is the list of actionlists.

(defconst city_shuffled_sites 
   (make-shuffled-sites-table

      64
      (
	 (lmap_make_random_site random_city_house_locf)
	 (lmap_set_locchar CLOSED_DOOR)
	 (lmap_make_random_site random_lock_door))
       

      (
	 (lmap_set_locf L_CLUB)
	 (lmap_set_locchar OPEN_DOOR))

      (
	 (lmap_set_locf L_ARMORER)
	 (lmap_set_locchar OPEN_DOOR))

      (
	 (lmap_set_locf L_GYM)
	 (lmap_set_locchar OPEN_DOOR))

      (
	 (lmap_set_locf L_THIEVES_GUILD)
	 (lmap_set_locchar CLOSED_DOOR))

      (
	 (lmap_set_locf L_HEALER)
	 (lmap_set_locchar OPEN_DOOR))

      (
	 (lmap_set_locf L_CASINO)
	 (lmap_set_locchar OPEN_DOOR))

      (
	 (lmap_set_locf L_DINER)
	 (lmap_set_locchar OPEN_DOOR))

      (
	 (lmap_set_locf L_CRAP)
	 (lmap_set_locchar OPEN_DOOR))

      (
	 (lmap_set_locf L_COMMANDANT)
	 (lmap_set_locchar OPEN_DOOR))
      (
	 (lmap_set_locf L_COMMANDANT)
	 (lmap_set_locchar OPEN_DOOR))
      (
	 (lmap_set_locf L_COMMANDANT)
	 (lmap_set_locchar OPEN_DOOR))

      (
	 (lmap_set_locf L_TAVERN)
	 (lmap_set_locchar OPEN_DOOR))

      (
	 (lmap_set_locf L_ALCHEMIST)
	 (lmap_set_locchar OPEN_DOOR))

      (
	 (lmap_set_locf L_DPW)
	 (lmap_set_locchar OPEN_DOOR))

      (
	 (lmap_set_locf L_LIBRARY)
	 (lmap_set_locchar OPEN_DOOR))

      (
	 (lmap_set_locf L_PAWN_SHOP)
	 (lmap_set_locchar OPEN_DOOR))

      (
	 (lmap_set_locf L_CONDO)
	 (lmap_set_locchar OPEN_DOOR))


      (
	 (lmap_set_locf L_BROTHEL)
	 (lmap_set_locchar CLOSED_DOOR))

      )
   "Shuffled sites in the city" )


(defconst village_shuffled_sites 
   (make-shuffled-sites-table
      24
      (;;Village house doors are not locked.
	 (lmap_make_random_site random_village_house_locf)
	 (lmap_set_locchar CLOSED_DOOR))

      (
	 (lmap_set_locf L_ARMORER)
	 (lmap_set_locchar OPEN_DOOR))

      (
	 (lmap_set_locf L_HEALER)
	 (lmap_set_locchar OPEN_DOOR))

      (
	 (lmap_set_locf L_COMMANDANT)
	 (lmap_set_locchar OPEN_DOOR))

      (
	 (lmap_set_locf L_TAVERN)
	 (lmap_set_locchar OPEN_DOOR))

      (
	 (lmap_set_locf L_CARTOGRAPHER)
	 (lmap_set_locchar OPEN_DOOR))
      )

   "Shuffled sites in the villages" )


;;;;
;;** Country map translation table **

;;For now I've left this table as it was in C.  If ease of adding new
;;entries is ever a big deal, make-cmap-readtable could take advantage
;;of the following regularities:
;;
;;The first entry defaults to base_terrain_type anded with 0xff, but
;;only because the values are defined that way.
;;
;;aux defaults to 0 
;;
;;current_terrain_type defaults to base_terrain_type

(defconst country_map 
   (make-cmap-readtable
      ( PASS
	 PASS
	 MOUNTAINS
	 0 
	 )
      ( CASTLE
	 CASTLE
	 MOUNTAINS
	 0 
	 )
      ( STARPEAK
	 STARPEAK
	 MOUNTAINS
	 0 
	 )
      ( CAVES
	 CAVES
	 MOUNTAINS
	 0 
	 )
      ( VOLCANO
	 VOLCANO
	 MOUNTAINS
	 0 
	 )
      ( DRAGONLAIR
	 DRAGONLAIR
	 DESERT
	 0 
	 )
      ( MAGIC_ISLE
	 MAGIC_ISLE
	 CHAOS_SEA
	 0 
	 )
      ( PALACE
	 PALACE
	 JUNGLE
	 0 
	 )
      ( ?a
	 VILLAGE
	 VILLAGE
	 1
	 )
      ( ?b
	 VILLAGE
	 VILLAGE
	 2
	 )
      ( ?c
	 VILLAGE
	 VILLAGE
	 3
	 )
      ( ?d
	 VILLAGE
	 VILLAGE
	 4
	 )
      ( ?e
	 VILLAGE
	 VILLAGE
	 5
	 )
      ( ?f
	 VILLAGE
	 VILLAGE
	 6
	 )
      ( ?1
	 TEMPLE
	 TEMPLE
	 1
	 )
      ( ?2
	 TEMPLE
	 TEMPLE
	 2
	 )
      ( ?3
	 TEMPLE
	 TEMPLE
	 3
	 )
      ( ?4
	 TEMPLE
	 TEMPLE
	 4
	 )
      ( ?5
	 TEMPLE
	 TEMPLE
	 5
	 )
      ( ?6
	 TEMPLE
	 TEMPLE
	 6
	 )
      ( PLAINS
	 PLAINS
	 PLAINS
	 0 
	 )
      ( TUNDRA
	 TUNDRA
	 TUNDRA
	 0 
	 )
      ( ROAD
	 ROAD
	 ROAD
	 0 
	 )
      ( MOUNTAINS
	 MOUNTAINS
	 MOUNTAINS
	 0 
	 )
      ( RIVER
	 RIVER
	 RIVER
	 0 
	 )
      ( CITY
	 CITY
	 CITY
	 0 
	 )
      ( FOREST
	 FOREST
	 FOREST
	 0 
	 )
      ( JUNGLE
	 JUNGLE
	 JUNGLE
	 0 
	 )
      ( SWAMP
	 SWAMP
	 SWAMP
	 0 
	 )
      ( DESERT
	 DESERT
	 DESERT
	 0 
	 )
      ( CHAOS_SEA
	 CHAOS_SEA
	 CHAOS_SEA
	 0 
	 )
      )
   "" )

;;;;;;;;;;;;;;;;;;;;
;; ***  Loaders  *** 

;;;;
;;* Special map loaders that don't make environments. *

(defconst jail_map_loader
   (make-map-loader-only 
      :lmap-table lmap_jail
      :map_idx MAP_jail
      :prepare_action
      '
      (
	 (lmap_set_loc_flags (SEEN | LIT)))))


(defconst city_map_loader_resurrect_guards 
   (make-map-loader-only
      :lmap-table lmap_city_resurrect_guards
      :map_idx MAP_city))

(defconst city_map_loader_build_order 
   (make-map-loader-only
      :lmap-table lmap_city
      :map_idx MAP_order
      :post-process post_process_calm_monster)

   "" )

(defconst city_map_loader_destroy_order 
   (make-map-loader-only
      :lmap-table lmap_city_destroy_order
      :map_idx MAP_order
      :post-process 'post_process_destroy_order)
   
   "" )

(defconst omega-list-auxilliary-loader-syms
   '(
       jail_map_loader
       city_map_loader_resurrect_guards
       city_map_loader_destroy_order)
   
   "" )

;;;;
;;** Loaders that make environments. **


;;* Map loaders *

(defconst village_env_loader
   (make-map-loader 
      'E_VILLAGE
      :difficulty 1
      :set-randomizer t
      :cache-type 'cache_in_temp
      :shuffled-sites village_shuffled_sites
      :flag-setter safe_flags
      :prepare_action
      '
      (
	 (lmap_set_loc_flags (SEEN | LIT)))
      

      :subenvs
      (list
	 (list 
	    :name "The Village of Star View"
	    :map_idx MAP_starview
	    :lmap-table
	    (make-readtable lmap_village
	       ("!"
		  (
		     (lmap_set_locchar  ALTAR)
		     (lmap_set_locf L_LAWSTONE)))))
	 
	 (list 
	    :name "The Village of Woodmere"
	    :map_idx MAP_woodmere
	    :lmap-table
	    (make-readtable lmap_village
	       ("!"
		  (
		     (lmap_set_locchar  ALTAR)
		     (lmap_set_locf L_BALANCESTONE)))))
	 
	 (list 
	    :name "The Village of Stormwatch"
	    :map_idx MAP_stormwat
	    :lmap-table
	    (make-readtable lmap_village
	       ("!"
		  (
		     (lmap_set_locchar  ALTAR) 
		     (lmap_set_locf L_CHAOSTONE)))))
	 
	 (list 
	    :name "The Village of Thaumaris"
	    :map_idx MAP_thaumari
	    :lmap-table
	    (make-readtable lmap_village
	       ("!"
		  (
		     (lmap_set_locchar  ALTAR) 
		     (lmap_set_locf L_MINDSTONE)))))
	 
	 (list 
	    :name "The Village of Skorch"
	    :map_idx MAP_skorch
	    :lmap-table
	    (make-readtable lmap_village
	       ("!"
		  (
		     (lmap_set_locchar  ALTAR) 
		     (lmap_set_locf L_SACRIFICESTONE)))))

	 (list
	    :name "The Village of Whorfen"
	    :map_idx MAP_whorfen
	    :lmap-table
	    (make-readtable lmap_village
	       ("!"
		  (
		     (lmap_set_locchar  ALTAR) 
		     (lmap_set_locf L_VOIDSTONE)))))))
   "" )

(defconst city_loader 
   (make-map-loader
      'E_CITY
      :lmap-table lmap_city
      :name "The City of Rampart"
      :map_idx MAP_city
      :post-process post_process_calm_monster
      :shuffled-sites city_shuffled_sites
      :difficulty 3
      :set-randomizer t
      :cache-type 'cache_in_city
      :flag-setter safe_flags
      :prepare_action
      '
      (
	 (lmap_set_loc_flags (SEEN | LIT)))

      :subrooms
      (list
	 ;;Hedgemaze
	 (make-subroom-t 
	    subroom_map 
	    (make-map-loader-only 
	       :lmap-table lmap_hedgemaze
	       :map_idx MAP_hedges
	       :prepare_action
	       '
	       (
		  (lmap_clear_loc_flags (SEEN | LIT)))
	       :post-process post_process_calm_monster)
	    :ulc 'sl_hedgemaze_ulc)
	 ;;Jail
	 (make-subroom-t 
	    subroom_map 
	    jail_map_loader
	    :ulc 'sl_jail_ulc)
	 ;;Order of paladins HQ.
	 (make-subroom-t 
	    subroom_map 
	    city_map_loader_build_order
	    :ulc 'sl_order_ulc)))
   

   "" )


(defconst hovel_loader 
   (make-map-loader
      'E_HOVEL
      :lmap-table
      (make-readtable lmap_house
	 ("#"
	    (
	       (lmap_set_locchar WALL)
	       (lmap_set_aux 10))))
      :name "A hovel: "
      :difficulty 3
      :set-randomizer t
      :map_idx  MAP_hovel 
      :prepare_action
      '
      (
	 (lmap_set_roomnumber RS_CORRIDOR)
	 (lmap_set_loc_flags (SEEN)))
      :post-process post_process_house)
   
   "" )
(defconst mansion_loader 
   (make-map-loader
      'E_MANSION
      :lmap-table
      (make-readtable lmap_house
	 ("#"
	    (
	       (lmap_set_locchar WALL)
	       (lmap_set_aux 150))))
      :name "A luxurious mansion: "
      :difficulty 7
      :set-randomizer t
      :map_idx  MAP_mansion 
      :prepare_action
      '
      (
	 (lmap_set_roomnumber RS_CORRIDOR))
      :post-process post_process_house)
   
   "" )
(defconst house_loader 
   (make-map-loader
      'E_HOUSE
      :lmap-table
      (make-readtable lmap_house
	  ("#"
	     (
		(lmap_set_locchar WALL)
		(lmap_set_aux 50))))
      :name "A house: "
      :difficulty 5
      :set-randomizer t
      :map_idx  MAP_house 
      :prepare_action
      '
      (
	 (lmap_set_roomnumber RS_CORRIDOR))
      :post-process post_process_house)
   
   "" )


(defconst arena_loader 
   (make-map-loader
      'E_ARENA
      :lmap-table lmap_arena
      :name "The Rampart Arena"
      :difficulty 5
      :map_idx MAP_arena
      :prepare_action
      '
      (
	 (lmap_set_roomnumber RS_ARENA)
	 (lmap_set_loc_flags (SEEN | LIT))))
   
   "" )

(defconst abyss_loader 
   (make-map-loader
      'E_ABYSS
      :lmap-table lmap_abyss
      :name "The Adept's Challenge"
      :difficulty 10
      :map_idx  MAP_abyss 
      :prepare_action
      '
      (
	 (lmap_set_roomnumber RS_ADEPT)))
   
   "Loader for the Abyss" )

(defconst dlair_loader 
   (make-map-loader
      'E_DLAIR
      :lmap-table lmap_dlair
      :difficulty 9
      :map_idx MAP_dlair
      :post-process post_process_dlair
      :flag-setter dlair_flags
      :subrooms
      (list
	 (make-subroom-t
	    subroom_name
	    RS_CAVERN
	    :max_x 'sl_dlair_roomsplit)

	 (make-subroom-t
	    subroom_name
	    RS_DRAGONLORD
	    :min_x 'sl_dlair_roomsplit))
      )
   "" )

(defconst starpeak_loader 
   (make-map-loader
      'E_STARPEAK
      :lmap-table lmap_speak
      :name "Near the oddly glowing peak of a mountain"
      :difficulty 9
      :map_idx  MAP_speak
      :prepare_action
      '
      (
	 (lmap_set_roomnumber RS_STARPEAK))
      :flag-setter speak_flags
      )
   "" )
(defconst circle_loader 
   (make-map-loader
      'E_CIRCLE
      :lmap-table lmap_circle
      :name "The Astral Demesne of the Circle of Sorcerors"
      :difficulty 8
      :map_idx  MAP_circle 
      :post-process post_process_set_m_sp_court
      :prepare_action
      '
      (
	 (lmap_set_roomnumber RS_CIRCLE))
      :flag-setter circle_flags
      )
   "" )
(defconst court_loader 
   (make-map-loader
      'E_COURT
      :lmap-table lmap_court
      :difficulty 3
      :name "The Court of the ArchMage."
      :map_idx  MAP_court 
      :post-process post_process_set_m_sp_court
      :prepare_action
      '
      (
	 (lmap_set_roomnumber RS_COURT))
      :flag-setter safe_flags
      )
   "" )

(defconst magic_isle_loader 
   (make-map-loader
      'E_MAGIC_ISLE
      :lmap-table lmap_misle
      :name "An island positively reeking of magic"
      :difficulty 8
      :map_idx  MAP_misle
      :prepare_action
      '
      (
	 (lmap_set_roomnumber RS_MAGIC_ISLE))
      :flag-setter misle_flags
      )
   
   "" )
(defconst temple_loader 
   (make-map-loader
      'E_TEMPLE
      :lmap-table lmap_temple
      :difficulty 8
      :map_idx  MAP_temple 
      :post-process post_process_no_op
      :flag-setter temple_flags
      :subenvs
      (list
	 (list
	    :name "The Shrine of the Noose"
	    :prepare_action
	    '
	    (
	       (lmap_set_roomnumber RS_ODIN))
	    )
	 (list
	    :name "The Temple of the Black Hand"
	    :prepare_action
	    '
	    (
	       (lmap_set_roomnumber RS_SET))
	    )
	 (list
	    :name "The Parthenon"
	    :prepare_action
	    '
	    (
	       (lmap_set_roomnumber RS_ATHENA))
	    )
	 (list
	    :name "The Church of the Far Side"
	    :prepare_action
	    '
	    (
	       (lmap_set_roomnumber RS_HECATE)))
	 
	 (list
	    :name "The Great Henge"
	    :lmap-table
	    (make-readtable lmap_temple
	       ("#"
		  (
		     (lmap_set_locf L_HEDGE)
		     (lmap_set_locchar HEDGE))))
	    :prepare_action
	    '
	    (
	       (lmap_set_roomnumber RS_DRUID))
	    )
	 (list
	    :name "The Halls of Fate"
	    :lmap-table
	    (make-readtable lmap_temple
		("?" 
		   (
		      (lmap_set_locchar ABYSS)
		      (lmap_set_locf L_ADEPT)))
		
		)
	    :prepare_action
	    '
	    (
	       (lmap_set_roomnumber RS_DESTINY))
	    )))
   
   
   "" )


;;;;
;; * Dungeon loaders *

(defconst caves_loader
   (make-dun-loader
      'E_CAVES
      :name "The Goblin Caves"
      :max-levels 10 
      :difficulty '(1 :depth-scale 0.3)

      :subenvs
      (list
	 (list
	    :lmap-table
	    (make-readtable lmap-dungeon-normal
	       ("%"
		  (
		     (lmap_make_random_site 
			random_base_specials))))
	    :level-type-list 
	    (list
	       (make-levplan-t 'dls_room_level)
	       (make-levplan-t 'dls_cavern_level :chance 3)))
	 
	 (list
	    :name "The Caves of the Goblins"
	    :lmap-table
	    (make-readtable lmap-dungeon-normal

	       ("%"
		  (
		     (lmap_make_random_site 
			random_base_specials)))

	       ("B"
		  (  (lmap_make_monster GREAT_WYRM))))
	    
	    :level-type-list '(dls_cavern_level))))

   "" )

(defconst sewers_loader
   (make-dun-loader
      'E_SEWERS
      :name "The Sewers"
      :max-levels 18
      :difficulty '(3 :depth-scale 0.16)
      :lmap-table lmap-dungeon-normal
      :subenvs
      (list
	 (list
	    :level-type-list 
	    (list
	       (make-levplan-t 'dls_room_level)
	       (make-levplan-t 'dls_sewer_level :chance 3)))
	 
	 (list
	    :name "The Sunken Cavern of the Great Wyrm"
	    :level-type-list '(dls_sewer_level))))
   "" )

(defconst castle_loader
   (make-dun-loader
      'E_CASTLE
      :name "The Archmage's Castle"
      :max-levels 16
      :level-type-list '(dls_room_level)
      :lmap-table lmap-dungeon-normal
      :base-population 23
      :difficulty '(4 :depth-scale 0.25))
   "" )

(defconst palace_loader
   (make-dun-loader
      'E_PALACE
      :name "The Palace Dungeons"
      :max-levels 14
      :level-type-list '(dls_room_level)
      :lmap-table lmap-dungeon-normal
      :difficulty '(4 :depth-scale 0.3 :phase-scale 0.3))

   "" )

;;Astral has a boss on every level.  
(defconst astral_loader
   (make-dun-loader
      'E_ASTRAL
      :max-levels 5
      :level-type-list '(dls_maze_level)
      :base-population 23
      :difficulty 8
      :subenvs
      (list
	 (list
	    :name "The Plane of Earth"
	    :lmap-table
	    (make-readtable lmap-dungeon-normal
	       ("<" ())

	       ("%"
		  (
		     (lmap_make_random_site 
			(
			   (30 
			      (
				 (lmap_set_locf L_RUBBLE)
				 (lmap_set_locchar RUBBLE)))
			   (70 
			      (
				 (lmap_make_random_site 
				    random_base_specials)))))))

	       ("#" 
		  ((lmap_set_locchar WALL)
		     (lmap_set_aux 500)))

	       ("B"
		  ((lmap_make_monster LORD_EARTH)))))
	 
	 
	 (list
	    :name "The Plane of Fire"
	    :lmap-table
	    (make-readtable lmap-dungeon-normal
	       ("<" ())
	       ("%"
		  (
		     (lmap_make_random_site 
			(
			   (30 
			      (
				 (lmap_set_locf L_FIRE)
				 (lmap_set_locchar FIRE)))
			   (70 
			      (
				 (lmap_make_random_site 
				    random_base_specials)))))))

	       ("#" 
		  (
		     (lmap_set_locf L_FIRE)
		     (lmap_set_locchar FIRE)))
	       ("B"
		  ((lmap_make_monster LORD_FIRE)))))
	 
	 (list
	    :name "The Plane of Water"
	    :lmap-table
	    (make-readtable lmap-dungeon-normal
	       ("<" ())
	       ("%"
		  (
		     (lmap_make_random_site 
			(
			   (30 
			      (
				 (lmap_set_locf L_WATER)
				 (lmap_set_locchar WATER)))
			   (70 
			      (
				 (lmap_make_random_site 
				    random_base_specials)))))))

	       ("#" 
		  (
		     (lmap_set_locf L_WATER)
		     (lmap_set_locchar WATER)))
	       ("B"
		  ((lmap_make_monster LORD_WATER)))))
	 
	 (list
	    :name "The Plane of Air"
	    :lmap-table
	    (make-readtable lmap-dungeon-normal
	       ("<" ())
	       ("%"
		  (
		     (lmap_make_random_site 
			(
			   (30 
			      (
				 (lmap_set_locf L_WHIRLWIND)
				 (lmap_set_locchar WHIRLWIND)))
			   (70 
			      (
				 (lmap_make_random_site 
				    random_base_specials)))))))

	       ("#" 
		  (
		     (lmap_set_locf L_WHIRLWIND)
		     (lmap_set_locchar WHIRLWIND)))
	       ("B"
		  ((lmap_make_monster LORD_AIR)))))
	 
	 (list
	    :name "The High Astral Plane"
	    :lmap-table
	    (make-readtable lmap-dungeon-normal
	       ("<" ())	       
	       (">"
		  (
		     (lmap_set_locf L_ENTER_CIRCLE)
		     (lmap_set_locchar STAIRS_DOWN)))

	       ("%"
		  (
		     (lmap_make_random_site 
			(
			   (30 
			      (
				 (lmap_set_locf L_ABYSS)
				 (lmap_set_locchar ABYSS)))
			   (70 
			      (
				 (lmap_make_random_site 
				    random_base_specials)))))))

	       
	       ("#" 
		  (
		     (lmap_set_locf L_ABYSS)
		     (lmap_set_locchar ABYSS)))
	       ("B"
		  ((lmap_make_monster ELEM_MASTER)))))

	 ))

   "" )


(defconst volcano_loader
   (make-dun-loader
      'E_VOLCANO
      :name "Hellwell Volcano"
      :max-levels 20
      :level-type-list '(dls_cavern_level dls_room_level dls_maze_level)
      :lmap-table lmap-dungeon-normal
      :base-population 33
      :difficulty '(5 :depth-scale 0.25))

   "" )

;;;;
;; * Other loaders *

;;These don't have special loader-maker functions because there's
;;only 1 environment of each.

(defconst tacmap_loader
   (make-env-loader-t
      'E_TACTICAL_MAP
      ;;"The Tactical Map"
      (make-tacmap-loader-t :subenvs '())
      :set-randomizer nil
      :base-difficulty 7
      ;;No effect yet.
      :post-process post_process_nitegloom)
   "" )

;;
(defconst country_loader
   (make-env-loader-t
      'E_COUNTRYSIDE
      (make-coun-loader-t
	 :cmap country_map)
      :cache-type 'cache_not_a_level
      :base-difficulty 7)
   "" )


;;;;;;;;;;;;;;;;;;;;;;;
;;Symbols of all the loaders.

(defconst omega_env_loaders 
   '(
        village_env_loader
        city_loader 
        hovel_loader 
        mansion_loader 
        house_loader 
        arena_loader 
        abyss_loader 
        dlair_loader 
        starpeak_loader 
        circle_loader 
        court_loader 
        magic_isle_loader 
        temple_loader 
        caves_loader
        sewers_loader
        castle_loader
        palace_loader
        astral_loader
        volcano_loader
        tacmap_loader
        country_loader)
   "" )


;;;;;;;;;;;;;;;;;;;;;;;;
;; *** Random tables ***

(defconst hedge_or_trifid 
   (make-random-table
       (90
	  (
	     ( lmap_set_locchar  HEDGE)
	     ( lmap_set_locf  L_HEDGE)))
       (10
	  (
	     ( lmap_set_locchar  HEDGE  ) 
	     ( lmap_set_locf  L_TRIFID  ))))
   "" )


(defconst random_temple_site_action
   (make-random-table
       ;;Half the time, do nothing.
       (6)

       (1
	  ((lmap_make_monster MEND_PRIEST)))
       (1
	  (
	     (lmap_set_locf L_MAGIC_POOL)
	     (lmap_set_locchar WATER)))
       (1
	  ((lmap_make_monster INNER_DEMON)))       
       (1
	  ((lmap_make_monster ANGEL)))       
       (1
	  ((lmap_make_monster HIGH_ANGEL)))       
       (1
	  ((lmap_make_monster ARCHANGEL))))
   
   "" )



(defconst random_maze_site_action
   (make-random-table
       (2 
	  (
	     (lmap_make_random_site random_site_safe_trap)
	     (lmap_set_locchar FLOOR)))
       
       (2
	  (
	     (lmap_make_logical_monster mon_random)
	     (lmap_set_locchar FLOOR)))
       (2
	  (
	     (lmap_make_treasure 5)
	     (lmap_set_locchar FLOOR)))
       (1
	  (
	     (lmap_set_locchar FLOOR))))
   
   "" )


(defconst random_minor_undead
   (make-random-table
       (1 ((lmap_make_monster GHOST)))
       (1 ((lmap_make_monster HAUNT))))

   "" )


(defconst random_major_undead
   (make-random-table
       (1 ((lmap_make_monster LICHE)))
       (1 ((lmap_make_monster VAMP_LORD))))

   "" )


(defconst maybe_house_npc
   (make-random-table
       (1 ((lmap_make_logical_monster mon_house_npc)))
       (1))

   "" )


(defconst maybe_mansion_npc
   (make-random-table
       (1 ((lmap_make_logical_monster mon_mansion_npc)))
       (1))

   "" )

(defconst random_site_safe_trap
   (make-random-table
       (1 ((lmap_set_locf L_TRAP_DART    )))
       (1 ((lmap_set_locf L_TRAP_PIT     )))
       (1 ((lmap_set_locf L_TRAP_DOOR    )))
       (1 ((lmap_set_locf L_TRAP_SNARE   )))
       (1 ((lmap_set_locf L_TRAP_BLADE   )))
       (1 ((lmap_set_locf L_TRAP_FIRE    )))
       (1 ((lmap_set_locf L_TRAP_TELEPORT)))
       )

   "" )

;;Also use this in install_traps
(defconst random_site_trap
   (make-random-table
       (1 ((lmap_set_locf L_TRAP_DART        )))
       (1 ((lmap_set_locf L_TRAP_PIT         )))
       (1 ((lmap_set_locf L_TRAP_DOOR        )))
       (1 ((lmap_set_locf L_TRAP_SNARE       )))
       (1 ((lmap_set_locf L_TRAP_BLADE       )))
       (1 ((lmap_set_locf L_TRAP_FIRE        )))
       (1 ((lmap_set_locf L_TRAP_TELEPORT    )))
       (1 ((lmap_set_locf L_TRAP_DISINTEGRATE)))
       (1 ((lmap_set_locf L_TRAP_SLEEP_GAS   )))
       (1 ((lmap_set_locf L_TRAP_ACID        )))
       (1 ((lmap_set_locf L_TRAP_MANADRAIN   )))
       (1 ((lmap_set_locf L_TRAP_ABYSS       )))
       (1 ((lmap_set_locf L_TRAP_SIREN       )))

       )

   "" )

;;The dungeon environments use this, and different ones may differ.

(defconst random_base_specials 
   (make-random-table
       (10
	  (
	     (lmap_make_logical_site site_random_altar)))

       (15
	  (
	     (lmap_set_locf L_RUBBLE)
	     (lmap_set_locchar RUBBLE)))
       (5
	  (
	     (lmap_set_locf L_LAVA)
	     (lmap_set_locchar LAVA)))

       (5
	  (
	     (lmap_set_locf L_FIRE)
	     (lmap_set_locchar FIRE)))

       (23
	  (
	     (lmap_make_logical_site site_alert_statue)))


       )

   "Base random specials.  Not used directly, but form the basis for
real dungeons specials" )


(defconst random_normal_specials 
   (make-random-table
      (5
	 (
	    (lmap_set_locf L_HEDGE)
	    (lmap_set_locchar HEDGE)))
      (2
	 (
	    (lmap_set_locf L_TRIFID)
	    (lmap_set_locchar HEDGE)))
      (5
	 (
	    (lmap_set_locf L_LIFT)
	    (lmap_set_locchar LIFT)))
      (30
	 (
	    (lmap_set_locf L_WATER)
	    (lmap_set_locchar WATER)))

      (70 
	 (
	    (lmap_make_random_site 
	       random_base_specials))))
      

   "Random specials in most dungeons")


(defconst random_volcano_specials 

   (make-random-table
      (5
	 (
	    (lmap_set_locf L_LIFT)
	    (lmap_set_locchar LIFT)))
      (30
	 (
	    (lmap_set_locf L_LAVA)
	    (lmap_set_locchar LAVA)))
      (70 
	 (
	    (lmap_make_random_site 
	       random_base_specials))))

   "Random specials in the volcano")



(defconst random_lock_door 
   (make-random-table
       (4
	  ((lmap_set_aux LOCKED)))       

       (1
	  ((lmap_set_aux TRUE))))
   
   "Usually lock a door, sometimes not.  Used by houses." )


(defconst random_city_house_locf 
   (make-random-table
       (1
	  ((lmap_set_locf L_HOVEL)))
       (4
	  ((lmap_set_locf L_HOUSE)))       

       (1
	  ((lmap_set_locf L_MANSION))))
   
   "A random house, hovel, or mansion in the city." )


(defconst random_village_house_locf 
   (make-random-table
       (1
	  ((lmap_set_locf L_HOVEL)))       
       (1
	  ((lmap_set_locf L_HOUSE))))

   "A random house or hovel in the villages." )


;;;;;;;;
;;Symbols of all the randoms.

(defconst omega-random-tables 

   '( hedge_or_trifid random_temple_site_action
       random_maze_site_action random_minor_undead random_major_undead
       maybe_house_npc maybe_mansion_npc random_site_safe_trap
       random_site_trap 

       random_normal_specials random_volcano_specials

       random_lock_door random_city_house_locf
       random_village_house_locf)
   

   "" )


;;; loaders.el ends here
