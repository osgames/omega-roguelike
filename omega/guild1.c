/* omega copyright (C) by Laurence Raphael Brothers, 1987,1988,1989 */
/* guild1.c */
/* L_ functions  */ 

/* These functions implement the various guilds. */
/* They are all l_ functions since they are basically activated*/
/* at some site or other. */

#include "game_time.h"
#include "glob.h"
#include "memory.h"
#include "ticket.h"
#include "util.h"

static void legion_handler (ticket_t * ticket)
{
  int * optr;

  if (FALSE == ticket_has_next_argument(ticket)) goto legion_error;
  optr = ticket_next_argument(ticket);

  if (TRUE == ticket_has_next_argument(ticket)) goto legion_error;

  if (Player.rank[LEGION] > 0)
    {
      if (Player.rank[LEGION] == COMMANDANT && Player.rank[NOBILITY] == DUKE)
	ticket_remove(ticket);
      else
	{
	  int days, obligations;

	  obligations = *optr;
	  days = obligations & 0xf;
	  if (days > 0) --days;
	  obligations = (obligations & (~0xf)) | days;
	  *optr = obligations;
	}
    }
  else
    {
      ticket_remove(ticket);
    }

  return;

 legion_error:
  ticket_reset_arguments(ticket); /* releases storage associated with ticket */
  ticket_remove(ticket);
  return;
}

static void salary_handler (ticket_t * ticket)
{
  int * salary;
  int * number;
  bank_account * account;

  if (FALSE == ticket_has_next_argument(ticket)) goto salary_error;
  salary = ticket_next_argument(ticket);

  if (FALSE == ticket_has_next_argument(ticket)) goto salary_error;
  number = ticket_next_argument(ticket);

  if (TRUE == ticket_has_next_argument(ticket)) goto salary_error;

  account = bank_index_number(*number);
  if (account == NULL) goto salary_error;

  if (Player.rank[LEGION] > 0)
    {
      account->balance += (*salary);
    }
  else
    {
      ticket_remove(ticket);
    }
  return;

 salary_error:
  ticket_reset_arguments(ticket); /* releases storage associated with ticket */
  ticket_remove(ticket);
  return;
}

static int merc_xps [COMMANDANT] =
{
  0, 400, 1500, 4000, -1
};

static int merc_salary [COMMANDANT+1] =
{
  0, 500, 1000, 2000, 5000, 10000
};

void l_merc_guild(void)
{
  ticket_t * ticket;

  print1("Legion of Destiny, Mercenary Guild, Inc.");

  if (nighttime())
    {
      print2("The barracks are under curfew right now.");
      return;
    }

  print2("You enter Legion HQ, ");
  if (Player.rank[LEGION] == COMMANDANT)
    {
      nprint2("Your aide follows you to the staff room.");
      morewait();
      clearmsg();
    }
  else if (Player.rank[LEGION] == COLONEL)
    {
      nprint2("and report to the Commandant.");
      morewait();
    }
  else if (Player.rank[LEGION] > 0)
    {
      nprint2("and report to your commander.");
      morewait();
    }
  else if (Player.rank[LEGION] < 0)
    {
      nprint2("and are intercepted by the security officer.");
      morewait();
      clearmsg();
    }

  switch (Player.rank[LEGION])
    {
    case -1:
      print1("'You were expelled for good cause! Leave the barracks at once!'");
      break;

    case 0:
      nprint2("and see the Recruiting Centurion.");
      morewait();
      print2("Do you wish to join the Legion? [yn] ");

      if ('y' == ynq2())
        {
          clearmsg();
          if (Player.rank[ARENA] > 0)
            {
              print1("The Centurion checks your record, and gets angry:");
              print2("'The legion don't need any Arena Jocks. Git!'");
            }
          else if (Player.rank[ORDER] > 0)
            {
              print1("The Centurion checks your record, and is amused:");
              print2("'A paladin in the ranks? You must be joking.'");
            }
          else if (Player.con < 12)
            {
              print1("The Centurion looks you over, sadly.");
              print2("You are too fragile to join the legion.");
            }
          else if (Player.str < 10)
            {
              print1("The Centurion looks at you contemptuously.");
              print2("Your strength is too low to pass the physical!");
            }
          else
            {
              pob newitem;
              bank_account * account;
	      unsigned long scheduled_time;

              print1("You are tested for strength and stamina...");
              morewait();
              nprint1(" and you pass!");

              print2("Commandant ");
              nprint2(Commandant);
              nprint2(" shakes your hand.");
              morewait();

              print1("You are also issued a shortsword and leather.");
              print2("You are now a Legionaire.");
              morewait();
              clearmsg();

              /* gain shortsword */
              newitem = new_object();
              *newitem = Objects[OB_SHORT_SWORD];
              gain_item(newitem);

              /* gain leather armor */
              newitem = new_object();
              *newitem = Objects[OB_LEATHER];
              gain_item(newitem);

              cinema_scene("As a Legionaire, you are required to report to HQ once a day",
			   "Your starting salary will be 500 AU per month paid into an account the",
                           "legion has set up for you. The password is \"loyalty\". Don't lose your card!");

              /* gain bank card for salary account */
              account = bank_create_account(TRUE, 0, "loyalty");
              account->balance = 500;
	      LegionAccount = account->number;

              newitem = bank_create_card(account->number, OB_DEBIT_CARD);
	      know_object(newitem);
              gain_item(newitem);

              clearmsg();

              LegionSalary = merc_salary[LEGIONAIRE];
	      set_need_to_visit(LegionObligations,1);
	      reset_failed_before(LegionObligations);

	      /* prepare to pay salary regularly */
	      ticket = ticket_new_periodic(salary_handler, TICKS_PER_MONTH);
	      ticket_add_argument(ticket, &LegionSalary);
	      ticket_add_argument(ticket, &LegionAccount);
	      ticket_schedule(ticket, 1 + get_current_ticket_time());

	      /* prepare to keep track of player visits */
	      scheduled_time = (TICKS_PER_HOUR * (24 - time_hour())) - (TICKS_PER_MINUTE * time_minute());
	      ticket = ticket_new_periodic(legion_handler, TICKS_PER_DAY);
	      ticket_add_argument(ticket, &LegionObligations);
	      ticket_schedule(ticket, scheduled_time); /* midnight */

              Player.rank[LEGION] = LEGIONAIRE;
              Player.guildxp[LEGION] = 1;
              Player.str++;
              Player.con++;
              Player.maxstr++;
              Player.maxcon++;
            }
        }
      break;

    case COMMANDANT:
      print1("You find the disposition of your forces satisfactory.");
      break;

    case COLONEL:
      if (failed_to_visit(LegionObligations))
	{
	  bank_account * account;

	  clearmsg();
	  print1("You have failed to check in on time! The Commandant is enraged!");
	  morewait();
	  clearmsg();

	  Player.guildxp[LEGION] = 1;

	  if (!get_failed_before(LegionObligations))
	    {
	      print1("You have been demoted to Legionaire!");
	      Player.rank[LEGION] = LEGIONAIRE;
	      LegionSalary = merc_salary[LEGIONAIRE];
	      set_need_to_visit(LegionObligations,1);
	      set_failed_before(LegionObligations);
	    }
	  else
	    {
	      print1("You are expelled from the Legion!");

	      Player.rank[LEGION] = -1;

	      account = bank_index_number(LegionAccount);
	      if (account != 0)
		bank_close_account(account);
	    }
	}
      else
	{
	  if ((Player.level > Commandantlevel)
	      && find_and_remove_item(CORPSEID, DEMON_EMP))
	    {  
	      print1("You liberated the Demon Emperor's Regalia!");
	      morewait();
	      clearmsg();

	      print1("The Legion is assembled in salute to you!");
	      print2("The Regalia is held high for all to see and admire.");
	      morewait();
	      clearmsg(); 

	      print1("Commandant ");
	      nprint1(Commandant);
	      nprint1(" promotes you to replace him,");
	      print2("and announces his own overdue retirement.");
	      morewait();
	      clearmsg();

	      print1("You are the new Commandant of the Legion!");
	      print2("The Emperor's Regalia is sold for a ridiculous sum.");
	      morewait();
	      clearmsg();

	      print1("You now know the Spell of Regeneration.");
	      print2("Your training is complete. You get top salary.");
	      morewait();
	      clearmsg();

	      if (Player.rank[NOBILITY] < DUKE)
		{
		  print1("As Commandant, you must speak with the duke once every two weeks.");
		  morewait();
		  clearmsg();
		}

	      strcpy(Commandant, Player.name);
	      Commandantlevel = Player.level;
	      Commandantbehavior = fixnpc(4);
	      save_hiscore_npc(8);

	      Spells[S_REGENERATE].known = TRUE;
	      Player.rank[LEGION] = COMMANDANT;
	      Player.str += 2;
	      Player.con += 2;
	      Player.maxstr += 2;
	      Player.maxcon += 2;

	      LegionSalary = merc_salary[COMMANDANT];
	      set_need_to_visit(LegionObligations,14);
	      reset_failed_before(LegionObligations);
	    }
	  else if (Player.level <= Commandantlevel)
	    {
	      clearmsg();
	      print1("The Commandant expresses satisfaction with your progress.");
	      print2("But your service record does not yet permit promotion.");
	      set_need_to_visit(LegionObligations,7);
	    }
	  else
	    {
	      clearmsg();
	      print1("Why do you come empty handed?"); 
	      print2("You must return with the Regalia of the Demon Emperor!");
	      set_need_to_visit(LegionObligations,7);
	    }
	}
      break;

    case FORCE_LEADER:
      if (failed_to_visit(LegionObligations))
	{
	  bank_account * account;

	  clearmsg();
	  print1("You have failed to check in on time! Your CO is enraged!");
	  morewait();
	  clearmsg();

	  Player.guildxp[LEGION] = 1;

	  if (!get_failed_before(LegionObligations))
	    {
	      print1("You have been demoted to Legionaire!");
	      Player.rank[LEGION] = LEGIONAIRE;
	      LegionSalary = merc_salary[LEGIONAIRE];
	      set_need_to_visit(LegionObligations,1);
	      set_failed_before(LegionObligations);
	    }
	  else
	    {
	      print1("You are expelled from the Legion!");

	      Player.rank[LEGION] = -1;

	      account = bank_index_number(LegionAccount);
	      if (account != 0)
		bank_close_account(account);
	    }
	}
      else
	{
	  clearmsg();
	  print1("Your CO expresses satisfaction with your progress.");

	  if (Player.guildxp[LEGION] < merc_xps[Player.rank[LEGION]])
	    {
	      print2("But your service record does not yet permit promotion.");
	      set_need_to_visit(LegionObligations,3);
	    }
	  else
	    {
	      print2("You have been promoted to Legion Colonel!");
	      morewait();

	      print1("Your next promotion is contingent on the return of");
	      print2("the Regalia of the Demon Emperor.");
	      morewait();

	      print1("The Demon Emperor holds court at the base of a volcano");
	      print2("to the far south, in the heart of a swamp.");
	      morewait();
	      clearmsg();

	      print1("You have been taught the spell of heroism!");
	      print2("You are given advanced training, and a raise.");
	      morewait();
	      clearmsg();

	      print1("You are now only required to check in once per week.");
	      morewait();
	      clearmsg();

	      Spells[S_HERO].known = TRUE;
	      Player.rank[LEGION]=COLONEL;
	      Player.str++;
	      Player.con++;
	      Player.maxstr++;
	      Player.maxcon++;

	      LegionSalary = merc_salary[COLONEL];
	      set_need_to_visit(LegionObligations,7);
	    }
	}
      break;

    case CENTURION:
      if (failed_to_visit(LegionObligations))
	{
	  bank_account * account;

	  clearmsg();
	  print1("You have failed to check in on time! Your CO is enraged!");
	  morewait();
	  clearmsg();

	  Player.guildxp[LEGION] = 1;

	  if (!get_failed_before(LegionObligations))
	    {
	      print1("You have been demoted to Legionaire!");
	      Player.rank[LEGION] = LEGIONAIRE;
	      LegionSalary = merc_salary[LEGIONAIRE];
	      set_need_to_visit(LegionObligations,1);
	      set_failed_before(LegionObligations);
	    }
	  else
	    {
	      print1("You are expelled from the Legion!");

	      Player.rank[LEGION] = -1;

	      account = bank_index_number(LegionAccount);
	      if (account != 0)
		bank_close_account(account);
	    }
	}
      else
	{
	  clearmsg();
	  print1("Your CO expresses satisfaction with your progress.");

	  if (Player.guildxp[LEGION] < merc_xps[Player.rank[LEGION]])
	    {
	      print2("But your service record does not yet permit promotion.");
	      set_need_to_visit(LegionObligations,2);
	    }
	  else
	    {
	      print2("You are now a Legion Force-Leader!");
	      morewait();
	      clearmsg();

	      print1("You receive more training, and another raise.");
	      print2("You are now only required to check in once every three days.");

	      Player.rank[LEGION]=FORCE_LEADER;
	      Player.maxstr++;
	      Player.str++;

	      LegionSalary = merc_salary[FORCE_LEADER];
	      set_need_to_visit(LegionObligations,3);
	    }
	}
      break;

    case LEGIONAIRE:
      clearmsg();
      if (failed_to_visit(LegionObligations))
	{
	  bank_account * account;

	  clearmsg();
	  print1("You have failed to check in on time! Your CO is enraged!");
	  morewait();
	  clearmsg();

	  print1("You are expelled from the Legion!");

	  Player.guildxp[LEGION] = 0;
	  Player.rank[LEGION] = -1;

	  account = bank_index_number(LegionAccount);
	  if (account != 0)
	    bank_close_account(account);
	}
      else
	{
	  print1("Your CO expresses satisfaction with your progress.");

	  if (Player.guildxp[LEGION] < merc_xps[Player.rank[LEGION]])
	    {
	      print2("But your service record does not yet permit promotion.");
	      set_need_to_visit(LegionObligations,1);
	    }
	  else
	    {
	      print2("You are promoted to Legion Centurion!");
	      morewait();
	      clearmsg();

	      print1("You get advanced training, and a higher salary.");
	      print2("You are now only required to check in once every two days.");

	      Player.rank[LEGION] = CENTURION;
	      Player.maxcon++;
	      Player.con++;

	      LegionSalary = merc_salary[CENTURION];
	      set_need_to_visit(LegionObligations,2);
	    }
	}
      break;
    }
}

void l_castle(void)
{
  pob o;
  int x, y;

  if (Player.level < 3) {
    print1("You can't possibly enter the castle, you nobody!");
    print2("Come back when you are famous.");
  }
  else {
    print1("You are ushered into the castle.");

    if (Player.rank[NOBILITY]<DUKE) {
      print2("His Grace, ");
      nprint2(Duke);
      nprint2("-- Duke of Rampart! <fanfare>");
      morewait();
      clearmsg();

      if (Player.rank[LEGION] == COMMANDANT)
	{
	  if (failed_to_visit(LegionObligations))
	    {
	      if (!get_failed_before(LegionObligations))
		{
		  print1("I suspect you are plotting against me, Commandant!");
		  print2("Guards! Bring the Commandant away at once!");
		  morewait();

		  set_failed_before(LegionObligations);
		  set_need_to_visit(LegionObligations,14);

		  p_damage(25,UNSTOPPABLE,"castle guards for lese majeste");
		  send_to_jail();
		}
	      else
		{
		  bank_account * account;

		  print1("You are a traitor, Commandant! I strip you of your office!");
		  print2("Guards! Remove this traitor from my presence at once!");
		  morewait();

		  Player.rank[LEGION] = -1;
		  Player.guildxp[LEGION] = 0;

		  account = bank_index_number(LegionAccount);
		  if (account != 0)
		    bank_close_account(account);

		  strcpy(Commandant, nameprint());
		  Commandantbehavior = 2912;
		  save_hiscore_npc(8);

		  p_damage(75,UNSTOPPABLE,"castle guards for lese majeste");
		  send_to_jail();
		}
	    }
	  else
	    {
	      set_need_to_visit(LegionObligations,14);
	    }
	}
    }

    if (Player.rank[NOBILITY]==0) {
      print1("Well, sirrah, wouldst embark on a quest? [yn] ");
      if (ynq1() == 'y') {
	print2("Splendid. Bring me the head of the Goblin King.");
	Player.rank[NOBILITY]=COMMONER;
      }
      else {
	print1("You scoundrel! Guards! Take this blackguard away!");
	morewait();
	p_damage(25,UNSTOPPABLE,"castle guards for lese majeste");
	send_to_jail();
      }
    }
    else if (Player.rank[NOBILITY]==COMMONER) {
      if (find_and_remove_item(CORPSEID,GOBLIN_KING)) {
	print1("Good job, sirrah! I promote you to the rank of esquire.");
	Player.rank[NOBILITY]=ESQUIRE;
	gain_experience(100);
	print2("Now that you have proved yourself true, another quest!");
	morewait();
	print1("Bring to me a Holy Defender!");
	print2("One is said to be in the possession of the Great Wyrm");
	morewait();
	clearmsg();
	print1("in the depths of the sewers below the city.");
      }
      else print2("Do not return until you achieve the quest, caitiff!");
    }
    else if (Player.rank[NOBILITY]==ESQUIRE) {
      if (find_and_remove_item(OB_DEFENDER,-1)) {
	print1("My thanks, squire. In return, I dub thee knight!");
	Player.rank[NOBILITY]=KNIGHT;
	gain_experience(1000);
	print2("If thou wouldst please me further...");
	morewait();
	print1("Bring me a suit of dragonscale armor.");
	print2("You might have to kill a dragon to get one....");
      }
      else print2("Greetings, squire. My sword? What, you don't have it?");
    }
    else if (Player.rank[NOBILITY]==KNIGHT) {
      if (find_and_remove_item(OB_DRAGONSCALE,-1)) {
	print1("Thanks, good sir knight.");
	print2("Here are letters patent to a peerage!");
	Player.rank[NOBILITY]=LORD;
	gain_experience(10000);
	morewait();
	print1("If you would do me a final service...");
	print2("I require the Orb of Mastery. If you would be so kind...");
	morewait();
	print1("By the way, you might find the Orb in the possession");
	print2("Of the Elemental Master on the Astral Plane");
      }
      else print2("Your quest is not yet complete, sir knight.");
    }
    else if (Player.rank[NOBILITY]==LORD) {
      if (find_item(&o,OB_ORB_MASTERY,-1)) {
	print1("My sincerest thanks, my lord.");
	print2("You have proved yourself a true paragon of chivalry");
	morewait();
	print1("I abdicate the Duchy in your favor....");
	print2("Oh, you can keep the Orb, by the way....");
	Player.rank[NOBILITY]=DUKE;
	gain_experience(10000);
	strcpy(Duke,Player.name);
	morewait();
	Dukebehavior = fixnpc(4);
	save_hiscore_npc(12);
	for (y = 52; y < 63; y++)
	    for (x = 2; x < 52; x++) {
		if (Level->site[x][y].p_locf == L_TRAP_SIREN) {
		    Level->site[x][y].p_locf = L_NO_OP;
		    lset(x, y, CHANGED);
		}
		if (x >= 12 && loc_statusp(x, y, SECRET)) {
		    lreset(x, y, SECRET);
		    lset(x, y, CHANGED);
		}
		if (x >= 20 && x <= 23 && y == 56) {
		    Level->site[x][y].locchar = FLOOR;
		    lset(x, y, CHANGED);
		}
	    }

      }
      else print2("I didn't really think you were up to the task....");
    }
  }
}

void l_arena(void)
{
  char response;
  pob newitem;

  print1("Rampart Coliseum");
  if (Player.rank[ARENA] == 0) {
    print2("Enter the games, or Register as a Gladiator? [e,r,ESCAPE] ");
    do response = (char) mcigetc();
    while ((response != 'e') && (response != 'r') && (response != ESCAPE));
  }
  else {
    print2("Enter the games? [yn] ");
    response = ynq2();
    if (response == 'y') response = 'e';
    else response = ESCAPE;
  }
  if (response == 'r') {
    if (Player.rank[ARENA]>0)
      print2("You're already a gladiator....");
    else if (Player.rank[ORDER]>0)
      print2("We don't let Paladins into our Guild.");
    else if (Player.rank[LEGION]>0)
      print2("We don't train no stinkin' mercs!");
    else if (Player.str < 13)
      print2("Yer too weak to train!");
    else if (Player.agi < 12)
      print2("Too clumsy to be a gladiator!");
    else {
      print1("Ok, yer now an Arena Trainee.");
      print2("Here's a wooden sword, and a shield");
      morewait();
      clearmsg();
      newitem = new_object();
      *newitem = Objects[OB_CLUB]; /* club */
      gain_item(newitem);
      newitem = new_object();
      *newitem = Objects[OB_LRG_RND_SHIELD]; /* shield */
      gain_item(newitem);
      Player.rank[ARENA] = TRAINEE;
      Arena_Opponent = 3;
      morewait();
      clearmsg();
      print1("You've got 5000Au credit at the Gym.");
      Gymcredit+=5000;
    }
  }
  else if (response == 'e') {
    print1("OK, we're arranging a match....");
    morewait();
    /* Init some variables. */
    base_Arena_reward = 0;
    vs_Arena_champion_p = FALSE;
    change_environment_push(E_ARENA, 0);
    morewait();
    clearmsg();
    print1("Let the battle begin....");
  }
  else clearmsg();
}
