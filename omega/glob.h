/* omega copyright (c) 1987,1988,1989 by Laurence Raphael Brothers */

/* this file contains extern declarations of global variables used
   throughout the program */
/* it includes the other header files, so every program module 
(except o.c) can just include this file. */

/* glob.h */

#ifndef INCLUDED_GLOB_H
#define INCLUDED_GLOB_H

#include "defs.h"

#include "extern.h"

#ifdef DEBUG /* Added by DG, naturally. */
/* Debug log file */
extern FILE *DG_debug_log;
extern int DG_debug_flag;
#endif

extern char SaveFileName[80];
/* stash save-file away; for emergency saves, default for planned saves */

/* This string holds the path to the library files */
extern char *Omegalib;

/* This string holds the path to the hiscore and log files */
extern char *Omegavar;

/* one of each monster */
extern struct monster Monsters[NUMMONSTERS];

/* one of each spell */
extern struct spell Spells[NUMSPELLS+1];

/* one of each item */
extern struct object Objects[TOTALITEMS];

/* locations of city sites [0] - found, [1] - x, [2] - y */
extern int CitySiteList[NUMCITYSITES][3];

extern struct player Player;                 
/* the player */

extern int Lev_Length;
/* level or country y dimension */

extern int Lev_Width;
/* level or country x dimension */

extern long GameStatus;
/* Game Status bit vector */

extern int ScreenLength;
extern int ScreenWidth;
/* How large is level window */

extern struct terrain Country[COUNTRY_WIDTH][COUNTRY_LENGTH];
/* The countryside */

extern struct level *Level;
/* Pointer to current Level */

extern int Current_Dungeon;
/* What is Dungeon now (an E_ constant) */

extern int Villagenum;
/* Current Village number */ 

extern int MaxDungeonLevels;
/*Deepest level allowed in dungeon */

extern int Current_Environment;
/* Which environment are we in (an E_ constant) */

extern int Dirs[2][9];                       
/* 9 xy directions */

extern int EDirs[2][9];
/* 9 xy directions for explosions */

extern struct monster *Arena_Monster;
/* Opponent in arena */

extern int Arena_Opponent;
/* case label of opponent in l_arena()*/

extern int Arena_Victory;
/* did player win in arena? */

extern int vs_Arena_champion_p;
/* Are we fiting the arena champion? */

extern int base_Arena_reward;
/* What the base arena reward is this time.  */

extern int Imprisonment;
/* amount of time spent in jail */

extern int Precipitation;
/* Hours of rain, snow, etc */

extern int Pawndate;
/* Pawn Shop item generation date */

extern pob Pawnitems[PAWNITEMS];
/* items in pawn shop */

extern int Constriction;
/* Dragonlord Attack State */

extern int Blessing;
/* Altar Blessing State */

extern int RitualRoom;
/* last room use of ritual magic */

extern int Lawstone;
/* magic stone counter */

extern int Chaostone;
/* magic stone counter */

extern int Mindstone;
/* magic stone counter */

extern int Searchnum;                    
/* number of times to search on 's' */

extern int Verbosity;
/* verbosity level */

extern int Behavior;
/* NPC behavior, if entered */

extern char Seed;                            
/* random seed */

extern char Stringbuffer[STRING_BUFFER_SIZE][80];
/* the last printed strings */

extern long Gymcredit;
/* credit at rampart gym */

extern int Spellsleft;
/* research allowance at college */

extern int LegionSalary;
/* legion salary */

extern int LegionAccount;
/* bank account number to which legion salary is paid */

extern int LegionObligations;
/* kepps track of a legionaire's fulfillment of legion obligations */

extern int Studiesleft;
/* study allowance at Monastery */

extern int HiMagic;
/* current level for l_throne */ 

#ifdef NEW_BANK
extern bank_account *bank;
/* ptr to a list of bank accounts */
#else
extern long Balance;
/* bank account */
#endif

extern long FixedPoints;
/* points are frozen after adepthood*/

extern char Password[64];
/* autoteller password */

extern list_t * Condoitems;
/* items in condo */

extern list_t * Bagitems;
/* items in bag of holding artifact */

extern char Str1[STRING_LEN],Str2[STRING_LEN],Str3[STRING_LEN],Str4[STRING_LEN];
/* Some string space, random uses */

extern char dole_too_soon;
extern char ritual_too_soon;
extern char club_hint_too_soon;
extern char tavern_hint_too_soon;
/* flags indicating that it is too soon (since the last time) to do something */

/* high score names, levels, behavior */

extern int Shadowlordbehavior,Archmagebehavior,Primebehavior,Justiciarbehavior;
extern int Commandantbehavior,Chaoslordbehavior,Lawlordbehavior;
extern int Championbehavior,Priestbehavior[7],Hibehavior,Dukebehavior;
extern int Grandmasterbehavior;
extern char Shadowlord[80],Archmage[80],Prime[80],Commandant[80],Duke[80];
extern char Champion[80],Priest[7][80],Hiscorer[80],Hidescrip[80];
extern char Chaoslord[80],Lawlord[80],Justiciar[80],Grandmaster[80];
extern int Shadowlordlevel,Archmagelevel,Primelevel,Commandantlevel,Dukelevel;
extern int Championlevel,Priestlevel[7],Hilevel,Justiciarlevel;
extern int Grandmasterlevel;
extern long Hiscore;
extern int Chaoslordlevel,Lawlordlevel,Chaos,Law;

/* New globals which used to be statics */
extern int twiddle;
extern int saved;
extern int onewithchaos;
extern int winnings;
extern int scroll_ids[30];
extern int potion_ids[30];
extern int stick_ids[30];
extern int ring_ids[30];
extern int cloak_ids[30];
extern int boot_ids[30];

extern int deepest[E_MAX + 1];
extern int level_seed[E_MAX + 1];

/* What a wall looks like, for secret locations. */
extern Symbol wall_showchar;

/* Flags to notice various conditions to be handled later. */
int32 notice_flags;

#endif /* INCLUDED_GLOB_H */
