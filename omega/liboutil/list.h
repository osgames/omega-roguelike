/*
  list.h - doubly linked list
  liboutil : Copyright (C) 2000,2001 by Willian Sheldon Simms
  Additional material by Tom Breton (Tehom), Copyright (C) 2001
  
  This file is part of liboutils, a library of useful utility
  routines released together with the roguelike game Omega

  liboutils is licensed under the terms of the Omega license,
  which can be found in the Omega source distribution in the
  directory omega-roguelike/omega/lib/license.txt
*/

#ifndef INCLUDED_LIST_H
#define INCLUDED_LIST_H

#include <stddef.h>  /* size_t  */

/********/

struct list;
struct list_iterator;
typedef struct list list_t;
typedef struct list_iterator list_iterator_t;

/* Types for callbacks. */
typedef int  (*test_f_t) (void * candidate, void * data);
typedef void (*act_f_t)  (void * element,   void * data);
typedef void (*free_f_t) (void * element);

/********/

#define list_is_empty(l)  (0 == list_size(l))

#define stack_new(s)      list_new(s)
#define stack_size(s)     list_size(s)
#define stack_peek(s)     list_get_head(s)
#define stack_pull(s)     list_remove_head(s)
#define stack_push(s,o)   list_prepend(s,o)

#define queue_new(q)      list_new(q)
#define queue_size(q)     list_size(q)
#define queue_peek(q)     list_get_head(q)
#define queue_get(q)      list_remove_head(q)
#define queue_put(q,o)    list_append(q,o)

/********/

list_t * list_new (void);
void list_delete (list_t * list);

/********/

size_t list_size (list_t * list);

/********/

void list_append (list_t * list, void * obj);
void list_prepend (list_t * list, void * obj);
void list_append_list (list_t * list1, list_t * list2);
void list_prepend_list (list_t * list1, list_t * list2);
void list_insert_in_order (list_t * list, void * obj, int (* compare) (void *, void *));

/********/

void * list_remove_head (list_t * list);
void * list_remove_tail (list_t * list);
void * list_remove (list_t * list, int idx);
void * list_remove_member (list_t * list, void * member);

/********/

void * list_get_head (list_t * list);
void * list_get_tail (list_t * list);
void * list_get (list_t * list, int idx);

/********/

list_iterator_t * list_iterator_create (list_t * list);
void list_iterator_delete (list_iterator_t * iter);
int list_iterator_has_next (list_iterator_t * iter);
void * list_iterator_next (list_iterator_t * iter);
void list_iterator_remove (list_iterator_t * iter);

/********/

void list_apply (list_t * list, void (*func)(void *));
void list_apply_w_param (list_t * list, act_f_t act_f, void * data);
void * list_single_find (list_t * list, test_f_t test_f, void * data);
list_t * collect_sublist(list_t * list, test_f_t test_f, void * data);

void fully_free_list(list_t ** p_list, free_f_t free_f);
void list_dont_free_v(void * data);

#define LIST_DECLARE_FREER(NAME, ELEMENT_FREER)\
void NAME(list_t ** p_list)\
{ fully_free_list(p_list, ELEMENT_FREER); }

#endif /* INCLUDED_LIST_H */
