/*
  allocator.h
  liboutil : Copyright (C) 2000,2001 by William Sheldon Simms

  This file is part of liboutils, a library of useful utility
  routines released together with the roguelike game Omega

  liboutils is licensed under the terms of the Omega license,
  which can be found in the Omega source distribution in the
  directory omega-roguelike/omega/lib/license.txt
*/

/*
 * OBJECT_TYPE : the type of object for which this function should handle the
 *               memory management.
 * BLOCK_SIZE : the size of blocks of memory allocated with function MALLOC
 *              must be at least sizeof(OBJECT_TYPE).
 * LINKNAME : accesses a pointer in OBJECT_TYPE of type (OBJECT_TYPE *) that
 *            can be used to build a linked list of OBJECT_TYPE.
 * MALLOC : a memory allocation function with the same prototype as malloc()
 *          that never returns null.
 */

#ifndef INCLUDED_ALLOCATOR_H
#define INCLUDED_ALLOCATOR_H

#define make_static_allocator(OBJECT_TYPE, BLOCK_SIZE, LINKNAME, MALLOC) \
\
static int OBJECT_TYPE##_count = 0; \
static int OBJECT_TYPE##_free_count = 0; \
static int OBJECT_TYPE##_block_count = 0; \
\
static OBJECT_TYPE * OBJECT_TYPE##_utility (OBJECT_TYPE * to_free) \
{ \
  static OBJECT_TYPE * free = 0; \
  OBJECT_TYPE * optr; \
\
  if (to_free) \
    { \
      to_free->LINKNAME = (void *)free; \
      free = to_free; \
      ++OBJECT_TYPE##_free_count; \
      return 0; \
    } \
\
  if (!free) \
    { \
      int idx; \
\
      free = MALLOC(BLOCK_SIZE); \
      for (idx = 1; idx < (BLOCK_SIZE / sizeof(OBJECT_TYPE)); ++idx) \
        (free + idx - 1)->LINKNAME = (void *)(free + idx); \
\
      (free + (BLOCK_SIZE / sizeof(OBJECT_TYPE)) - 1)->LINKNAME = 0; \
      ++OBJECT_TYPE##_block_count; \
      OBJECT_TYPE##_count += (BLOCK_SIZE / sizeof(OBJECT_TYPE)); \
      OBJECT_TYPE##_free_count += (BLOCK_SIZE / sizeof(OBJECT_TYPE)); \
    } \
\
  optr = free; \
  free = (void *)free->LINKNAME; \
  optr->LINKNAME = 0; \
  --OBJECT_TYPE##_free_count; \
  return optr; \
} \
/* a prototype for the purpose of allowing a following semicolon */ \
static OBJECT_TYPE * OBJECT_TYPE##_utility (OBJECT_TYPE * to_free)

#define make_allocator(OBJECT_TYPE, BLOCK_SIZE, LINKNAME, MALLOC) \
\
static int OBJECT_TYPE##_count = 0; \
static int OBJECT_TYPE##_free_count = 0; \
static int OBJECT_TYPE##_block_count = 0; \
\
OBJECT_TYPE * OBJECT_TYPE##_utility (OBJECT_TYPE * to_free) \
{ \
  static OBJECT_TYPE * free = 0; \
  OBJECT_TYPE * optr; \
\
  if (to_free) \
    { \
      to_free->LINKNAME = (void *)free; \
      free = to_free; \
      ++OBJECT_TYPE##_free_count; \
      return 0; \
    } \
\
  if (!free) \
    { \
      int idx; \
\
      free = MALLOC(BLOCK_SIZE); \
      for (idx = 1; idx < (BLOCK_SIZE / sizeof(OBJECT_TYPE)); ++idx) \
        (free + idx - 1)->LINKNAME = (void *)(free + idx); \
\
      (free + (BLOCK_SIZE / sizeof(OBJECT_TYPE)) - 1)->LINKNAME = 0; \
      ++OBJECT_TYPE##_block_count; \
      OBJECT_TYPE##_count += (BLOCK_SIZE / sizeof(OBJECT_TYPE)); \
      OBJECT_TYPE##_free_count += (BLOCK_SIZE / sizeof(OBJECT_TYPE)); \
    } \
\
  optr = free; \
  free = (void *)free->LINKNAME; \
  optr->LINKNAME = 0; \
  --OBJECT_TYPE##_free_count; \
  return optr; \
} \
/* a prototype for the purpose of allowing a following semicolon */ \
OBJECT_TYPE * OBJECT_TYPE##_utility (OBJECT_TYPE * to_free)

#endif /* INCLUDED_ALLOCATOR_H */
