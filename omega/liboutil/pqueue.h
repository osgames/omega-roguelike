/*
  pqueue.h - priority queue
  liboutil : Copyright (C) 2000,2001,2003 by William Sheldon Simms

  This file is part of liboutils, a library of useful utility
  routines released together with the roguelike game Omega

  liboutils is licensed under the terms of the Omega license,
  which can be found in the Omega source distribution in the
  directory omega-roguelike/omega/lib/license.txt
*/

#ifndef INCLUDED_PQUEUE_H
#define INCLUDED_PQUEUE_H

#include <stdint.h>

struct priority_queue;
typedef struct priority_queue priority_queue_t;

/* typedef uint64_t priority_t; */
typedef unsigned long priority_t;

/****************/

priority_queue_t * pq_new (unsigned int max_size);
void pq_delete (priority_queue_t * pq);

/****************/

priority_t pq_get_current_priority (priority_queue_t * pq);

void * pq_get (priority_queue_t * pq);
void * pq_remove (priority_queue_t * pq);
void * pq_remove_member (priority_queue_t * pq, void * member);

/****************/

int pq_insert (priority_queue_t * pq, priority_t priority, void * obj);

#endif /* INCLUDED_PQUEUE_H */
