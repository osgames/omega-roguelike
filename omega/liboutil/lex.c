/*
  lex.c - lexical analyser
  liboutil : Copyright (C) 2003 by William Sheldon Simms

  This file is part of liboutils, a library of useful utility
  routines released together with the roguelike game Omega

  liboutils is licensed under the terms of the Omega license,
  which can be found in the Omega source distribution in the
  directory omega-roguelike/omega/lib/license.txt
*/

/*
  This is not a lexical analyzer generator, like lex or flex.
  It is also not completely general purpose. It recognizes
  tokens in C strings. By default it recognizes C-style
  integers, identifiers, character constants and string constants.
  Users can configure the lexer by specifying which identifiers
  are to be treated as keywords. Unrecognized characters are
  returned individually as tokens. Character set issues are side-
  stepped by generating an error for any character with a value
  greater than 127 except within character or string constants.
 */

#include "lex.h"
#include "map.h"
#include <string.h>

void * malloc_nofail (size_t size);

map_t * kwmap = NULL;
uintptr_t next_kwidx = TOKEN_KEYWORD_FIRST;

static int is_idstart (int c)
{
  if (c >= 'a' && c <= 'z') return 1;
  if (c >= 'A' && c <= 'Z') return 1;
  if (c == '_') return 1;
  return 0;
}

static int is_idrest (int c)
{
  if (c >= 'a' && c <= 'z') return 1;
  if (c >= 'A' && c <= 'Z') return 1;
  if (c >= '0' && c <= '9') return 1;
  if (c == '_') return 1;
  return 0;
}

static int lexint (char * buf, token_t * token, int sign)
{
  unsigned long uval = 0;

  if (*buf < '0' || *buf > '9') return 0;

  while (*buf >= '0' && *buf <= '9')
    {
      uval *= 10;
      uval += (*buf++ - '0');
    }

  if (sign > 0)
    {
      token->idx = TOKEN_UINT;
      token->val.uint = uval;
      token->rest = buf;
    }
  else
    {
      long sval;
      sval = (long)uval;
      if (sval < 0) return 0; /* error - magnitude too large for signed long */

      token->idx = TOKEN_SINT;
      token->val.sint = -sval;
      token->rest = buf;
    }

  return 1;
}

static int lexid (char * buf, token_t * token)
{
  int len;
  char * ptr;
  uintptr_t kwidx;

  len = 1;
  ptr = buf + 1;

  while (is_idrest(*ptr++))
    ++len;

  ptr = malloc_nofail(len + 1);

  memcpy(ptr, buf, len);

  ptr[len] = 0;

  token->idx = TOKEN_ID;
  token->val.text = ptr;
  token->rest = buf + len;

  if (kwmap != NULL)
    if (map_find(kwmap, (uintptr_t)ptr, &kwidx))
      token->idx = (long)kwidx;

  return 1;
}

static void lexdelimited (char * buf, token_t * token)
{
  int len;
  int delim;
  char * ptr;

  delim = *buf;

  len = 0;
  ptr = buf + 1;

  while (*ptr != delim && *ptr != '\0')
    {
      if (*ptr++ == '\\')
	if (*ptr != '\0')
	  ++ptr;

      ++len;
    }

  if (*ptr == '\0')
    return; /* error */

  token->val.text = ptr = malloc_nofail(len + 1);

  ++buf;
  while (*buf != delim && *buf != '\0')
    {
      if (*buf == '\\')
	{
	  ++buf;
	  if (*buf != '\0')
	    ++buf;
	}

      *ptr++ = *buf++;
    }

  token->idx = TOKEN_ID; /* dummy - just not TOKEN_ERROR */
  token->val.text[len] = 0;
  token->rest = buf + 1;
}

static int lexcconst (char * buf, token_t * token)
{
  lexdelimited(buf, token);

  if (token->idx != TOKEN_ERROR)
    token->idx = TOKEN_CCONST;

  return 1;
}

static int lexsconst (char * buf, token_t * token)
{
  lexdelimited(buf, token);

  if (token->idx != TOKEN_ERROR)
    token->idx = TOKEN_SCONST;

  return 1;
}

static int kwcmp (uintptr_t a, uintptr_t b)
{
  int result;
  result = strcmp((char *)a, (char *)b);
  return result;
}

long lex_set_keyword (char * kwtext)
{
  if (kwmap == NULL)
    kwmap = map_new(kwcmp);

  if (map_set(kwmap, (uintptr_t)kwtext, next_kwidx, NULL))
    return (long)next_kwidx++;

  return 0;
}

token_t lex_fresh_token (void)
{
  token_t token;

  token.buf = NULL;
  token.rest = NULL;
  token.line = 1;

  return token;
}

int lex (unsigned char * buf, token_t * token)
{
 lex_top:

  token->idx = TOKEN_END;

  if (*buf == 0) return 1;

  token->idx = TOKEN_ERROR;
  token->rest = buf;

  if (*buf >= 128) return 0;

  if (*buf == '"') return lexsconst(buf, token);

  if (*buf == '\'') return lexcconst(buf, token);

  if (is_idstart(*buf)) return lexid(buf, token);

  if (*buf == '-') return lexint(buf+1, token, -1);

  if (*buf >= '0' && *buf <= '9') return lexint(buf, token, +1);

  if (*buf == '/')
    if (*(buf+1) == '/')
      {
	while (*buf != '\n')
	  ++buf;

	++(token->line);
	token->rest = ++buf;
	goto lex_top;
      }

  if (*buf == ' ' || *buf == '\t' || *buf == '\v' || *buf == '\r' || *buf == '\n')
    {
      if (*buf == '\n')	++(token->line);

      token->rest = ++buf;
      goto lex_top;
    }

  token->idx = *buf;
  token->rest = buf + 1;

  return 1;
}

int lex_file (FILE * inf, token_t * token)
{
 lex_file_top:

  if (token->buf == NULL || token->rest == NULL || token->rest[0] == '\0')
    {
      int ch;
      int len;
      fpos_t pos;

      if (token->rest != NULL && token->rest[0] == '\0' && token->buf != NULL)
	free(token->buf);

      token->idx = TOKEN_ERROR;

      if (-1 == fgetpos(inf, &pos)) return 0;

      len = 0;
      ch = fgetc(inf);
      while (ch != '\n' && ch != EOF)
	{
	  ++len;
	  ch = fgetc(inf);
	}

      if (ch == '\n')
	++len;

      if (len == 0)
	{
	  token->idx = TOKEN_END;
	  return 1;
	}

      if (-1 == fsetpos(inf, &pos)) return 0;

      token->buf = malloc_nofail(len+1);
      token->buf[len] = 0;

      ch = 0;
      while (len--)
	token->buf[ch++] = (unsigned char)fgetc(inf);

      token->rest = token->buf;
    }

  if (lex(token->rest, token) == 0) return 0;

  if (token->idx == TOKEN_END) goto lex_file_top;

  return 1;
}

#if 0
void * malloc_nofail (size_t size)
{
  return malloc(size);
}

static void tokinfo (token_t * token)
{
  switch (token->idx)
    {
    case TOKEN_ID:
      printf("   id : %s\n", token->val.text);
      break;

    case TOKEN_SINT:
      printf(" sint : %ld\n", token->val.sint);
      break;

    case TOKEN_UINT:
      printf(" uint : %lu\n", token->val.uint);
      break;

    case TOKEN_CCONST:
      printf(" ccon : %s\n", token->val.text);
      break;

    case TOKEN_SCONST:
      printf(" scon : %s\n", token->val.text);
      break;

    default:
      if (token->idx >= TOKEN_KEYWORD_FIRST)
	printf(" keyw : %s (%d)\n", token->val.text, token->idx);
      else
	printf(" char : '%c' (%d)\n", token->idx, token->idx);
    }
}

static void dostr (char * str)
{
  token_t token;

  token = lex_fresh_token();

  lex(str, &token);
  while (token.idx != TOKEN_END && token.idx != TOKEN_ERROR)
    {
      tokinfo(&token);
      lex(token.rest, &token);
    }

  if (token.idx == TOKEN_END)
    printf("end\n");
  else
    printf("error\n");
}

static void dofile (FILE * inf)
{
  token_t token;

  token = lex_fresh_token();

  lex_file(inf, &token);
  while (token.idx != TOKEN_END && token.idx != TOKEN_ERROR)
    {
      tokinfo(&token);
      lex_file(inf, &token);
    }

  if (token.idx == TOKEN_END)
    printf("end\n");
  else
    printf("error\n");
}

int main (void)
{
  static char test_ok[] = "{foo = abstract(123,\"abc\",'xyz')}";
  static char test_nok[] = "break: foo + 'incomple";

  printf("set keywords 'abstract' (%ld) and 'break' (%ld)\n",
	 lex_set_keyword("abstract"), lex_set_keyword("break"));

  printf("\nscanning \"%s\"\n", test_ok);
  dostr(test_ok);

  printf("\nscanning \"%s\"\n", test_nok);
  dostr(test_nok);

  printf("\nset keywords 'TRUE' (%ld) and 'FALSE' (%ld)\n",
	 lex_set_keyword("TRUE"), lex_set_keyword("FALSE"));

  printf("\nscanning stdin\n");
  dofile(stdin);

  return 0;
}
#endif
