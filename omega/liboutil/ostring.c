/*
  ostring.c - character strings
  liboutil : Copyright (C) 2003 by William Sheldon Simms

  This file is part of liboutils, a library of useful utility
  routines released together with the roguelike game Omega

  liboutils is licensed under the terms of the Omega license,
  which can be found in the Omega source distribution in the
  directory omega-roguelike/omega/lib/license.txt
*/

#include <string.h>
#include <stddef.h>

#include "rb.h"
#include "map.h"
#include "allocator.h"
#include "ostring.h"

void * malloc_nofail (size_t size);

struct string_block
{
  unsigned int  free;
  unsigned char mem[1];
};

typedef struct string_block string_block_t;

struct string
{
  unsigned short   len;
  unsigned char *  buf;
  string_block_t * blk;
};

/* keeps track of string buffer blocks in a searchable way */
static rb_tree_t * block_tree = NULL;

/* maintains a set of strings */
static map_t * string_set = NULL;

/**********************/

make_static_allocator(string_t, (1024 * sizeof(string_t)), buf, malloc_nofail);

#define free_string(p) string_t_utility(p);
#define alloc_string() string_t_utility(0);

/**********************/

static int cmp (uintptr_t a, uintptr_t b)
{
  if (a == b) return 0;
  if (a < b) return -1;
  return 1;
}

static int scmp (uintptr_t a, uintptr_t b)
{
  return strcmp((void *)a, (void *)b);
}

/**********************/

static string_block_t * new_block (unsigned short len)
{
  size_t size;
  rb_element_t * el;
  string_block_t * blk;

  size = 4096;
  while (len > size)
    size *= 2;

  blk = malloc_nofail(size);
  blk->free = size - offsetof(string_block_t, mem);

  el = rb_element_new();
  rb_element_set_key(el, blk->free);
  rb_element_set_val(el, (uintptr_t)blk);
  rb_insert_element(block_tree, el);

  return blk;
}

static string_block_t * get_block (rb_element_t * el, unsigned short len)
{
  string_block_t * blk;

  if (el == rb_nil(block_tree))
    {
      blk = NULL;
    }
  else if ((uintptr_t)len == rb_element_key(el))
    {
      blk = (void *)rb_element_val(el);
    }
  else if ((uintptr_t)len < rb_element_key(el))
    {
      blk = get_block(rb_left(el), len);
      if (blk == NULL)
	blk = (void *)rb_element_val(el);
    }
  else
    {
      blk = get_block(rb_right(el), len);
      if (blk == NULL)
	blk = new_block(len);
    }

  return blk;
}

/**********************/

static string_t * string_new_len (unsigned short len)
{
  string_t * str;

  if (block_tree == NULL)
    block_tree = rb_new(cmp);

  str = alloc_string();

  str->len = len + 1;
  str->blk = get_block(rb_root(block_tree), str->len);

  if (str->blk == NULL)
    str->blk = new_block(str->len); /* needed for case: block_tree is empty */

  str->blk->free -= str->len;
  str->buf = str->blk->mem + str->blk->free;
  return str;
}

string_t * string_new (unsigned char * cstr)
{
  uintptr_t val;
  string_t * str;

  if (string_set == NULL)
    string_set = map_new(scmp);

  if (map_find(string_set, (uintptr_t)cstr, &val))
    return (void *)val;

  str = string_new_len(strlen(cstr));
  strcpy(str->buf, cstr);

  map_set(string_set, (uintptr_t)str->buf, (uintptr_t)str, NULL);
  return str;
}

void string_delete (string_t * str)
{
  size_t num;
  unsigned char * src;
  unsigned char * dst;

  src = str->blk->mem + str->blk->free;
  num = str->buf - src;

  if (num != 0)
    {
      /* move free space to beginning of block */
      dst = src + str->len;
      memmove(dst, src, num);
    }

  str->blk->free += str->len;

  str->buf = NULL; /* poison */
  str->blk = NULL; /* poison */
  free_string(str);
}

/**********************/

unsigned short string_len (string_t * str)
{
  return str->len - 1;
}

unsigned char * const string_cstr (string_t * str)
{
  return str->buf;
}
