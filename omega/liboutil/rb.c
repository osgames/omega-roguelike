/*
  rb.c - red-black balanced binary tree
  liboutil : Copyright (C) 2003 by William Sheldon Simms
  based on Cormen, Leiserson, Rivest : Algorithms, Ch. 14

  This file is part of liboutils, a library of useful utility
  routines released together with the roguelike game Omega

  liboutils is licensed under the terms of the Omega license,
  which can be found in the Omega source distribution in the
  directory omega-roguelike/omega/lib/license.txt
*/

#include <assert.h>  /* assert() */
#include <stddef.h>  /* size_t */
#include <stdio.h>

#include "allocator.h"
#include "rb.h"

void * malloc_nofail (size_t size);

/**********************/

typedef enum rb rb_t;

enum rb { red, black };

struct rb_element
{
  rb_t color;
  uintptr_t key;
  uintptr_t val;
  rb_element_t * left;
  rb_element_t * right;
  rb_element_t * parent;
};

struct rb_tree
{
  unsigned int size;
  rb_element_t * nil;
  rb_element_t * root;
  rb_compare_t cmp;
  rb_tree_t * next;
};

/**********************/

make_static_allocator(rb_element_t, (1024 * sizeof(rb_element_t)), left, malloc_nofail);
make_static_allocator(rb_tree_t, (32 * sizeof(rb_tree_t)), next, malloc_nofail);

#define free_rb_element(p) rb_element_t_utility(p);
#define alloc_rb_element() rb_element_t_utility(0);

#define free_rb_tree(p) rb_tree_t_utility(p);
#define alloc_rb_tree() rb_tree_t_utility(0);

/**********************/

rb_element_t * rb_element_new (void)
{
  rb_element_t * elem;

  elem = alloc_rb_element();

  elem->color = black;

  elem->key = 0;
  elem->val = 0;

  elem->left   = 0;
  elem->right  = 0;
  elem->parent = 0;

  return elem;
}

void rb_element_delete (rb_element_t * elem)
{
  assert(elem != NULL);

  free_rb_element(elem);
}

rb_tree_t * rb_new (rb_compare_t cmp)
{
  rb_tree_t * rb;

  assert(cmp != NULL);

  rb = alloc_rb_tree();

  rb->cmp  = cmp;
  rb->nil  = rb_element_new();
  rb->root = rb->nil;

  rb->nil->left  = rb->nil;
  rb->nil->right = rb->nil;

  rb->size   = 0;
  rb->next   = 0;

  return rb;
}

void rb_delete (rb_tree_t * rb)
{
  assert(rb != NULL);
  assert(0 == rb->size);

  while (rb->root != rb->nil)
    rb_remove_element(rb, rb->root);

  free_rb_tree(rb);
}

/**********************/

unsigned int rb_size (rb_tree_t * rb)
{
  assert(rb != NULL);
  return rb->size;
}

rb_element_t * rb_nil (rb_tree_t * rb)
{
  assert(rb != NULL);
  return rb->nil;
}

uintptr_t rb_element_key (rb_element_t * elem)
{
  assert(elem);
  return elem->key;
}

uintptr_t rb_element_val (rb_element_t * elem)
{
  assert(elem);
  return elem->val;
}

void rb_element_set_key (rb_element_t * elem, uintptr_t key)
{
  assert(elem);
  elem->key = key;
}

void rb_element_set_val (rb_element_t * elem, uintptr_t val)
{
  assert(elem);
  elem->val = val;
}

/**********************/

rb_element_t * rb_lookup (rb_tree_t * rb, uintptr_t key)
{
  rb_element_t * x;

  assert(rb != NULL);

  x = rb->root;

  while (x != rb->nil)
    {
      int cmpval;
      cmpval = rb->cmp(key, x->key);

      if (cmpval == 0)
	break;
      else if (cmpval < 0)
	x = x->left;
      else
	x = x->right;
    }

  if (x == rb->nil) return NULL;
  return x;
}

rb_element_t * rb_root (rb_tree_t * rb)
{
  return rb->root;
}

rb_element_t * rb_left (rb_element_t * x)
{
  return x->left;
}

rb_element_t * rb_right (rb_element_t * x)
{
  return x->right;
}

static rb_element_t * rb_subtree_minimum (rb_tree_t * rb, rb_element_t * x)
{
  assert(x != NULL);

  if (x == rb->nil) return x;

  while (x->left != rb->nil)
    x = x->left;

  return x;
}

static rb_element_t * rb_subtree_maximum (rb_tree_t * rb, rb_element_t * x)
{
  assert(x != NULL);

  if (x == rb->nil) return x;

  while (x->right != rb->nil)
    x = x->right;

  return x;
}

rb_element_t * rb_minimum (rb_tree_t * rb)
{
  return rb_subtree_minimum(rb, rb->root);
}

rb_element_t * rb_maximum (rb_tree_t * rb)
{
  return rb_subtree_maximum(rb, rb->root);
}

rb_element_t * rb_successor (rb_tree_t * rb, rb_element_t * x)
{
  rb_element_t * y;

  assert(x != NULL);

  if (x == rb->nil) return x;

  if (x->right != rb->nil) return rb_subtree_minimum(rb, x->right);

  for (y = x->parent; x == y->right; y = y->parent)
    x = y;

  return y;
}

rb_element_t * rb_predecessor (rb_tree_t * rb, rb_element_t * x)
{
  rb_element_t * y;

  assert(x != NULL);

  if (x == rb->nil) return x;

  if (x->left != rb->nil) return rb_subtree_maximum(rb, x->left);

  for (y = x->parent; x == y->left; y = y->parent)
    x = y;

  return y;
}

/**********************/

static void rotate_left (rb_tree_t * rb, rb_element_t * x)
{
  rb_element_t * y;

  assert(x->right != rb->nil);

  y = x->right;
  x->right = y->left;

  if (y->left != rb->nil)
    y->left->parent = x;

  y->parent = x->parent;

  if (x->parent == rb->nil)
    rb->root = y;
  else if (x->parent->left == x)
    x->parent->left = y;
  else
    x->parent->right = y;

  y->left = x;
  x->parent = y;
}

static void rotate_right (rb_tree_t * rb, rb_element_t * y)
{
  rb_element_t * x;

  assert(y->left != rb->nil);

  x = y->left;
  y->left = x->right;

  if (x->right != rb->nil)
    x->right->parent = y;

  x->parent = y->parent;

  if (y->parent == rb->nil)
    rb->root = x;
  else if (y->parent->left == y)
    y->parent->left = x;
  else
    y->parent->right = x;

  x->right = y;
  y->parent = x;
}

static void bst_insert (rb_tree_t * rb, rb_element_t * z)
{
  rb_element_t * x;
  rb_element_t * y = rb->nil;

  x = rb->root;

  while (x != rb->nil)
    {
      y = x;

      if (rb->cmp(z->key, x->key) < 0)
	x = x->left;
      else
	x = x->right;
    }

  z->parent = y;

  if (y == rb->nil)
    rb->root = z;
  else if (rb->cmp(z->key, y->key) < 0)
    y->left = z;
  else
    y->right = z;

  z->left  = rb->nil;
  z->right = rb->nil;
}

void rb_insert_element (rb_tree_t * rb, rb_element_t * x)
{
  rb_element_t * y;

  bst_insert(rb, x);

  x->color = red;

  while (x != rb->root && x->parent->color == red)
    {
      if (x->parent == x->parent->parent->left)
	{
	  y = x->parent->parent->right;

	  if (y->color == red)
	    {
	      x->parent->color = black;
	      y->color = black;
	      x->parent->parent->color = red;
	      x = x->parent->parent;
	    }
	  else
	    {
	      if (x == x->parent->right)
		{
		  x = x->parent;
		  rotate_left(rb, x);
		}

	      x->parent->color = black;
	      x->parent->parent->color = red;
	      rotate_right(rb, x->parent->parent);
	    }
	}
      else
	{
	  y = x->parent->parent->left;

	  if (y->color == red)
	    {
	      x->parent->color = black;
	      y->color = black;
	      x->parent->parent->color = red;
	      x = x->parent->parent;
	    }
	  else
	    {
	      if (x == x->parent->left)
		{
		  x = x->parent;
		  rotate_right(rb, x);
		}

	      x->parent->color = black;
	      x->parent->parent->color = red;
	      rotate_left(rb, x->parent->parent);
	    }
	}
    }

  rb->root->color = black;

  ++(rb->size);
}

static void rb_remove_fixup (rb_tree_t * rb, rb_element_t * x)
{
  rb_element_t * w;

  while (x != rb->root && x->color == black)
    {
      if (x == x->parent->left)
	{
	  w = x->parent->right;

	  if (w->color == red)
	    {
	      w->color = black;
	      x->parent->color = red;
	      rotate_left(rb, x->parent);
	      w = x->parent->right;
	    }

	  if (w->left->color == black && w->right->color == black)
	    {
	      w->color = red;
	      x = x->parent;
	    }
	  else
	    {
	      if (w->right->color == black)
		{
		  w->left->color = black;
		  w->color = red;
		  rotate_right(rb, w);
		  w = x->parent->right;
		}

	      w->color = x->parent->color;
	      x->parent->color = black;
	      w->right->color = black;
	      rotate_left(rb, x->parent);
	      x = rb->root;
	    }
	}
      else
	{
	  w = x->parent->left;

	  if (w->color == red)
	    {
	      w->color = black;
	      x->parent->color = red;
	      rotate_right(rb, x->parent);
	      w = x->parent->left;
	    }

	  if (w->right->color == black && w->left->color == black)
	    {
	      w->color = red;
	      x = x->parent;
	    }
	  else
	    {
	      if (w->left->color == black)
		{
		  w->right->color = black;
		  w->color = red;
		  rotate_left(rb, w);
		  w = x->parent->left;
		}

	      w->color = x->parent->color;
	      x->parent->color = black;
	      w->left->color = black;
	      rotate_right(rb, x->parent);
	      x = rb->root;
	    }
	}
    }

  x->color = black;
}

void rb_remove_element (rb_tree_t * rb, rb_element_t * z)
{
  rb_element_t * x;
  rb_element_t * y;

  if (z->left == rb->nil || z->right == rb->nil)
    y = z;
  else
    y = rb_successor(rb, z);

  if (y->left != rb->nil)
    x = y->left;
  else
    x = y->right;

  x->parent = y->parent;

  if (y->parent == rb->nil)
    rb->root = x;
  else if (y == y->parent->left)
    y->parent->left = x;
  else
    y->parent->right = x;

  if (y != z)
    {
      z->key = y->key;
      z->val = y->val;
    }
#if 0
  printf("0:\n"); rb_dump(rb);
#endif
  if (y->color == black)
    rb_remove_fixup(rb, x);

  rb_element_delete(y);

  --(rb->size);
}

/**********************/

/* returns the violated property. 0 == no error */

static int rb_verify_recursive (rb_tree_t * rb, rb_element_t * elem, int * bhp)
{
  int lbh, rbh, result;

  assert(bhp);
  assert(elem);
  assert(elem->left);
  assert(elem->right);

  if (elem == rb->nil)
    {
      *bhp = 1;
      return 0;
    }

  if (elem->color == red)
    if (elem->left->color != black || elem->right->color != black)
      return 3;

  result = rb_verify_recursive(rb, elem->left, &lbh);
  if (result) return result;

  result = rb_verify_recursive(rb, elem->right, &rbh);
  if (result) return result;

  if (lbh != rbh) return 4;

  if (elem->color == black)
    *bhp = lbh + 1;
  else
    *bhp = lbh;

  return 0;
}

int rb_verify (rb_tree_t * rb)
{
  int bh;

  assert(rb);
  return rb_verify_recursive(rb, rb->root, &bh);
}

#if 0

#define ITERATIONS 1000000

#include <time.h>
#include <stdlib.h>

void * malloc_nofail (size_t size)
{
  return malloc(size);
}

int test_cmp (uintptr_t a, uintptr_t b)
{
  return a-b;
}

int rb_rand (void)
{
  return (rand() / 0x100) % 0x10000;
}

void rb_dump_recursive (rb_tree_t * rb, rb_element_t * elem, int level)
{
  int idx;

  for (idx = 0; idx < level; ++idx)
    printf("  ");

  if (elem == rb->nil)
    {
      printf("nil:black\n");
      return;
    }

  printf("%x:%s\n", elem->key, elem->color == red ? "red" : "black");

  rb_dump_recursive(rb, elem->left, level+1);
  rb_dump_recursive(rb, elem->right, level+1);
}

void rb_dump (rb_tree_t * rb)
{
  putchar('\n');
  rb_dump_recursive(rb, rb->root, 0);
}

#define nelem(k) elem = rb_element_new(); elem->left = elem->right = rb->nil; elem->key = (k)

void c_test (rb_tree_t * rb)
{
  rb_element_t * elem;
  rb_element_t * xtra;

  nelem(2);
  rb->root = elem;
  elem->color = red;
  elem->parent = rb->nil;

  nelem(1);
  rb->root->left = elem;
  elem->color = black;
  elem->parent = rb->root;

  nelem(4);
  rb->root->right = xtra = elem;
  elem->color = black;
  elem->parent = rb->root;

  nelem(3);
  xtra->left = elem;
  elem->color = red;
  elem->parent = xtra;
  xtra = elem;

  rb_dump(rb);
  printf("\nrb_verify() : %d\n", rb_verify(rb));

  branches = 0;
  xtra = rb_lookup(rb, 1);
  rb_remove_element(rb, xtra);
  rb_dump(rb);
  printf("\nrb_verify() : %d\n", rb_verify(rb));
  printf("branches = %2.2x\n", branches);
}

int main (void)
{
  int ins, del, tins, tdel;
  int which, idx, err, rn;
  rb_tree_t * rb;
  rb_element_t * elem;

  ins = del = tins = tdel = 0;
  srand((unsigned int)time(0));

  rb = rb_new(test_cmp);

#if 0
  c_test(rb);
  exit(0);
#endif

  for (idx = 0; idx < ITERATIONS; ++idx)
    {
      if ((idx & 0xFFF) == 0)
	{
	  printf("iteration %d  inserts %d  deletes %d  size %u\r",
		 idx, ins, del, rb->size);
	  fflush(stdout);
	  tins += ins;
	  tdel += del;
	  ins = del = 0;
	}

      rn = rb_rand();
      elem = rb_lookup(rb, rn);

      if (elem == rb->nil)
	{
	  elem = rb_element_new();
	  elem->key = rb_rand();
	  rb_insert_element(rb, elem);
	  which = 0;
	  ++ins;
	}
      else
	{
	  rb_remove_element(rb, elem);
	  which = 1;
	  ++del;
	}

      err = rb_verify(rb);
      if (err != 0)
	{
	  printf("iteration %d\n", idx);
	  printf("rb_verify() failed with property %d\n", err);
	  if (which)
	    printf("after rb_remove_element()\n");
	  else
	    printf("after rb_insert_element()\n");
	  printf("rb->size = %u  branches = %2.2x\n", rb->size, branches);
	  break;
	}
    }


  if (idx == ITERATIONS)
    {
      printf("iteration %d  total inserts %d  total deletes %d  size %u\n",
	     idx, tins, tdel, rb->size);
      printf("%d iterations completed successfully\n", idx);
    }

  return 0;
}

#endif
