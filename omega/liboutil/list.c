/*
  list.c - doubly linked list
  liboutil : Copyright (C) 2000,2001 by William Sheldon Simms
  Additional material by Tom Breton (Tehom), Copyright (C) 2001

  This file is part of liboutils, a library of useful utility
  routines released together with the roguelike game Omega

  liboutils is licensed under the terms of the Omega license,
  which can be found in the Omega source distribution in the
  directory omega-roguelike/omega/lib/license.txt
*/

#include <assert.h>  /* assert() */
#include <stddef.h>  /* size_t */

#include "allocator.h"
#include "list.h"

void * malloc_nofail (size_t size);

/**********************/

typedef struct list_element list_element_t;

struct list_element
{
  void * contents;
  list_element_t * prev;
  list_element_t * next;
};

struct list_iterator
{
  list_t * list;
  list_element_t * elem;
  list_iterator_t * next;
};

struct list
{
  size_t size;
  list_element_t * iter;
  list_element_t * head;
  list_element_t * tail;
  list_t * next;
};

/**********************/

make_static_allocator(list_element_t, (1024 * sizeof(list_element_t)), next, malloc_nofail);
make_static_allocator(list_iterator_t, (128 * sizeof(list_iterator_t)), next, malloc_nofail);
make_static_allocator(list_t, (128 * sizeof(list_t)), next, malloc_nofail);

#define s_free_list_element(p) list_element_t_utility(p);
#define s_alloc_list_element() list_element_t_utility(0);

#define s_free_iterator(p) list_iterator_t_utility(p);
#define s_alloc_iterator() list_iterator_t_utility(0);

#define s_alloc_list() list_t_utility(0);
#define s_free_list(p) list_t_utility(p);

/**********************/

list_t * list_new (void)
{
  list_t * list;

  list = s_alloc_list();

  list->iter = 0;
  list->head = 0;
  list->tail = 0;
  list->next = 0;
  list->size = 0;

  return list;
}

void list_delete (list_t * list)
{
  if (!list) return;
  assert(0 == list->size);
  s_free_list(list);
}

/**********************/

size_t list_size (list_t * list)
{
  if (!list) return 0;
  return list->size;
}

/**********************/

void list_prepend (list_t * list, void * obj)
{
  list_element_t * elem;

  assert(obj);
  assert(list);

  assert(0 == list_remove_member(list, obj));

  elem = s_alloc_list_element();
  elem->contents = obj;
  elem->next = list->head;
  elem->prev = 0;

  if (list->size)
    list->head->prev = elem;
  else
    list->tail = elem;

  list->head = elem;
  ++(list->size);
}

void list_append (list_t * list, void * obj)
{
  list_element_t * elem;

  assert(obj);
  assert(list);

  assert(0 == list_remove_member(list, obj));

  elem = s_alloc_list_element();
  elem->contents = obj;
  elem->next = 0;
  elem->prev = list->tail;

  if (list->size)
    list->tail->next = elem;
  else
    list->head = elem;

  list->tail = elem;
  ++(list->size);
}

void list_insert_in_order (list_t * list,
                           void * obj,
                           int (* compare) (void *, void *))
{
  list_element_t * idx;
  list_element_t * elem;

  assert(obj);
  assert(list);
  assert(compare);

  elem = s_alloc_list_element();

  elem->contents = obj;
  elem->next = 0;
  elem->prev = 0;

  /* empty list is trivially ordered */
  if (0 == list->size)
    {
      list->head = elem;
      list->tail = elem;
      list->size = 1;
      return;
    }

  /* smaller than the first element, make it the first */
  if (compare(obj, list->head->contents) < 0)
    {
      elem->next = list->head;
      list->head->prev = elem;
      list->head = elem;
      ++(list->size);
      return;
    }

  /* search for the right place */
  idx = list->head;
  while (idx && compare(obj, idx->contents) >= 0)
    idx = idx->next;

  /* ran off the list, make it last */
  if (0 == idx)
    {
      elem->prev = list->tail;
      list->tail->next = elem;
      list->tail = elem;
      ++(list->size);
      return;
    }

  /* insert in the middle (before the stop element) */
  assert(idx->prev);

  elem->next = idx;
  elem->prev = idx->prev;
  idx->prev->next = elem;
  idx->prev = elem;

  ++(list->size);

  return;
}

void list_prepend_list (list_t * list1, list_t * list2)
{
  assert(list1);
  assert(list2);

  if (0 == list2->size)
    return;

  if (0 == list1->size)
    {
      *list1 = *list2;
    }
  else
    {
      list1->size += list2->size;
      list2->tail->next = list1->head;
      list1->head->prev = list2->tail;
      list1->head = list2->head;
    }

  list2->head = 0;
  list2->tail = 0;
  list2->size = 0;
  return;
}

void list_append_list (list_t * list1, list_t * list2)
{
  assert(list1);
  assert(list2);

  if (0 == list2->size)
    return;

  if (0 == list1->size)
    {
      *list1 = *list2;
    }
  else
    {
      list1->size += list2->size;
      list1->tail->next = list2->head;
      list2->head->prev = list1->tail;
      list1->tail = list2->tail;
    }

  list2->head = 0;
  list2->tail = 0;
  list2->size = 0;
  return;
}

/**********************/

void * list_remove_head (list_t * list)
{
  void * obj;
  list_element_t * elem;

  assert(list);

  if (0 == list->size) return 0;

  elem = list->head;

  list->head = elem->next;
  if (list->head) list->head->prev = 0;

  obj = elem->contents;
  s_free_list_element(elem);

  if (0 == --(list->size)) list->tail = 0;

  return obj;
}

void * list_remove_tail (list_t * list)
{
  void * obj;
  list_element_t * elem;

  assert(list);

  if (0 == list->size) return 0;

  elem = list->tail;

  list->tail = elem->prev;
  if (list->tail) list->tail->prev = 0;

  obj = elem->contents;
  s_free_list_element(elem);

  if (0 == --(list->size)) list->head = 0;

  return obj;
}

void * list_remove (list_t * list, int idx)
{
  void * obj;
  list_element_t * elem;

  assert(list);

  for (elem = list->head; elem && idx; --idx)
    elem = elem->next;

  if (idx) return 0;

  if (elem->prev) elem->prev->next = elem->next;
  if (elem->next) elem->next->prev = elem->prev;

  if (elem == list->head) list->head = elem->next;
  if (elem == list->tail) list->tail = elem->prev;

  obj = elem->contents;
  s_free_list_element(elem);
  --(list->size);

  return obj;
}

void * list_remove_member (list_t * list, void * obj)
{
  list_element_t * elem;

  for (elem = list->head; elem; elem = elem->next)
    {
      if (elem->contents == obj)
        {
          if (elem->prev) elem->prev->next = elem->next;
          if (elem->next) elem->next->prev = elem->prev;

          if (elem == list->head) list->head = elem->next;
          if (elem == list->tail) list->tail = elem->prev;

          s_free_list_element(elem);
          --(list->size);

          return obj;
        }
    }

  return 0;
}

/**********************/

void * list_get_head (list_t * list)
{
  assert(list);

  if (0 == list->size) return 0;
  return list->head->contents;
}

void * list_get_tail (list_t * list)
{
  assert(list);

  if (0 == list->size) return 0;
  return list->tail->contents;
}

void * list_get (list_t * list, int idx)
{
  list_element_t * elem;

  assert(list);

  for (elem = list->head; elem && idx; --idx)
    elem = elem->next;

  if (idx) return 0;
  return elem->contents;
}

/**********************/

void list_iterator_delete (list_iterator_t * iter)
{
  if (iter)
    s_free_iterator(iter);
}

list_iterator_t * list_iterator_create (list_t * list)
{
  list_iterator_t * iter;

  if (!list) return 0;
  iter = s_alloc_iterator();
  iter->list = list;
  iter->elem = list->head;
  return iter;
}

int list_iterator_has_next (list_iterator_t * iter)
{
  if (!iter) return 0;
  assert(iter->list);
  return iter->elem != 0;
}

void * list_iterator_next (list_iterator_t * iter)
{
  void * obj;

  assert(iter);
  assert(iter->list);
  obj = iter->elem->contents;
  iter->elem = iter->elem->next;
  return obj;
}

void list_iterator_remove (list_iterator_t * iter)
{
  list_element_t * elem;

  assert(iter);
  assert(iter->list);

  if (iter->elem)
    elem = iter->elem->prev;
  else
    elem = iter->list->tail;

  assert(elem);

  if (elem->prev) elem->prev->next = elem->next;
  if (elem->next) elem->next->prev = elem->prev;

  if (iter->list->head == elem) iter->list->head = iter->list->head->next;
  if (iter->list->tail == elem) iter->list->tail = iter->list->tail->prev;

  s_free_list_element(elem);
  --(iter->list->size);
}

/**********************/

void list_apply (list_t * list, void (*func)(void *))
{
  list_element_t * elem;
  list_element_t * next;

  if (!list) return;

  /* allow the element to which func is applied to be removed by func */
  for (elem = list->head; elem; elem = next)
    {
      assert(elem->contents);
      next = elem->next;
      func(elem->contents);
    }
}

/* New functions by Tom Breton (Tehom) */

/* Like list_apply, but allow a general param. */
void list_apply_w_param (list_t * list, act_f_t act_f, void * data)
{
  list_element_t * elem;
  list_element_t * next;

  if (!list) return;

  for (elem = list->head; elem; elem = next)
    {
      assert(elem->contents);
      next = elem->next;
      act_f(elem->contents, data);
    }
}

/* Return contents of the first element matching TEST_F, or NULL. */
void * list_single_find(list_t * list, test_f_t test_f, void * data)
{
  list_element_t * elem;
  list_element_t * next;

  if (!list) return NULL;

  for (elem = list->head; elem; elem = next)
    {
      assert(elem->contents);
      next = elem->next;
      if(test_f(elem->contents, data))
	{ return elem->contents; }
    }
  return NULL;
}     


/** Helper type and function to create a sublist. **/
typedef struct collect_sublist_data_t
{
  list_t * retval;
  test_f_t test_f;
  void   * data;
} collect_sublist_data_t;

/* Collect subs */
static void collect_sublist_helper(void * v_candidate, void * v_slld)
{
  collect_sublist_data_t * slld  = v_slld;
  /* If it passes the test, collect it. */
  if(slld->test_f(v_candidate, slld->data))  
    { list_append(slld->retval, v_candidate); }  
}

/* Allocates a sublist of those items passing TEST_F. */
list_t * collect_sublist(list_t * list, test_f_t test_f, void * data)
{
  list_t * retval = list_new();
  collect_sublist_data_t slld = { retval, test_f, data, };
  list_apply_w_param(list, collect_sublist_helper, &slld );
  return retval;
}     

/* Delete a list and everything in it.
   NB, this function only takes responsibility for NULLing the
   immediate pointer p_list points to.  Other pointers, if any, are
   the caller's responsibility.
*/
void fully_free_list(list_t ** p_list, free_f_t free_f)
{
  list_t * list = *p_list;
  if (list)
    {
      while (list_size(list))
	{ free_f(list_remove_head(list)); }
      list_delete(list);
      *p_list = 0;
    }
}

/* No-op element freer, for lists that don't own their elements. */
void list_dont_free_v(void * data) {};
