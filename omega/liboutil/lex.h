/*
  lex.h - lexical analyser
  liboutil : Copyright (C) 2003 by William Sheldon Simms

  This file is part of liboutils, a library of useful utility
  routines released together with the roguelike game Omega

  liboutils is licensed under the terms of the Omega license,
  which can be found in the Omega source distribution in the
  directory omega-roguelike/omega/lib/license.txt
*/

#ifndef INCLUDED_LEX_H
#define INCLUDED_LEX_H

#include <stdio.h>

typedef struct token token_t;
typedef enum tokenidx tokenidx_t;
typedef union tokenval tokenval_t;

enum tokenidx
{
  TOKEN_ERROR = -1,
  TOKEN_END = 0,
  TOKEN_ID = 128,
  TOKEN_SINT,
  TOKEN_UINT,
  TOKEN_CCONST,
  TOKEN_SCONST,
  TOKEN_KEYWORD_FIRST = 256
};

union tokenval
{
  signed long     sint;
  unsigned long   uint;
  unsigned char * text;
};

struct token
{
  tokenidx_t      idx;  /* what kind of token */
  tokenval_t      val;  /* value of token (if not error) */
  unsigned char * rest; /* where scanning left off (NULL for end-of-string) */

  /* the rest of the members are for internal use */
  unsigned int    line;
  unsigned char * buf;
};

/*
 * user interface
 */

/* tell lexer to treat a string (that must have the form of an identifier
   as a keyword. returns the token index of the new keyword. */

long lex_set_keyword (char * kwtext);

/* return a properly initialized token. should be called before lex()
   or lex_file() */

token_t lex_fresh_token (void);

/* return tokens from a string. after each token is returned, the member token->rest
   contains a pointer to the character after the end of the token in the original
   string. this pointer should be passed as buf the next time lex() is called. end
   of string is signified by returning TOKEN_END in token->idx. */

int lex (unsigned char * buf, token_t * token);

/* return tokens from a file. end of file is signified by returning TOKEN_END in
   token->idx */

int lex_file (FILE * inf, token_t * token);

#endif /* INCLUDED_LEX_H */
