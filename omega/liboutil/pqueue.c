/*
  pqueue.c - priority queue
  liboutil : Copyright (C) 2000,2001,2003 by William Sheldon Simms

  This file is part of liboutils, a library of useful utility
  routines released together with the roguelike game Omega

  liboutils is licensed under the terms of the Omega license,
  which can be found in the Omega source distribution in the
  directory omega-roguelike/omega/lib/license.txt
*/

#include <assert.h>  /* assert () */
#include <stddef.h>  /* size_t */
#include <stdlib.h>  /* free() */
#include <stdbool.h> /* true, false */

#include "allocator.h"
#include "pqueue.h"

#if 1
void * malloc_nofail (size_t size);
#else
static void * malloc_nofail (size_t size)
{
  void * ptr;
  ptr = malloc(size);
  if (0 == ptr) exit(-1);
  return ptr;
}
#endif

#define PQ_TEST

#ifdef PQ_TEST
#include <stdio.h>
#include <time.h>

static bool pq_is_heap (priority_queue_t * pq);
static void pq_internal (priority_queue_t * pq);
#endif

/*******************************************/

typedef struct heap_object heap_object_t;

struct heap_object
{
  priority_t priority;
  void * contents;
};

struct priority_queue
{
  unsigned int cur_size;
  unsigned int max_size;
  heap_object_t * heap;
  priority_queue_t * next;
};

/*******************************************/

make_static_allocator(priority_queue_t, (16 * sizeof(priority_queue_t)), next, malloc_nofail);

#define alloc_pq() priority_queue_t_utility(0);
#define free_pq(p) priority_queue_t_utility(p);

/*******************************************/

priority_queue_t * pq_new (unsigned int max_size)
{
  priority_queue_t * pq;

  pq = alloc_pq();

  pq->cur_size = 0;
  pq->max_size = max_size;
  pq->heap = malloc_nofail(max_size * sizeof(heap_object_t));
  pq->next = 0;

  return pq;
}

void pq_delete (priority_queue_t * pq)
{
  assert(pq);
  assert(0 == pq->cur_size);
  free(pq->heap);
  free_pq(pq);
}

static void recursive_heapify (priority_queue_t * pq, unsigned int p_idx)
{
  unsigned int l_idx;
  unsigned int r_idx;
  unsigned int smallest;

  heap_object_t temp;
  heap_object_t * heap;

  heap = pq->heap;

  l_idx = 2 * p_idx + 1;
  r_idx = l_idx + 1;

  if (l_idx >= pq->cur_size) return;

  if (heap[l_idx].priority < heap[p_idx].priority)
    smallest = l_idx;
  else
    smallest = p_idx;

  if (r_idx < pq->cur_size)
    if (heap[r_idx].priority < heap[smallest].priority)
      smallest = r_idx;

  if (smallest != p_idx)
    {
      temp = heap[p_idx];
      heap[p_idx] = heap[smallest];
      heap[smallest] = temp;
      recursive_heapify(pq, smallest);
    }
}

static void heapify (priority_queue_t * pq)
{
  assert(pq);
  recursive_heapify(pq, 0);
}

/*
 * Returns the queued object with minimum priority, without removing it from
 * the queue.
 */

void * pq_get (priority_queue_t * pq)
{
  assert(pq);
  if (0 == pq->cur_size) return 0;
  return pq->heap->contents;
}

/*
 * Returns the priority of the queued object with minimum priority
 */

priority_t pq_get_current_priority (priority_queue_t * pq)
{
  assert(pq);
  assert(pq->cur_size);
  return pq->heap->priority;
}

/*
 * Removes the queued object with minimum priority from the queue and returns it.
 */

void * pq_remove (priority_queue_t * pq)
{
  void * thing;

  assert(pq);
  if (0 == pq->cur_size) return 0;

  thing = pq->heap->contents;
  pq->cur_size = pq->cur_size - 1;

  if (pq->cur_size)
    {
      pq->heap[0] = pq->heap[pq->cur_size];
      heapify(pq);
    }

#ifdef PQ_TEST
  if (pq_is_heap(pq) == false)
    {
      printf("\npq_remove(): Priority queue %p is not a heap!\n", pq);
      pq_internal(pq);
    }
#endif

  return thing;
}

/*
 * Removes a specified queued object, if present, from the priority queue.
 */

void * pq_remove_member (priority_queue_t * pq, void * member)
{
  unsigned int idx;
  unsigned int p_idx;
  void * thing;
  heap_object_t temp;
  heap_object_t * heap;

  assert(pq);
  assert(member);

  if (0 == pq->cur_size) return 0;

  thing = 0;
  heap = pq->heap;

  for (idx = 0; idx < pq->cur_size; ++idx)
    {
      if (heap[idx].contents == member)
        {
          thing = heap[idx].contents;
	  pq->cur_size = pq->cur_size - 1;

	  if (pq->cur_size > idx)
	    {
	      heap[idx] = heap[pq->cur_size];
	      recursive_heapify(pq, idx);

	      /* let the element bubble up like in pq_insert() */
	      while (idx > 0)
		{
		  p_idx = (idx - 1) / 2;
		  if (heap[p_idx].priority <= heap[idx].priority) break;

		  temp = heap[p_idx];
		  heap[p_idx] = heap[idx];
		  heap[idx] = temp;

		  idx = p_idx;
		}
	    }

          break;
        }
    }

  /* don't let users try to remove non-existant stuff */
  assert(thing);

#ifdef PQ_TEST
  if (pq_is_heap(pq) == false)
    {
      printf("\npq_remove_member(): Priority queue %p is not a heap!\n", pq);
      pq_internal(pq);
    }
#endif

  return thing;
}

/*
 * Inserts an object in the priority queue and returns 1 unless the queue is full,
 * which case it returns 0.
 */

int pq_insert (priority_queue_t * pq, priority_t priority, void * obj)
{
  unsigned int i_idx;
  unsigned int p_idx;
  heap_object_t * heap;

  assert(pq);
  assert(obj);

  if (pq->cur_size == pq->max_size) return 0;

  heap = pq->heap;
  i_idx = pq->cur_size;
  pq->cur_size = pq->cur_size + 1;

  while (i_idx > 0)
    {
      p_idx = (i_idx - 1) / 2;
      if (heap[p_idx].priority <= priority) break;

      heap[i_idx] = heap[p_idx];
      i_idx = p_idx;
    }

  heap[i_idx].priority = priority;
  heap[i_idx].contents = obj;

#ifdef PQ_TEST
  if (pq_is_heap(pq) == false)
    {
      printf("\npq_insert(): Priority queue %p is not a heap!\n", pq);
      pq_internal(pq);
    }
#endif

  return 1; /* success */
}

#ifdef PQ_TEST
static void pq_internal (priority_queue_t * pq)
{
  int idx;

  printf("\nInfo for priority queue %p :\n", pq);
  printf("Max Size     : %u\n", pq->max_size);
  printf("Current Size : %u\n", pq->cur_size);
  printf("*** Entries ***\n");

  for (idx = 0; idx < pq->cur_size; ++idx)
    {
      printf("%lu:%p\n", pq->heap[idx].priority, pq->heap[idx].contents);
    }

  putchar('\n');
  putchar('\n');
}

/*
  The pq->heap array is a heap as long as every parent element has a priority less than
  or equal to the priorities of its child elements.
 */
static bool pq_is_heap (priority_queue_t * pq)
{
  unsigned int parent;
  unsigned int l_child;
  unsigned int r_child;
  heap_object_t * heap;

  assert(pq);
  if (0 == pq->cur_size) return true;

  heap = pq->heap;

  parent  = 0;
  l_child = 1;
  r_child = 2;

  for (;;)
    {
      if (l_child == pq->cur_size) break;
      if (heap[l_child].priority < heap[parent].priority) return false;

      if (r_child == pq->cur_size) break;
      if (heap[r_child].priority < heap[parent].priority) return false;

      ++parent;
      l_child += 2;
      r_child += 2;
    }

  return true;
}

#if 0
#define PQ_SIZE 10

int main (void)
{
  int idx;
  int * foo;
  priority_queue_t * pq;

  srand(time(0));

  pq = pq_new(PQ_SIZE);

  idx = 0;
  while (1)
    {
      foo = malloc_nofail(sizeof(int));
      *foo = rand();
      if (0 == pq_insert(pq, *foo, foo)) break;
      ++idx;
    }

  printf("inserted %d objects: ", idx);
  while (idx)
    {
      foo = pq_remove(pq);
      --idx;
      if (0 == foo)
        {
          printf("Thought I could remove %d more objects.\n", idx);
          pq_internal(pq);
          break;
        }
      printf("\n%d", *foo);
    }

  putchar('\n');

  pq_delete(pq);
  return 0;
}
#endif
#endif
