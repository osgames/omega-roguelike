/*
  map.c - general-purpose map (with ordered keys)
  liboutil : Copyright (C) 2003 by William Sheldon Simms

  This file is part of liboutils, a library of useful utility
  routines released together with the roguelike game Omega

  liboutils is licensed under the terms of the Omega license,
  which can be found in the Omega source distribution in the
  directory omega-roguelike/omega/lib/license.txt
*/

#include <assert.h>

#include "allocator.h"
#include "map.h"

void * malloc_nofail (size_t size);

/**********************/

struct map_iterator
{
  map_t * map;
  rb_element_t * elem;
  map_iterator_t * next;
};

/**********************/

make_static_allocator(map_iterator_t, (128 * sizeof(map_iterator_t)), next, malloc_nofail);

#define s_free_iterator(p) map_iterator_t_utility(p);
#define s_alloc_iterator() map_iterator_t_utility(0);

/**********************/

void map_iterator_delete (map_iterator_t * iter)
{
  if (iter == NULL) return;
  s_free_iterator(iter);
}

map_iterator_t * map_iterator_create (map_t * map)
{
  map_iterator_t * iter;

  assert(map);

  if (map == NULL) return NULL;

  iter = s_alloc_iterator();

  iter->map  = map;
  iter->elem = rb_minimum(map);
  iter->next = NULL;

  return iter;
}

int map_iterator_has_next (map_iterator_t * iter)
{
  assert(iter);

  if (map_size(iter->map) == 0) return 0;

  if (iter->elem == rb_nil(iter->map)) return 0;

   return 1;
}

void * map_iterator_next (map_iterator_t * iter)
{
  void * value;
  rb_element_t * elem;

  assert(iter);

  if (iter->elem == rb_nil(iter->map)) return NULL;

  elem = rb_successor(iter->map, iter->elem);

  value = (void *)rb_element_val(iter->elem);

  iter->elem = elem;

  return value;
}

void * map_iterator_remove (map_iterator_t * iter)
{
  void * value;
  rb_element_t * elem;

  assert(iter);

  if (iter->elem == rb_nil(iter->map)) return NULL;

  elem = rb_successor(iter->map, iter->elem);

  value = (void *)rb_element_val(iter->elem);

  rb_remove_element(iter->map, iter->elem);

  iter->elem = elem;

  return value;
}

uintptr_t map_iterator_key (map_iterator_t * iter)
{
  assert(iter);
  return rb_element_key(iter->elem);
}

uintptr_t map_iterator_value (map_iterator_t * iter)
{
  assert(iter);
  return rb_element_val(iter->elem);
}

/**********************/

map_t * map_new (map_compare_t cmp)
{
  return rb_new(cmp);
}

void map_delete (map_t * map)
{
  rb_delete(map);
}

int map_set (map_t * map, uintptr_t key, uintptr_t val, uintptr_t * oldval)
{
  rb_element_t * elem;

  assert(map != NULL);
  assert(key != 0);
  assert(val != 0);

  elem = rb_lookup(map, key);

  if (elem != NULL)
    {
      if (oldval != NULL)
	{
	  *oldval = rb_element_val(elem);
	  rb_element_set_val(elem, val);
	}

      return 0;
    }

  elem = rb_element_new();
  rb_element_set_key(elem, key);
  rb_element_set_val(elem, val);
  rb_insert_element(map, elem);

  return 1;
}

int map_find (map_t * map, uintptr_t key, uintptr_t * val)
{
  rb_element_t * elem;

  assert(map != NULL);

  elem = rb_lookup(map, key);

  if (elem == NULL) return 0;

  *val = rb_element_val(elem);
  return 1;
}

unsigned int map_size (map_t * map)
{
  return rb_size(map);
}

/* end */
