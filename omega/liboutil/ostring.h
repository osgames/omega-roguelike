/*
  ostring.h - character strings
  liboutil : Copyright (C) 2003 by William Sheldon Simms

  This file is part of liboutils, a library of useful utility
  routines released together with the roguelike game Omega

  liboutils is licensed under the terms of the Omega license,
  which can be found in the Omega source distribution in the
  directory omega-roguelike/omega/lib/license.txt
*/

#ifndef INCLUDED_OSTRING_H
#define INCLUDED_OSTRING_H

struct string;
typedef struct string string_t;

string_t * string_new (unsigned char * cstr);
void string_delete (string_t * str);
unsigned short string_len (string_t * str);
unsigned char * const string_cstr (string_t * str);

#endif /* INCLUDED_OSTRING_H */
