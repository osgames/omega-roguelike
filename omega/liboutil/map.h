/*
  map.h - general-purpose map (with ordered keys)
  liboutil : Copyright (C) 2003 by William Sheldon Simms

  This file is part of liboutils, a library of useful utility
  routines released together with the roguelike game Omega

  liboutils is licensed under the terms of the Omega license,
  which can be found in the Omega source distribution in the
  directory omega-roguelike/omega/lib/license.txt
*/

#ifndef INCLUDED_MAP_H
#define INCLUDED_MAP_H

#include "rb.h"

typedef rb_tree_t map_t;
typedef rb_compare_t map_compare_t;

struct map_iterator;
typedef struct map_iterator map_iterator_t;

/********/

void map_iterator_delete (map_iterator_t * iter);
map_iterator_t * map_iterator_create (map_t * map);
int map_iterator_has_next (map_iterator_t * iter);
void * map_iterator_next (map_iterator_t * iter);
void * map_iterator_remove (map_iterator_t * iter);
uintptr_t map_iterator_key (map_iterator_t * iter);
uintptr_t map_iterator_value (map_iterator_t * iter);

/********/

map_t * map_new (map_compare_t cmp);
void map_delete (map_t * map);
int map_set (map_t * map, uintptr_t key, uintptr_t val, uintptr_t * oldval);
int map_find (map_t * map, uintptr_t key, uintptr_t * val);
unsigned int map_size (map_t * map);

#endif /* INCLUDED_MAP_H */
