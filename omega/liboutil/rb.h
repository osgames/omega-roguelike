/*
  rb.h - red-black balanced binary tree
  liboutil : Copyright (C) 2003 by William Sheldon Simms

  This file is part of liboutils, a library of useful utility
  routines released together with the roguelike game Omega

  liboutils is licensed under the terms of the Omega license,
  which can be found in the Omega source distribution in the
  directory omega-roguelike/omega/lib/license.txt
*/

#ifndef INCLUDED_RB_H
#define INCLUDED_RB_H

#include <stdlib.h>  /* NULL */
#include <stdint.h>  /* uintptr_t */

#ifndef UINTPTR_MAX
/* assume a unsigned long can hold a pointer if the compiler doesn't supply uintptr_t */
typedef unsigned long uintptr_t;
#define UINTPTR_MIN ULONG_MIN
#define UINTPTR_MAY ULONG_MAX
#endif

struct rb_tree;
typedef struct rb_tree rb_tree_t;

struct rb_element;
typedef struct rb_element rb_element_t;

typedef int (* rb_compare_t) (uintptr_t a, uintptr_t b);

rb_tree_t * rb_new (rb_compare_t cmp);
void rb_delete (rb_tree_t * rb);

rb_element_t * rb_element_new (void);
void rb_element_delete (rb_element_t * elem);

unsigned int rb_size (rb_tree_t * rb);

uintptr_t rb_element_key (rb_element_t * elem);
uintptr_t rb_element_val (rb_element_t * elem);
void rb_element_set_key (rb_element_t * elem, uintptr_t key);
void rb_element_set_val (rb_element_t * elem, uintptr_t val);

rb_element_t * rb_nil (rb_tree_t * rb);
rb_element_t * rb_root (rb_tree_t * rb);
rb_element_t * rb_left (rb_element_t * x);
rb_element_t * rb_right (rb_element_t * x);
rb_element_t * rb_lookup (rb_tree_t * rb, uintptr_t key);
rb_element_t * rb_minimum (rb_tree_t * rb);
rb_element_t * rb_maximum (rb_tree_t * rb);
rb_element_t * rb_successor (rb_tree_t * rb, rb_element_t * x);
rb_element_t * rb_predecessor (rb_tree_t * rb, rb_element_t * x);

void rb_insert_element (rb_tree_t * rb, rb_element_t * x);
void rb_remove_element (rb_tree_t * rb, rb_element_t * z);

int rb_verify (rb_tree_t * rb);

#endif /* INCLUDED_RB_H */
