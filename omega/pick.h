/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/* *** Declarations to interactively pick from lists and arrays. *** */

/* Whether a prefix has grown or shrunk. */
enum { unq_init, uniq_prefix_shorter, uniq_prefix_longer, };

/* Pick from an array, where the names are pre-sorted in alphabetical
   order.  Case does not count.

   element_test returns whether element INDEX is valid, by some
   condition(s) other than prefix matching.

   element_name return the name of element INDEX

   menuprint_element may be either NULL or a function that menuprints
   an element's name and associated data.  `arraypick' sets up the
   menu window.
   
   type_of_elements is a string saying the type of elements.

   last_index is the last valid element index, ie one less than the
   total number of elements.  EG, an array of 10 elements would pass
   `9'.

   `arraypick' will return the index of the element chosen, or ABORT
   if none is chosen.  */
int arraypick(
	      char * prompt,
	      char * found_none_message,
	      bool (*element_test)(int index),
	      char * (*element_name)(int index),
	      void   (*menuprint_element)(int index),
	      char * type_of_elements,
	      int    last_index);


/* Pick from a generalized list.
   
   Caller must ensure buffer points to a buffer of sufficient size to
   hold any ambiguous prefix + 1.

   update_count must return a count of possible matching elements.

   show_known should display help of some sort.  Generally this means
   listing the possible choices.

   selected_name must return the name of a possible matching element.
   It need not care which; ensuring that it is called in meaningful
   conditions is pick_from_list's responsibility.
   
   `pick_from_list' does not promise to return p_data in a state
   corresponding to the final prefix, but does promise to return
   prefix itself in the final state.  */

bool pick_from_list(char * prompt,
		    char * found_none_message,
		    void * p_data,
		    char * buffer,
		    int (*update_count)(void * p_data, char * prefix, int hint),
		    void (*show_known)(void * p_data),
		    char * (*selected_name)(void * p_data));

