/* omega copyright (c) 1987,1988,1989 by Laurence Raphael Brothers */
/* lev.c */

#include "game_time.h"
#include "glob.h"
#include "liboutil/list.h"
#include "memory.h"
#include "util.h"
#include "findloc.h"

/* Functions dealing with dungeon and country levels aside from actual
level structure generation */

int try_to_place_monsterid(int monsterid, int * x, int * y);
int roll_monster_id(int level);
int resolve_monster_id(int monsterid);

/* monsters for tactical encounters */
void make_country_monsters (Symbol terrain)
{
  static int plains[10] = {BUNNY,BUNNY,HORNET,QUAIL,HAWK,DEER,WOLF,LION,BRIGAND,RANDOM};
  static int forest[10] = {BUNNY,QUAIL,HAWK,BADGER,DEER,DEER,WOLF,BEAR,BRIGAND,RANDOM};
  static int jungle[10] = {ANTEATER,PARROT,MAMBA,ANT,ANT,HYENA,HYENA,ELEPHANT,LION,RANDOM};
  static int river[10] = {QUAIL,TROUT,TROUT,MANOWAR,BASS,BASS,CROC,CROC,BRIGAND,RANDOM};
  static int swamp[10] = {BASS,BASS,CROC,CROC,BOGTHING,ANT,ANT,RANDOM,RANDOM,RANDOM};
  static int desert[10] = {HAWK,HAWK,CAMEL,CAMEL,HYENA,HYENA,LION,LION,RANDOM,RANDOM};
  static int tundra[10] = {WOLF,WOLF,BEAR,BEAR,DEER,DEER,RANDOM,RANDOM,RANDOM,RANDOM};
  static int mountain[10] = {BUNNY,SHEEP,WOLF,WOLF,HAWK,HAWK,HAWK,RANDOM,RANDOM,RANDOM};
  static int village[10] = {GUARD,SHEEP,SHEEP,MERCHANT,ITIN_MERCH,ZERO_NPC,MEND_PRIEST,HAWK,HORSE,RANDOM};
  static int city[10] = {GUARD,GUARD,GUARD,SHEEP,HORSE,MERCHANT,ITIN_MERCH,ZERO_NPC,MEND_PRIEST,RANDOM};
  static int road_day[10] = {GUARD,MERCHANT,ITIN_MERCH,MEND_PRIEST,WEREHUMAN,HAWK,WOLF,GRUNT,SNEAK_THIEF,RANDOM};
  static int road_night[10] = {DEER,WOLF,GOBLIN,SNEAK_THIEF,APPR_NINJA,BRIGAND,WEREHUMAN,GENIN,RANDOM,RANDOM};

  int * monsters;
  int idx, nummonsters;

  nummonsters = (1 + random_range(5)) * (1 + random_range(3));

  switch (terrain)
    {
    case PLAINS: monsters = plains; break;
    case FOREST: monsters = forest; break;
    case JUNGLE: monsters = jungle; break;
    case RIVER: monsters = river; break;
    case SWAMP: monsters = swamp; break;
    case MOUNTAINS:
    case PASS:
    case VOLCANO: monsters = mountain; break;
    case DESERT: monsters = desert; break;
    case TUNDRA: monsters = tundra; break;
    case VILLAGE: monsters = village; break;
    case CITY: monsters = city; break;
    case ROAD:
      if (nighttime())
        monsters = road_night;
      else
        monsters = road_day;
      break;
    default:
      monsters = NULL;
  }

  for (idx = 0; idx < nummonsters; ++idx)
    {
      monster * mon;
      mon = new_monster();
      {
	int monsterid, x, y, placed;
	if (monsters == NULL)
	  { monsterid = RANDOM; }
	else
	  { monsterid = monsters[random_range(10)]; }

	monsterid = resolve_monster_id(monsterid);
	
	/* If there's no place for it, don't make the monster.  */
	placed = try_to_place_monsterid( monsterid, &x, &y);
	if(!placed) { continue; }

	mon = make_creature(monsterid);
	mon->sense = Level->level_width;
	add_monster_xy(Level, x, y, mon);
      }
    }
}

/* get the list of objects at a particular location on a particular level */
list_t * get_object_list (level * lev, int x, int y)
{
  assert(lev);
  assert(inbounds(x, y));

  return lev->site[x][y].object_list;
}

/* get the list of monsters on a particular level */
list_t * get_monster_list (level * lev)
{
  assert(lev);

  return lev->monster_list;
}

/* get the monster (if any) at a particular location on a particular level */
monster * get_monster (level * lev, int x, int y)
{
  assert(lev);
  assert(inbounds(x, y));

  return lev->site[x][y].creature;
}

/* add a particular object to a particular place on a particular level */
void add_object_to_level (level * lev, object * obj, int x, int y)
{
  assert(lev);
  assert(obj);
  assert(inbounds(x,y));

  if (VOID_CHAR == lev->site[x][y].locchar || ABYSS == lev->site[x][y].locchar)
    {
      free_object(obj);
      /* maybe print a message here? */
      return;
    }

  if (0 == lev->site[x][y].object_list)
    lev->site[x][y].object_list = list_new();

  list_prepend(lev->site[x][y].object_list, obj);
}

void add_monster_xy (level * lev, int x, int y, monster * mon)
{
  assert(lev);
  assert(mon);
  assert(inbounds(x,y));
  assert(0 == lev->site[x][y].creature);

  mon->x = x;
  mon->y = y;
  lev->site[x][y].creature = mon;

  if (0 == lev->monster_list)
    lev->monster_list = list_new();

  list_prepend(lev->monster_list, mon);

  /* if the given level is the current level, */
  /* then set a ticket for the monster */
  if (Level == lev)
    set_monster_handler(mon);
}

/* Remove regardless whether we're in its turn or something else's.  */
void remove_monster_from_level (level * lev, monster * mon)
{
  assert(lev);
  assert(mon);
  if(!m_statusp(mon, M_GONE))
    {
      assert(lev->site[mon->x][mon->y].creature == mon);
      lev->site[mon->x][mon->y].creature = 0;
      /* Indicate that it's gone, so monster_handler can do the rite
	 thing. */
      m_status_set(mon, M_GONE);
    }
}

/* move monster from one location to another on a level */
void move_monster (level * lev, monster * mon, int x, int y)
{
  assert(mon);
  assert(inbounds(x, y));
  assert(0 == lev->site[x][y].creature);

  lev->site[mon->x][mon->y].creature = 0;
  lev->site[x][y].creature = mon;

  mon->x = x;
  mon->y = y;

  m_movefunction(mon, lev->site[x][y].p_locf);
}

/* monstertype is more or less Current_Dungeon */
/* The caves and sewers get harder as you penetrate them; the castle
is completely random, but also gets harder as it is explored;
the astral and the volcano just stay hard... */

void populate_level (int monstertype)
{
  int i, j, k;
  int monsterid;
  int nummonsters;

  monsterid = RANDOM;
  nummonsters = 8 + 3 * (1 + random_range(difficulty() / 3));

  if (monstertype == E_CASTLE)
    nummonsters += 10;
  else if (monstertype == E_ASTRAL)
    nummonsters += 10;
  else if (monstertype == E_VOLCANO)
    nummonsters += 20;

  for (k = 0; k < nummonsters; ++k)
    {
      switch (monstertype)
        {
        case E_CAVES:
          if ((10 * Level->depth + random_range(100)) > 150)
            monsterid = GOBLIN_SHAMAN;
          else if ((10 * Level->depth + random_range(100)) > 100)
            monsterid = GOBLIN_CHIEF;
          else if (random_range(100) > 50)
            monsterid = GOBLIN;
          else
            monsterid = RANDOM;
          break;

        case E_SEWERS:
          if (!random_range(3))
            monsterid = -1;
          else
            switch (random_range(3 + Level->depth))
              {
              case 0: monsterid = SEWER_RAT; break;
              case 1: monsterid = AGGRAVATOR; break;
              case 2: monsterid = BLIPPER; break;
              case 3: monsterid = NIGHT_GAUNT; break;
              case 4: monsterid = NASTY; break;
              case 5: monsterid = MURK; break;
              case 6: monsterid = CATOBLEPAS; break;
              case 7: monsterid = ACID_CLOUD; break;
              case 8: monsterid = DENEBIAN; break;
              case 9: monsterid = CROC; break;
              case 10: monsterid = TESLA; break;
              case 11: monsterid = SHADOW; break;
              case 12: monsterid = BOGTHING; break;
              case 13: monsterid = WATER_ELEM; break;
              case 14: monsterid = TRITON; break;
              case 15: monsterid = ROUS; break;
              default: monsterid = RANDOM; break;
              }
          break;

        case E_ASTRAL:
          if (random_range(2))
            switch(random_range(12))
              {
                /* random astral creatures */
              case 0: monsterid = THOUGHTFORM; break;
              case 1: monsterid = FUZZY; break;
              case 2: monsterid = BAN_SIDHE; break;
              case 3: monsterid = GRUE; break;
              case 4: monsterid = SHADOW; break;
              case 5: monsterid = ASTRAL_VAMP; break;
              case 6: monsterid = MANABURST; break;
              case 7: monsterid = RAKSHASA; break;
              case 8: monsterid = ILL_FIEND; break;
              case 9: monsterid = MIRRORMAST; break;
              case 10: monsterid = ELDER_GRUE; break;
              case 11: monsterid = SHADOW_SLAY; break;
              }
          else if (random_range(2) && (Level->depth == 1)) /* plane of earth */
            monsterid = EARTH_ELEM;
          else if (random_range(2) && (Level->depth == 2)) /* plane of air */
            monsterid = AIR_ELEM;
          else if (random_range(2) && (Level->depth == 3)) /* plane of water */
            monsterid = WATER_ELEM;
          else if (random_range(2) && (Level->depth == 4)) /* plane of fire */
            monsterid = FIRE_ELEM;
          else if (random_range(2) && (Level->depth == 5)) /* deep astral */
            switch (random_range(12))
              {
              case 0: monsterid = NIGHT_GAUNT; break;
              case 1: monsterid = SERV_LAW; break;
              case 2: monsterid = SERV_CHAOS; break;
              case 3: monsterid = FROST_DEMON; break;
              case 4: monsterid = OUTER_DEMON; break;
              case 5: monsterid = DEMON_SERP; break;
              case 6: monsterid = ANGEL; break;
              case 7: monsterid = INNER_DEMON; break;
              case 8: monsterid = FDEMON_L; break;
              case 9: monsterid = HIGH_ANGEL; break;
              case 10: monsterid = DEMON_PRINCE; break;
              case 11: monsterid = ARCHANGEL; break;
              }
          else
            monsterid = RANDOM;
          break;

        case E_VOLCANO:
          if (random_range(2))
            {
              do
                monsterid = ML4 + random_range(ML10 - ML4);
              while (Monsters[monsterid].uniqueness != COMMON);
            }
          else
            switch(random_range(Level->depth / 2 + 2))
              {
                /* evil & fire creatures */
              case 0: monsterid = HAUNT; break;
              case 1: monsterid = INCUBUS; break;
              case 2: monsterid = DRAGONETTE; break;
              case 3: monsterid = FROST_DEMON; break;
              case 4: monsterid = SPECTRE; break;
              case 5: monsterid = LAVA_WORM; break;
              case 6: monsterid = FIRE_ELEM; break;
              case 7: monsterid = LICHE; break;
              case 8: monsterid = RAKSHASA; break;
              case 9: monsterid = DEMON_SERP; break;
              case 10: monsterid = NAZGUL; break;
              case 11: monsterid = FLAME_DEV; break;
              case 12: monsterid = LOATHLY; break;
              case 13: monsterid = ZOMBIE; break;
              case 14: monsterid = INNER_DEMON; break;
              case 15: monsterid = BAD_FAIRY; break;
              case 16: monsterid = DRAGON; break;
              case 17: monsterid = FDEMON_L; break;
              case 18: monsterid = SHADOW_SLAY; break;
              case 19: monsterid = DEATHSTAR; break;
              case 20: monsterid = VAMP_LORD; break;
              case 21: monsterid = DEMON_PRINCE; break;
              default: monsterid = RANDOM; break;
              }
          break;

        case E_CASTLE:
          if (1 == random_range(4))
            {
              if (difficulty() < 5)
                monsterid = ENCHANTOR;
              else if (difficulty() < 6)
                monsterid = NECROMANCER;
              else if (difficulty() < 8)
                monsterid = FIRE_ELEM;
              else
                monsterid = THAUMATURGIST;
            }
          else
            monsterid = RANDOM;
          break;

        default:
          monsterid = RANDOM; break;
        }

      monsterid = resolve_monster_id(monsterid);

      {
	/* If there's no place for it, don't make the monster.  */
	int placed = try_to_place_monsterid( monsterid, &i, &j);
	if(!placed) { continue; }
      }
      {
	monster * mon = make_creature(monsterid);
	add_monster_xy(Level, i, j, mon);
      }
    }
}

int try_to_place_monsterid(int monsterid, int * x, int * y)
{
  /* If LURKER, find a doorway?  If flying, allow more terrain?  */
  if (m_statusp(&Monsters[monsterid], ONLYSWIM))
    {
      return findspace_aux( x, y, water_space, 0);
    }
  else
    {
      return find_floorspace(x, y);
    }
}

/* Add a wandering monster possibly */
void wandercheck (void)
{
  if (random_range(MaxDungeonLevels) < difficulty())
    {
      int x, y;
      monster * mon;
      int monsterid = roll_monster_id(difficulty());
      int placed = try_to_place_monsterid( monsterid, &x, &y);
      if(!placed) { return; }

      mon = make_creature(monsterid);
      m_status_set(mon,WANDERING);
      add_monster_xy(Level, x, y, mon);
    }
}

/* create a monster of type mid and place it on the current level at x,y */
monster * make_site_monster (int i, int j, int mid)
{
  int monsterid = resolve_monster_id(mid);
  monster * mon = make_creature(monsterid);
  if(mid < 0)
    { m_status_set(mon,WANDERING); }

  add_monster_xy(Level, i, j, mon);
  return mon;
}

/* Make the id index a real monster type */
int resolve_monster_id(int monsterid)
{
  if (monsterid < 0)
    { return roll_monster_id(difficulty()); }
  return monsterid;
}

/* Figure out an appropriate monster type for the difficulty-level */
int roll_monster_id(int level)
{
  int monster_range;
  int mid;

  switch (level)
    {
    case 0:monster_range = ML1; break;
    case 1:monster_range = ML2; break;
    case 2:monster_range = ML3; break;
    case 3:monster_range = ML4; break;
    case 4:monster_range = ML5; break;
    case 5:monster_range = ML6; break;
    case 6:monster_range = ML7; break;
    case 7:monster_range = ML8; break;
    case 8:monster_range = ML9; break;
    case 9:monster_range = ML10; break;
    default:monster_range = NUMMONSTERS; break;
    }

  do
    { mid = random_range(monster_range); }
  while (Monsters[mid].uniqueness != COMMON);

  return mid;
}

/* make and return an appropriate monster for the level and depth */
/* called by populate_level, doesn't actually add to mlist for some reason */
/* eventually to be more intelligent */
pmt m_create (int x, int y, int kind, int level)
{
  pmt newmonster;

  newmonster = make_creature(roll_monster_id(level));

  /* no duplicates of unique monsters */
  if (kind == WANDERING) m_status_set(newmonster,WANDERING);
  newmonster->x = x;
  newmonster->y = y;

  return newmonster;
}

/* make creature # mid, totally random if mid == -1 */
/* make creature allocates space for the creature */
pmt make_creature (int mid)
{
  monster * newmonster;
  object * ob;
  int treasures;

  newmonster = new_monster();

  if (mid == -1) mid = random_range(ML9);
  *newmonster = Monsters[mid];

  if (ANGEL == mid || HIGH_ANGEL == mid || ARCHANGEL == mid)
    {
      int deity = 1 + random_range(6);
      newmonster->aux1 = deity;
      newmonster->monstring = angeltype( mid, deity );
    }
  else if (ZERO_NPC == mid || WEREHUMAN == mid)
    {
      /* generic 0th level human, or a were-human */
      newmonster->corpsestr = mancorpse();
      /* DAG use same (static data) string, after "dead " part */
      newmonster->monstring = 5 + newmonster->corpsestr;
    }
  else if (SATYR == mid || INCUBUS == mid)
    {
      /* the nymph/satyr and incubus/succubus */
      if (Player.preference == 'f'
          || (Player.preference != 'm' && random_range(2)))
        {
          newmonster->monchar = 'n'|CLR(RED);
          newmonster->monstring = "nymph";
          newmonster->corpsestr = "dead nymph";
        }
      else
        {
          newmonster->monchar = 's'|CLR(RED);
          newmonster->monstring = "satyr";
          newmonster->corpsestr = "dead satyr";
        }

      if (newmonster->id == INCUBUS)
        {
          if ((newmonster->monchar&0xff) == 'n')
            newmonster->corpsestr = "dead succubus";
          else newmonster->corpsestr = "dead incubus";
        }
    }
  if (NPC == mid)
    make_log_npc(newmonster);
  else if (mid == HISCORE_NPC)
    {
      make_hiscore_npc(newmonster, random_range(NPC_TOTAL_NUMBER));
    }
  else
    {
      int idx;
      if (newmonster->sleep < random_range(100))
        m_status_set(newmonster,AWAKE);

      give_mon_object_by_id(newmonster, newmonster->startthing);

      /* DAG -- monster with treasure of 1 would be same as 0, shouldn't be. */
      treasures = random_range(newmonster->treasure + 1);
      for(idx = 0; idx < treasures; ++idx)
        {
          do
            {
              ob = create_object(newmonster->level);
              if (ob->uniqueness != COMMON)
                {
                  Objects[ob->id].uniqueness = UNIQUE_UNMADE;
                  free_obj(ob, TRUE);
                  ob = NULL;
                }
            }
          while (!ob);

          m_pickup(newmonster,ob);
        }
    }

  return newmonster;
}


/* drop treasures randomly onto level */
void stock_level (void)
{
  int i,j,k,x;
  int numtreasures;
  int cash_multiple = 1;

  numtreasures = 2 * random_range(difficulty()/4) + 14;

  if (E_CAVES == Current_Dungeon)
    cash_multiple = 3;

  /* put cash anywhere, including walls, put other treasures only on floor */
  for (k = 0; k < numtreasures; ++k)
    {
      do
        {
          i = random_range(Level->level_width);
          j = random_range(Level->level_length);
        }
      while (Level->site[i][j].locchar != FLOOR);
      make_site_treasure(i, j, difficulty());

      for (x = 0; x < cash_multiple; ++x)
        {
          object * obj;

          i = random_range(Level->level_width);
          j = random_range(Level->level_length);

          obj = new_object();
          make_cash(obj, difficulty());
          add_object_to_level(Level, obj, i, j);
        }
    }
}

/* make a new object (of at most level itemlevel) at site i,j on level*/
void make_site_treasure(int i, int j, int itemlevel)
{
  object * obj;
  obj = create_object(itemlevel);
  add_object_to_level(Level, obj, i, j);
}

/* make a specific new object at site i,j on level*/
void make_specific_treasure (int i, int j, int itemid)
{
  object * obj;

  if (Objects[itemid].uniqueness == UNIQUE_TAKEN) return;

  obj = new_object();
  *obj = Objects[itemid];
  add_object_to_level(Level, obj, i, j);
}

/* returns a "level of difficulty" based on current environment
   and depth in dungeon. Is somewhat arbitrary. value between 1 and 10.
   May not actually represent real difficulty, but instead level
   of items, monsters encountered.    */
int difficulty(void)
{
  int depth = 1;
  if (Level != NULL) depth = Level->depth;
  switch(Current_Environment) {
  case E_COUNTRYSIDE: return(7);
  case E_CITY: return(3);
  case E_VILLAGE: return(1);
  case E_TACTICAL_MAP: return(7);
  case E_SEWERS: return(depth/6)+3;
  case E_CASTLE: return(depth/4)+4;
  case E_CAVES: return(depth/3)+1;
  case E_VOLCANO: return(depth/4)+5;
  case E_ASTRAL: return(8);
  case E_ARENA: return(5);
  case E_HOVEL: return(3);
  case E_MANSION: return(7);
  case E_HOUSE: return(5);
  case E_DLAIR: return(9);
  case E_ABYSS: return(10);
  case E_STARPEAK: return(9);
  case E_CIRCLE: return(8);
  case E_MAGIC_ISLE: return(8);
  case E_TEMPLE: return(8);

  case E_PALACE:  /* varies by phase of moon */
    {
      int diff = 0;
      switch(time_phase() / 2)
        {
        case 0: diff = 0; break;
        case 1: case 11: diff = 1; break;
        case 2: case 10: diff = 2; break;
        case 3: case 9:  diff = 3; break;
        case 4: case 8:  diff = 5; break;
        case 5: case 7:  diff = 6; break;
        case 6: diff = 7; break;
        }

      return( min( ((depth+diff)/3)+4,9));
    }
  default: return(3);
  }
}
