/* omega copyright (C) by Laurence Raphael Brothers, 1987,1988,1989 */
/* command2.c */

/* This file contains toplevel commands called from command1.c */

#include <unistd.h>
#include <ctype.h>

#include "glob.h"
#include "lev.h"
#include "liboutil/list.h"
#include "game_time.h"
#include "memory.h"
#include "util.h"
#include "command.h"


/* no op a turn.... */
post_command_state_t cmd_rest(int dir, int first_time)
{
  if (random_range(20) == 1) {
    switch(random_range(10)) {
      case 0: print3(" Time passes slowly.... "); break;
      case 1: print3(" Tick. Tock. Tick. Tock. "); break;
      case 2: print3(" Ho Hum. "); break;
      case 3: print3(" Beauty Sleep. Well, in your case, Ugly Sleep. "); break;
      case 4: print3(" And with Strange Aeons, even Death may die. "); break;
      case 5: print3(" La Di Da. "); break;
      case 6: print3(" Time keeps on tickin' tickin' -- into the future.... ");
  	      break;
      case 7: print3(" Boooring! "); break;
      case 8: print3(" You think I like watching you sleep? "); break;
      case 9: print3(" You sure have an early bedtime! "); break;
    }
    morewait();
  }
  return base_state;
}



/* read a scroll, book, tome, etc. */
post_command_state_t cmd_peruse(int dir, int first_time)
{
  clearmsg();

  if (Player.status[BLINDED] > 0)
    {
      print3("You're blind -- you can't read!!!");
      return base_state;
    }
  if (Player.status[AFRAID] > 0)
    {
      print3("You are too afraid to stop to read a scroll!");
      return base_state;
    }

  {
    int index;
    index = getitem_prompt("Read -- ", SCROLL);
    if (index == no_dir)
      { return base_state; }

    {
      struct object *obj;
      obj = Player.possessions[index];
      if (obj->objchar != SCROLL) {
	print3("There's nothing written on ");
	nprint3(itemid(obj));
	RETURN_post_command_state( 0, 5 );
      }
      else {
	nprint1("You carefully unfurl the scroll....");
	morewait();
	item_use(obj);
	dispose_lost_objects(1,obj);
	RETURN_post_command_state( 0, 20 );
      }
    }
  }
}


post_command_state_t cmd_quaff(int dir, int first_time)
{
  int index;
  clearmsg();

  index = getitem_prompt("Quaff -- ", POTION);
  if (index == no_dir)
    { return base_state; }

  {
    struct object *obj;

    obj = Player.possessions[index];
    if (obj->objchar != POTION) {
      print3("You can't drink ");
      nprint3(itemid(obj));
      RETURN_post_command_state( 0,  5 );
    }
    else {
      print1("You drink it down.... ");
      item_use(obj);
      morewait();
      dispose_lost_objects(1,obj);
      RETURN_post_command_state( 0,  10 );
    }
  }
}


post_command_state_t cmd_activate(int dir, int first_time)
{
  int index = ABORT;
  char response;

  clearmsg();

  print1("Activate -- item [i] or artifact [a] or quit [ESCAPE]?");
  do
    { response = (char) mcigetc(); }
  while ((response != 'i') && (response != 'a') && (response != ESCAPE));

  if (response == ESCAPE)
    { return base_state; }

  {
    if (response == 'i')
      index = getitem_prompt("Activate -- ", THING);
    else if (response == 'a')
      index = getitem_prompt("Activate -- ", ARTIFACT);

    if (index == ABORT)
      { return base_state; }

    clearmsg();
    print1("You activate it.... ");
    morewait();

    item_use(Player.possessions[index]);
    RETURN_post_command_state( 0,  10 );
  }
}


post_command_state_t cmd_eat(int dir, int first_time)
{
  int index;

  clearmsg();

  index = getitem_prompt("Eat -- ", FOOD);

  if (index == ABORT)
    { return base_state; }

  {
    struct object *obj;
    obj = Player.possessions[index];
    if ((obj->objchar != FOOD) && (obj->objchar != CORPSE))
      {
	print3("You can't eat ");
	nprint3(itemid(obj));
	RETURN_post_command_state( 0, 5 );
      }


    if (obj->usef == I_FOOD)
      {
	Player.food = max(0, Player.food + obj->aux);
	if (pflagp(WANT_PECAN_TWIRL) && (obj->id == OB_TWIRL))
	  {
	    print3("The flavor of the pecan twirl floods your mind! You stagger!");
	    morewait();
	    clearmsg();
	    learnspell(0); /* learn a random spell */
	  }
      }

    item_use(obj);
    dispose_lost_objects(1, obj);
    set_notice_flags(notice_food);

    /* Takes the same time for country or level. */
    RETURN_post_command_state( 0, 30 );

  }
}


post_command_state_t cmd_search(int dir, int first_time)
{
  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  if (Player.status[AFRAID] > 0)
    {
      print3("You are too terror-stricken to stop to search for anything.");
      return base_state;
    }

  {
    int found = FALSE;
    int i;
    for (i=0;i<9;i++)
      {
	found |= searchat(Player.x+Dirs[0][i],Player.y+Dirs[1][i]);
      }

    if(found)
      {
	set_notice_flags(notice_vision);
	RETURN_post_command_state( 0,  20 );
      }

    RETURN_post_command_state( Searchnum,  20 );
  }
}


/* pick up a thing where the player is */
post_command_state_t cmd_pickup(int dir, int first_time)
{
  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  if (0 == list_size(Level->site[Player.x][Player.y].object_list))
    {
      print3("There's nothing there!");
      return base_state;
    }

  if (Player.status[SHADOWFORM])
    {
      print3("You can't really interact with the real world "
	     "in your shadowy state.");
      return base_state;
    }


    {
      pickup_at(Player.x, Player.y);
      RETURN_post_command_state( 0, Player.speed * 10 / 5 );
    }
}

/* floor inventory */
static void apply_print_inventory (void * vobj)
{
  object * obj;
  obj = vobj;
  menuprint(itemid(obj));
  menuprint("\n");
}

void floor_inv(void)
{
  menuclear();

  if (list_size(Level->site[Player.x][Player.y].object_list))
    list_apply(Level->site[Player.x][Player.y].object_list, apply_print_inventory);

  showmenu();
  morewait();
  set_notice_flags(notice_drew_menu);
}

post_command_state_t cmd_drop(int dir, int first_time)
{
  int index;

  clearmsg();

  index = getitem_prompt("Drop -- ", CASH);

  if (index == ABORT)
    { return base_state; }
  if (index == CASHVALUE)
    {
      drop_money();
      RETURN_post_command_state( 0, Player.speed * 5 / 5 );
    }

    {
      pob o = Player.possessions[index];

      if (o->used && cursed(o))
        {
          print3("You can't seem to get rid of: ");
          nprint3(itemid(o));
	  return base_state;
        }


      if (o->number == 1)
	{
	  p_drop_at(Player.x, Player.y, o);
	  conform_lost_objects(1, o);
	  set_notice_flags(notice_equip);
	  RETURN_post_command_state( 0, Player.speed * 5 / 5 );
	}
      else
	{
	  int num;
	  num = getnumber(o->number);
	  if (num <= 0)
	    { return base_state; }
	
	  {
	    p_drop_at(Player.x, Player.y, split_item(num, o));
	    dispose_lost_objects(num, o);
	    set_notice_flags(notice_equip);
	    RETURN_post_command_state( 0, Player.speed * 5 / 5 );
	  }
	}
    }
}



/* talk to the animals -- learn their languages.... */

post_command_state_t cmd_talk(int dir, int first_time)
{
  int dx,dy,index=0;
  char response;

  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  /*  This could use set_up_command_dir.  */
  clearmsg();
  print1("Talk --");
  index = getdir();

  if (index == no_dir)
    { return base_state; }

  {
    dx = Dirs[0][index];
    dy = Dirs[1][index];

    if (!inbounds(Player.x + dx, Player.y + dy) ||
	!get_monster(Level, Player.x + dx, Player.y + dy))
      {
        print3("There's nothing there to talk to!!!");
	return base_state;
      }

      {
	long duration = 0;
	struct monster *m;
        m = get_monster(Level, Player.x + dx, Player.y + dy);

        menuclear();
        strcpy(Str1,"     Talk to ");
        strcat(Str1,m->monstring);
        strcat(Str1,":");
        menuprint(Str1);
        menuprint("\na: Greet.");
        menuprint("\nb: Threaten.");
        menuprint("\nc: Surrender.");
        menuprint("\nESCAPE: Clam up.");
        showmenu();

        do
          response = menugetc();
        while (response != 'a' && response != 'b' && response != 'c' && response != ESCAPE);

        switch (response)
          {
          case 'a': monster_talk(m); duration = 10; break;
          case 'b': threaten(m);     duration = 10; break;
          case 'c': surrender(m);    duration = 10; break;
          default: duration = 0; break;
          }

	set_notice_flags(notice_drew_menu);
	RETURN_post_command_state( 0,  duration );
      }
  }
}


/* try to deactivate a trap */
post_command_state_t cmd_disarm(int dir, int first_time)
{
  int x,y,index=0;
  pob o;

  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  /*  This could use set_up_command_dir.  */
  clearmsg();
  print1("Disarm -- ");
  index = getdir();

  if (index == no_dir)
    { return base_state; }

  {
    x = Dirs[0][index]+Player.x;
    y = Dirs[1][index]+Player.y;

    if (! inbounds(x,y))
    {
      print3("Whoa, off the map...");
      return base_state;
    }

    if (Level->site[x][y].locchar != TRAP)
      {
	print3("You can't see a trap there!");
	return base_state;
      }

    {
      if (random_range(50+difficulty()*5) <
	  Player.dex*2+Player.level*3+Player.rank[THIEVES]*10) {
	print1("You disarmed the trap!");
	if (random_range(100) < Player.dex+Player.rank[THIEVES]*10) {
	  o = new_object();
	  switch(Level->site[x][y].p_locf) {
	  case L_TRAP_DART:
	    *o=Objects[OB_TRAP_DART];
	    break;
	  case L_TRAP_DISINTEGRATE:
	    *o=Objects[OB_TRAP_DISINTEGRATE];
	    break;
	  case L_TRAP_SLEEP_GAS:
	    *o=Objects[OB_TRAP_SLEEP];
	    break;
	  case L_TRAP_TELEPORT:
	    *o=Objects[OB_TRAP_TELEPORT];
	    break;
	  case L_TRAP_ABYSS:
	    *o=Objects[OB_TRAP_ABYSS];
	    break;
	  case L_TRAP_FIRE:
	    *o=Objects[OB_TRAP_FIRE];
	    break;
	  case L_TRAP_SNARE:
	    *o=Objects[OB_TRAP_SNARE];
	    break;
	  case L_TRAP_ACID:
	    *o=Objects[OB_TRAP_ACID];
	    break;
	  case L_TRAP_MANADRAIN:
	    *o=Objects[OB_TRAP_MANADRAIN];
	    break;
	  default:
	    /* DAG can't use free_obj() as object not initialized */
	    delete_object(o);
	    o = NULL;
	    break;
	  }
	  if (o != NULL) {
	    print2("You manage to retrieve the trap components!");
	    morewait();
	    know_object(o);
	    gain_item(o);
	    gain_experience(25);
	  }
	}
	Level->site[x][y].p_locf = L_NO_OP;
	Level->site[x][y].locchar = FLOOR;
	lset(x, y, CHANGED);
	gain_experience(5);
      }
      else if (random_range(10+difficulty()*2) > Player.dex) {
	print1("You accidentally set off the trap!");
	physical_moveplayer (x, y);
      }
      else
	{ print1("You failed to disarm the trap."); }

      RETURN_post_command_state( 0,  30 );
    }
  }
}


/* is it more blessed to give, or receive? */
post_command_state_t cmd_give(int dir, int first_time)
{
  int index;
  int x,y,dindex=0;

  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  clearmsg();
  print1("Give to monster --");
  dindex = getdir();
  if (dindex == no_dir)
    { return base_state; }

  {
    x = Dirs[0][dindex]+Player.x;
    y = Dirs[1][dindex]+Player.y;

    if (! inbounds(x, y))
      {
	print3("Whoa, off the map...");
	return base_state;
      }

    if (!get_monster(Level, x, y))
      {
        print3("There's nothing there to give something to!!!");
	return base_state;
      }

    {
      struct monster *m;
      m = get_monster(Level, x, y);
      clearmsg();
      index = getitem_prompt("Give what? ", CASH);
      if (index == ABORT)
	{ return base_state; }

      if (index == CASHVALUE)
	{ give_money(m); }
      else
	{
	  pob old_obj = Player.possessions[index];
	  if (!cursed(old_obj))
	    {
	      pob obj;
	      obj = split_item(1, old_obj);
	      conform_lost_objects(1,old_obj);
	      print2("Given: ");
	      nprint2(itemid(obj));
	      morewait();
	      /* WDT bug fix: I moved the print above the givemonster
	       * call.  If that turns out looking ugly, I should change it to
	       * a sprintf or strcat.  At any rate, it was wrong before because
	       * it was accessing an object which had already been freed as part
	       * of givemonster. */
	      givemonster(m, obj);
	      set_notice_flags(notice_equip);
	    }
	  else
	    {
	      print3("You can't even give away: ");
	      nprint3(itemid(old_obj));
	    }
	}
      RETURN_post_command_state( 0,  10 );
    }
  }
}


/* zap a wand, of course */
post_command_state_t cmd_zapwand(int dir, int first_time)
{
  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  clearmsg();

  if (Player.status[AFRAID] > 0)
    {
      print3("You are so terror-stricken you can't hold a wand straight!");
      return base_state;
    }
  {
    int index;
    index = getitem_prompt("Zap -- ", STICK);
    if (index == ABORT)
      { return base_state; }

    {
      struct object *obj;
      obj = Player.possessions[index];
      if (obj->objchar != STICK)
	{
	  print3("You can't zap: ");
	  nprint3(itemid(obj));
	  return base_state;
	}

      {
	if (obj->charge < 1)
	  { print3("Fizz.... Pflpt. Out of charges. "); }
	else
	  {
	    obj->charge--;
	    item_use(obj);
	  }

	return base_state;
      }
    }
  }
}

/* cast a spell */
post_command_state_t cmd_magic(int dir, int first_time)
{
  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  clearmsg();

  if (Player.status[AFRAID] > 0)
    {
      print3("You are too afraid to concentrate on a spell!");
      return base_state;
    }

  {
    int index;
    index = getspell();
    set_notice_flags(notice_drew_menu);

    if (index == ABORT)
      { return base_state; }
    else
      {
	int drain;
	drain = Spells[index].powerdrain;

	if (1 == get_lunarity())
	  drain = drain / 2;
	else if (-1 == get_lunarity())
	  drain = drain *2;

	if (drain > Player.mana)
	  {
	    if (-1 == get_lunarity() && Player.mana >= (drain / 2))
	      print3("The contrary moon has made that spell too draining! ");
	    else
	      print3("You lack the power for that spell! ");
	  }
	else
	  {
	    Player.mana -= drain;
	    cast_spell(index);
	  }

	set_notice_flags(notice_py_data);
	RETURN_post_command_state( 0, 12 );
      }
  }
}


/* go upstairs ('>' command) */
post_command_state_t cmd_upstairs(int dir, int first_time)
{
  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  if (Level->site[Player.x][Player.y].locchar != STAIRS_UP)
    {
      print3("Not here!");
      return base_state;
    }

  p_movefunction_aux(Level->site[Player.x][Player.y].p_locf, TRUE);

  return base_state;
}


/* go downstairs ('>' command) */
post_command_state_t cmd_downstairs(int dir, int first_time)
{
  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  if (Level->site[Player.x][Player.y].locchar != STAIRS_DOWN)
    {
      print3("Not here!");
      return base_state;
    }

  p_movefunction_aux(Level->site[Player.x][Player.y].p_locf, TRUE);
  return base_state;
}



/* set various player options */
/* have to redefine in odefs for next full recompile */
post_command_state_t cmd_setoptions(int dir, int first_time)
{
  int slot = 1,to,done = FALSE;
  int response;

  clearmsg();
  menuclear();

  display_options();

  move_slot(1,1,NUMOPTIONS);
  clearmsg();
  print1("Currently selected option is preceded by highlit >>");
  print2("Move selected option with '>' and '<', ESCAPE to quit.");
  do {
    response = mcigetc();
    switch(response) {
    case 'j':
    case '>':
#ifdef KEY_DOWN
    case KEY_DOWN:
#endif
      to = slot + 1;
#ifndef COMPRESS_SAVE_FILES
      if (to == 8)	/* COMPRESS_OPTION */
	to = 9;
#endif
      slot = move_slot(slot,to,NUMOPTIONS+1);
      break;
    case 'k':
    case '<':
#ifdef KEY_UP
    case KEY_UP:
#endif
      to = slot - 1;
#ifndef COMPRESS_SAVE_FILES
      if (to == 8)	/* COMPRESS_OPTION */
	to = 7;
#endif
      if (to > 0)
	slot = move_slot(slot,to,NUMOPTIONS+1);
      break;
#ifdef KEY_HOME
    case KEY_HOME:
      slot = move_slot(slot,1,NUMOPTIONS+1);
      break;
#endif
#ifdef KEY_LL
    case KEY_LL:
      slot = move_slot(slot,NUMOPTIONS,NUMOPTIONS+1);
      break;
#endif
    case 't':
      if (slot <= NUMTFOPTIONS)
	optionset(pow2(slot-1));
      else if (slot == VERBOSITY_LEVEL)
	Verbosity = TERSE;
      else print3("'T' is meaningless for this option.");
      break;
    case 'f':
      if (slot <= NUMTFOPTIONS)
	optionreset(pow2(slot-1));
      else print3("'F' is meaningless for this option.");
      break;
    case 'm':
      if (slot == VERBOSITY_LEVEL)
	Verbosity = MEDIUM;
      else print3("'M' is meaningless for this option.");
      break;
    case 'v':
      if (slot == VERBOSITY_LEVEL)
	Verbosity = VERBOSE;
      else print3("'V' is meaningless for this option.");
      break;
    case '1':case '2':case '3':case '4':case '5':
    case '6':case '7':case '8':case'9':
      if (slot == SEARCH_DURATION)
	Searchnum = response - '0';
      else print3("A number is meaningless for this option.");
      break;
    case ESCAPE:
      done = TRUE;
      break;
    default: print3("That response is meaningless for this option."); break;
    }
    display_option_slot(slot);
    move_slot(slot,slot,NUMOPTIONS+1);
  } while (! done);
  if (optionp(SHOW_COLOUR))
    colour_on();
  else
    colour_off();

  /* Does this belong here? */
#ifdef AMIGA
  show_screen();
#endif
  set_notice_flags(notice_drew_menu);
  return base_state;
}


/* name an item */
post_command_state_t cmd_callitem(int dir, int first_time)
{
  int index;

  clearmsg();
  index = getitem_prompt("Call -- ", NULL_ITEM);
  if (index == ABORT)
    { return base_state; }

  if (index == CASHVALUE)
    {
      print3("Can't rename cash!");
      return base_state;
    }

  {
    pob obj;
    obj = Player.possessions[index];
    if (obj->known)
      print3("That item is already identified!");
    else {
      print1("Call it:");
      obj->objstr = salloc(msgscanstring());
      clearmsg();
      print2("Also call all similar items by that name? [yn] ");
      if (ynq2() == 'y') {
	Objects[obj->id].objstr = obj->objstr;
      }
    }
  }
  return base_state;
}



/* open a door */
post_command_state_t cmd_opendoor(int dir, int first_time)
{
  int dir_idx;
  int ox,oy;

  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  clearmsg();
  print1("Open --");
  dir_idx = getdir();
  if (dir_idx == no_dir)
    { return base_state; }

  {
    ox = Player.x + Dirs[0][dir_idx];
    oy = Player.y + Dirs[1][dir_idx];
    if (Level->site[ox][oy].locchar == OPEN_DOOR)
      {
	print3("That door is already open!");
	return base_state;
      }

    if (Level->site[ox][oy].locchar == PORTCULLIS)
      {
	print1("You try to lift the massive steel portcullis....");
	if (random_range(100) < Player.str)
	  {
	    print2("Incredible. You bust a gut and lift the portcullis.");
	    Level->site[ox][oy].locchar = FLOOR;
	    lset(ox, oy, CHANGED);
	    set_notice_flags(notice_lev_altered);
	  }
	else
	  {
	    print2("Argh. You ruptured yourself.");
	    p_damage(Player.str,UNSTOPPABLE,"a portcullis");
	  }
	RETURN_post_command_state( 0, Player.speed * 15 / 5 );
      }

    if ((Level->site[ox][oy].locchar != CLOSED_DOOR) ||
	     loc_statusp(ox,oy,SECRET))
      {
	print3("You can't open that!");
	return base_state;
      }
    if (Level->site[ox][oy].aux == LOCKED)
      {
	print3("That door seems to be locked.");
        RETURN_post_command_state( 0, Player.speed * 5 / 5 );
      }

    {
      Level->site[ox][oy].locchar = OPEN_DOOR;
      lset(ox, oy, CHANGED);
      set_notice_flags(notice_lev_altered);
      RETURN_post_command_state( 0, Player.speed * 5 / 5 );
    }
  }
}


/* Try to destroy some location */
post_command_state_t cmd_bash_location(int dir, int first_time)
{
  int dir_idx;
  int ox,oy;

  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  clearmsg();
  print1("Bashing --");
  dir_idx = getdir();
  if (dir_idx == no_dir)
    { return base_state; }

  {
    ox = Player.x + Dirs[0][dir_idx];
    oy = Player.y + Dirs[1][dir_idx];
    if ((Current_Environment == E_CITY) &&
	(ox == 0) &&
	(oy == 0)) {
      print1("Back Door WIZARD Mode!");
      print2("You will invalidate your score if you proceed.");
      morewait();
      if (cinema_confirm("You are about to enable WIZARD Mode.")=='y') {
	print2("You feel like a cheater.");
	setgamestatus(CHEATED);
      }
      else print2("A sudden tension goes out of the air....");
    }
    else {
      if (Level->site[ox][oy].locchar == WALL) {
	print1("You hurl yourself at the wall!");
	p_damage(Player.str,NORMAL_DAMAGE,"a suicidal urge");
      }
      else if (Level->site[ox][oy].locchar == OPEN_DOOR) {
	print1("You hurl yourself through the open door!");
	print2("Yaaaaah! ... thud.");
	morewait();
	p_damage(3,UNSTOPPABLE,"silliness");
	physical_moveplayer (ox, oy);
	/* Monsters are surprised.  Which we indicate by taking less
	   time ourselves.  */
	RETURN_post_command_state( 0,  Player.speed * 1 / 5 );
      }
      else if (Level->site[ox][oy].locchar == CLOSED_DOOR) {
	if (loc_statusp(ox,oy,SECRET)) {
	  print1("You found a secret door!");
	  lreset(ox,oy,SECRET);
	  lset(ox, oy, CHANGED);
	}
	if (Level->site[ox][oy].aux == LOCKED) {
	  if (random_range(50+difficulty()*10) < Player.str) {
	    print2("You blast the door off its hinges!");
	    Level->site[ox][oy].locchar = FLOOR;
	    lset(ox, oy, CHANGED);
	    physical_moveplayer (ox, oy);
	    /* monsters are surprised... */
	    RETURN_post_command_state( 0,  Player.speed * 1 / 5 );
	  }
	  else {
	    print1("Crash! The door holds.");
	    if (random_range(30) > Player.str)
	      p_damage(max(1,statmod(Player.str)),UNSTOPPABLE,"a door");
	  }
	}
	else {
	  print2("You bash open the door!");
	  if (random_range(30) > Player.str)
	    {p_damage(1,UNSTOPPABLE,"a door");}
	  Level->site[ox][oy].locchar = OPEN_DOOR;
	  lset(ox, oy, CHANGED);
	  set_notice_flags(notice_lev_altered);
	  physical_moveplayer (ox, oy);
	  /* monsters are surprised... */
	  RETURN_post_command_state( 0,  Player.speed * 1 / 5 );
	}
      }
      else if (Level->site[ox][oy].locchar == STATUE) {
	statue_random(ox,oy);
      }
      else if (Level->site[ox][oy].locchar == PORTCULLIS) {
	print1("Really, you don't have a prayer.");
	if (random_range(1000) < Player.str) {
	  print2("The portcullis flies backwards into a thousand fragments.");
	  print3("Wow. What a stud.");
	  gain_experience(100);
	  Level->site[ox][oy].locchar = FLOOR;
	  Level->site[ox][oy].p_locf = L_NO_OP;
	  lset(ox, oy, CHANGED);
	  set_notice_flags(notice_lev_altered);
	}
	else {
	  print2("You only hurt yourself on the 3'' thick steel bars.");
	  p_damage(Player.str,UNSTOPPABLE,"a portcullis");
	}
      }
      else if (Level->site[ox][oy].locchar == ALTAR) {
	if ((Player.patron > 0)&&(Level->site[ox][oy].aux == Player.patron)) {
	  print1("You have a vision! An awesome angel hovers over the altar.");
	  print2("The angel says: 'You twit, don't bash your own altar!'");
	  print3("The angel slaps you upside the head for your presumption.");
	  p_damage(Player.hp-1,UNSTOPPABLE,"an annoyed angel");
	}
	else if (Level->site[ox][oy].aux == 0) {
	  print1("The feeble powers of the minor godling are not enough to");
	  print2("protect his altar! The altar crumbles away to dust.");
	  print3("You feel almost unbearably smug.");
	  Level->site[ox][oy].locchar = RUBBLE;
	  Level->site[ox][oy].p_locf = L_RUBBLE;
	  lset(ox, oy, CHANGED);
	  set_notice_flags(notice_lev_altered);
	  gain_experience(5);
	}
	else {
	  print1("You have successfully annoyed a major deity. Good job.");
	  print2("Zzzzap! A bolt of godsfire strikes!");
	  if (Player.rank[PRIESTHOOD] > 0)
	    print3("Your own deity's aegis defends you from the bolt!");
	  p_damage(max(0,random_range(100)-Player.rank[PRIESTHOOD]*20),
		   UNSTOPPABLE,
		   "a bolt of godsfire");
	  if (Player.rank[PRIESTHOOD]*20+Player.pow+Player.level >
	      random_range(200)) {
	    morewait();
	    print1("The altar crumbles...");
	    Level->site[ox][oy].locchar = RUBBLE;
	    Level->site[ox][oy].p_locf = L_RUBBLE;
	    lset(ox, oy, CHANGED);
	    set_notice_flags(notice_lev_altered);	
	    morewait();
	    if (Player.rank[PRIESTHOOD]) {
	      print2("You sense your deity's pleasure with you.");
	      morewait();
	      print3("You are surrounded by a golden glow.");
	      cleanse(1);
	      heal(10);
	    }
	    gain_experience(500);
	  }
	}
      }
      else {
	print3("You restrain yourself from total silliness.");
	return base_state;
      }
    }
    RETURN_post_command_state( 0,  Player.speed * 10 / 5 );
  }
}


/* attempt destroy an item */
post_command_state_t cmd_bash_item(int dir, int first_time)
{
  int item;
  pob obj;

  clearmsg();
  item = getitem_prompt("Destroy an item -- ", NULL_ITEM);
  if (item == ABORT)
    { return base_state; }

  if (item == CASHVALUE)
    {
      print3("You can't destroy cash!");
      return base_state;
    }

  {
    obj = Player.possessions[item];
    if (Player.str+random_range(20) > obj->fragility+random_range(20)) {
      /* damage_item() can free object, but need these after... */
      int oid;
      unsigned char olevel;
      olevel = obj->level;
      oid = obj->id;
      if ( damage_item(obj) )
	{
	  if ( OB_POTION_CHAOS == oid )
	    {
	      /* In either case, chaotic or lawful, destroying a PoCh is
	       * lawful. */
	      Player.alignment += random_range(10);
	      Player.alignment += random_range(10);
	      if ( Player.alignment < 0 )
		{
		  print2("You feel a sense of inner constraint!");
		  gain_experience(olevel * olevel * 5);
		}
	      else
		print2("You feel wonderfully lawful!");
	
	    }
	  else if ( OB_SCROLL_LAW == oid )
	    {
	      Player.alignment -= random_range(10);
	      Player.alignment -= random_range(10);
	      if ( Player.alignment < 0 )
		{
		  print2("You feel deliciously chaotic!");
		  gain_experience(olevel * olevel * 5);
		}
	      else
		print2("You feel a sense of inner turmoil!");
	    }
	  else if (Player.alignment < 0)
	    {
	      print2("That was fun....");
	      gain_experience(olevel * olevel * 5);
	    }
	  set_notice_flags(notice_equip);
	}
    }
    else {
      if (obj->objchar == WEAPON) {
	print2("The weapon turned in your hand -- you hit yourself!");
	p_damage(random_range(obj->dmg+abs(obj->plus)),
		 NORMAL_DAMAGE,
		 "a failure at vandalism");
      }
      else if (obj->objchar == ARTIFACT) {
	print2("Uh Oh -- Now you've gotten it angry....");
	p_damage(obj->level*10,
		 UNSTOPPABLE,
		 "an enraged artifact");
      }
      else {
	print2("Ouch! Damn thing refuses to break...");
	p_damage(1,UNSTOPPABLE,"a failure at vandalism");
      }
    }
    RETURN_post_command_state( 0,  Player.speed * 10 / 5 );

  }
}


post_command_state_t cmd_save(int dir, int first_time)
{
  save(optionp(COMPRESS_OPTION), FALSE);
  return base_state;
}
/* guess what this does */
/* if force is true, exiting due to some problem - don't bomb out */
void save(int compress, int force)
{
  char fname[100];
  int pos, ok = TRUE;

  clearmsg();
  if (!force && ok) {
    ok = (cinema_confirm("You're about to save and exit.") == 'y');
  }
  if (force || ok) {
    sprintf(Str1, "Enter savefile name [default %s]: ", SaveFileName );
    print1(Str1);
    strcpy(fname,msgscanstring());
    if (fname[0] == '\0') {
      /* no file name entered, use default.  DAG */
      strcpy( fname, SaveFileName );
    }
#ifdef MSDOS
    for (pos = 0; fname[pos] && isalnum((int) fname[pos]); pos++)
      ;
#else
    for (pos = 0; fname[pos] && isprint((int) fname[pos]) && !isspace((int) fname[pos]);
      pos++)
      ;
#endif
    if (fname[pos]) {
      sprintf(Str1, "Illegal character '%c' in filename - Save aborted.", fname[pos]);
      print1(Str1);
      ok = FALSE;
    }
#ifdef MSDOS
    if (strlen(fname) > FNAME_MAX_LEN)
    {
      /* WDT -- copied from SYSV block below. */
      sprintf(Str1, "Save name longer than %d characters - Save aborted.",
	FNAME_MAX_LEN);
      print1(Str1);
      ok = FALSE;
    }
#else
# ifdef SYSV
    if (strlen(fname) > FNAME_MAX_LEN - EXT_LENGTH - 1)
    {
      sprintf(Str1, "Save name longer than %d characters - Save aborted.",
	FNAME_MAX_LEN - EXT_LENGTH - 1);
      print1(Str1);
      ok = FALSE;
    }
# endif
#endif
    if (ok)
      {
	if (save_game(compress,fname)) {
	  endgraf();
	  printf("Bye!\n");
	  exit(0);
	}
	else
	  print1("Internal error -- unable to save.");
      }
  }
  if (force) {
    morewait();
    clearmsg();
    print1("The game is quitting - you will lose your character.");
    print2("Try to save again? ");
    if (ynq2() == 'y')
      save(compress, force);
  }
  /* if we get here, we failed to save */
  if (Current_Environment != E_COUNTRYSIDE)
    { monster_tickets_on(); }

}


/* close a door */
post_command_state_t cmd_closedoor(int dir, int first_time)
{
  int dir_idx;

  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  clearmsg();

  print1("Close --");
  dir_idx = getdir();
  if (dir_idx == no_dir)
    { return base_state; }

  {
    int ox,oy;
    ox = Player.x + Dirs[0][dir_idx];
    oy = Player.y + Dirs[1][dir_idx];
    if (Level->site[ox][oy].locchar == CLOSED_DOOR) {
      print3("That door is already closed!");
      return base_state;
    }
    if (Level->site[ox][oy].locchar != OPEN_DOOR)
      {
	print3("You can't close that!");
	return base_state;
      }

    {
      Level->site[ox][oy].locchar = CLOSED_DOOR;
      lset(ox, oy, CHANGED);
      set_notice_flags(notice_lev_altered);
      RETURN_post_command_state( 0, Player.speed * 2 / 5 );
    }
  }
}

