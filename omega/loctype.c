/*
  loctype.c - location types
  Copyright (C) 2003 by Willian Sheldon Simms

  licensed as a part of Omega under the terms of the Omega
  license which can be found in the Omega source distribution
  in the directory omega-roguelike/omega/lib/license.txt
*/

#include "loctype.h"
#include "defs.h"

int is_door (location_t * site)
{
  if (site->loctype > LOCTYPE_DOOR_LAST) return FALSE;
  if (site->loctype < LOCTYPE_DOOR_FIRST) return FALSE;
  return TRUE;
}

int is_closed_door (location_t * site)
{
  if (is_door(site) == FALSE) return FALSE;
  if (site->loctype == LOCTYPE_DOOR_OPEN) return FALSE;
  return TRUE;
}

int is_locked_door (location_t * site)
{
  if (site->loctype == LOCTYPE_DOOR_LOCKED) return TRUE;
  if (site->loctype == LOCTYPE_DOOR_SECRET_LOCKED) return TRUE;
  return FALSE;
}

int is_secret_door (location_t * site)
{
  if (site->loctype == LOCTYPE_DOOR_SECRET_LOCKED) return TRUE;
  if (site->loctype == LOCTYPE_DOOR_SECRET_UNLOCKED) return TRUE;
  return FALSE;
}

/* end */
