/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/* Functions for level storage management.  Ie, functions to find,
   allocate, clear, and free levels */

#include "glob.h"
#include "lev.h"
#include "util.h"
#include "load.h"
#include "memory.h"
#include "save.h"
#include "game_time.h"
#include <time.h>  /* Just for time() */

/* Globals */
struct level *Level=NULL;             /* Pointer to current Level */


/*  ***********************************  */

int cache_type(int env_idx)
{
  env_loader_t *   p_loader =
    env_loader_find( env_idx, env_loader_table );
  if(!p_loader) { return cache_not_a_level; }
  return p_loader->cache_type;
}

int level_matches(env_params_t* p_env_params, plv the_level)
{
  return
    (the_level
     && (the_level->environment == p_env_params->env_idx)
     && (the_level->subtype     == p_env_params->subtype));
}


/* I removed all uses outside of this file, but to be conservative
   I'll let these variables continue to exist. */
/* WDT HACK! The above comment confuses me. Should they not exist? */

static struct level *City=NULL;              /* The city of Rampart */
static struct level *TempLevel=NULL;         /* Place holder */
static struct level *Dungeon=NULL;           /* Pointer to current Dungeon */

void lev_init(void)
{ City = Level = TempLevel = Dungeon = NULL; }
void lev_term(void)
{}


/* Get a completely fresh level. */
plv get_fresh_level(void)
{
  plv thislevel = checkmalloc(sizeof(levtype));
  memset(thislevel,0,sizeof(levtype));
  clear_level(thislevel);
  return thislevel;
}



#ifdef DJGPP
void check_memory(void)
{
  clear_screen();
  print1("There should be over 300 K free on the drive.");
  print2("Save _before_ the free space gets below 300 K.");
  morewait();
  system("dir");
  morewait();
  clear_screen();
  xredraw();
}

#else /*!defined DJGPP */
void check_memory(void) {}

#endif

/* Deallocate current dungeon */
void free_dungeon(void)
{
  plv tlv;

  while (Dungeon != NULL) {
    tlv = Dungeon;
    Dungeon = Dungeon->next;
    free_level(tlv);
  }
  Dungeon = NULL;
}

static void push_level_in_dungeon_stack(plv new_level)
{
  new_level->next = Dungeon;
  Dungeon = new_level;
}

/* Tries to find the level of depth levelnum in Dungeon;
   Returns a level in any case. */
static plv in_dungeon_find_level(int levelnum)
{
  struct level * new_level = NULL;
  if (Dungeon != NULL)
    {
      struct level * dungeon   = Dungeon;
      while((dungeon->next != NULL) && (dungeon->depth != levelnum))
	{ dungeon = dungeon->next; }
      if (dungeon->depth == levelnum)
	{
	  dungeon->last_visited = time(NULL);
	  return(dungeon);
	}
    }

  /* It's a fresh level, so make sure we can find it again. */
  new_level = get_fresh_level();
  push_level_in_dungeon_stack(new_level);
  return new_level;
}


/*
   The logic is parallel to general_find_level.  TempLevel, City, and
   Dungeon are kept completely separate.  A level that belongs in one
   cache doesn't belong in any of the others, ever.

   Called to release any level, after NULLing TempLevel if it will be
   used.  */
void free_or_cache_level(plv oldlevel)
{
  if(!oldlevel) { return; }


  switch(cache_type(oldlevel->environment))
  {
    default:
      free_level(oldlevel);
      break;

    case cache_in_city:
      /* City is always the same level, so no need for fancy checking.  */
      City = oldlevel;
      break;

    case cache_in_temp:
      if(TempLevel)
	{
	  assert(TempLevel != Level);
	  assert(TempLevel != City);
	  free_level(TempLevel);
	}
    TempLevel = oldlevel;
    break;

    case cache_in_dungeon:
    /* Do nothing because all dungeon levels are kept until a new dungeon wipes them away.  */
      break;
	
    case cache_not_a_level:
      /* Error because countryside is not a level. */
      error("Shouldn't be trying to save countryside's \"Level\"");
      break;
  }
}



plv general_find_level(env_params_t* p_env_params)
{
  int env_idx = p_env_params->env_idx;
  plv old_level = Level;

  if(level_matches(p_env_params, Level))
    { return Level; }
  Level = NULL;

  switch(cache_type(env_idx))
    {
    default:
    case cache_dont:
      /* There's no cache to check. */
      break;

      case cache_in_city:
	if (City)
	  {
	    Level = City;
	  }
	break;

      case cache_in_temp:
	if(level_matches(p_env_params, TempLevel))
	  {
	    Level = TempLevel;
	    TempLevel = NULL;
	  }	
	break;

      case cache_in_dungeon:
	/* Maybe change dungeons. */
      if(env_idx != Current_Dungeon)
	{
	  free_dungeon();
	  Current_Dungeon = env_idx;
	}
      Level = in_dungeon_find_level(p_env_params->subtype);
      break;
	
      case cache_not_a_level:
	error("Shouldn't be trying to retrieve countryside's \"Level\"");
	break;
    }

  free_or_cache_level(old_level);

  if(Level) { return Level; }

  /* No cache hit, so proceed without it. */
  Level = get_fresh_level();
  return Level;
}


int release_some_lev_memory(void)
{
  level * lp;
  level ** p_lp;
  level ** oldest;

  oldest = p_lp = &Dungeon;
  for (lp = Dungeon; lp; lp = lp->next)
    {
      if ((*oldest)->last_visited > lp->last_visited)
      { oldest = p_lp; }
      p_lp = &(lp->next);
    }

  if (*oldest && (*oldest != Level))
    {
      lp = *oldest;
      *oldest = (*oldest)->next;
      free_level(lp);
      return TRUE;
    }

  return FALSE;
}

/**** Save and restore ****/

static int maybe_save_level(FILE *fd, plv level)
{
  int ok = 1;
  char really_save = 0;
  if(level == Level)
    { really_save = 1; }
  else
    if(level == NULL)
    { really_save = 2; }

  SAVE_SCALAR(char, really_save);
  if(really_save == 0)
    {
      ok &= save_level(fd,level);
    }
  return ok;
}

static plv maybe_restore_level(FILE *fd, int version)
{
  char really_save;
  RESTORE_SCALAR(char, really_save);
  switch(really_save)
    {
    default:
      error("Value is not meaningful");
      return NULL;

      case 0:
	return restore_level(fd, version);

      case 1:
	return Level;

      case 2:
	return NULL;
    }
}



/* Save the levels we know, which mostly means the cache, so that game
   behavior isn't changed by save/restore.
*/
int save_known_levels(FILE *fd)
{
  int ok = 1;
  ok &= save_check(fd, "knlv");
  ok &= save_country(fd);
  monster_tickets_off(TRUE);
  ok &= save_level(fd,Level);

  /* Save the fixed caches.  */
  ok &= maybe_save_level(fd,City);
  ok &= maybe_save_level(fd,TempLevel);

  /* save each dungeon level, in order. */
  {
    plv dungeon = Dungeon;
    while (dungeon != NULL)
      {
	ok &= maybe_save_level(fd, dungeon);
	dungeon = dungeon->next;
      }
    /* Mark the end by a NULL, which is legal for maybe_save_level. */
    ok &= maybe_save_level(fd, NULL);
  }

  return ok;
}


void restore_known_levels(FILE *fd, int version)
{
  plv prime_level;
  restore_check(fd, "knlv");
  restore_country(fd, version);
  prime_level = restore_level(fd, version);

  /* Restore the caches. */
  City = maybe_restore_level(fd, version);
  TempLevel = maybe_restore_level(fd, version);

  /* restore each dungeon level, in order. */
  {
    plv head = maybe_restore_level(fd, version);
    Dungeon = head;
    while (head)
      {
	/* The NULL terminator will finish the loop. */
	plv tail = maybe_restore_level(fd, version);
	head->next = tail;
	head = tail;
      }
  }
  Level = prime_level;
}


/* Free up monsters and items on a level*/
void free_level (plv level)
{
  int i,j;

  fully_free_monster_list(&level->monster_list);
  fully_free_knowable_locs(&level->knowable_locs);
  fully_free_special_locs(&level->special_locs);
  for (i = 0; i < level->level_width; ++i)
    for (j = 0; j < level->level_length; ++j)
      { fully_free_object_list(&level->site[i][j].object_list); }

  free(level);
}


/* erase the level w/o deallocating it*/
/* Overlaps some with free_level.  But it also sets the level to
   neutral information, which free_level doesn't.  It's as if free_level takes
   a shortcut because it knows the level won't be used again.  */
void clear_level (plv level)
{
  int i,j;

  if (level != NULL)
    {
      level->generated = FALSE;
      level->numrooms = 0;
      level->level_length = 0;
      level->level_width = 0;
      level->tunnelled = 0;
      level->depth = 0;
      level->next = NULL;
      level->last_visited = time(NULL);
      level->clicks_contain_time = FALSE;

      fully_free_monster_list(&level->monster_list);
      fully_free_knowable_locs(&level->knowable_locs);
      fully_free_special_locs(&level->special_locs);
      for(i = 0; i < MAXWIDTH; ++i)
        {
          for(j = 0; j < MAXLENGTH; ++j)
            {
              location * loc = &(level->site[i][j]);

	      fully_free_object_list(&loc->object_list);

              loc->locchar = WALL;
              loc->showchar = SPACE;
              loc->creature = NULL;


              /* PGM: clear_level is called from restore_level, before it
                 knows anything about the current environment, which is where
                 difficulty() gets its information! */
              loc->aux = 20 * difficulty();
              loc->buildaux = 0;
              loc->p_locf = L_NO_OP;
              loc->lstatus = 0;
              loc->roomnumber = RS_WALLSPACE;
            }
        }
    }
}

