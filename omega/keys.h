/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/* Declarations dealing with key lookup. */

#include "defs.h"
#include "lut.h"
#include "command.h"

/*
   Key is the key that evokes this action.

   Dir is direction, only used to inform directional commands.

   Command is the command to call.

*/
typedef struct cmd_lookup_t
{
  int         key;
  int         dir;
  command_f_t command;
}
cmd_lookup_t;

def_lookup_table_type(cmd_lookup_t, cmd_lookup);

extern char * how_to_escape;

extern cmd_lookup_table_t * cmd_lookup_tables[];
extern const int num_cmd_lookup_tables;

extern cmd_lookup_table_t country_command_table;
extern cmd_lookup_table_t dungeon_command_table;


