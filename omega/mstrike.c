/* omega copyright (c) 1987,1988,1989 by Laurence Raphael Brothers */
/* mstrike.c */
/* monster strike functions */

#include "glob.h"
#include "liboutil/list.h"
#include "util.h"

void m_firebolt (monster_t * m, player_t * p)
{
  fbolt(m->x, m->y, p->x, p->y, m->hit, m->dmg);
}

void m_nbolt (monster_t * m, player_t * p)
{
  nbolt(m->x, m->y, p->x, p->y, m->hit, m->dmg);
}


void m_lball (monster_t * m, player_t * p)
{
  lball(m->x, m->y, p->x, p->y, m->dmg);
}

void m_fireball (monster_t * m, player_t * p)
{
  fball(m->x, m->y, p->x, p->y, m->dmg);
}

void m_snowball (monster_t * m, player_t * p)
{
  snowball(m->x, m->y, p->x, p->y, m->dmg);
}

static void apply_vanish_monster (void * vmon)
{
  monster * mon;
  mon = vmon;
  plotspot(mon->x, mon->y, FALSE);
}
    
void m_blind_strike (monster_t * m, player_t * p)
{
  if (p->status[BLINDED] > 0) return;
  if (is_monster_visible(p,m) == FALSE) return;
  if (distance(m->x, m->y, p->x, p->y) >= 5) return;

  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s gazes at you menacingly", m->monstring);
  else
    sprintf(Str2, "%s gazes at you menacingly", m->monstring);

  mprint(Str2);

  if (p_immune(p, GAZE) == FALSE)
    {
      list_apply(m->dlevel->monster_list, apply_vanish_monster);
      p->status[BLINDED] = 1 + random_range(4);
      mprint("You've been blinded!");
    }
  else
    mprint("You gaze steadily back....");
}

void m_strike_sonic (monster_t * m, player_t * p)
{
  if (m->uniqueness == COMMON)
    sprintf(Str2, "The %s screams at you!", m->monstring);
  else
    sprintf(Str2, "%s ", m->monstring);

  p_damage(m->dmg, OTHER_MAGIC, "a sonic blast");
}
