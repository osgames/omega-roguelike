/*
 * memory.c
 * omega : Copyright (c) 1987-2001 by Laurence Raphael Brothers
 */

#include "glob.h"
#include "liboutil/allocator.h"

make_allocator(object, (256 * sizeof(object)), next, checkmalloc);
make_allocator(monster, (128 * sizeof(monster)), next, checkmalloc);
make_allocator(account, (64 * sizeof(account)), next_account, checkmalloc);

void print_allocation_info (void)
{
  printf("objects  (used/free/total): %d/%d/%d in %d blocks\n",
         object_count - object_free_count, object_free_count, object_count, object_block_count);
  printf("monsters (used/free/total): %d/%d/%d in %d blocks\n",
         monster_count - monster_free_count, monster_free_count, monster_count, monster_block_count);
  printf("accounts (used/free/total): %d/%d/%d in %d blocks\n",
         account_count - account_free_count, account_free_count, account_count, account_block_count);

  printf("monsters on level: %d\n", (int)list_size(Level->monster_list));

  putchar('\n');
  fflush(stdout);
}

/* Imitate the pseudo-standard function lfind, as documented at
 * http://www.cplusplus.com/ref/cstdlib/lfind.html. */
void *my_lfind( const void * key, const void * base, size_t num, size_t width,
                int (*fncomparison)(const void *, const void * ) )
{
    const char *table = base;
    unsigned int i;
    for ( i = 0; i<num*width; i+= width )
    {
        if ( !fncomparison(key, table+i) )
            return (void*)(table+i);
    }
    return NULL;
}

