/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/*  *** Player commands involving movement ***  */

#include "glob.h"
#include "game_time.h"
#include "lev.h"
#include "util.h"
#include "findloc.h"
#include "flow.h"
#include "command.h"


typedef int loc_flags_t;
typedef int loc_movecost_t;

/* Location's capability data is bitwise.  That's not strictly needed
   for the moment, but extending to richer possibilities, like
   disarming trapped sites vs entering them, requires parallel
   possibilities.
*/
enum
{
  locb_can_enter         = 0x0001,
  locb_warn              = 0x0002,
  locb_horse_wont_enter  = 0x0004,
  locb_has_monster       = 0x0008, /* Not a property of terrain. */
  locb_can_open          = 0x0010, /* Not used yet. */
  locb_can_close         = 0x0020, /* Not used yet. */
  locb_can_disarm        = 0x0040, /* Not used yet. */
  locb_can_interact      = 0x0080, /* So predictive running could avoid
				      distractions. Not used yet. */
  locb_interesting       = 0x0100, /* So running can stop near
				      interesting things. */
  locb_has_objects       = 0x0200, /* Not a property of terrain. */

  locb_actable_conditions =
  locb_can_enter | locb_has_monster,
};


/* The actions possible toward a location.  Actions (now) express only
   forward possibility.  Eg, "can_enter_but_horse_wont" is no longer a
   valid action.
*/
enum what_can_do
{
  can_nothing, can_deal_monster, can_enter, can_enter_carefully,
};


/* flags and cost_to_traverse are theoretically separate, but if
   locb_warn is set, cost_to_traverse is hier, and if locb_can_enter
   is not set, cost_to_traverse must be maximal.  */
typedef struct terrain_can_t
{
  loc_movecost_t cost_to_traverse;
  loc_flags_t    flags;
} terrain_can_t;

#define RETURN_TERRAIN_CAN(COST, FLAGS) \
{\
  terrain_can_t ret_val = { COST, FLAGS, };\
  return ret_val;\
}


/* Forward declarations */
static bool enterable_p(terrain_can_t potentials);
static void say_country_motion(void);
static void p_cmovefunction(void);
static post_command_state_t movepincountry( int,int );
static loc_flags_t c_loc_flags(int x, int y);


post_command_state_t cmd_stay(int dir, int first_time)
{
  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }
  p_movefunction_aux(Level->site[Player.x][Player.y].p_locf, TRUE);
  RETURN_post_command_state( 0,  5 );
}

post_command_state_t cmd_single_move(int dir, int first_time)
{
  post_command_state_t state = cmd_move(dir, first_time);
  state.repeats_allowed = 0;
  return state;
}


post_command_state_t cmd_move(int dir, int first_time)
{
  int delta_x = Dirs[0][dir];
  int delta_y = Dirs[1][dir];


  if (Current_Environment == E_COUNTRYSIDE)
    {
      return movepincountry(delta_x,delta_y);
    }
  else
    {
      return moveplayer(delta_x,delta_y, first_time);
    }
}


/* Here the player will try to move, not some other action. */
post_command_state_t true_moveplayer (int x, int y)
{
  if (Player.maxweight < Player.itemweight
      && random_range(2)
      && (!Player.status[LEVITATING]))
    {
      if (pflagp(MOUNTED))
	{
	  print1("Your horse refuses to carry you and your pack another step!");
	  print2("Your steed bucks wildly and throws you off!");
	  p_damage(10, UNSTOPPABLE,"a cruelly abused horse");
	  resetpflag(MOUNTED);
	  summon(-1, HORSE);
	}
      else
	{
	  p_damage(1, UNSTOPPABLE, "a rupture");
	  print3("The weight of your pack drags you down. You can't move.");
	}

      return base_state;
    }

  {
    int duration_scale = 5;
    int repeats = 300;

    physical_moveplayer(x, y);
    /* causes moves to take effectively 30 seconds in town without
       monsters being sped up compared to player */
    /* With tickets, this is now broken until monster move times are
	rescaled similarly, but combat scales OK.  */
    if (Current_Environment == E_CITY ||
	Current_Environment == E_VILLAGE)
      {
	duration_scale = 30;
      }

    RETURN_post_command_state( repeats,
			       Player.speed * duration_scale / 5 );
  }
}

/* Here the player will physically move, no tests. */
void physical_moveplayer (int x, int y)
{
  Player.x = x;
  Player.y = y;
  set_notice_flags(notice_loc);
  p_movefunction(Level->site[x][y].p_locf);
}



/* handle a h,j,k,l, etc. */
post_command_state_t movepincountry(int dx, int dy)
{
  int i,takestime = TRUE;
  if ((Player.maxweight < Player.itemweight) &&
      random_range(2) &&
      (! Player.status[LEVITATING])) {
    if (pflagp(MOUNTED)) {
      print1("Your horse refuses to carry you and your pack another step!");
      print2("Your steed bucks wildly and throws you off!");
      p_damage(10,UNSTOPPABLE,"a cruelly abused horse");
      resetpflag(MOUNTED);
      set_notice_flags( notice_x_mounted );
      morewait();
      print1("With a shrill neigh of defiance, your former steed gallops");
      print2("off into the middle distance....");
      if (Player.packptr != 0) {
	morewait();
	print1("You remember (too late) that the contents of your pack");
	print2("were kept in your steed's saddlebags!");
	for(i=0;i<MAXPACK;i++) {
	  if (Player.pack[i] != NULL)
	    free_obj( Player.pack[i], TRUE );
	  Player.pack[i] = NULL;
	}
	Player.packptr = 0;
	set_notice_flags( notice_equip );
      }
    }
    else {
      p_damage(1,UNSTOPPABLE,"a rupture");
      print3("The weight of your pack drags you down. You can't move.");
    }

    RETURN_post_command_state( 0, 10 );
  }

  if (Player.status[IMMOBILE] > 0)
    {
      print3("You are unable to move.");
      return base_state;
    }

  if (pflagp(LOST))
    {
      print3("Being lost, you strike out randomly....");
      set_notice_flags(notice_dont_repeat);
      morewait();
      /* Avoid terrain the player would probably refuse to enter
	 anyways. */
      do
	{
	  dx = random_range(3)-1;
	  dy = random_range(3)-1;
	}
      while(c_loc_flags(Player.x+dx,Player.y+dy) & locb_warn);
    }

  if (!moveable_p(Player.x+dx,Player.y+dy))
    {
      return base_state;
    }


  {
    Player.x += dx;
    Player.y += dy;
    if (! pflagp(MOUNTED))
      {
	pob boots = Player.possessions[O_BOOTS];
	if (boots && (boots->usef == I_BOOTS_7LEAGUE)) {
	  takestime = FALSE;
	  if (boots->blessing < 0) {
	    print1("Whooah! -- Your boots launch you into the sky....");
	    print2("You come down in a strange location....");
	    teleport_to_random_in_country();
	    morewait();
	    clearmsg();
	    print1("Your boots disintegrate with a malicious giggle...");
	    dispose_lost_objects(1,boots);
	    set_notice_flags(notice_equip);
	  }
	  else if (boots->known != 2) {
	    print1("Wow! Your boots take you 7 leagues in a single stride!");
	    know_object_specialness(boots);
	  }
	}
      }
    if (pflagp(LOST) && (Precipitation < 1) &&
	c_statusp(Player.x, Player.y, SEEN)) {
      print3("Ah! Now you know where you are!");
      morewait();
      resetpflag(LOST);
    }
    /* No need to print it, now that Locw says "Lost". */

    if (Precipitation > 0) { Precipitation--; }
    c_set(Player.x, Player.y, SEEN);
    say_country_motion();
    set_notice_flags(notice_loc);
    {
      long duration =
	takestime ? terrain_duration() : 0;
      p_cmovefunction();
      RETURN_post_command_state( 30, duration );
    }
  }
}


static void say_country_motion(void)
{

  if (Player.patron == DRUID) {
    switch(random_range(32)) {
      case 0:print2("Along the many paths of nature..."); break;
      case 1:print2("You move swiftly through the wilderness."); break;
    }
  }
  else if (pflagp(MOUNTED)) {
    switch(random_range(32)) {
      case 0:
      case 1:print2("Clippity Clop.");break;
      case 2:print2("....my spurs go jingle jangle jingle....");break;
      case 3:print2("....as I go riding merrily along....");break;
    }
  }
  else if (Player.possessions[O_BOOTS] &&
      Player.possessions[O_BOOTS]->usef == I_BOOTS_7LEAGUE) {
    switch(random_range(32)) {
      case 0:print2("Boingg!"); break;
      case 1:print2("Whooosh!"); break;
      case 2:print2("Over hill, over dale...."); break;
      case 3:print2("...able to leap over 7 leagues in a single bound....");
	break;
    }
  }
  else if (Player.status[SHADOWFORM]) {
    switch(random_range(32)) {
      case 0:print2("As swift as a shadow."); break;
      case 1:print2("\"I walk through the trees...\""); break;
    }
  }
  else switch(random_range(32)) {
    case 0:print2("Trudge. Trudge."); break;
    case 1:print2("The road goes ever onward...."); break;
  }
}

static void p_cmovefunction(void)
{
  switch(Country[Player.x][Player.y].current_terrain_type)
    {
    default:
      /* Do nothing. */
      break;

    case CHAOS_SEA:
      mprint("You have entered the sea of chaos...");
      morewait();
      l_chaos();
      break;

    case CITY:
      if (pflagp(LOST))
	{
	  resetpflag(LOST);
	  mprint("Well, I guess you know where you are now....");
	}
      break;

    case VILLAGE:
      if (pflagp(LOST))
	{
	  resetpflag(LOST);
	  mprint("The village guards let you know where you are....");
	}
      break;

    case CAVES:
      mprint("You notice a concealed entrance into the hill.");
      break;

    case CASTLE:
      mprint("The castle is hewn from solid granite. The drawbridge is down.");
      break;

    case TEMPLE:
      mprint("You notice an entrance conveniently at hand.");
      break;

    case MAGIC_ISLE:
      mprint("There is a narrow causeway to the island from here.");
      break;

    case STARPEAK:
      mprint("The top of the mountain seems to glow with a allochroous aura.");
      break;

    case DRAGONLAIR:
      mprint("You are at a cave entrance from which you see the glint of gold.");
      break;

    case PALACE:
      mprint("The palace dungeons are still intact...");
      break;

    case VOLCANO:
      mprint("A shimmer of heat lightning plays about the crater rim.");
      break;
    }

  outdoors_random_event();
}


int terrain_duration_base(int x, int y)
{
  int base_duration_hours = 0;
  int druid_speedup = 0;

  switch(Country[x][y].current_terrain_type)
    {
    default:
      base_duration_hours = 1;
      break;

    case RIVER:
      base_duration_hours = 3;
      break;

    case ROAD:
      base_duration_hours = 1;
      break;

    case PLAINS:
      base_duration_hours = 2;
      druid_speedup = 50;
      break;

    case TUNDRA:
      base_duration_hours = 2;
      druid_speedup = 50;
      break;

    case FOREST:
      base_duration_hours = 3;
      druid_speedup = 50;
      break;

    case JUNGLE:
      base_duration_hours = 4;
      druid_speedup = 50;
      break;

    case DESERT:
      base_duration_hours = 4;
      druid_speedup = 50;
      break;

    case MOUNTAINS:
      base_duration_hours = 8;
      druid_speedup = 38;
      break;

    case PASS:
      base_duration_hours = 1;
      break;

    case CHAOS_SEA:
      base_duration_hours = 1;
      break;

    case SWAMP:
      base_duration_hours = 8;
      druid_speedup = 25;
      break;

    case CITY:
      base_duration_hours = 0;
      break;

    case VILLAGE:
      base_duration_hours = 0;
      break;

    case CAVES:
      base_duration_hours = 1;
      break;

    case CASTLE:
      base_duration_hours = 1;
      break;

    case TEMPLE:
      base_duration_hours = 1;
      break;

    case MAGIC_ISLE:
      base_duration_hours = 1;
      break;

    case STARPEAK:
      base_duration_hours = 1;
      break;

    case DRAGONLAIR:
      base_duration_hours = 1;
      break;

    case PALACE:
      base_duration_hours = 1;
      break;

    case VOLCANO:
      base_duration_hours = 1;
      break;
    }


  if(
     (Player.status[SHADOWFORM])
     || (pflagp(MOUNTED))
     || (Player.patron == DRUID))
    {
      /* The logic of fasterness could be clearer.  Mountedness,
	 shadowform, and druidness are mixed in together, and
	 advanced druidness previously had an effect only in the
	 forest.  */
      int time_coefficient;
      if ((Player.rank[PRIESTHOOD] > 0) && Player.patron == DRUID)
	{ druid_speedup += druid_speedup / 2; }
      time_coefficient = 100 - druid_speedup;
      SET_TO_MAX(time_coefficient, 0);
      return (base_duration_hours * time_coefficient) / 100;
    }
    else
      {
	return base_duration_hours;
      }

}

long terrain_duration(void)
{
  return (long)terrain_duration_base(Player.x, Player.y) * TICKS_PER_HOUR;
}


/** Enterability **/


/* site_potentials and site_shadow_potentials should be the only
   functions that report terrain's intrinsic potentials. */
terrain_can_t site_shadow_potentials(plc site)
{
  switch(site->p_locf)
    {
    case L_CHAOS:
    case L_ABYSS:
    case L_VOID:
      RETURN_TERRAIN_CAN(2, locb_can_enter | locb_warn);
	
    default:
      RETURN_TERRAIN_CAN(1, locb_can_enter);
    }
}


static terrain_can_t site_potentials(plc site)
{

  /* Secret locations appear to be walls. */
  if(site->lstatus & SECRET)
    { RETURN_TERRAIN_CAN( 100, 0); }

  switch(site->locchar)
    {
    default:
      RETURN_TERRAIN_CAN(1, locb_can_enter);

    case ALTAR:
      RETURN_TERRAIN_CAN(2, locb_can_enter | locb_can_interact | locb_interesting);
    case OPEN_DOOR:
      if(site->p_locf == L_NO_OP)
	{ RETURN_TERRAIN_CAN(1, locb_can_enter); }
      else
	{ RETURN_TERRAIN_CAN(1, locb_can_enter | locb_can_interact); }

    case STAIRS_DOWN:
    case STAIRS_UP:
      RETURN_TERRAIN_CAN(1, locb_can_enter | locb_interesting);

    case WALL:
    case PORTCULLIS:
      RETURN_TERRAIN_CAN(100, 0);

    case STATUE:
    case CLOSED_DOOR:
      RETURN_TERRAIN_CAN(100, locb_interesting);

    case HEDGE:
    case LAVA:
    case FIRE:
    case WHIRLWIND:
    case ABYSS:
    case VOID_CHAR:
    case RUBBLE:
    case LIFT:
    case TRAP:
      RETURN_TERRAIN_CAN(3, locb_can_enter | locb_warn | locb_horse_wont_enter);

    case WATER:
      if(site->p_locf == L_WATER)
	{ RETURN_TERRAIN_CAN(3, locb_can_enter | locb_warn); }
      else
	{ RETURN_TERRAIN_CAN(4,
			 locb_can_enter | locb_warn |
			 locb_horse_wont_enter); }
    }
}

loc_flags_t site_flags(plc site)
{
  terrain_can_t can = site_potentials(site);
  return can.flags;
}


/** Information functions to support predictive moving (citymove, etc) **/

/* SHADOWFORM and MOUNTED are considered here. */
static terrain_can_t predict_loc_potentials(int x, int y)
{
  if (! inbounds(x,y))
    { RETURN_TERRAIN_CAN(100, 0); }

  /* Only allow grids we know about. */
  if (!loc_statusp(x,y,SEEN))
    { RETURN_TERRAIN_CAN(100, 0); }

  /* Shadowform has its own simple tests. */
  if (Player.status[SHADOWFORM])
    {
      return site_shadow_potentials(&Level->site[x][y]);
    }


  { /* Immobile monsters block entering.  Mobile monsters will prolly
       have moved.
    */
    pmt mon = get_monster(Level, x, y);
    if (mon &&
	/* Can't move... */
	(!m_statusp(mon, MOBILE) ||
	 /* ...or has no reason to move.  (Esp. for guards) */
	 !m_statusp(mon, WANDERING | HOSTILE | NEEDY)))
      { RETURN_TERRAIN_CAN(100, 0); }
  }

  {
    terrain_can_t potentials =
      site_potentials(&Level->site[x][y]);
    if (pflagp(MOUNTED) && (potentials.flags & locb_horse_wont_enter))
      { RETURN_TERRAIN_CAN(100, 0); }
    return potentials;
  }
}

static loc_flags_t predict_loc_flags(int x, int y)
{
  terrain_can_t potentials = predict_loc_potentials( x, y);
  return potentials.flags;
}

/* These could consider cost, by setting cost_threshhold first. */
bool predict_can_enter_easily(int x, int y)
{
  int locb = predict_loc_flags(x, y);
  return (locb & locb_can_enter) && !(locb & locb_warn);
}

/* Unused. */
bool predict_can_enter_at_all(int x, int y)
{
  int locb = predict_loc_flags(x, y);
  return (locb & locb_can_enter);
}



/* Non-predictive enterability */


/* SHADOWFORM and MOUNTED are considered here. */
static terrain_can_t loc_potentials(int x, int y)
{
  if (! inbounds(x,y))
    { RETURN_TERRAIN_CAN(100, 0); }

  /* Shadowform has its own simple tests. */
  if (Player.status[SHADOWFORM])
    {
      return site_shadow_potentials(&Level->site[x][y]);
    }

  { /* Presence of monster blocks other possibilities. */
    pmt mon = get_monster(Level, x, y);
    if (mon)
      { RETURN_TERRAIN_CAN(100, locb_has_monster); }
  }

  return site_potentials(&Level->site[x][y]);
}

static loc_flags_t loc_flags(int x, int y)
{
  terrain_can_t potentials = loc_potentials(x, y);
  return potentials.flags;
}

static terrain_can_t c_loc_potentials(int x, int y)
{
  /* NB, cost to traverse is not the same as duration, they just
      tend to often be the same number.  */
  loc_movecost_t duration = terrain_duration_base( x, y);

  switch(Country[x][y].current_terrain_type)
    {
    default:
      RETURN_TERRAIN_CAN(duration, locb_can_enter);
      break;

    case ROAD:
    case PASS:
      RETURN_TERRAIN_CAN(duration, locb_can_enter);
      break;

    case CITY:
    case VILLAGE:
    case CAVES:
    case CASTLE:
    case TEMPLE:
    case MAGIC_ISLE:
    case STARPEAK:
    case DRAGONLAIR:
    case PALACE:
    case VOLCANO:
      RETURN_TERRAIN_CAN(duration, locb_can_enter | locb_interesting);
      break;

    case CHAOS_SEA:
    case MOUNTAINS:
      /* The terrains that really have warning flags.  All others are
	 faking it until duration-cost is considered. */
      RETURN_TERRAIN_CAN(duration + 50, locb_can_enter | locb_warn);
      break;
    }
  /*NOTREACHED*/
}

static loc_flags_t c_loc_flags(int x, int y)
{
  terrain_can_t potentials = c_loc_potentials(x, y);
  return potentials.flags;
}

/* Return the visible potentials.  */
static terrain_can_t loc_visible_potentials( int dir, int col, int row)
{
  /* New location */
  int y = row + Dirs[1][dir];
  int x = col + Dirs[0][dir];

  if ( !inbounds(x,y) )
    { RETURN_TERRAIN_CAN(100, 0); }

  if(Current_Environment == E_COUNTRYSIDE)
    {
      if(!c_statusp(col, row, SEEN))
	{ RETURN_TERRAIN_CAN(1, locb_can_enter); }
      return c_loc_potentials(x, y);
    }

  /* Shadowform ignored for now.  Perhaps shadowform could have its
     own simpler running style: stop when the actual terrain would change.  */

  {
    pmt mon = get_monster(Level, x, y);
    if (mon &&
	(!m_statusp(mon, M_INVISIBLE) || Player.status[TRUESIGHT]))
      { RETURN_TERRAIN_CAN(100, locb_has_monster); }
  }

  /* Unknown spaces look like one could enter them.  */
  if(!loc_statusp(col, row, SEEN))
    { RETURN_TERRAIN_CAN(1, locb_can_enter); }

  {
    plc site = &Level->site[x][y];
    terrain_can_t potentials = site_potentials(site);
    if(list_size(site->object_list))
      { potentials.flags |= locb_has_objects; }
    return potentials;
  }
}

static loc_flags_t loc_visible_flags( int dir, int col, int row)
{
  terrain_can_t potentials = loc_visible_potentials( dir, col, row);
  return potentials.flags;
}


static bool see_wall(int dir, int x, int y)
{
  int row = y + Dirs[1][dir];
  int col = x + Dirs[0][dir];
  /* Not in bounds is wall, for our purposes. */
  if(!inbounds(col,row))
    { return TRUE; }

  if(Current_Environment == E_COUNTRYSIDE)
    {
      if(!c_statusp(col, row, SEEN))
	{ return FALSE; }
      {
	terrain_can_t potentials = c_loc_potentials( col, row);
	return !enterable_p(potentials);
      }
    }

  /*  Fail if we don't know the site at all.  */
  if(!loc_statusp(col, row, SEEN))
    { return FALSE; }
  {
    terrain_can_t potentials = site_potentials( &Level->site[col][row]);
    return !enterable_p(potentials);
  }
}



int confirm_enter(void)
{
  if(!optionp(CONFIRM)) { return TRUE; }

  {
    char * warning_prompt = "";
    if(Current_Environment == E_COUNTRYSIDE)
      {
	warning_prompt = "That's dangerous terrain, and slow going.";
      }
    else
      {
	warning_prompt =
	  Player.status[SHADOWFORM] ?
	  "That looks dangerous." :
	  "Look where you're about to step!";
      }
    return (cinema_confirm(warning_prompt) == 'y');
  }
}


int moveable_p(int x, int y)
{
  loc_flags_t locb;
  if(Current_Environment == E_COUNTRYSIDE)
    {
      locb = c_loc_flags(x, y);
    }
  else
    {
      locb = loc_flags(x, y);
    }

  if(!(locb & locb_can_enter))
    { return FALSE; }

  if(locb & locb_warn)
    { return confirm_enter(); }
  else
    { return TRUE; }
}


/* Whether it makes sense to continue an action.  So player doesn't
   accidentally "continue" from running into fiting a monster.  For
   now, the test is just whether we've changed action. */
bool continuable_action(int what_to_do, bool reset)
{
  static int last_act_done = can_nothing;
  if(reset)
    {
      last_act_done = what_to_do;
      return TRUE;
    }
  else
    return (what_to_do == last_act_done);
}

bool remaining_actable_conditions(int * p_locb, bool print)
{
  /* Nothing to do. */
  if(!(*p_locb & locb_actable_conditions))
    {
      /*  Player bumped his head. */
      if(print)
	{ print3("Ouch!"); }
      return FALSE;
    }

  /* Horse balks. */
  if(pflagp(MOUNTED) && (*p_locb & locb_horse_wont_enter))
    {
      *p_locb &= ~locb_can_enter;
      if(!(*p_locb & locb_actable_conditions))
	{
	  if(print)
	    { print1("You can't convince your steed to continue."); }
	  return FALSE;
	}
    }

  if((Player.status[IMMOBILE] > 0))
    {
      *p_locb &= ~locb_can_enter;
      if(!(*p_locb & locb_actable_conditions))
	{
	  print3("You are unable to move.");
	  return FALSE;
	}
    }
  return TRUE;
}

post_command_state_t act_on_xy(int what_to_do, int x, int y)
{
  switch(what_to_do)
    {
    default:
      error("Unknown type of action");
      return base_state;

    case can_deal_monster:
      {
	fight_monster(get_monster(Level, x, y));
	RETURN_post_command_state( 1, Player.speed * 5 / 5 );
      }

    case can_enter_carefully:
      if(!confirm_enter())
	{ return base_state; }
      return true_moveplayer(x, y);
	
    case can_enter:
      return true_moveplayer(x, y);
    }
}

/* The name is misleading.  Handles action directed towards a spot,
   not neccessarily moving the player.
*/
post_command_state_t moveplayer (int dx, int dy, int first_time)
{
  int x = Player.x + dx;
  int y = Player.y + dy;
  int locb = loc_flags(x, y);
  int what_to_do = can_nothing;
  bool proceed = remaining_actable_conditions(&locb, first_time);

  /* Check for early impossibility before trying confirms and such. */
  if(!proceed)
    { return base_state; }

  /*** Decide action, based on possibilities. ***/

  if(locb & locb_has_monster)
    {
      what_to_do = can_deal_monster;
    }
  else
  if(locb & locb_can_enter)
    {
      if(locb & locb_warn)
	{
	  what_to_do = can_enter_carefully;
	}
      else
	{
	  what_to_do = can_enter;
	}
    }
  else
    /* Paranoia.  This shouldn't be reached.  */
    { return base_state; }

  /***  Stop repeating if we changed actions.  ***/
  if(!continuable_action(what_to_do, first_time))
    { return base_state; }

  /*** Commit the action ***/
  return act_on_xy(what_to_do, x, y);
}



post_command_state_t  moveto_site (list_t * spots)
{
  bool success =
    setup_flow_for_moveto(spots, predict_can_enter_easily);

  /* If we can get there... */
  if(success)
    {
      mprint("You're on your way...");
      /* Set up our continuing action, since we have no true first move.  */
      continuable_action(can_enter, TRUE);
      /* No time has been taken yet.  Command will repeat.  */
      RETURN_post_command_state(254, 0);
    }
  else
    {
      /* We could try again, with predict_can_enter_at_all.  Perhaps with a
	 confirmation, since such a path could be dangerous.  */
      mprint("You can't get there from here!");
      return base_state;
    }
}



post_command_state_t cmd_city_move(int dir, int first_time)
{
  /* Country doesn't have knowable sites. */
  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  if(!first_time)
    { return player_move_toward_goal(); }

  if (Player.status[IMMOBILE] > 0)
    {
      print3("You can't even move!");
      return base_state;
    }
  {
    post_command_state_t state;
    list_t * sites = parsecitysite();
    if(!sites) { return base_state; }

    state = moveto_site(sites);
    fully_free_list(&sites, delete_coords);
    return state;
  }
}

post_command_state_t cmd_moveto_spot(int dir, int first_time)
{
  /* Country doesn't have knowable sites. */
  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  if(!first_time)
    { return player_move_toward_goal(); }

  if (Player.status[IMMOBILE] > 0)
    {
      print3("You can't even move!");
      return base_state;
    }
  {
    int x, y;
    bool success;
    print1("Pick a spot to move to.  ");
    success = set_spot_from_player(&x, &y);
    if(!success)
      { return base_state; }

    {
      post_command_state_t state;
      list_t * sites = list_new();
      /* We don't alloc or free `spot', since this list is born and
	 dies here, and has all known elements. */
      coords_alloced_t spot = { x, y, NULL, };
      list_prepend(sites, &spot);
      state = moveto_site(sites);
      fully_free_list(&sites, list_dont_free_v);
      return state;
    }
  }
}

post_command_state_t cmd_name_spot(int dir, int first_time)
{
  int x, y;
  bool success;
  print1("Pick a spot to name.  ");
  success = set_spot_from_player(&x, &y);
  if(!success)
    { return base_state; }

  print1("Call this spot what? ");
  {
    char * name = msgscanstring();
    add_knowable_user_loc(x, y, name);
    return base_state;
  }
}


/* This code and commentary are borrowed from Angband.  However, the
   details of the supporting code and functions are fairly different,
   and the algorithm is rearranged (more clearly, I think).  I've kept
   the Angband comments, because they are useful in understanding
   what the code does.  I redrew some diagrams that had gotten
   munged.

   The functions largely retain their sense, but see_nothing and
   see_wall use x,y ordering, unlike Angband which uses y,x.

   - Tehom
*/

/*
 * The running algorithm:			-CJS-
 *
 * In the diagrams below, the player has just arrived in the
 * grid marked as '@', and he has just come from a grid marked
 * as 'o', and he is about to enter the grid marked as 'x'.
 *
 * Of course, if the "requested" move was impossible, then you
 * will of course be blocked, and will stop.
 *
 * Overview: You keep moving until something interesting happens.
 * If you are in an enclosed space, you follow corners. This is
 * the usual corridor scheme. If you are in an open space, you go
 * straight, but stop before entering enclosed space. This is
 * analogous to reaching doorways. If you have enclosed space on
 * one side only (that is, running along side a wall) stop if
 * your wall opens out, or your open space closes in. Either case
 * corresponds to a doorway.
 *
 * What happens depends on what you can really SEE. (i.e. if you
 * have no light, then running along a dark corridor is JUST like
 * running in a dark room.) The algorithm works equally well in
 * corridors, rooms, mine tailings, earthquake rubble, etc, etc.
 *
 * These conditions are kept in static memory:
 * find_openarea	 You are in the open on at least one
 * side.
 * find_breakleft	 You have a wall on the left, and will
 * stop if it opens
 * find_breakright	 You have a wall on the right, and will
 * stop if it opens
 *
 * To initialize these conditions, we examine the grids adjacent
 * to the grid marked 'x', two on each side (marked 'L' and 'R').
 * If either one of the two grids on a given side is seen to be
 * closed, then that side is considered to be closed. If both
 * sides are closed, then it is an enclosed (corridor) run.
 *
 * LL		L
 * @x	       LxR
 * RR	       @R
 *
 * Looking at more than just the immediate squares is
 * significant. Consider the following case. A run along the
 * corridor will stop just before entering the center point,
 * because a choice is clearly established. Running in any of
 * three available directions will be defined as a corridor run.
 * Note that a minor hack is inserted to make the angled corridor
 * entry (with one side blocked near and the other side blocked
 * further away from the runner) work correctly. The runner moves
 * diagonally, but then saves the previous direction as being
 * straight into the gap. Otherwise, the tail end of the other
 * entry would be perceived as an alternative on the next move.
 *
 * #.#
 * ##.##
 * .@x..
 * ##.##
 * #.#
 *
 * Likewise, a run along a wall, and then into a doorway (two
 * runs) will work correctly. A single run rightwards from @ will
 * stop at 1. Another run right and down will enter the corridor
 * and make the corner, stopping at the 2.
 *
 *      #@x	  1
 *      ########### ######
 *      2	    #
 *      #############
 *      #
 *
 * After any move, the function area_affect is called to
 * determine the new surroundings, and the direction of
 * subsequent moves. It examines the current player location
 * (at which the runner has just arrived) and the previous
 * direction (from which the runner is considered to have come).
 *
 * Moving one square in some direction places you adjacent to
 * three or five new squares (for straight and diagonal moves
 * respectively) to which you were not previously adjacent,
 * marked as '!' in the diagrams below.
 *
 * ...!	  ...
 * .o@!	  .o.!
 * ...!	  ..@!
 *         !!!
 *
 * You STOP if any of the new squares are interesting in any way:
 * for example, if they contain visible monsters or treasure.
 *
 * You STOP if any of the newly adjacent squares seem to be open,
 * and you are also looking for a break on that side. (that is,
 * find_openarea AND find_break).
 *
 * You STOP if any of the newly adjacent squares do NOT seem to be
 * open and you are in an open area, and that side was previously
 * entirely open.
 *
 * Corners: If you are not in the open (i.e. you are in a corridor)
 * and there is only one way to go in the new squares, then turn in
 * that direction. If there are more than two new ways to go, STOP.
 * If there are two ways to go, and those ways are separated by a
 * square which does not seem to be open, then STOP.
 *
 * Otherwise, we have a potential corner. There are two new open
 * squares, which are also adjacent. One of the new squares is
 * diagonally located, the other is straight on (as in the diagram).
 * We consider two more squares further out (marked below as ?).
 *
 * We assign "option" to the straight-on grid, and "option2" to the
 * diagonal grid, and "check_dir" to the grid marked 's'.
 *
 * .s
 * @x?
 * #?
 *
 * If they are both seen to be closed, then it is seen that no
 * benefit is gained from moving straight. It is a known corner.
 * To cut the corner, go diagonally, otherwise go straight, but
 * pretend you stepped diagonally into that next location for a
 * full view next time. Conversely, if one of the ? squares is
 * not seen to be closed, then there is a potential choice. We check
 * to see whether it is a potential corner or an intersection/room entrance.
 * If the square two spaces straight ahead, and the space marked with 's'
 * are both blank, then it is a potential corner and enter if find_examine
 * is set, otherwise must stop because it is not a corner.
 */



typedef unsigned char byte;
/*
 * Hack -- allow quick "cycling" through the legal directions
 */
static const byte cycle[] =
{ 3, 7, 1, 4, 0, 6, 2, 5, 3, 7, 1, 4, 0, 6, 2, 5, 3, };


/*
 * Hack -- map each direction into the "middle" of the "cycle[]" array
 */
static const byte chome[] =
{ 4, 10, 6, 8, 11, 7, 5, 9, };


/*
 * The direction we are running
 */
static byte find_current;

/*
 * The direction we came from
 */
static byte find_prevdir;

/*
 * We are looking for open area
 */
static bool find_openarea;

/*
 * We are looking for a break
 */
static bool find_breakright;
static bool find_breakleft;


/* The "movement cost" of the terrain we start on, to make it possible
   to run intelligently on less-than-ideal terrain. (eg watery
   corridors) */
static int cost_threshhold;

static bool dir_diagonal_p(int dir)
{
  return (dir < first_cardinal_dir);
}

static bool enterable_p(terrain_can_t potentials)
{
  return (potentials.cost_to_traverse <= cost_threshhold);
}

static bool see_nothing(int dir, int x, int y)
{
  int row = y + Dirs[1][dir];
  int col = x + Dirs[0][dir];
  if(!inbounds(col,row))
    { return TRUE; }

  if(Current_Environment == E_COUNTRYSIDE)
    {
      if(c_statusp(col, row, SEEN))
	{ return FALSE; }
    }
  else
    {
      if(loc_statusp(col, row, SEEN))
	{ return FALSE; }
    }
  return TRUE;
}



/*
 * Initialize the running algorithm for a new direction.
 *
 * Diagonal Corridor -- allow diaginal entry into corridors.
 *
 * Blunt Corridor -- If we seem to be immediately moving into a
 * corner, heading in a cardinal direction, then force a turn to the
 * side.
 *
 * Diagonal Corridor    Blunt Corridor
 *        ^
 *        |                   ##
 *       #x#                 @x#
 *       @.                   |#
 *                            v */
static void run_init(int dir)
{
  int		row, col, deepleft, deepright;
  int		i, shortleft, shortright;

  /* Save the direction */
  find_current = dir;

  /* Assume running straight */
  find_prevdir = dir;

  /* Assume looking for open area */
  find_openarea = TRUE;

  /* Assume not looking for breaks */
  find_breakright = find_breakleft = FALSE;

  /* Assume no nearby walls */
  deepleft = deepright = FALSE;
  shortright = shortleft = FALSE;

  /* Find the destination grid */
  row = Player.y + Dirs[1][dir];
  col = Player.x + Dirs[0][dir];

  /* Set cost threshhold, which must be set before calling see_wall.  */
  {
    terrain_can_t potentials =
      (Current_Environment == E_COUNTRYSIDE) ?
      c_loc_potentials(col, row) :
      loc_potentials(col, row);

    cost_threshhold = potentials.cost_to_traverse;
  }

  /* Extract cycle index */
  i = chome[dir];

  /* Check for walls */
  if (see_wall(cycle[i+1], Player.x, Player.y))
    {
      find_breakleft = TRUE;
      shortleft = TRUE;
    }
  else if (see_wall(cycle[i+1], col, row))
    {
      find_breakleft = TRUE;
      deepleft = TRUE;
    }

  /* Check for walls */
  if (see_wall(cycle[i-1], Player.x, Player.y))
    {
      find_breakright = TRUE;
      shortright = TRUE;
    }
  else if (see_wall(cycle[i-1], col, row))
    {
      find_breakright = TRUE;
      deepright = TRUE;
    }

  /* Looking for a break */
  if (find_breakleft && find_breakright)
    {
      /* Not looking for open area */
      find_openarea = FALSE;

      /* Hack -- allow angled corridor entry */
      if (dir_diagonal_p(dir))
	{
	  if (deepleft && !deepright)
	    {
	      find_prevdir = cycle[i - 1];
	    }
	  else if (deepright && !deepleft)
	    {
	      find_prevdir = cycle[i + 1];
	    }
	}

      /* Hack -- allow blunt corridor entry */
      else if (see_wall(cycle[i], col, row))
	{
	  if (shortleft && !shortright)
	    {
	      find_prevdir = cycle[i - 2];
	    }
	  else if (shortright && !shortleft)
	    {
	      find_prevdir = cycle[i + 2];
	    }
	}
    }
}

/* Hack:  these options are hard-coded for now. */
#define find_examine TRUE
#define find_cut TRUE



/*
 * Update the current "run" direction
 *
 * Return TRUE if the running should be stopped
 */
static bool run_test(void)
{
  int			i;
  loc_flags_t interesting_conditions =
    /* Bitvector of the various "interesting" conditions.  This
       assumes all interesting conditions are things player mite want
       to step on (piles, stairs) or (hack) things we can't step on
       anyways (monsters).  If not, we'll need 2 bitvectors. */
    (locb_has_monster
     |
     (optionp(PICKUP)  ? locb_has_objects : 0)
     |
     (optionp(RUNSTOP) ? locb_interesting : 0));


  /* Where we came from */
  int prev_dir = find_prevdir;


  /* Range of adjacent grids that weren't adjacent last time. */
  int max = (dir_diagonal_p(prev_dir) ? 1 : 0) + 1;

  /* Test the space we're directly on.  dir 8 is (0, 0). */
  {
    loc_flags_t flags =
      loc_visible_flags( 8, Player.x, Player.y);
    if(flags & interesting_conditions)	
      { return (TRUE); }
  }


  if (find_openarea)
    {
      if (see_wall(find_current, Player.x, Player.y))
	{ return (TRUE); }

      /* Look at every newly adjacent square except the one directly
	 ahead of us (i == 0), which we'll consider next step when
	 we're directly on it (for piles, stairs, etc).
      */
      for (i = -max; i <= max; i++)
	if(i != 0)
	  {
	    int i_cyc = chome[prev_dir] + i;
	    int i_dir = cycle[i_cyc];

	    terrain_can_t potentials =
	      loc_visible_potentials( i_dir, Player.x, Player.y);

	    if(potentials.flags & interesting_conditions)	
	      { return (TRUE); }
	
	    if(enterable_p(potentials))
	      {
		if ((i < 0))
		  { if(find_breakright) { return TRUE; } }
		else
		  if((i > 0))
		    { if(find_breakleft) { return TRUE; } }
	      }
	    /* Obstacle, while looking for open area */
	    else
	      {
		if (i < 0)
		  {
		    /* Can't go from from open running to both sides
		       closed (a corridor), so stop if the other side is
		       closed too.  */
		    if (find_breakleft)
		      {
			return (TRUE);
		      }
		
		    /* Remember that this side is now closed.  */
		    find_breakright = TRUE;
		  }

		else if (i > 0)
		  {
		    /* Parallel logic to the (i < 0) case above. */
		    if (find_breakright)
		      {
			return (TRUE);
		      }
		    find_breakleft = TRUE;
		  }
	      }
	  }
    }
  else
    {
      /* These directions aren't known yet */
      int option    = no_dir;
      int option2   = no_dir;
      int check_dir = no_dir;
      int interest_dir = no_dir;

      for (i = -max; i <= max; i++)
	{
	  int i_cyc = chome[prev_dir] + i;
	  int i_dir = cycle[i_cyc];

	  terrain_can_t potentials =
	    loc_visible_potentials( i_dir, Player.x, Player.y);
	
	  /* We can't skip the location we will be on, because we
	     don't know which one it is yet.  */
	  if(potentials.flags & interesting_conditions)	
	    {
	      /* We stop near interesting spots unless we'll be on
		 them next step, but we don't yet know which way we'll
		 go.  So if there are interesting spots in 2
		 directions, one or the other should stop us, so stop.
		 Otherwise remember the direction and we'll check it
		 later. */
	      if(interest_dir >= 0)
		{ return (TRUE); }
	      else
		{ interest_dir = i_dir; }
	    }
	
	  if(enterable_p(potentials))
	    {
	      /* The first direction seen */
	      if (option < 0)
		{
		  option = i_dir;
		}

	      /* Three directions open. Stop running. */
	      else if (option2 >= 0)
		{
		  return (TRUE);
		}

	      /* Two non-adjacent directions open.  Stop running. */
	      else if (option != cycle[i_cyc - 1])
		{
		  return (TRUE);
		}

	      /* Two (adjacent) directions open.  We make the one
		 that's diagonal primary, and we put the "mirror
		 diagonal" that's on the other side of the cardinal
		 direction into check_dir. */

	      /* Case: New direction is diagonal. */
	      else if (dir_diagonal_p(i_dir))
		{
		  check_dir = cycle[i_cyc - 2];
		  option2 = i_dir;
		}

	      /* Case: New direction is cardinal. */
	      else
		{
		  check_dir = cycle[i_cyc + 1];
		  option2 = option;
		  option = i_dir;
		}
	    }
	}

      /* No options */
      if (option < 0)
	{
	  return (TRUE);
	}

      /* One option */
      else if (option2 < 0)
	{
	  /* Primary option */
	  find_current = option;

	  /* No other options */
	  find_prevdir = option;
	}

      /* Two options, examining corners */
      else if (find_examine && !find_cut)
	{
	  /* Primary option */
	  find_current = option;

	  /* Hack -- pretend we entered diagonally, so that next time
	     we will be heading into the other arm of the corner.  */
	  find_prevdir = option2;
	}

      /* Two options, pick one */
      else
	{
	  /* Get next location */
	  int row = Player.y + Dirs[1][option];
	  int col = Player.x + Dirs[0][option];

	  /* Don't see that it is closed off. */
	  /* This could be a potential corner or an intersection. */
	  if (!see_wall(option, col, row) ||
	      !see_wall(check_dir, col, row))
	    {
	      /* Can not see anything ahead and in the direction we */
	      /* are turning, assume that it is a potential corner. */
	      if (find_examine &&
		  see_nothing(option, col, row) &&
		  see_nothing(option2, col, row))
		{
		  find_current = option;
		  find_prevdir = option2;
		}

	      /* STOP: we are next to an intersection or a room */
	      else
		{
		  return (TRUE);
		}
	    }

	  /* This corner is seen to be enclosed; we cut the corner. */
	  else if (find_cut)
	    {
	      find_current = option2;
	      find_prevdir = option2;
	    }

	  /* This corner is seen to be enclosed, and we */
	  /* deliberately go the long way. */
	  else
	    {
	      find_current = option;
	      find_prevdir = option2;
	    }

	  /* Now that we know which direction we're going in, if we
	     found something interesting, stop unless we're going to
	     be on it anyways next step.   */
	  if((interest_dir >= 0) && (interest_dir != find_current) )
	    { return TRUE; }
	}
    }

  /* OK to keep running. */
  return (FALSE);
}


/* Take one running step (== Angband's run_step)*/
post_command_state_t cmd_run(int dir, int first_time)
{
  if (first_time)
    {
      int run_dir =
	get_deliberate_dir(dir, "Run in direction: ", FALSE);

      if(run_dir == no_dir)
	{ return base_state; }

      /* Accept most terrains for a first step.  Kluuge: do so by
	 pre-setting the cost-threshhold, which see_wall uses. */
      cost_threshhold = 50;

      /* Don't "run" into a wall.  Coherently part of run_init, but
	  separated because it's the only part that may fail.  */
      if (see_wall(run_dir, Player.x, Player.y))
	{ return base_state; }

      /* Set up the current running state. */
      run_init(run_dir);
    }
  else
    {
      /* Update run.  TRUE return value means we should stop. */
      if (run_test())
	{ return base_state; }
    }

  return cmd_move(find_current, first_time);
}


/* Jump, that is */
post_command_state_t cmd_vault(int dir, int first_time)
{

  if (Current_Environment == E_COUNTRYSIDE)
    { return base_state; }

  clearmsg();

  if (Player.status[IMMOBILE] > 0)
    {
      print3("You are unable to move.");
      return base_state;
    }

    {
      int x, y;
      int jumper = 0;
      pob boots = Player.possessions[O_BOOTS];
      if (boots && boots->usef == I_BOOTS_JUMPING)
	{ jumper = 2; }

      mprint("Jump where?");
      set_spot_from_player(&x, &y);

      if (!los_p(Player.x, Player.y, x, y))
        {
	  print3("The way is obstructed.");
	  return base_state;
	}

      if (Player.itemweight > Player.maxweight)
        {
	  print3("You are too burdened to jump anywhere.");
	  return base_state;
	}
      if (distance(x, y, Player.x, Player.y) > (jumper + max(2, 2 + statmod(Player.agi))))
        {
	  print3("The jump is too far for you.");
	  return base_state;
	}
      if (get_monster(Level, x, y))
        {
	  print3("You can't jump on another creature.");
	  return base_state;
	}
      if (!moveable_p(x, y))
	{
	  print3("You can't jump there.");
	  return base_state;
	}

        {
	  int extra_time = 0;
          if (!jumper && (random_range(30) > Player.agi))
            {
              mprint("Oops -- took a tumble.");
	      extra_time = 20;
              p_damage(Player.itemweight / 250, UNSTOPPABLE, "clumsiness");
            }

	  physical_moveplayer(x, y);
	  RETURN_post_command_state( 0,
				     extra_time + (Player.speed * 10 / 5));
        }
    }
}
