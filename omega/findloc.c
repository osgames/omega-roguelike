/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/* *** Functions to find or set x,y locations by various criterions. *** */

#include "glob.h"
#include "lev.h" /* Just for get_monster */
#include "util.h"
#include "load.h"
#include "liboutil/list.h"
#include "liboutil/allocator.h"
#include "findloc.h"
#include "save.h"
#include "pick.h"

/***************/

/* List helper.  Forces list to exist. */
void list_prepend_force(list_t ** list, void * data)
{
  if(!*list)
    { *list = list_new(); }

  list_prepend(*list, data);
}


/**** L_* site data ****/

/* Data describing the knowable L_ sites.  This data could be extended
   to assist signprint and other functions.
 */
typedef struct locf_data_t
{
  char   * sitename;
  char   * full_sitename;  /* For l_tourist to say. */
  locf_t   index;
  unsigned short flags;
} locf_data_t;



def_lookup_table_type( locf_data_t, locf_data);
def_lookup_table_funcs(locf_data_t, locf_data, index);

/* Data from aux3.c::sitenames and site2.c::l_tourist.  Some data
   l_tourist doesn't provide is punted. */
locf_data_t locf_data_array[] =
{
  {"alchemist", "Ambrosias' Potions et cie", L_ALCHEMIST,},
  {"arena", "arena", L_ARENA,},
  {"armorer", "Julie's Armor of Proof and Weapons of Quality", L_ARMORER,},
  {"bank", "bank", L_BANK,},
  {"brothel", "brothel", L_BROTHEL,},
  {"cartographer", "Ye Olde Mappe Shoppe", L_CARTOGRAPHER,},
  {"casino", "Rampart Mithril Nugget Casino", L_CASINO,},
  {"castle", "castle", L_CASTLE,},
  {"city gates", "city gates", L_COUNTRYSIDE,},
  {"collegium magii", "collegium magii", L_COLLEGE,},
  {"condo", "Luxury Condominiums", L_CONDO,},
  {"countryside", "tactical exit", L_TACTICAL_EXIT, },
  {"department of public works", "Rampart Department of Public Works", L_DPW,},
  {"diner", "The Rampart Diner", L_DINER,},
  {"down stairs", "down stairs",  L_STAIRS_DOWN, },
  {"elevator", "elevator", L_LIFT, },
  {"explorers' club", "Rampart Explorers' Club", L_CLUB,},
  {"fast food", "Commandant Sonder's Rampart-fried Lyzzard partes", L_COMMANDANT,},
  {"gymnasium", "The Rampart Gymnasium", L_GYM,},
  {"healer", "Rampart Healers", L_HEALER,},
  {"hospice", "hospice", L_CHARITY,},
  {"les crapuleux", "Les Crapeuleaux, fine dining", L_CRAP,},
  {"library", "Rampart Public Library", L_LIBRARY,},
  {"mercenary guild", "mercenary guild", L_MERC_GUILD,},
#ifdef INCLUDE_MONKS
  {"monastery", "monastery", L_MONASTERY,},
#endif
  {"oracle", "oracle", L_ORACLE,},
  {"order of paladins", "order of paladins", L_ORDER,},
  {"pawn shop", "Knight's Pawn Shop", L_PAWN_SHOP,},
  {"sewer entrance", "sewer entrance", L_SEWER,},
  {"sorcerors' guild", "sorcerors' guild", L_SORCERORS,},
  {"tavern", "The Centaur and Nymph tavern", L_TAVERN,},
  {"temple", "temple", L_TEMPLE,},
  {"thieves' guild", "thieves' guild", L_THIEVES_GUILD,},
  {"tourist information", "tourist information", L_TOURIST,},
  {"up stairs", "up stairs",  L_STAIRS_UP, },
};
locf_data_table_t locf_data_table =
{
  locf_data_array,
  sizeof(locf_data_array)/ sizeof(locf_data_t),
};


/* Get the `l_tourist'-style sitename corresponding to index. */
char * get_full_sitename(locf_t index)
{
  locf_data_t * p_locf_data =
    locf_data_find( index, locf_data_table);
  if(!p_locf_data)
    { return "An unnamed site"; }
  return p_locf_data->full_sitename;
}


/**** Knowable locs ****/

enum knowable_loc_types
{
  slt_end_of_list,     /* Pseudo-type used for save/restore.  No real
			  sites use this.  */
  slt_user_site,       /* Sites the user created.  Always known. */
  slt_unknown,         /* An unknown site. */
  slt_known_primary,   /* Normal, regular known site. */
  slt_known_secondary, /* A site that doesn't print because another
			  known site has the same name. */
};
enum knowable_loc_flags
{
  slf_changed   = pow2(0),
};

typedef struct knowable_loc_t
{
  int x, y;
  /* sitename can be NULL, alloc'ed, or static, depending on type. */
  char *        sitename;
  /* type is enumerated in knowable_loc_types. */
  unsigned char  type;
  /* Either an enumerated L_* function usually corresponding to x,y in
     Level, or `sl_players_spot' */
  locf_t         index;
  unsigned short flags;
  struct knowable_loc_t * next;
} knowable_loc_t;


make_static_allocator(knowable_loc_t, (128 * sizeof(knowable_loc_t)),
		      next, checkmalloc);

#define alloc_knowable_loc() knowable_loc_t_utility(0)
#define free_knowable_loc(p) knowable_loc_t_utility(p)



/* Filters. */
int knowable_matches_index_vf(void * v_sl, void * v_index)
{
  knowable_loc_t * p_sl = v_sl;
  int * index = v_index;
  return(p_sl->index == *index);
}

int knowable_obscures_vf(void * v_sl, void * v_index)
{
  knowable_loc_t * p_sl = v_sl;
  int * index = v_index;
  switch(p_sl->type)
    {
    default:
      return FALSE;

    case slt_known_primary:
    case slt_known_secondary:  /* If we see secondary, we know the primary exists. */
      return(p_sl->index == *index);
    }
}

bool knowable_obscured_p(int index)
{
  return
    list_single_find(Level->knowable_locs, knowable_obscures_vf, &index) != 0;
}


int knowable_matches_coords_vf(void * v_sl, void * v_data)
{
  knowable_loc_t * p_sl = v_sl;
  coords_alloced_t * p_coords = v_data;
  return((p_sl->x == p_coords->x) && (p_sl->y == p_coords->y));
}


/*
   Most knowable locations are generated again when we
   restore-generate the level, so we only store changes.

   We signal when we're done by storing the pseudotype
   slt_end_of_list.

   Flags are not saved because only the "changed" flag is meaningful
   and that one is implied.  */

BEGIN_SAVER(save_1_knowable_loc, knowable_loc_t)
{
  unsigned char type = ptr ? ptr->type : slt_end_of_list;
  assert(ptr ? (ptr->type != slt_end_of_list) : TRUE);
  switch(type)
    {
    default: {}
      /* Save nothing for slt_unknown. */
      break;

    case slt_end_of_list:
      SAVE_SCALAR(unsigned char, type);
      break;
	
    case slt_known_primary:
    case slt_known_secondary:
      /* Only store it if it has changed. */
      if( ptr->flags & slf_changed)
	{
	  SAVE_SCALAR(unsigned char, type);
	  SAVE_FIELD(int, x);
	  SAVE_FIELD(int, y);
	}
      break;

    case slt_user_site:
      {
	SAVE_SCALAR(unsigned char, type);
	SAVE_FIELD(int, x);
	SAVE_FIELD(int, y);
	SAVE_STRING_FIELD(sitename);
      }
      break;
    }
}
END_SAVER

bool restore_1_knowable_loc(FILE *fd, int version)
{
  unsigned char type;
  RESTORE_SCALAR(unsigned char, type);

  switch(type)
    {
    default:
      error("Unknown type of knowable-location");
      break;

    case slt_end_of_list:
      /* Signal that the list is ended. */
      return FALSE;

    case slt_known_primary:
    case slt_known_secondary:
      {
	/* Doesn't matter whether it was primary/secondary.
	   `make_loc_known_at' will enforce the "only 1
	   primary" condition. */
	int x, y;
	RESTORE_SCALAR(int, x);
	RESTORE_SCALAR(int, y);
	make_loc_known_at(x, y);
      }
      break;

    case slt_user_site:
      {
	int x; int y; char namestring[80];
	RESTORE_SCALAR(int, x);
	RESTORE_SCALAR(int, y);
	filescanstring(fd, namestring);
	add_knowable_user_loc(x, y, namestring);
      }
      break;
    }
  return TRUE;
}


/* Save: store them into a file.  */
int save_knowable_locs(FILE* fd, list_t * special_locs)
{
  int ok = 1;
  ok &= save_check(fd, "know");
  {
    list_iterator_t * iter = list_iterator_create(special_locs);
    while(list_iterator_has_next(iter))
      {
	knowable_loc_t * p_sl = list_iterator_next(iter);
	ok &= save_1_knowable_loc(fd, p_sl);
      }
    list_iterator_delete(iter);
  }
  ok &= save_1_knowable_loc(fd, NULL);
  return ok;
}

/* Restore: recreate them from a file. */
void restore_knowable_locs(FILE *fd, int version)
{
  /* We don't need to deal with the list here.  It's usually created
     by restore-generation, and in any case functions that store into
     it create it if needed.
  */
  restore_check(fd, "know");
  while(restore_1_knowable_loc(fd, version));
}

void delete_knowable_loc(void * v_sl)
{
  knowable_loc_t * p_sl = v_sl;
  switch(p_sl->type)
    {
    default:
      /* Nothing to free. */
      break;

    case slt_user_site:
      free(p_sl->sitename);
      break;
    }
  free_knowable_loc(v_sl);
}

LIST_DECLARE_FREER(fully_free_knowable_locs, delete_knowable_loc);

/* There is no interface for deleting singles, because we never seem
   to do that. */


/*** Creating knowable locs ***/

void add_knowable_user_loc(int x, int y, char * namestring)
{
  knowable_loc_t * p_sl = alloc_knowable_loc();
  assert(Level);
  p_sl->x     = x;
  p_sl->y     = y;
  p_sl->index = sl_players_spot;
  p_sl->type  = slt_user_site;
  p_sl->sitename = salloc(namestring);
  p_sl->flags    = slf_changed;
  list_prepend_force(&Level->knowable_locs, p_sl);
}



void try_add_knowable_loc(int index, int x, int y, int known)
{
  locf_data_t * p_locf_data =
    locf_data_find( index, locf_data_table);

  if(p_locf_data)
    {
      knowable_loc_t * p_sl = alloc_knowable_loc();
      assert(Level);
      p_sl->x     = x;
      p_sl->y     = y;
      p_sl->index = index;
      if(!known)
	{ p_sl->type = slt_unknown; }
      else
	{
	  bool is_secondary = knowable_obscured_p(p_sl->index);
	  p_sl->type =
	    (is_secondary ? slt_known_secondary : slt_known_primary);
	}

      p_sl->sitename = p_locf_data->sitename;
      p_sl->flags    = 0;

      list_prepend_force(&Level->knowable_locs, p_sl);
    }
}

/*** Making knowable locs known ***/

/* Make special location P_SL known if it exists and is unknown.
   Return whether anything became known.
 */
static int make_loc_known_aux(knowable_loc_t * p_sl)
{
  if(!p_sl)
    { return FALSE; }
  switch(p_sl->type)
    {
    default:
      return FALSE;

    case slt_unknown:
      {
	/* Figure out whether it has the same name as any known site
	   before it itself becomes known, because afterwards it
	   always matches something: itself. */
	bool is_secondary = knowable_obscured_p(p_sl->index);
	p_sl->flags |= slf_changed;
	p_sl->type = is_secondary ?
	  slt_known_secondary :
	  slt_known_primary;
      }
      return TRUE;
    }
  /*NOTREACHED*/
}


/* Try to make a special location corresponding to INDEX known.
   Return whether anything became known.

   Punt: if there are multiple such locations (eg, L_COMMANDANT), only
   the first one found is treated.  Used for city-site type
   functionality.  */
int make_loc_known(int index)
{
  knowable_loc_t * p_sl =
    list_single_find(Level->knowable_locs, knowable_matches_index_vf, &index);
  return
    make_loc_known_aux(p_sl);
}

/* Try to make a special location at position X,Y known.  Return
   whether anything became known.

   Punt: We don't bother about discovering adjacent sites, because all
   the sites that are grouped with adjacent copies of themselves (city
   gates, college, temple, etc) are known from the start anyways.  */
int make_loc_known_at(int x, int y)
{
  knowable_loc_t * p_sl;
  coords_alloced_t coords;
  coords.x = x;
  coords.y = y;
  p_sl =
    list_single_find(Level->knowable_locs, knowable_matches_coords_vf, &coords);
  return
    make_loc_known_aux(p_sl);
}

void make_all_knowable_locs_known(void)
{
  assert(Level);
  if(!Level->knowable_locs)
    { return; }
  {
    list_iterator_t * iter = list_iterator_create(Level->knowable_locs);
    while(list_iterator_has_next(iter))
      {
	knowable_loc_t * p_sl = list_iterator_next(iter);
	make_loc_known_aux(p_sl);
      }
    list_iterator_delete(iter);
  }
}



/*** Retrieving knowable locs by name ***/

typedef struct sl_uniqueness_helper_t
{
  list_t * sites;
  int    count;
  char   * prefix;
} sl_uniqueness_helper_t;


bool sl_site_should_print(knowable_loc_t * p_sl, char * prefix)
{
  switch(p_sl->type)
    {
    default:
      return FALSE;

    case slt_known_primary:
    case slt_user_site:
      assert(p_sl->sitename);
      return strprefix(prefix, p_sl->sitename);
    }
}


bool sl_site_known(knowable_loc_t * p_sl, char * prefix)
{
  switch(p_sl->type)
    {
    default:
      return FALSE;

    case slt_known_primary:
    case slt_known_secondary:
    case slt_user_site:
      assert(p_sl->sitename);
      return strprefix(prefix, p_sl->sitename);
    }
}


int sl_site_known_callback(void * v_sl, void * v_prefix)
{
  knowable_loc_t * p_sl = v_sl;
  char * prefix = v_prefix;
  return sl_site_known(p_sl, prefix);
}

void sl_func_update_count(void * v_sl, void * v_data)
{
  knowable_loc_t * p_sl = v_sl;
  sl_uniqueness_helper_t * p_data = v_data;

  if(sl_site_should_print(p_sl,p_data->prefix))
    { p_data->count++; }
}

int sl_update_site_count(void * v_data, char * prefix, int hint)
{
  sl_uniqueness_helper_t * p_data = v_data;
  /* Clear the count. */
  p_data->count = 0;
  /* Paranoia - set prefix, tho surely it is already set. */
  p_data->prefix = prefix;
  list_apply_w_param(p_data->sites, sl_func_update_count, p_data);
  return p_data->count;
}


void sl_maybe_print(void * v_sl, void * v_prefix)
{
  knowable_loc_t * p_sl = v_sl;
  char * prefix = v_prefix;

  if(sl_site_should_print(p_sl, prefix))
    {
      menuprint(p_sl->sitename);
      menuprint("\n");
    }
}

void showknownsites(void * v_data)
{
  sl_uniqueness_helper_t * p_data = v_data;
  set_notice_flags(notice_drew_menu);
  menuclear();
  if(p_data->count == 0)
    {
      menuprint("\nNo known sites match that prefix!");
    }
  else
    {
      menuprint("\nPossible Sites:\n");
      list_apply_w_param(p_data->sites, sl_maybe_print, p_data->prefix);
    }
  showmenu();
}


char * selected_site_name(void * v_data)
{
  sl_uniqueness_helper_t * p_data = v_data;
  knowable_loc_t * p_sl =
    list_single_find( p_data->sites, sl_site_known_callback, p_data->prefix);

  assert(p_sl);
  return p_sl->sitename;
}


/*** coords ****/
make_static_allocator(coords_alloced_t, (128 * sizeof(coords_alloced_t)),
		      next, checkmalloc);

#define alloc_coords() coords_alloced_t_utility(0)
#define free_coords(p) coords_alloced_t_utility(p)

void delete_coords(void * element)
{ free_coords(element); }


/* Callback to possibly add a location from a knowable_loc_t. */
typedef struct collect_coords_helper_t
{
  list_t * list;
  char * prefix;
} collect_coords_helper_t;

void make_coords_from_special_loc (void * element, void * v_data)
{
  knowable_loc_t * p_sl = element;
  collect_coords_helper_t * data = v_data;
  if(sl_site_known(p_sl, data->prefix))
    {
      coords_alloced_t * p_coords = alloc_coords();
      p_coords->x = p_sl->x;
      p_coords->y = p_sl->y;
      list_append(data->list, p_coords);
    }
}



/* To deal with sites that occur more than once, we just use them all.
   parsecitysite returns either NULL or a list_t* of
   (coords_alloced_t *).  */
list_t * parsecitysite(void)
{
  bool success;
  char prefix[80] = "";
  sl_uniqueness_helper_t site_data;

  assert(Level);
  if(!Level->knowable_locs)
    {
      print3("You don't know any sites here.");
      return NULL;
    }

  site_data.prefix = prefix;
  site_data.sites = Level->knowable_locs;
  success =
    pick_from_list
    (
     "Move to which establishment [? for help, ESCAPE to quit]",
     "You don't know any sites here.",
     &site_data,
     prefix,
     sl_update_site_count,
     showknownsites,
     selected_site_name
     );


  /* We'll alloc a list here, and return it or NULL. */
  if (success)
    {
      list_t * list = list_new();
      collect_coords_helper_t data = { list, prefix, };
      list_apply_w_param(Level->knowable_locs,
			 make_coords_from_special_loc, &data);
      return list;
    }
  else
    { return NULL; }
}



/***** Special locs *****/

typedef struct special_loc_t
{
  int x, y;
  /* Enumerated in special_loc_meanings */
  locf_t         index;
  struct special_loc_t * next;
} special_loc_t;

int sl_func_matches_index(void * v_sl, void * v_index)
{
  special_loc_t * p_sl = v_sl;
  int * index = v_index;
  return(p_sl->index == *index);
}

make_static_allocator(special_loc_t, (128 * sizeof(special_loc_t)),
		      next, checkmalloc);

#define alloc_special_loc() special_loc_t_utility(0)
#define free_special_loc(p) special_loc_t_utility(p)

static void delete_special_loc(void * v_sl)
{ free_special_loc(v_sl); }


LIST_DECLARE_FREER(fully_free_special_locs, delete_special_loc);



/** Creating/modifying special locs **/

void add_game_special_loc(int index, int x, int y)
{
  special_loc_t * p_sl = alloc_special_loc();
  assert(Level);
  p_sl->x     = x;
  p_sl->y     = y;
  p_sl->index = index;
  list_prepend_force(&Level->special_locs, p_sl);
}

/* Find a special loc for modifying.  If not found, make it but return
   nothing (because there's nothing to modify) */
static special_loc_t * get_special_loc_to_modify(int index, int x, int y)
{
  special_loc_t * p_sl =
    list_single_find(Level->special_locs, sl_func_matches_index, &index);
  if(p_sl) { return p_sl; }

  add_game_special_loc(index, x, y);
  return NULL;
}

/* Change special loc, creating if it doesn't exist. */
void change_special_loc(int index, int x, int y)
{
  special_loc_t * p_sl = get_special_loc_to_modify(index, x, y);
  if(!p_sl) { return; }
  p_sl->x = x;
  p_sl->y = y;
}

/* Like change_special_loc, but instead of completely changing it, use
   the bottom rite corner (max) of past and present values. */
void special_loc_retain_max(int index, int x, int y)
{
  special_loc_t * p_sl = get_special_loc_to_modify(index, x, y);
  if(!p_sl) { return; }
  SET_TO_MAX(p_sl->x, x);
  SET_TO_MAX(p_sl->y, y);
}

/* Like special_loc_retain_max, but instead of completely changing it, use
   the top left corner (min) of past and present values. */
void special_loc_retain_min(int index, int x, int y)
{
  special_loc_t * p_sl = get_special_loc_to_modify(index, x, y);
  if(!p_sl) { return; }
  SET_TO_MIN(p_sl->x, x);
  SET_TO_MIN(p_sl->y, y);
}

/** Retrieving special locs **/

/* Collects a list of special locs.  return list is allocated but
   elements on it are shared with the original list.  */
list_t * get_special_loc_list(test_f_t test_f, void * data)
{
  assert(Level);
  if(!Level->special_locs)
    { return NULL; }
  return collect_sublist(Level->special_locs, test_f, data);
}

/* Pick randomly among matching locs.  */
int get_random_multiloc(int index, int * x, int * y)
{
  int success = FALSE;
  int num_locs;
  list_t * locs = get_special_loc_list(sl_func_matches_index, &index);
  if(!locs) { return FALSE; }

  num_locs = list_size(locs);
  if(num_locs > 0)
    {
      int choice = random_range(num_locs);
      special_loc_t * p_sl = list_get(locs, choice);
      *x = p_sl->x;
      *y = p_sl->y;
      success = TRUE;
    }

  fully_free_list(&locs, list_dont_free_v);
  return success;
}


int get_special_loc(int index, int * x, int * y)
{
  if(index == sl_level_ulc)
    {
      *x = 0;
      *y = 0;
      return TRUE;
    }

  if(index == sl_level_lrc)
    {
      *x = Level->level_width;
      *y = Level->level_length;
      return TRUE;
    }

  {
    special_loc_t * p_sl =
      list_single_find(Level->special_locs,
		       sl_func_matches_index,
		       &index);
    if(!p_sl) { return FALSE; }
    *x = p_sl->x;
    *y = p_sl->y;
    return TRUE;
  }
}

/**** Find random-ish locations by some criterion  ****/

/* A variant findspace.  Stays in the central area.  */
int find_central_space(int *x, int *y)
{
  *x = Level->level_width/2;
  *y = Level->level_length/2;
  while(TRUE)
    {
      if(spaceok(*x,*y,0))
	{ return TRUE; }

      if (*y < Level->level_length/2 + 5)
	{ ++*y; }
      else if (*x > Level->level_width/2 - 10)
	{
	  --*x;
	  *y = Level->level_length/2 - 5;
	}
      else
	{ return FALSE; }
    }
}


int find_random_country_loc(int *x, int *y)
{
  do
    {
      *x = random_range(COUNTRY_WIDTH);
      *y = random_range(COUNTRY_LENGTH);
    }
  while (Country[*x][*y].current_terrain_type == CHAOS_SEA);
  return TRUE;
}


/* Find a site with certain characteristics. */
int find_country_site( int base_terrain_type, int aux, int * px, int * py)
{
  int x, y;
  for(y=0;y<COUNTRY_LENGTH;y++) {
    for(x=0;x<COUNTRY_WIDTH;x++) {
      struct terrain * p_terr = &Country[x][y];	
      if((p_terr->base_terrain_type == base_terrain_type)
	 && (p_terr->aux == aux))
	{
	  *px = x;
	  *py = y;
	  return TRUE;
	}
    }
  }
  return FALSE;
}

/* finds floor space on level with buildaux not equal to baux,
sets x,y there. There must *be* floor space somewhere on level */

/*  Callback functions for findspace_aux  */

/* Is site a floor space? */
int spaceok (int x_idx, int y_idx, void* dummy)
{
  if (FLOOR != Level->site[x_idx][y_idx].locchar) { return FALSE; }
  if (get_monster(Level, x_idx, y_idx)) { return FALSE; }
  if (loc_statusp(x_idx, y_idx, SECRET)) { return FALSE; }
  return TRUE;
}

/* Is site a water space?  For placing water monsters. */
int water_space (int x_idx, int y_idx, void* dummy)
{
  if (WATER != Level->site[x_idx][y_idx].locchar) { return FALSE; }
  if (get_monster(Level, x_idx, y_idx)) { return FALSE; }
  if (loc_statusp(x_idx, y_idx, SECRET)) { return FALSE; }
  return TRUE;
}


/*  Callback function that helps implement the old findspace.  */
int space_in_another_room (int x_idx, int y_idx, void* data)
{
  return
    (Level->site[x_idx][y_idx].buildaux != *(int *)data)
    &&
    spaceok( x_idx, y_idx, 0);
}

/* Interfaces.  */

/* The traditional findspace.  Could be renamed
    findspace_in_another_room. */
int findspace( int *x, int *y, int baux )
{
  return findspace_aux( x, y, space_in_another_room, (void *)&baux );
}

int find_floorspace( int *x, int *y )
{
  return findspace_aux( x, y, spaceok, 0);
}

/*  Support for findspace. */

/*  Used to guarantee that the staggers are OK.  If small and large
    are in the wrong order, it just takes an extra iteration.  */
bool mutually_prime_p( int small, int large )
{
  while(TRUE)
    {
      int remainder = large % small;
      switch(remainder)
	{
	default:
	  large = small;
	  small = remainder;
	  break;

	case 0:
	  return FALSE;

	case 1:
	  return TRUE;
	}
    }
}

int findspace_aux(
		   int *x,
		   int *y,
		   int (*spaceok) (int x_idx, int y_idx, void* data),
		   void* data)
{

  /* Start at a random place.  */
  int i_outer  = random_range( Level->level_width );
  int j_actual = random_range( Level->level_length );
  int i, j;  /* The actual loop variables. */

  /* Kluuge.  These number are mutually prime to the length,width of
     all the known levels.  If we wished to handle almost-arbitrary
     level dimensions, we'd find appropriate i and j steps by
     generate-and-test from level_dimension/4 to level_dimension*3/4.
     The i and j steps could be found almost independently, just they
     shouldn't to be the same.  */

  int i_step = 17, j_step = 13;

  assert( mutually_prime_p(i_step, Level->level_width));
  assert( mutually_prime_p(j_step, Level->level_length));
  assert( i_step != j_step);

  /* In effect, we translate the dimensions i+j and j separately.
      Since modulo the level dimensions, both i+Constant and j loop
      over the whole level in their respective loops, and each of
      their translations cover the whole level on 1 dimension, and
      those translations are independent, the translations of (i+j, j)
      cover the whole level.  */

  for(i = 0;
      i < Level->level_width;
      i++,
	i_outer += i_step)
    {
      int i_actual = i_outer;
      for(j = 0;
	  j < Level->level_length;
	  j++,
	    j_actual += j_step,
	    i_actual += i_step)
	{
	  i_actual = i_actual % Level->level_width;
	  j_actual = j_actual % Level->level_length;

	  if ( spaceok( i_actual, j_actual, data))
	    {
	      /* Done. */
	      *x = i_actual;
	      *y = j_actual;
	      return TRUE;
	    }
	}
    }

  return FALSE;
}

/**/

/* setspot itself is in command1.c */
int set_spot_from_player(int *x, int *y)
{
  *x = Player.x;
  *y = Player.y;
  return setspot(x, y);
}

