/* omega copyright (C) by Laurence Raphael Brothers, 1987,1988,1989 */
/* aux1.c */
/* auxiliary functions for those in com.c, also see aux2.c and aux3.c */

#include "glob.h"
#include "lev.h"  /* get_monster() */
#include "util.h"
#include "command.h" /* Just for cmd_abortshadowform */

static char * roomname (int index)
{
  switch (index)
    {
    case RS_ZORCH:         return "A place zorched by powerful magic.";
    case RS_COURT:         return "The Court of the ArchMage.";
    case RS_CIRCLE:        return "The Astral Demesne of the Circle of Sorcerors";
    case RS_MAGIC_ISLE:    return "An island positively reeking of magic";
    case RS_STARPEAK:      return "Near the oddly glowing peak of a mountain";
    case RS_VOLCANO:       return "Deep within the bowels of the earth";
    case RS_HIGHASTRAL:    return "The High Astral Plane";
    case RS_EARTHPLANE:    return "The Plane of Earth";
    case RS_WATERPLANE:    return "The Plane of Water";
    case RS_FIREPLANE:     return "The Plane of Fire";
    case RS_AIRPLANE:      return "The Plane of Air";
    case RS_KITCHEN:       return "A kitchen";
    case RS_BATHROOM:      return "A bathroom";
    case RS_BEDROOM:       return "A bedroom";
    case RS_DININGROOM:    return "A dining room";
    case RS_SECRETPASSAGE: return "A secret passage";
    case RS_CLOSET:        return "A stuffy closet";
    case RS_ARENA:         return "The Rampart Arena";
    case RS_DROWNED_SEWER: return "A water-filled sewer node";
    case RS_DRAINED_SEWER: return "An unused sewer node";
    case RS_SEWER_DUCT:    return "A winding sewer duct";
    case RS_DESTINY:       return "The Halls of Fate";
    case RS_DRUID:         return "The Great Henge";
    case RS_HECATE:        return "The Church of the Far Side";
    case RS_SET:           return "The Temple of the Black Hand";
    case RS_ATHENA:        return "The Parthenon";
    case RS_ODIN:          return "The Shrine of the Noose";
    case RS_ADEPT:         return "The Adept's Challenge";
    case RS_WYRM:          return "The Sunken Cavern of the Great Wyrm.";
    case RS_OCEAN:         return "The Underground Ocean.";
    case RS_PONDS:         return "A series of subterranean pools and streams.";
    case RS_DRAGONLORD:    return "The Lair of the DragonLord.";
    case RS_GOBLINKING:    return "The Caves of the Goblins.";
    case RS_CAVERN:        return "A vast natural cavern.";
    case RS_CORRIDOR:      return "A dimly lit corridor.";
    case RS_WALLSPACE:     return "A niche hollowed out of the wall.";
      /* following are above ROOMBASE */
    case RS_GARDEROBE:     return "An abandoned garderobe.";
    case RS_CELL:          return "A dungeon cell.";
    case RS_TILED:         return "A tiled chamber.";
    case RS_CRYSTAL_CAVE:  return "A crystal cavern.";
    case RS_BEDROOM2:      return "Someone's bedroom.";
    case RS_STOREROOM:     return "An old storeroom.";
    case RS_CHARRED:       return "A room with charred walls.";
    case RS_MARBLE_HALL:   return "A marble hall.";
    case RS_EERIE_CAVE:    return "An eerie cave.";
    case RS_TREASURE:      return "A ransacked treasure-chamber.";
    case RS_SMOKEY:        return "A smoke-filled room.";
    case RS_APARTMENT:     return "A well-appointed apartment.";
    case RS_ANTECHAMBER:   return "An antechamber.";
    case RS_HAREM:         return "An unoccupied harem.";
    case RS_MULTIPURPOSE:  return "A multi-purpose room.";
    case RS_STALACTITES:   return "A room filled with stalactites.";
    case RS_GREENHOUSE:    return "An underground greenhouse.";
    case RS_WATERCLOSET:   return "A water closet.";
    case RS_STUDY:         return "A study.";
    case RS_LIVING_ROOM:   return "A living room.";
    case RS_DEN:           return "A comfortable den.";
    case RS_ABATOIR:       return "An abatoir.";
    case RS_BOUDOIR:       return "A boudoir.";
    case RS_STAR_CHAMBER:  return "A star chamber.";
    case RS_MANMADE_CAVE:  return "A manmade cavern.";
    case RS_SEWER_CONTROL: return "A sewer control room";
    case RS_SHRINE:        return "A shrine to High Magic";
    case RS_MAGIC_LAB:     return "A magic laboratory";
    case RS_PENTAGRAM:     return "A room with inscribed pentagram";
    case RS_OMEGA_DAIS:    return "A chamber with a blue crystal omega dais";

    default: return "A room of mystery and allure";
    }
}

/* check to see if too much tunneling has been done in this level */
void tunnelcheck (player_t * p)
{
  if (p->dlevel->environment == E_ASTRAL) return;
  if (p->dlevel->depth == 0 && p->dlevel->environment != E_DLAIR) return;

  ++(p->dlevel->tunnelled);

  if (p->dlevel->tunnelled > (p->dlevel->level_length / 4))
    mprint("Dust and stone fragments fall on you from overhead.");
  if (p->dlevel->tunnelled > (p->dlevel->level_length / 2))
    mprint("You hear groaning and creaking noises.");
  if (p->dlevel->tunnelled > (3 * p->dlevel->level_length / 4))
    mprint("The floor trembles and you hear a loud grinding screech.");
  if (p->dlevel->tunnelled > p->dlevel->level_length)
    {
      mprint("With a scream of tortured stone, the entire dungeon caves in!!!");
      gain_experience(p, 5000);

      if (p->status[SHADOWFORM] > 0)
	{
	  try_to_emerge_to_environment(E_COUNTRYSIDE);
	  assert(p->dlevel->environment == E_COUNTRYSIDE);
	  flatten_country_loc(p->x, p->y);
	  print1("In your shadowy state, you float back up to the surface.");
	  return;
	}

      mprint("You are flattened into an unpleasant jellylike substance.");
      p_death(p, "dungeon cave-in");
    }
}

/* displays a room's name */
void showroom (player_t * p, int room)
{
  int show_style = 0;

  switch (p->dlevel->environment)
    {
    default:
      show_style = 1;
      break;

    case E_MANSION:
      show_style = 1;
      strcpy(Str2, "A luxurious mansion: ");
      break;

    case E_HOUSE:
      show_style = 1;
      strcpy(Str2, "A house: ");
      break;

    case E_HOVEL:
      show_style = 1;
      strcpy(Str2, "A hovel: ");
      break;

    case E_CITY:
      show_style = 0;
      strcpy(Str2, "The City of Rampart");
      break;

    case E_VILLAGE_STAR_VIEW:
      strcpy(Str2, "The Village of Star View");
      show_style = 0;
      break;

    case E_VILLAGE_WOODMERE:
      strcpy(Str2, "The Village of Woodmere");
      show_style = 0;
      break;

    case E_VILLAGE_STORMWATCH:
      strcpy(Str2, "The Village of Stormwatch");
      show_style = 0;
      break;

    case E_VILLAGE_THAUMARIS:
      strcpy(Str2, "The Village of Thaumaris");
      show_style = 0;
      break;

    case E_VILLAGE_SKORCH:
      strcpy(Str2, "The Village of Skorch");
      show_style = 0;
      break;

    case E_VILLAGE_WHORFEN:
      strcpy(Str2, "The Village of Whorfen");
      show_style = 0;
      break;

    case E_CAVES:
      show_style = 2;
      strcpy(Str2, "The Goblin Caves: ");
      break;

    case E_CASTLE:
      show_style = 2;
      strcpy(Str2, "The Archmage's Castle: ");
      break;

    case E_ASTRAL:
      show_style = 2;
      strcpy(Str2, "The Astral Plane: ");
      break;

    case E_VOLCANO:
      show_style = 2;
      strcpy(Str2, "The Volcano: ");
      break;

    case E_PALACE:
      show_style = 2;
      strcpy(Str2, "The Palace Dungeons: ");
      break;

    case E_SEWERS:
      show_style = 2;
      strcpy(Str2, "The Sewers: ");
      break;

    case E_TACTICAL_MAP:
      show_style = 0;
      strcpy(Str2, "The Tactical Map ");
      break;
  }

  /* The combination of env-family and roomname != 0 could determine show_style. */
  switch (show_style)
    {
    default:
      break;

    case 1:
      strcat(Str2, roomname(room));
      break;

    case 2:
      sprintf(Str1, "Level %2d (%s)", p->dlevel->depth, roomname(room));
      strcat(Str2, Str1);
      break;
    }

  locprint(Str2);
}

int player_on_sanctuary (player_t * p)
{
  if (p->x == p->sx && p->y == p->sy) return TRUE;

  if (p->patron)
    if (p->dlevel->site[p->x][p->y].locchar == ALTAR)
      if (p->dlevel->site[p->x][p->y].aux == p->patron)
	return TRUE;

  return FALSE;
}

/* search once particular spot */
int searchat (player_t * p, int x, int y)
{
  int idx;
  int found = FALSE;
  location_t * site;

  if (inbounds(p->dlevel, x, y) == FALSE) return;
  if (random_range(3) == 0 && p->status[ALERT] == 0) return;

  site = &(p->dlevel->site[x][y]);

  if (ploc_status(site, SECRET))
    {
      ploc_reset(site, SECRET);
      ploc_set(site, CHANGED);

      if (site->locchar == OPEN_DOOR || site->locchar == CLOSED_DOOR)
	{
	  mprint("You find a secret door!");

	  for (idx = 0; idx <= 8; ++idx)
	    {
	      lset(x + direction[idx].x, y + direction[idx].y, STOPS);
	      lset(x + direction[idx].x, y + direction[idx].y, CHANGED);
	    }
	}
      else
	{
	  mprint("You find a secret passage!");
	}

      found = TRUE;
    }

  if (is_trap(site) && site->locchar != TRAP)
    {
      mprint("You find a trap!");

      site->locchar = TRAP;
      ploc_set(site, CHANGED);

      found = TRUE;
    }

  return found;
}

/* This is to be called whenever anything might change player performance in
   melee, such as changing weapon, statistics, etc. */
void calc_melee (player_t * p)
{
  calc_weight(p);

  p->maxweight  = 10 * p->str * p->agi;
  p->absorption = p->status[PROTECTION];
  p->defense    = 2 * statmod(p->agi) + (p->level / 2);
  p->hit        = p->level + statmod(p->dex) + 1;
  p->dmg        = statmod(p->str)+3;
  p->speed      = 5 - min(4, statmod(p->agi) / 2);

  if (p->status[HASTED] > 0)
    p->speed = p->speed / 2;

  if (p->status[SLOWED] > 0)
    p->speed = p->speed * 2;

  if (p->itemweight > 0)
    switch (p->maxweight / p->itemweight)
      {
      case 0: p->speed += 6; break;
      case 1: p->speed += 3; break;
      case 2: p->speed += 2; break;
      case 3: p->speed += 1; break;
      }

  if (p->status[ACCURATE])
    p->hit += 20;

  if (p->status[HERO])
    {
      p->hit     += p->dex;
      p->dmg     += p->str;
      p->defense += p->agi;
      p->speed   = p->speed / 2;
    }

  p->speed = max(1, min(25, p->speed));

#ifdef INCLUDE_MONKS
  if (p->rank[MONKS] > 0)
    {
      /* monks are faster when not in armor or on horseback */
      if (p->possessions[O_ARMOR] == NULL)
	p->speed += min(0, p->rank[MONKS] - 1);
    }
#endif

  if (pflagp(MOUNTED))
    {
      p->speed  = 3;
      p->hit   += 10;
      p->dmg   += 10;
    }

  /* weapon */
  /* have to check for used since it could be a 2h weapon just carried
     in one hand */
  if (p->possessions[O_WEAPON_HAND] != NULL && p->possessions[O_WEAPON_HAND]->used == TRUE)
    {
      Symbol what;
      what = p->possessions[O_WEAPON_HAND]->objchar;
      if (what == WEAPON || what == MISSILEWEAPON)
	{
	  p->hit += (p->possessions[O_WEAPON_HAND]->hit + p->possessions[O_WEAPON_HAND]->plus);
	  p->dmg += (p->possessions[O_WEAPON_HAND]->dmg + p->possessions[O_WEAPON_HAND]->plus);
	}
    }

#ifdef INCLUDE_MONKS
  if (p->rank[MONKS] > 0)
    {
      /* aren't monks just obscene? PGM */
      if (p->possessions[O_WEAPON_HAND] == NULL) /* barehanded */
        {
          /* all monks get a bonus in unarmed combat */
          p->hit     += (p->rank[MONKS] * p->level);
          p->dmg     += (p->rank[MONKS] * p->level);
          p->defense += (p->rank[MONKS] * p->level);

          if (p->rank[MONKS] == MONK_GRANDMASTER)
            {
              /* Grandmaster does 3x damage in unarmed combat. */
              p->dmg *= 3;
            }
        }
    }
#endif

  /* shield or defensive weapon */
  if (p->possessions[O_SHIELD] != NULL)
    p->defense += (p->possessions[O_SHIELD]->aux + p->possessions[O_SHIELD]->plus);

  /* armor */
  if (p->possessions[O_ARMOR] != NULL)
    {
      p->absorption += p->possessions[O_ARMOR]->dmg;
      p->defense += (p->possessions[O_ARMOR]->plus - p->possessions[O_ARMOR]->aux);
    }

  if (strlen(p->meleestr) > (2 * maneuvers()))
    default_maneuvers();

  comwinprint();
  showflags();
  dataprint();
}

/* player attacks monster m */
void fight_monster (player_t * p, monster_t * m)
{
  int hitmod = 0;

  if (p->status[AFRAID] > 0 && p->status[BERSERK] == 0)
    {
      print3("You are much too afraid to fight!");
      return;
    }

  if (player_on_sanctuary(p) && p->status[BERSERK] == 0)
    {
      /* If player is berserk, call desecrate after attacking */
      print3("You restrain yourself from desecrating this holy place.");
      return;
    }

  if (p->status[SHADOWFORM] > 0)
    {
      print3("Your attack has no effect in your shadowy state.");
      return;
    }

  if (p->status[BERSERK] <= 0 && m_statusp(m, HOSTILE) == FALSE)
    if (optionp(BELLICOSE))
      return;
    else if ('y' == cinema_confirm("You're attacking without provokation."))
      return;

  hitmod += (get_lunarity(p) * p->level / 2);

 /* chaotic action */
  if (m->attacked == FALSE)
    p->alignment -= 2;

  m_status_set(m, AWAKE);
  m_status_set(m, HOSTILE);
  m->attacked = TRUE;

  p->hit += hitmod;
  tacplayer(m);
  p->hit -= hitmod;

  if (player_on_sanctuary())
    {
      mprint("Your violence has desecrated this place.");
      sanctify(-1);
    }
}

/* Attempt to break an object o */
int damage_item (player_t * p, object_t * o)
{
  /* special case -- break star gem */
  if (o->id == OB_STARGEM)
    {
      print1("The Star Gem shatters into a million glistening shards....");

      if (p->dlevel->environment == E_STARPEAK)
	{
	  if (gamestatusp(KILLED_LAWBRINGER) == FALSE)
	    print2("You hear an agonizing scream of anguish and despair.");

	  morewait();
	  print1("A raging torrent of energy escapes in an explosion of magic!");
	  print2("The energy flows to the apex of Star Peak where there is");
	  morewait();

	  clearmsg();
	  print1("an enormous explosion!");
	  morewait();

	  annihilate(1);
	  print3("You seem to gain strength in the chaotic glare of magic!");

	  p->str = max(p->str, p->maxstr + 5); /* FIXED! 12/25/98 */
	  p->pow = max(p->pow, p->maxpow + 5); /* ditto */

	  p->alignment -= 200;
	  dispose_lost_objects(1,o);
	}
      else
	{
	  morewait();
	  print1("The shards coalesce back together again, and vanish");
	  print2("with a muted giggle.");

	  dispose_lost_objects(1,o);
	  Objects[o->id].uniqueness = UNIQUE_UNMADE; /* FIXED! 12/30/98 */
	}

      return TRUE;
    }

  if (o->fragility >= random_range(30)) return FALSE;

  if (o->objchar == STICK && o->charge > 0)
    {
      sprintf(Str1, "Your %s explodes!", o->blessing >= 0 ? o->truename : o->cursestr);
      print1(Str1);
      morewait();

      nprint1(" Ka-Blamm!!!");

      /* general case. Some sticks will eventually do special things */
      morewait();
      manastorm(p->x, p->y, 10 * o->charge * o->level);
      dispose_lost_objects(1,o);
      return TRUE;
    }

  if (o->blessing > 0 && o->level > random_range(10))
    {
      sprintf(Str1, "Your %s glows strongly.", itemid(o));
      print1(Str1);
      return FALSE;
    }

  if (o->blessing < -1 && o->level > random_range(10))
    {
	sprintf(Str1, "You hear an evil giggle from your %s", itemid(o));
	print1(Str1);
	return FALSE;
    }

  if (o->plus > 0)
    {
	sprintf(Str1, "Your %s glows and then fades.", itemid(o));
	print1(Str1);
	--(o->plus);
	return FALSE;
    }

  if (o->blessing > 0)
    print1("You hear a faint despairing cry!");
  else if (o->blessing < 0)
    print1("You hear an agonized scream!");

  sprintf(Str1, "Your %s shatters in a thousand lost fragments!", itemid(o));
  print2(Str1);
  morewait();
  dispose_lost_objects(1,o);
  return TRUE;
}

/* game over, you lose! */
void p_death (player_t * p, char * fromstring)
{
  print3("You died!");
  morewait();

  display_death(fromstring);

  player_dump(p);
  lev_term();

  endgraf();
  exit(0);
}

/* do dmg points of damage of type dtype, from source fromstring */
void p_damage (player_t * p, int dmg, int dtype, char * fromstring)
{
  if (p_immune(p, dtype))
    {
      mprint("You resist the effects!");
      return;
    }

  set_notice_flags(notice_hurt);

  if (dtype == NORMAL_DAMAGE)
    p->hp -= max(1,(dmg - p->absorption));
  else
    p->hp -= dmg;

  if (p->hp < 1)
    p_death(p, fromstring);

  dataprint();
}

int dir_OK_p (int dir)
{
  return (dir >= 0) && (dir < 8);
}

int get_deliberate_dir (int key_dir, char * prompt, int force_prompt)
{
  if (dir_OK_p(key_dir))
    {
      return key_dir;
    }
  else
    {
      return get_direction(prompt, force_prompt);
    }
}

/* Note that this is *NOT* re-entrant, due to the staticness of old_dir.  */
int set_up_command_dir(int key_dir, int first_time, char * prompt, int force_prompt)
{
  static int old_dir;

  if (first_time)
    old_dir = get_deliberate_dir(key_dir, prompt, force_prompt);

  return old_dir;
}

int getdir (void)
{
  return get_direction("Select direction: ", TRUE);
}

/* returns english string equivalent of number */
static char * wordnum (int num)
{
  switch (num)
    {
    case 0:  return "zero ";
    case 1:  return "one ";
    case 2:  return "two ";
    case 3:  return "three ";
    case 4:  return "four ";
    case 5:  return "five ";
    case 6:  return "six ";
    case 7:  return "seven ";
    case 8:  return "eight ";
    case 9:  return "nine ";
    case 10: return "ten ";
    default: break;
    }

  return "big";
}

/* functions describes monster m's state for examine function */
char *mstatus_string (monster_t * m, player_t * p)
{
  if (m_statusp(m, M_INVISIBLE) && p->status[TRUESIGHT] <= 0) return "Some invisible creature";

  if (m->uniqueness != COMMON)
    {
      if (m->hp < Monsters[m->id].hp / 3)
	sprintf(Str2, "%s, who is grievously injured ", m->monstring);
      else if (m->hp < Monsters[m->id].hp / 2)
	sprintf(Str2, "%s, who is severely injured ", m->monstring);
      else if (m->hp < Monsters[m->id].hp)
	sprintf(Str2, "%s, who is injured ", m->monstring);

      return Str2;
    }

  if (m->hp < Monsters[m->id].hp / 3)
    strcpy(Str2, "a grievously injured ");
  else if (m->hp < Monsters[m->id].hp / 2)
    strcpy(Str2, "a severely injured ");
  else if (m->hp < Monsters[m->id].hp)
    strcpy(Str2, "an injured ");
  else
    strcpy(Str2, getarticle(m->monstring));

  if (m->level > Monsters[m->id].level)
    {
      strcat(Str2," (level ");
      strcat(Str2, wordnum(m->level + 1 - Monsters[m->id].level));
      strcat(Str2,") ");
    }

  strcat(Str2, m->monstring);
  return Str2;
}

/* for the examine function */
void describe_player (player_t * p)
{
  if (p->hp < (p->maxhp / 5))
    print1("A grievously injured ");
  else if (p->hp < (p->maxhp  / 2))
    print1("A seriously wounded ");
  else if (p->hp < p->maxhp)
    print1("A somewhat bruised ");
  else
    print1("A fit ");

  if (p->status[SHADOWFORM])
    nprint1("shadow");
  else
    nprint1(levelname(p->level));

  nprint1(" named ");
  nprint1(p->name);

  if (pflagp(p, MOUNTED))
    nprint1(" (riding a horse.)");
}

/* access to player experience... */
/* share out experience among guild memberships */
void gain_experience (player_t * p, long amount)
{
  int i;
  int count = 0;
  long share;

  p->xp += amount;

  gain_level(); /* actually, check to see if should gain level */

  for (i = 0; i < NUMRANKS; ++i)
    if (p->guildxp[i] > 0)
      ++count;

  if (count == 0) return;

  share = amount / count;

  for (i = 0; i < NUMRANKS; ++i)
    if (p->guildxp[i] > 0)
      p->guildxp[i] += share;

  share = amount - (share * count);

  if (share > 0)
    {
      for (i = 0; i < NUMRANKS && share > 0; ++i)
	if (p->guildxp[i] > 0)
	  {
	    ++(p->guildxp[i]);
	    --share;
	  }
    }
}

/* try to hit a monster in an adjacent space. If there are none
   return FALSE. Note if you're berserk you get to attack ALL
   adjacent monsters! */

int goberserk (player_t * p)
{
  int idx;
  int went_berserk = FALSE;
  char meleestr[80];

  if (p->status[SHADOWFORM] >= 1000)
    return;

  strcpy(meleestr, p->meleestr);
  strcpy(p->meleestr, "lLlClH");

  for (idx = 0; idx < 8; ++idx)
    {
      int x, y;

      x = p->x + direction[idx].x;
      y = p->y + direction[idx].y;

      if (get_monster(p->dlevel, x, y))
        {
	  /* Handle special cases: perm shadowform, fear, temp shadowform */

	  if (p->status[AFRAID] > 0 && went_berserk == FALSE)
	    mprint("Your fear is submerged in a torrent of rage!");

	  if (p->status[SHADOWFORM] > 0)
	    {
	      mprint("With a scream you drop back into the real world and attack!");
	      cmd_abortshadowform(no_dir, TRUE);
	    }

          went_berserk = TRUE;

          fight_monster(get_monster(p->dlevel, x, y));
          morewait();
        }
    }

  strcpy(p->meleestr, meleestr);
  return went_berserk;
}

/* checks current food status of player, every hour, and when food is eaten */
void foodcheck (player_t * p)
{
  if (p->food > FOOD_LEVEL_BLOATED)
    {
      print3("You vomit up your huge meal.");
      p->food = FOOD_LEVEL_RAVENOUS;
    }
  else if (p->food == FOOD_LEVEL_PECKISH)
    {
      print3("Time for a smackerel of something.");
    }
  else if (p->food == FOOD_LEVEL_HUNGRY)
    {
      print3("You feel hungry.");
    }
  else if (p->food == FOOD_LEVEL_RAVENOUS)
    {
      print3("You are ravenously hungry.");
      set_notice_flags(notice_hurt); /* Is this too strong a notice? */
    }
  else if (p->food == FOOD_LEVEL_WEAK)
    {
      print3("You feel weak.");
      set_notice_flags(notice_hurt);
    }
  else if (p->food <= FOOD_LEVEL_STARVING)
    {
      set_notice_flags(notice_hurt);
      print3("You're starving!");
      p_damage(p, -5 * p->food, UNSTOPPABLE, "starvation");
    }

  showflags(p);
}

static int lightable_room_p (int room)
{
  switch (room)
    {
    default:
      return room > ROOMBASE;

    case RS_CAVERN:
    case RS_SEWER_DUCT:
    case RS_KITCHEN:
    case RS_BATHROOM:
    case RS_BEDROOM:
    case RS_DININGROOM:
    case RS_CLOSET:
      return TRUE;
    }
}

/* see whether room should be illuminated */

void roomcheck_2 (player_t * p, int force)
{
  static int oldroomno = -1;
  int roomno;

  roomno = p->dlevel->site[p->x][p->y].roomnumber;

  if (force == TRUE || roomno != oldroomno)
    {
      oldroomno = roomno;
      showroom(roomno);
    }

  /* This perhaps should be inside the previous test. */

  if (lightable_room_p(roomno) == FALSE) return;
  if (loc_statusp(p->dlevel, p->x, p->y, LIT) == TRUE) return;
  if (p->status[BLINDED] > 0) return;
  if (p->status[ILLUMINATION] == 0 && difficulty() > 6) return;

  spreadroomlight(player->dlevel, p->x, p->y, roomno);
  levelrefresh();  /* Belongs where it is used, not here. */
}

/*  A holdover from earlier.  */
void roomcheck (void)
{
  set_notice_flags(notice_new_level);
}

void monster_name(char * storage, monster_t * m)
{
  if (m->uniqueness == COMMON)
    sprintf(storage, "The %s", m->monstring);
  else
    strcpy(storage,m->monstring);
}

/* ask for mercy */
void surrender (player_t * p, monster_t * m)
{
  int i;
  long bestitem;
  long bestvalue;

  switch (random_range(4))
    {
    case 0: print1("You grovel at the monster's feet..."); break;
    case 1: print1("You cry 'uncle'!");                    break;
    case 2: print1("You beg for mercy.");                  break;
    case 3: print1("You yield to the monster.");           break;
    }

  if (m->talkf == M_NO_OP || m->talkf == M_TALK_STUPID)
    {
      print3("Your plea is ignored.");
      return;
    }

  if (is_monster_guard(m))
    {
      if (m_statusp(m,HOSTILE))
	monster_talk(m);
      else
	{
	  monster_name(Str2, m);
	  print2(Str2);
	  nprint2(" (bored): Have you broken a law? [yn] ");

	  if (ynq2() == 'y')
	    {
	      monster_name(Str2, m);
	      print2(Str2);
	      nprint2(" grabs you, and drags you to court.");
	      morewait();
	      send_to_jail();
	    }
	  else
	    {
	      print2("Then don't bother me. Scat!");
	    }
	}

      return;
    }

  morewait();
  print1("Your surrender is accepted.");

  if (p->cash > 0)
    nprint1(" All your gold is taken....");

  p->cash = 0;
  bestvalue = 0;
  bestitem = ABORT;

  for (i = 1; i < MAXITEMS; ++i)
    if (p->possessions[i] != NULL)
      if (bestvalue < true_item_value(p->possessions[i]))
	{
          bestitem = i;
          bestvalue = true_item_value(p->possessions[i]);
        }

  if (bestitem != ABORT)
    {
      print2("You also give away your best item... ");
      nprint2(itemid(p->possessions[bestitem]));
      nprint2(".");
      morewait();
      givemonster(m, p->.possessions[bestitem]);
      morewait(); /* msgs come from givemonster */
      conform_unused_object(p->possessions[bestitem]);
      p->possessions[bestitem] = NULL;
    }

  print2("You feel less experienced... ");
  p->xp = max(0, p->xp - m->xpv);

  nprint2("The monster seems more experienced!");
  m->level = (min(10, m->level+1));

  m->hp  += m->level*20;
  m->hit += m->level;
  m->dmg += m->level;
  m->ac  += m->level;
  m->xpv += m->level*10;

  morewait();
  clearmsg();

  if (m->talkf == M_TALK_EVIL && random_range(10))
    {
      print1("It continues to attack you, laughing evilly!");
      m_status_set(m, HOSTILE);
      m_status_reset(m, GREEDY);
    }
  else if (m->id == HORNET || m->id == GUARD)
    {
      print1("It continues to attack you. ");
    }
  else
    {
      print1("The monster leaves, chuckling to itself....");
      m_teleport(m);
    }

  dataprint();
}

/* threaten a monster */
void threaten (player_t * p, monster_t * m)
{
  char response;

  switch (random_range(4))
    {
    case 0: mprint("You demand that your opponent surrender!");      break;
    case 1: mprint("You threaten to do bodily harm to it.");         break;
    case 2: mprint("You attempt to bluster it into submission.");    break;
    case 3: mprint("You try to cow it with your awesome presence."); break;
    }

  morewait(); /* FIXED! 12/25/98 */

  if (m_statusp(m, HOSTILE) == FALSE)
    {
      print3("You only annoy it with your futile demand.");
      m_status_set(m, HOSTILE);
      return;
    }

  if (((2 * m->level) > p->level && m->hp > p->dmg) || m->uniqueness != COMMON)
    {
      print1("It sneers contemptuously at you.");
      return;
    }

  if (m->talkf != M_TALK_GREEDY &&
      m->talkf != M_TALK_HUNGRY &&
      m->talkf != M_TALK_EVIL &&
      m->talkf != M_TALK_MAN &&
      m->talkf != M_TALK_BEG &&
      m->talkf != M_TALK_THIEF &&
      m->talkf != M_TALK_MERCHANT &&
      m->talkf != M_TALK_IM)
    {
      print1("Your demand is ignored");
      return;
    }

  print1("It yields to your mercy.");
  p->alignment += 3;

  print2("Kill it, rob it, or free it? [krf] ");
  do
    response = (char) mcigetc();
  while (response != 'k' && response != 'r' && response != 'f');

  if (response == 'k')
    {
      m_death(m);
      print2("You treacherous rogue!");
      p->alignment -= 13;
    }
  else if (response == 'r')
    {
      p->alignment -= 2;
      print2("It drops its treasure and flees.");
      m_dropstuff(m);
      m_remove(m);
    }
  else
    {
      p->alignment += 2;
      print2("'If you love something set it free ... '");

      if (random_range(100) == 13)
	{
	  morewait();
	  print2("'...If it doesn't come back, hunt it down and kill it.'");
	}

      print3("It departs with a renewed sense of its own mortality.");
      m_remove(m);
    }
}

/* name of the player's experience level */
char * levelname (int level)
{
  switch (level)
    {
    case 0:  return "neophyte";
    case 1:  return "beginner";
    case 2:  return "tourist";
    case 3:  return "traveller";
    case 4:  return "wayfarer";
    case 5:  return "peregrinator";
    case 6:  return "wanderer";
    case 7:  return "hunter";
    case 8:  return "scout";
    case 9:  return "trailblazer";
    case 10: return "discoverer";
    case 11: return "explorer";
    case 12: return "senior explorer";
    case 13: return "ranger";
    case 14: return "ranger captain";
    case 15: return "ranger knight";
    case 16: return "adventurer";
    case 17: return "experienced adventurer";
    case 18: return "skilled adventurer";
    case 19: return "master adventurer";
    case 20: return "hero";
    case 21: return "superhero";
    case 22: return "demigod";
    default:
      if (level >= 100)
	return "Ultimate Master of Omega";
      else
	sprintf(Str3, "Order %d Master of Omega", level / 10 - 2);
      break;
    }

  return Str3;
}
