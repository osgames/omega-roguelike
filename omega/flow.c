/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */
/* Based partly on code from Angband and my own moveto patch for Angband. */

/* Functions to handle running and Citymoving. */


#include "glob.h"
#include "lev.h"
#include "liboutil/list.h"
#include "findloc.h"
#include "flow.h"
#include "command.h" /* Just for the command struct and `moveplayer' */

/* Unlike Angband, this code uses x,y ordering, */
/* bool is defined in curses.h */
/* uint is defined in <sys/types.h> */


enum
{
  /* Value signalling impassable terrain. */
  impassable_flow_value = 0,
  /* Value signalling too-distant terrain. */
  beyond_max_flow_value = 255,
  max_flow_value        = beyond_max_flow_value - 1,

  /* Constants for updating the flow */
  max_flow_base = 128,
  min_flow_base = 1,
  flow_base_range = max_flow_base + 1 - min_flow_base,
  max_safe_flow_travel = max_flow_value - max_flow_base,
  typical_flow_travel  = 32,
  
  /* Size of the circular queue used by "update_flow()" */
  flow_max = 2048,
};


/* If we support multiple flows, flow_cost_t could be enclosed by a
   structure that contains cache-description values:

   What test (pointer to function).  Later, perhaps allowing both a
   boolean test and a costing function.  

   The current flow_base value.

   State: "dirty", "only-initted", "outdated", "ready".

   The largest non-blocking cost for a move (for updating flow_base
   for monsters... but it's never other than 1) */


typedef unsigned char grid_cost_t;
typedef grid_cost_t flow_cost_t_aux[MAXLENGTH];
/* flow_cost_t is a pointer, not a 2D array, so we can pass it as a
   pointer without screwing up pointer arithmetic. */
typedef flow_cost_t_aux *flow_cost_t; 


void init_flow(flow_cost_t flow_cost, grid_test_fp grid_test)
{
  int x, y;

  /* Check the entire dungeon or country */
  for (y = 0; y < Lev_Length; y++)
    {
      for (x = 0; x < Lev_Width; x++)
	{
	  /* Init the grid according to the passability of the
	     terrain.  Impassable terrain is 0, "nearer than near", so
	     it will never be touched by the flow algorithm.  Passable
	     terrain is beyond_max_flow_value, "farther than far", so
	     the flow algorithm will always modify it if it processes
	     that grid.  */
	  flow_cost[x][y] =
	    (grid_test(x, y)) ? beyond_max_flow_value : impassable_flow_value;
	}
    }
}

/* This function is partly due to Ben Harrison of Angband. */
/*
 * Hack -- fill in the "cost" field of every grid that the player can
 * "reach" with the number of steps needed to reach that grid.  This
 * also yields the "distance" of the player from every grid.
 *
 * Hack -- use the local "flow_y" and "flow_x" arrays as a "circular
 * queue" of cave grids.
 *
 * We no longer use a "when" array.
 * 
 * We do not need a priority queue because the cost from grid to grid
 * is always "one" (even along diagonals) and we process them in order.
 */

/* Only valid within `update_flow_aux' */
#define ENQUEUE(COST, X, Y)\
{\
  /* Save the flow cost */\
  flow_cost[X][Y] = COST;\
\
  /* Enqueue that entry */\
  flow_x[flow_tail] = X;\
  flow_y[flow_tail] = Y;\
\
  /* Advance the queue */\
  ++flow_tail;\
  if (flow_tail == flow_max) { flow_tail = 0; }\
}


#if 1
#define uint unsigned int
void update_flow_aux( list_t * spots,
			flow_cost_t flow_cost, int min_cost,
			int max_flow_travel ) 
{
  int d;

  int flow_x[flow_max];
  int flow_y[flow_max];

  int flow_tail = 0;
  int flow_head = 0;

  unsigned max_cost = min_cost + max_flow_travel;

  /*** Start Grids ***/
  {
    list_iterator_t * iter = list_iterator_create(spots);
    while(list_iterator_has_next(iter))
      {
	coords_alloced_t * obj = list_iterator_next(iter);
	ENQUEUE(min_cost, obj->x, obj->y);

	/* In practice, the number of starting grids won't be anywhere
	   near as large as the size of the queue. */
	assert(flow_tail < flow_max);
      }
    list_iterator_delete(iter);
  }
  

  /*** Process Queue ***/

  /* Now process the queue */
  while (flow_head != flow_tail)
    {
      /* Extract the next entry */
      int tx = flow_x[flow_head];
      int ty = flow_y[flow_head];
      unsigned child_cost;

      /* Forget that entry by stepping past it (with wrap) */
      flow_head++;
      if (flow_head == flow_max) { flow_head = 0; }

      /* Child cost */
      child_cost = flow_cost[tx][ty] + 1;

      /* Hack -- Limit flow depth */
      if (child_cost >= max_cost) { continue; }

      /* Add the "children" */
      for (d = 0; d < num_dirs; d++)
	{
	  int old_tail = flow_tail;

	  /* Child location */
	  int x = tx + Dirs[0][d];
	  int y = ty + Dirs[1][d];

	  /* Out-of-bounds grids can't be reached. */
	  if(!inbounds(x, y)) { continue; }
	  
	  /* If the previous path to this grid was as good or better,
	     don't process it again.

	     It's OK if a cost entry was from a previous calculation.
	     It doesn't need to be recalculated.

	     Since we start from a lower index this time, a value from
	     a previous calculation can only be as low if our new best
	     route goes thru the previous center-position, OR if
	     (special) the value stands for impassable terrain (0),
	     which is never touched here.

	     Entries with cost 0 mean impassable, so that we needn't
	     keep calculating what terrain is passable.
	  */
	  
	  if( flow_cost[x][y] <= child_cost )
	    { continue; }

	  ENQUEUE(child_cost, x, y);
	  /* Hack -- Overflow by forgetting new entry */
	  if (flow_tail == flow_head) { flow_tail = old_tail; }
	}
    }
}
#else
/* Untested.  For "cost-conscious" flow, as would be needed for
   country flow or flow thru possibly dangerous sites like water.

   This does not use a priority queue, which would be faster but more
   complex.  One possible compromise is to sort just immediate
   children as they are inserted, which would still tend to inspect
   the cheapest grids first.
 */
typedef int (*cost_f)(int x, int y);
void update_flow_aux_4( list_t * spots,
			cost_f cost_f,
			flow_cost_t flow_cost, int min_cost,
			int max_flow_travel ) 
{
  int d;

  int flow_x[flow_max];
  int flow_y[flow_max];

  int flow_tail = 0;
  int flow_head = 0;

  unsigned max_cost = min_cost + max_flow_travel;

  /*** Start Grids ***/
  {
    list_iterator_t * iter = list_iterator_create(spots);
    while(list_iterator_has_next(iter))
      {
	coords_alloced_t * obj = list_iterator_next(iter);
	ENQUEUE(min_cost, obj->x, obj->y);

	/* In practice, the number of starting grids won't be anywhere
	   near as large as the size of the queue. */
	assert(flow_tail < flow_max);
      }
    list_iterator_delete(iter);
  }
  

  /*** Process Queue ***/

  /* Now process the queue */
  while (flow_head != flow_tail)
    {
      /* Extract the next entry */
      int tx = flow_x[flow_head];
      int ty = flow_y[flow_head];
      unsigned child_cost;

      /* Forget that entry by stepping past it (with wrap) */
      flow_head++;
      if (flow_head == flow_max) { flow_head = 0; }

      /* Child cost */
      child_cost = flow_cost[tx][ty] + cost_f ? 1: 0;

      /* Hack -- Limit flow depth */
      if (child_cost >= max_cost) { continue; }

      /* Add the "children" */
      for (d = 0; d < num_dirs; d++)
	{
	  int old_tail = flow_tail;

	  /* Child location */
	  int x = tx + Dirs[0][d];
	  int y = ty + Dirs[1][d];

	  /* Out-of-bounds grids can't be reached. */
	  if(!inbounds(x, y)) { continue; }
	  
	  /* If the previous path to this grid was as good or better,
	     don't process it again.

	     It's OK if a cost entry was from a previous calculation.
	     It doesn't need to be recalculated.

	     Since we start from a lower index this time, a value from
	     a previous calculation can only be as low if our new best
	     route goes thru the previous center-position, OR if
	     (special) the value stands for impassable terrain (0),
	     which is never touched here.

	     Entries with cost 0 mean impassable, so that we needn't
	     keep calculating what terrain is passable.
	  */
	  
	  if( flow_cost[x][y] <= child_cost )
	    { continue; }
	  {
	    unsigned child_cost_2 = child_cost + (cost_f ? cost_f(x, y) : 0);
	    ENQUEUE(child_cost_2, x, y);
	  }
	  /* Hack -- Overflow by forgetting new entry */
	  if (flow_tail == flow_head) { flow_tail = old_tail; }
	}
    }
}
#endif


/* If this returns non-zero, ip will be pointing to an index usable
    with Dirs */
bool
towards_goal_by_flow_aux(int x1, int y1, int *ip, flow_cost_t flow_cost )
{
  int i;
  int original_cost = flow_cost[x1][y1];
  int best_cost = original_cost;
  
  /* Check nearby grids, diagonals first */
  for (i = 0; i < num_dirs; i++)
    {
      /* Get the location */
      int x = x1 + Dirs[0][i];
      int y = y1 + Dirs[1][i];
      int cost;
      
      if(!inbounds(x, y)) { continue; }

      cost = flow_cost[x][y];

      /* Ignore impassable locations */
      if (cost == impassable_flow_value) { continue; }

      /* Ignore worse paths */
      if (cost > best_cost) { continue; }

      /* Check for monsters here.  This test could be a parameter.  */
      if( (Current_Environment != E_COUNTRYSIDE) &&
	 get_monster(Level, x, y))
	{ continue; }
      
      /* Save the cost */
      best_cost = cost;

      /* Save the current best index to move towards. */
      (*ip) = i;
    }

  /* Only true if we found a good move. */
  return (best_cost < original_cost);
}

/*****  Player's CitySite moving *****/

flow_cost_t move_cost = NULL;

post_command_state_t player_move_toward_goal(void)
{
  int dir;

  bool success =
    towards_goal_by_flow_aux(Player.x, Player.y, &dir, move_cost );

  if(success)
    {
      int dx = Dirs[0][dir];
      int dy = Dirs[1][dir];
      bool reaching_goal =
	(move_cost[Player.x + dx][Player.y + dy] == min_flow_base);
      /* We *do* sign_print, etc on the last step. */
      if(reaching_goal)
	{ resetpflag(MOVING_TO_GOAL); }
      /* Move the player.  If player fails to move, the return value
	 will halt repititions.  */
      return
	moveplayer(dx,dy,FALSE);
    }
  else
    { return base_state; }
}


bool setup_flow_for_moveto(list_t * spots, grid_test_fp grid_test) 
{
  if(move_cost == NULL)
    { move_cost = checkmalloc(sizeof(flow_cost_t_aux[MAXWIDTH])); }
  init_flow( move_cost, grid_test);
  update_flow_aux( spots, move_cost, min_flow_base,
		   max_flow_value - min_flow_base );

  {
    grid_cost_t cost = move_cost[Player.x][Player.y];
    return
      ((impassable_flow_value < cost) && (cost < beyond_max_flow_value));
  }
}

/*****  Monsters' "smell" moving *****/


#if 0

/* For monsters, to "smell" player.  This code is *UNTESTED* in Omega.
   It needs to allocate flow_cost.  It would probably need to be
   extended to account for monsters' different movement styles.  */

flow_cost_t flow_cost = NULL;

/* Shift all the costs a constant amount, to prepare for a new cycle */
void cycle_flow(flow_cost_t flow_cost)
{
  int y, x;
  
  /* Cycle the flow */
  for (y = 0; y < Lev_Length; y++)
    {
      for (x = 0; x < Lev_Width; x++)
	{
	  unsigned old_cost = flow_cost[y][x];
	  unsigned new_cost;
	  /* Skip impassable terrain. */
	  if( old_cost == 0 )
	    { continue; }

	  /* Increase previous costs by the same amount we'll increase
	     flow_base by, but we don't let it overflow the valid
	     range, ie, exceed beyond_max_flow_value. */
	  
	  if(old_cost >= (beyond_max_flow_value - flow_base_range))
	    {
	      new_cost = beyond_max_flow_value;
	    }
	  else
	    {
	      new_cost = old_cost + flow_base_range;
	    }

	  flow_cost[x][y] = new_cost;
	}
    }
}

/***** Medium level functions ******/


static int flow_base       = max_flow_base;
static int flow_is_known   = 0;
static int flow_is_initted = 0;


/*
 * Hack -- forget the "flow" information
 */
void forget_flow(void)
{

  /* Nothing to forget. */
  if (!flow_is_known)
    { return; }

  init_flow( cave_cost, grid_is_passable );
  flow_base = max_flow_base;
  flow_is_known = FALSE;
}

void update_flow_xy( int px, int py, flow_cost_t flow_cost,
		      int min_cost, int max_flow_travel )
{
  list_t * list = list_new();
  /* We don't alloc or free `spot', since this list is born and
     dies here, and has all known elements. */
  coords_alloced_t spot = { px, py, NULL, };
  list_prepend(list, &spot);
  update_flow_aux( list, flow_cost, min_cost, max_flow_travel);
  fully_free_list(&list, list_dont_free_v);
}


void update_flow(void)
{
  /* This code presumes Player is the target */
  int px = Player.x;
  int py = Player.y;

  if(!flow_is_initted)
    {
      init_flow( cave_cost, grid_is_passable );
      flow_base = max_flow_base;
      flow_is_initted = TRUE;
    }

  /* Everything calculated last time is considered one step "further
     away" than it was before.  This is accomplished by simply
     reducing the base cost one step. */
  flow_base--;

  /* If base cost has gotten too small, cycle. */
  if (flow_base < min_flow_base)
    {
      cycle_flow(cave_cost);
      flow_base = max_flow_base;  
    }

  /* Do the work. */
  update_flow_xy( px, py, cave_cost, flow_base,
		   typical_flow_travel ); 

  flow_is_known = TRUE;
}

/* A monster's "walking-distance" from the player is apparent walking
   distance minus current flow_base.
 */
bool can_smell_player( int x, int y, int aaf)
{
  int cost = cave_cost[x][y];

  return
    (cost != impassable_flow_value) &&
    (cost < beyond_max_flow_value) &&
    ((cost - flow_base) <= aaf);
}

/* Abbreviation */
bool towards_goal_by_flow(int x1, int y1, int *ip )
{
  return towards_goal_by_flow_aux( x1, y1, ip, cave_cost );
}
#endif



