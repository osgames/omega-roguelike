/* omega copyright (c) 1987-2001 by Laurence Raphael Brothers */
/* util.h */

#ifndef INCLUDED_UTIL_H
#define INCLUDED_UTIL_H

#include "defs.h"

/* produce a random unsigned int in the range 0..max-1 inclusive */
unsigned int random_range (unsigned int max);

/* return the effect of the moon's phase on the player */
int get_lunarity (void);

/* free all space associated with an object */
void free_object (object *);

#endif /* INCLUDED_UTIL_H */
