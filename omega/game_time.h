/* omega copyright (c) 1987-2001 by Laurence Raphael Brothers */
/* game_time.h */

#ifndef INCLUDED_GAME_TIME_H
#define INCLUDED_GAME_TIME_H

#include <stdio.h>

#include "defs.h"
#include "ticket.h"

#define TICKS_PER_SECOND 1UL
#define TICKS_PER_MINUTE (60 * TICKS_PER_SECOND)
#define TICKS_PER_HOUR   (60 * TICKS_PER_MINUTE)
#define TICKS_PER_DAY    (24 * TICKS_PER_HOUR)
#define TICKS_PER_WEEK   (7 * TICKS_PER_DAY)
#define TICKS_PER_MONTH  (30 * TICKS_PER_DAY)

void player_handler (ticket_t * ticket);

void init_time (void);
void set_monster_handler (monster * mon);
void apply_insert_ticket (void * vmon);
void apply_remove_ticket (void * vmon);

void set_waste_time (int flag);
unsigned long get_elapsed_time (void);

int time_year (void);
int time_month (void);
int time_day (void);
int time_hour (void);
int time_minute (void);
int time_phase (void);

int nighttime (void);

int save_time (FILE * fd);
int restore_time (FILE * fd);

void monster_tickets_on(void);
void monster_tickets_off(int save);

#define hour() time_hour()

#endif /* INCLUDED_GAME_TIME_H */
