/* Copyright 2001 Tehom (Tom Breton), licensed under the terms of the
 * Omega license as part of Omega. */

/* Declarations dealing with commands and quasi-commands. */

#if !defined __COMMAND_H
#define __COMMAND_H

/** Typedefs **/

/*
  repeats_allowed == 0:  Stop repeating.  
  repeats_allowed == anything else:
           First time, that's the repeat count.
	   Repeating, it's ignored.
  
  duration == 0: Command took zero time, allow another command.
  
 */
typedef struct post_command_state_t
{
  int repeats_allowed;
  int duration;
} post_command_state_t;

typedef   post_command_state_t (*command_f_t)(int, int);

/** Macros **/

#define RETURN_post_command_state(REPEATS_ALLOWED,DURATION)\
{\
  post_command_state_t  result = { REPEATS_ALLOWED, DURATION };\
  return result;\
}

/** Constants **/

extern const post_command_state_t base_state;

/** The commands. **/

/* command1.c */
post_command_state_t cmd_repeat_cmd      (int dir, int first_time);


/* command2.c */
post_command_state_t cmd_rest            (int dir, int first_time);
post_command_state_t cmd_peruse          (int dir, int first_time);
post_command_state_t cmd_quaff           (int dir, int first_time);
post_command_state_t cmd_activate        (int dir, int first_time);
post_command_state_t cmd_eat             (int dir, int first_time);
post_command_state_t cmd_search          (int dir, int first_time);
post_command_state_t cmd_pickup          (int dir, int first_time);
post_command_state_t cmd_drop            (int dir, int first_time);
post_command_state_t cmd_talk            (int dir, int first_time);
post_command_state_t cmd_disarm          (int dir, int first_time);
post_command_state_t cmd_give            (int dir, int first_time);
post_command_state_t cmd_zapwand         (int dir, int first_time);
post_command_state_t cmd_magic           (int dir, int first_time);
post_command_state_t cmd_upstairs        (int dir, int first_time);
post_command_state_t cmd_downstairs      (int dir, int first_time);
post_command_state_t cmd_setoptions      (int dir, int first_time);
post_command_state_t cmd_callitem        (int dir, int first_time);
post_command_state_t cmd_opendoor        (int dir, int first_time);
post_command_state_t cmd_bash_location   (int dir, int first_time);
post_command_state_t cmd_bash_item       (int dir, int first_time);
post_command_state_t cmd_save            (int dir, int first_time);
post_command_state_t cmd_closedoor       (int dir, int first_time);
post_command_state_t cmd_single_move     (int dir, int first_time);
post_command_state_t cmd_run             (int dir, int first_time);
post_command_state_t cmd_move            (int dir, int first_time);
post_command_state_t cmd_stay            (int dir, int first_time);
post_command_state_t cmd_moveto_spot     (int dir, int first_time);
post_command_state_t cmd_name_spot       (int dir, int first_time);
post_command_state_t cmd_run_step        (int dir, int first_time);

/* Quasi-commands for movement: */
post_command_state_t moveplayer          (int dx, int dy, int first_time);
post_command_state_t true_moveplayer     (int x, int y);
post_command_state_t running_moveplayer  (int x, int y);


/* command3.c functions */
post_command_state_t cmd_examine         (int dir, int first_time);
post_command_state_t cmd_help            (int dir, int first_time);
post_command_state_t cmd_version         (int dir, int first_time);
post_command_state_t cmd_fire            (int dir, int first_time);
post_command_state_t cmd_quit            (int dir, int first_time);
post_command_state_t cmd_nap             (int dir, int first_time);
post_command_state_t cmd_charid          (int dir, int first_time);
post_command_state_t cmd_wizard          (int dir, int first_time);
post_command_state_t cmd_vault           (int dir, int first_time);
post_command_state_t cmd_tacoptions      (int dir, int first_time);
post_command_state_t cmd_pickpocket      (int dir, int first_time);
post_command_state_t cmd_rename_player   (int dir, int first_time);
post_command_state_t cmd_abortshadowform (int dir, int first_time);
post_command_state_t cmd_tunnel          (int dir, int first_time);
post_command_state_t cmd_hunt            (int dir, int first_time);
post_command_state_t cmd_dismount_steed  (int dir, int first_time);
post_command_state_t cmd_city_move       (int dir, int first_time);
post_command_state_t cmd_frobgamestatus  (int dir, int first_time);



/* command4.c functions */
post_command_state_t cmd_no_op                 (int dir, int first_time);
post_command_state_t cmd_escape                (int dir, int first_time);
post_command_state_t cmd_keymap                (int dir, int first_time); 
post_command_state_t cmd_error                 (int dir, int first_time);
post_command_state_t cmd_print_allocation_info (int dir, int first_time);
post_command_state_t cmd_player_dump           (int dir, int first_time);
post_command_state_t cmd_display_pack          (int dir, int first_time);
post_command_state_t cmd_xredraw               (int dir, int first_time);
post_command_state_t cmd_bufferprint           (int dir, int first_time);
post_command_state_t cmd_redraw                (int dir, int first_time);
post_command_state_t cmd_wizard_draw           (int dir, int first_time);
post_command_state_t cmd_wish                  (int dir, int first_time);
post_command_state_t cmd_do_inventory_control  (int dir, int first_time);
post_command_state_t cmd_inventory_control     (int dir, int first_time);
post_command_state_t cmd_show_license          (int dir, int first_time);
post_command_state_t cmd_check_memory          (int dir, int first_time);
post_command_state_t cmd_editstats             (int dir, int first_time);
post_command_state_t cmd_countrysearch         (int dir, int first_time);
post_command_state_t cmd_enter_site            (int dir, int first_time);
post_command_state_t cmd_goberserk             (int dir, int first_time);
post_command_state_t cmd_show_key              (int dir, int first_time);


/* flow.c */
/* Quasi-command */
post_command_state_t player_move_toward_goal(void);

#endif /*!defined __COMMAND_H*/
