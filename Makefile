### These definitions are used if you 'make install'
### the value of LIBDIR should be the same as OMEGALIB in defs.h
BINDIR = $(DESTDIR)omega
LIBDIR = $(DESTDIR)omega/lib
VARDIR = $(DESTDIR)omega/lib # should be used for hiscore and log files
TARGET ?= omega/omega

### This is used for the "tarballs" target
TMPDIR = /tmp

### choose your optimization level
OFLAG = -O2
#OFLAG = -O2 -pipe

### choose your OS
#SYSFLAG = -DBSD
SYSFLAG = -DLINUX
#SYSFLAG = -DSYSV
#SYSFLAG = -DMSDOS -DGO32 # MSDOS (djgpp) must have both defined.
#SYSFLAG = -DAMIGA

### edit as appropriate for your compiler
CFLAGS = -ggdb -Wall -march=athlon -mcpu=athlon $(OFLAG) $(SYSFLAG) # -W -Wtraditional
##Just for here, because my own system is quirky - Tom
#CFLAGS = -ggdb -Wall $(OFLAG) $(SYSFLAG)  -I/usr/src/linux-2.2.5/include
#CFLAGS = -Wall $(OFLAG) $(SYSFLAG)


### define a compiler
CC = gcc

### CPP should contain the command to run the C preprocessor.
#CPP = cc -E
#CPP = /lib/cpp
CPP = $(CC) -E

### Select one of the following that is appropriate for your curses...
### Comment ALL of them out if you are using opcurses
#LIBS = -lncurses -ltermcap
#LIBS = -lcurses -ltermlib
#LIBS = -lpdcurses
#Linux links in term?? automatically.
#LIBS = -lncurses

### uncomment to use "op-curses" package
OPDEF = -DUSE_OPCURSES
CUROBJ = opcurses/curses.o opcurses/curgtk.o

### uncomment to compile using opcurses GTK+ driver
CPPFLAGS = `gtk-config --cflags` -DUSE_OPCURSES
LDFLAGS = `gtk-config --libs`

### Select one of the following keymaps
KEYSRC = keys_extended
#KEYSRC = keys_original

#################### that's it for changing the Makefile ####################

INCPATH = -I. -Iomega/.
CFLAGS += $(INCPATH)
CLROBJ = clrgen.o
DATEOBJ = date.o

OUTILSRC = omega/liboutil/list.c omega/liboutil/pqueue.c omega/liboutil/rb.c omega/liboutil/map.c \
           omega/liboutil/lex.c
OUTILOBJ = omega/liboutil/list.o omega/liboutil/pqueue.o omega/liboutil/rb.o omega/liboutil/map.o \
           omega/liboutil/lex.o

VPATH=omega/ omega/tools omega/tools/libsrc
SRC = omega.c aux1.c aux2.c aux3.c bank.c char.c \
      command1.c command2.c command3.c command4.c command5.c \
	  effect1.c \
      effect2.c effect3.c etc.c file.c gen.c guild1.c guild2.c\
      init.c inv.c item.c itemf1.c itemf2.c itemf3.c lev.c map.c\
      mmelee.c mmove.c mon.c move.c movef.c mspec.c mstrike.c mtalk.c\
      newrand.c priest.c pdump.c save.c scr.c site1.c site2.c spell.c\
      stats.c trap.c util.c memory.c ticket.c game_time.c \
      load.c loaders.c levmem.c levchange.c levgame.c sitereify.c \
	random.c site3.c findloc.c pick.c \
	flow.c nsave.c \
	$(KEYSRC).c

OBJ = $(SRC:.c=.o)

all: lib $(TARGET)

$(TARGET): $(CUROBJ) $(CLROBJ) $(OUTILOBJ) $(OBJ) $(DATEOBJ)
	$(CC) $(LDFLAGS) $(OFLAG) $(SYSFLAG) $(CUROBJ) $(CLROBJ) $(DATEOBJ) $(OUTILOBJ) $(OBJ) $(LIBS) -o $(TARGET)
	rm date.c date.o


date.c: makedate
	./makedate > date.c

include omega/tools/Makefile
include omega/tools/libsrc/Makefile

tarballs: distclean
	cd ../opcurses; make clean
	mv ../opcurses $(TMPDIR)
	cd ../..; tar -zcvf $(TMPDIR)/omega-`cat omega-roguelike/omega/version.txt`.tar.gz omega-roguelike
	cd $(TMPDIR); tar -zcvf opcurses-`cat opcurses/version.txt`.tar.gz opcurses
	mv $(TMPDIR)/opcurses ..
	mv $(TMPDIR)/omega-`cat version.txt`.tar.gz ..
	mv $(TMPDIR)/opcurses-`cat ../opcurses/version.txt`.tar.gz ..

install: all $(BINDIR) $(LIBDIR) $(VARDIR)
	cp $(TARGET) $(BINDIR)/
	chmod 4711 $(BINDIR)/$(TARGET)
	- cp lib/*.txt $(LIBDIR)/
	cp lib/*.backup $(LIBDIR)/
	cp lib/*.xpm $(LIBDIR)/
	cp lib/maps.dat $(LIBDIR)/
	cp lib/omega.hi $(VARDIR)/
	cp lib/omega.log $(VARDIR)/
	chmod 0644 $(LIBDIR)/help*.txt $(LIBDIR)/license.txt $(LIBDIR)/motd.txt
	chmod 0644 $(LIBDIR)/thanks.txt $(LIBDIR)/update.txt
	chmod 0600 $(LIBDIR)/abyss.txt $(LIBDIR)/scroll[1234].txt $(LIBDIR)/maps.dat
	chmod 0600 $(LIBDIR)/omega.hi.backup $(LIBDIR)/omega.log.backup
	chmod 0600 $(VARDIR)/omega.hi $(VARDIR)/omega.log

install_not_suid: all $(BINDIR) $(LIBDIR) $(VARDIR)
	cp $(TARGET) $(BINDIR)/
	chmod 0711 $(BINDIR)/omega
	- cp lib/*.txt $(LIBDIR)/
	cp lib/*.backup $(LIBDIR)/
	cp lib/*.xpm $(LIBDIR)/
	cp lib/maps.dat $(LIBDIR)/
	cp lib/omega.hi $(VARDIR)/
	cp lib/omega.log $(VARDIR)/
	chmod 0644 $(LIBDIR)/help*.txt $(LIBDIR)/license.txt $(LIBDIR)/motd.txt
	chmod 0644 $(LIBDIR)/thanks.txt $(LIBDIR)/update.txt
	chmod 0600 $(LIBDIR)/abyss.txt $(LIBDIR)/scroll[1234].txt $(LIBDIR)/maps.dat
	chmod 0600 $(LIBDIR)/omega.hi.backup $(LIBDIR)/omega.log.backup
	chmod 0600 $(VARDIR)/omega.hi $(VARDIR)/omega.log

lib: crypt maps.dat
	cp maps.dat omega/lib/maps.dat

clean: toolsclean libsrcclean
	-rm -f $(OBJ) $(OUTILOBJ) $(CLROBJ) clrgen.h clrgen.c genclr.o genclr omega.dep $(TARGET)

distclean: clean
	-rm -f ../omega*.tar.gz ../opcurses*.tar.gz
	cp lib/omega.hi.backup lib/omega.hi
	cp lib/omega.log.backup lib/omega.log

$(CUROBJ): opcurses/curses.h opcurses/cmacros.h opcurses/xcurses.h

$(CLROBJ): clrgen.h

$(OBJ): defs.h extern.h glob.h iinit.h minit.h clrgen.h

clrgen.h clrgen.c: genclr genclr.c minit.h defs.h
	$(CPP) $(INCPATH) $(OPDEF) $(SYSFLAG) -DOMEGA_CLRGEN omega/*.c omega/*.h | ./genclr clrgen.c clrgen.h

genclr: genclr.o
	$(CC) $(LDFLAGS) genclr.o -o genclr

omega.dep: $(SRC) $(OUTILSRC)
	$(CC) -MM -MG $(INCPATH) $(SYSFLAG) $? >> omega.dep

include omega.dep

