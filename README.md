# Omega-rpg

Originally developed at https://sourceforge.net/projects/omega-roguelike/ by [dagibbs](http://sourceforge.net/users/dagibbs), [wsxyz](http://sourceforge.net/users/wsxyz) and [wtanksle](http://sourceforge.net/users/wtanksle) and published under the LGPL-2.0 license.